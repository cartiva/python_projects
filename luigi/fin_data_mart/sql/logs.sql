﻿select * from ops.luigi_task_log
order by run_start_ts desc 

-- delete from ops.luigi_task_log

select a.task_id, name, event_name, ts 
-- select *
from tasks a
inner join  task_events b on a.id = b.task_id
where ts::date = current_date
order by ts desc 
limit 100

-- delete from task_Events


drop table tasks cascade;
drop table task_events cascade;
drop table task_parameters cascade;
