# encoding=utf-8
import luigi
import db_cnx
import ops
import csv
import datetime
import sys

"""
if i can figure out how, wrap run in a try...except just like in the current tasks
no failure complete := true
any failure complete := false

change ops.luigi_task_log, run_id not a serial, sum sort of uuid generated in python for the pipeline
12/31/16
place ops.luigi_log_pass(run_id) just before except Exception as error:
"""


class ExtGlptrns(luigi.Task):
    """
    Nightly extract of 45 days of glptrns into arkona.ext_glptrns
    """
    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        db2_con = None
        pg_con = None
        try:
            with db_cnx.arkona_report() as db2_con:
                file_name = 'files/ext_glptrns_tmp.csv'
                with db2_con.cursor() as db2_cur:
                    sql = """
                        select
                          gtco#, gttrn#, gtseq#, gtdtyp, gttype, trim(gtpost), gtrsts, gtadjust, gtpsel,
                          trim(gtjrnl), gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
                          trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
                          trim(upper(gtdesc)), gttamt, gtcost, gtctlo,gtoco#
                        from rydedata.glptrns a
                        --where gtdate between current_date - 1 day and current_date
                        where gtdate = current_date
                        --order by gtdate
                        --fetch first 20 rows only
                    """
                    db2_cur.execute(sql)
                if sys.version_info > (3, 0):
                    with open(file_name, 'w', newline='') as f:
                        csv.writer(f).writerows(db2_cur)
                else:
                    with open(file_name, 'wb') as f:
                        csv.writer(f).writerows(db2_cur)
                with db_cnx.pg_jon_localhost() as pg_con:
                    with pg_con.cursor() as pg_cur:
                        pg_cur.execute("truncate arkona.ext_glptrns_tmp")
                        with open(file_name, 'r') as io:
                            pg_cur.copy_expert("""copy arkona.ext_glptrns_tmp from stdin with csv encoding 'latin-1'""",
                                               io)
            with self.output().open("w") as out_file:
                # write to the output file
                out_file.write('passed')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if db2_con:
                db2_con.close()
            if pg_con:
                pg_con.close()


class Glpdept(luigi.Task):
    """
    download rydedata.glpdept -> arkona.ext_glpdept_tmp
    compares arkona.ext_glpdept_tmp to arkona.ext_glpdept
    if there are any differences, raise Exception
    """

    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    @luigi.Task.event_handler(luigi.Event.FAILURE)
    # def on_failure(self, exception):
    def mourn_failure(self, exception):
        print('Boooooooooooooooooooooooooooooooooo')
    #     self.complete = False

    def on_success(self):
        print('Wahoo - Passsssssssssssssssssssssssssssssssssssssssssssssssssss')

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        db2_con = None
        pg_con = None
        try:
            with db_cnx.arkona_report() as db2_con:
                file_name = 'files/ext_glpdept_tmp.csv'
                with db2_con.cursor() as db2_cur:
                    sql = """
                        select TRIM(COMPANY_NUMBER),TRIM(DEPARTMENT_CODE),TRIM(DEPT_DESCRIPTION)
                        from rydedata.glpdept a
                    """
                    db2_cur.execute(sql)
                    if sys.version_info > (3, 0):
                        with open(file_name, 'w', newline='') as f:
                            csv.writer(f).writerows(db2_cur)
                    else:
                        with open(file_name, 'wb') as f:
                            csv.writer(f).writerows(db2_cur)
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate arkona.ext_glpdept_tmp")
                    with open(file_name, 'r') as io:
                        pg_cur.copy_expert("""copy arkona.ext_glpdept_tmp from stdin with csv encoding 'latin-1 '""",
                                           io)
                    pg_con.commit()
                    sql = """
                        select count (1)
                        from ((
                            select *
                            from arkona.ext_glpdept d
                            except
                            select *
                            from arkona.ext_glpdept_tmp e)
                          union (
                            select *
                            from arkona.ext_glpdept_tmp f
                            except
                            select *
                            from arkona.ext_glpdept g)) x
                    """
                    pg_cur.execute(sql)
                    the_count = pg_cur.fetchone()[0]
                    if the_count == 0:
                        with self.output().open("w") as out_file:
                            # write to the output file
                            out_file.write('passed')
                    else:
                        raise Exception('there are differences between ext_glpdept and ext_glpdept_tmp')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if db2_con:
                db2_con.close()
            if pg_con:
                pg_con.close()


class Glpmast(luigi.Task):
    """

    """

    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        db2_con = None
        pg_con = None
        try:
            with db_cnx.arkona_report() as db2_con:
                file_name = 'files/ext_glpmast_tmp.csv'
                with db2_con.cursor() as db2_cur:
                    sql = """
                        select TRIM(COMPANY_NUMBER),TRIM(FISCAL_ANNUAL),YEAR,TRIM(ACCOUNT_NUMBER),TRIM(ACCOUNT_TYPE),
                          TRIM(ACCOUNT_DESC),TRIM(ACCOUNT_SUB_TYPE),TRIM(ACCOUNT_CTL_TYPE),TRIM(DEPARTMENT),
                          TRIM(TYPICAL_BALANCE),TRIM(DEBIT_OFFSET_ACCT),TRIM(CRED_OFFSET_ACCT),OFFSET_PERCENT,
                          AUTO_CLEAR_AMOUNT,TRIM(WRITE_OFF_ACCOUNT),TRIM(SCHEDULE_BY),TRIM(RECONCILE_BY),
                          TRIM(COUNT_UNITS),TRIM(CONTROL_DESC1),TRIM(VALIDATE_STOCK_NUMBER),TRIM(OPTION_3),
                          TRIM(OPTION_4),TRIM(OPTION_5),BEGINNING_BALANCE,JAN_BALANCE01,FEB_BALANCE02,MAR_BALANCE03,
                          APR_BALANCE04,MAY_BALANCE05,JUN_BALANCE06,JUL_BALANCE07,AUG_BALANCE08,SEP_BALANCE09,
                          OCT_BALANCE10,NOV_BALANCE11,DEC_BALANCE12,ADJ_BALANCE13,UNITS_BEG_BALANCE,JAN_UNITS01,
                          FEB_UNITS02,MAR_UNITS03,APR_UNITS04,MAY_UNITS05,JUN_UNITS06,JUL_UNITS07,AUG_UNITS08,
                          SEP_UNITS09,OCT_UNITS10,NOV_UNITS11,DEC_UNITS12,ADJ_UNITS13,TRIM(ACTIVE)
                        from rydedata.glpmast
                    """
                    db2_cur.execute(sql)
                    if sys.version_info > (3, 0):
                        with open(file_name, 'w', newline='') as f:
                            csv.writer(f).writerows(db2_cur)
                    else:
                        with open(file_name, 'wb') as f:
                            csv.writer(f).writerows(db2_cur)
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate arkona.ext_glpmast")
                    with open(file_name, 'r') as io:
                        pg_cur.copy_expert("""copy arkona.ext_glpmast from stdin with csv encoding 'latin-1 '""", io)
            with self.output().open("w") as out_file:
                # write to the output file
                out_file.write('passed')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if db2_con:
                db2_con.close()
            if pg_con:
                pg_con.close()


class DimJournal(luigi.Task):
    """

    """

    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        # return None
        return Glpdept()


    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        db2_con = None
        pg_con = None
        try:
            with db_cnx.arkona_report() as db2_con:
                file_name = 'files/ext_glpjrnd_tmp.csv'
                with db2_con.cursor() as db2_cur:
                    sql = """
                        select TRIM(COMPANY_NUMBER),TRIM(JOURNAL_CODE),TRIM(JOURNAL_DESC),TRIM(CONTROL_PROMPT),
                        TRIM(DOCUMENT_PROMPT),TRIM(OVERRIDE_PROMPT),TRIM(TRANSACTION_DESC),TRIM(COST_REQUIRED),
                        COST_RELATION_PCT,TRIM(COUNT_SALES_UNITS),TRIM(COUNT_ASSET_UNITS),TRIM(UPD_DATE_LAST_CHG),
                        TRIM(UPD_DATE_LAST_PAY),TRIM(ALLOW_SUSP_TRANS),TRIM(PROCESS_BY_BATCH),
                        TRIM(REPLACE_CTL_W_OVR),TRIM(COST_CODE)
                        from rydedata.glpjrnd
                    """
                    db2_cur.execute(sql)
                    if sys.version_info > (3, 0):
                        with open(file_name, 'w', newline='') as f:
                            csv.writer(f).writerows(db2_cur)
                    else:
                        with open(file_name, 'wb') as f:
                            csv.writer(f).writerows(db2_cur)
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate arkona.ext_glpjrnd_tmp")
                    with open(file_name, 'r') as io:
                        pg_cur.copy_expert("""copy arkona.ext_glpjrnd_tmp from stdin with csv encoding 'latin-1 '""",
                                           io)
                    pg_con.commit()
                    sql = """
                        select count (1)
                        from ((
                            select *
                            from arkona.ext_glpjrnd d
                            except
                            select *
                            from arkona.ext_glpjrnd_tmp e)
                          union (
                            select *
                            from arkona.ext_glpjrnd_tmp f
                            except
                            select *
                            from arkona.ext_glpjrnd g)) x
                    """
                    pg_cur.execute(sql)
                    the_count = pg_cur.fetchone()[0]
                    if the_count == 0:
                        with self.output().open("w") as out_file:
                            # write to the output file
                            out_file.write('passed')
                    else:
                        raise Exception('there are differences between ext_glpjrnd and ext_glpjrnd_tmp')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if db2_con:
                db2_con.close()
            if pg_con:
                pg_con.close()


class XfmGlptrns(luigi.Task):
    """
    takes nightly ext_glptrns_tmp
    """
    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        return ExtGlptrns()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        pg_con = None
        try:
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate arkona.xfm_glptrns")
                    sql = """
                        insert into arkona.xfm_glptrns
                        select a.*, md5(a::text) as hash
                        from (
                          select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct,
                            gtctl_, gtdoc_, gtref_, gttamt::numeric(12,2), coalesce(gtdesc, 'NONE')
                          from arkona.ext_glptrns_tmp
                          where gtco_ = 'RY1'
                            and gtpost in ('Y','V')
                            and left(gtacct, 1) <> '3'
                          group by gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_,
                            gtdoc_, gtref_, gttamt, gtdesc) a;
                    """
                    pg_cur.execute(sql)
            with self.output().open("w") as out_file:
                # write to the output file
                out_file.write('passed')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if pg_con:
                pg_con.close()


class DimAccount(luigi.Task):
    """

    """

    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        return [Glpdept(), Glpmast()]

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        pg_con = None
        try:
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate arkona.xfm_glpmast")
                    sql = """
                        insert into arkona.xfm_glpmast
                        select account_number, account_type, account_desc,
                          case account_sub_type
                            when 'A' then 'RY1'
                            when 'B' then 'RY2'
                            else 'XXX'
                          end,
                          department,
                          case typical_balance
                            when 'D' then 'Debit'
                            when 'C' then 'Credit'
                            else 'X'
                          end
                        from arkona.ext_glpmast
                        where account_sub_type <> 'C' -- crookston accounts
                        group by account_number, account_type, account_desc, account_sub_type,
                          department, typical_balance
                    """
                    pg_cur.execute(sql)
                    pg_con.commit()
                    sql = """
                        select count(1)
                        from (
                            (select account, account_type, description, store_code, department_code, typical_balance
                            from arkona.xfm_glpmast
                            except
                            select account, account_type_code, description, store_code, department_code, typical_balance
                            from fin.dim_account)
                        union
                            (select account, account_type_code, description, store_code, department_code,typical_balance
                            from fin.dim_account
                            where current_row = true
                                and account not in ('none', '*E*', '*VOID', '*SPORD')
                            except
                            select account, account_type, description, store_code, department_code, typical_balance
                            from arkona.xfm_glpmast)) x
                    """
                    pg_cur.execute(sql)
                    the_count = pg_cur.fetchone()[0]
                    if the_count == 0:
                        with self.output().open("w") as out_file:
                            # write to the output file
                            out_file.write('passed')
                    else:
                        raise Exception('there are differences between xfm_glpmast and dim_account')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if pg_con:
                pg_con.close()


class DimGlDescription(luigi.Task):
    """
    nightly xfm of ext_glptrns to xfm_glptrns
    """

    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        return XfmGlptrns()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        pg_con = None
        try:
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute("truncate fin.xfm_gl_description")
                    sql = """
                        insert into fin.xfm_gl_description
                        select trans, seq, post_status, description
                        from arkona.xfm_glptrns
                    """
                    pg_cur.execute(sql)
                    pg_con.commit()
                    sql = """
                        insert into fin.dim_gl_description (description, row_from_date, current_row, row_reason)
                        select distinct description, current_date, true, 'added from nightly etl'
                        from fin.xfm_gl_description a
                        where not exists (
                          select 1
                          from fin.dim_gl_description
                          where description = a.description);
                    """
                    pg_cur.execute(sql)
            with self.output().open("w") as out_file:
                # write to the output file
                out_file.write('passed')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if pg_con:
                pg_con.close()


class FactGl(luigi.Task):
    """

    """

    run_timestamp = luigi.Parameter(default=str(datetime.datetime.now()).replace(" ", ""))

    def requires(self):
        return [DimAccount(), DimJournal(), DimGlDescription()]

    def output(self):
        # define output filename and path
        return luigi.LocalTarget("fact_gl/" + self.__class__.__name__ + ".txt")

    def run(self):
        run_id = ops.luigi_log_start(self.__class__.__name__)
        pg_con = None
        try:
            with db_cnx.pg_jon_localhost() as pg_con:
                with pg_con.cursor() as pg_cur:

                    """
                        insert new rows into fin.fact_gl
                    """
                    sql = """
                        insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
                          account_key,control,doc,ref,amount,gl_description_key)
                        select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
                          e.post_status, g.journal_key, h.datekey,
                          coalesce(i.account_key, j.account_key) as account_key,
                          e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
                        from arkona.xfm_glptrns e
                        left join fin.fact_gl ee on e.trans = ee.trans
                          and e.seq = ee.seq
                          and e.post_status = ee.post_status
                        left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
                          and e.the_date between f.row_from_date and f.row_thru_date
                        left join fin.dim_doc_type ff on 1 = 1
                          and ff.doc_type_code = 'none'
                        left join fin.dim_journal g on e.journal_code = g.journal_code
                          and e.the_date between g.row_from_date and g.row_thru_date
                        left join dds.day h on e.the_date = h.thedate
                        left join fin.dim_account i on e.account = i.account
                          and e.the_date between i.row_from_date and i.row_thru_date
                        left join fin.dim_account j on 1 = 1
                          and j.account = 'none'
                        left join fin.xfm_gl_description k on e.trans = k.trans
                          and e.seq = k.seq
                          and e.post_status = k.post_status
                        left join fin.dim_gl_description m on k.description = m.description
                        where ee.trans is null;
                    """
                    pg_cur.execute(sql)

                    """
                       check for changed rows, if there are any, send an email
                    """
                    sql = """
                        select count(*)
                        from arkona.xfm_glptrns e
                        inner join (
                          select 'fact', trans, seq, post_status, md5(a::text) as hash
                          from (
                            select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
                              d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
                            from fin.fact_gl a
                            inner join arkona.xfm_glptrns aa on a.trans = aa.trans
                              and a.seq = aa.seq and a.post_status = aa.post_status
                            inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
                            inner join fin.dim_journal c on a.journal_key = c.journal_key
                            inner join dds.day d on a.date_key = d.datekey
                            inner join fin.dim_account e on a.account_key = e.account_key
                            inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
                              on e.trans = f.trans
                                and e.seq = f.seq
                                and e.post_status = f.post_status
                                and e.hash <> f.hash
                    """
                    pg_cur.execute(sql)
                    the_count = pg_cur.fetchone()[0]
                    if the_count == 0:
                        with self.output().open("w") as out_file:
                            # write to the output file
                            out_file.write('passed')
                    else:
                        raise Exception('there are differences between xfm_glpmast and dim_account')
            ops.luigi_log_pass(run_id)
        except Exception as error:
            ops.luigi_log_error(str(run_id), str(error).replace("'", ""))
        finally:
            if pg_con:
                pg_con.close()

if __name__ == '__main__':
    luigi.run(main_task_cls=Glpdept)
    # luigi.run(cmdline_args=["--workers 2"], main_task_cls=DimGlDescription)
