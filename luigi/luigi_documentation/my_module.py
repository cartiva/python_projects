import luigi

"""
    first problem was needing to activate the virtualenv in terminal:
    Z:\E\python_venvs\luigi\Scripts>activate.bat

    which resulted in a prompt:
    (luigi) Z:\E\python_venvs\luigi\Scripts>

    change directory to the project:
    (luigi) Z:\E\python_venvs\luigi\Scripts>cd ..\..\..\python projects\luigi\luigi_documentation

    and, miracle of miracles, this worked in the terminal:
    (luigi) Z:\E\python projects\luigi\luigi_documentation>python -m luigi --module my_module MyTask --x 100 --local-scheduler

    can't get it to run in ide, it doesn't fail, but it doesn't print 145 either
    may be a goofy example
"""

class MyTask(luigi.Task):

    x = luigi.IntParameter()
    y = luigi.IntParameter(default=45)

    def run(self):
        print(self.x + self.y)

