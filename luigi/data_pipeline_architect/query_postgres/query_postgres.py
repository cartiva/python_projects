# these modules are needed for the task
import luigi
# import psycopg2
import db_cnx

"""
http://datapipelinearchitect.com/luigi-query-postgresql/
it fucking worked

modify it to use db_cnx
"""


class QueryPostgres(luigi.Task):
    def output(self):
        # the output will be a .csv file
        return luigi.LocalTarget("same_purchases.csv")

    def run(self):
        # # these are here for convenience, you'll use
        # # environment variables for production code
        # host = "localhost"
        # database = "Test1"
        # user = "postgres"
        # password = "cartiva"
        #
        # conn = psycopg2.connect(
        #     dbname=database,
        #     user=user,
        #     host=host,
        #     password=password)
        # cur = conn.cursor()
        with db_cnx.pg_jon_localhost() as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    SELECT account, department, account_type
                    from fin.dim_account
                """
                pg_cur.execute(sql)
                rows = pg_cur.fetchall()
                with self.output().open("w") as out_file:
                    # write a csv header 'by hand'
                    out_file.write("account, department, account_type")
                    for row in rows:
                        out_file.write("\n")
                        # without the :%s, the date will be output in year-month-day format
                        # the star before row causes each element to be placed by itself into format
                        out_file.write("{}, {}, {}".format(*row))

if __name__ == '__main__':
    # if you want to execute one task per default without further arguments
    luigi.run(main_task_cls=QueryPostgres)
