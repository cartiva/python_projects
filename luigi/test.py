import luigi
from luigi.contrib.ssh import RemoteTarget
from luigi import format
from luigi.format import GzipFormat
from luigi.s3 import S3Target, S3Client
from luigi.configuration import get_config
import arrow
import datetime
import pysftp
import codecs
import csv
import pysftp

config = get_config()
aws_access_key_id = config.get('aws', 'aws_access_key')
aws_access_secret_key = config.get('aws', 'aws_access_secret_key')
s3bucket = config.get('aws', 's3_bucket_name')

ftphost = config.get('sftp', 'hostname')
username = config.get('sftp', 'username')
ftppassword = config.get('sftp', 'password')
ftpdir = config.get('sftp', 'ftpdir')


class CheckFileExists(luigi.Task):
    '''
    Check to see if the file exits
    '''
    filename = luigi.Parameter()
    filedate = luigi.DateParameter(default=(datetime.date.today() - datetime.timedelta(1)))
    filetype = luigi.Parameter(default='.csv')

    def requires(self):
        return []

    def complete(self):
        '''
        Connect to the SFTP, check to see if file exists
        '''
        sftp = pysftp.Connection(sftphost, username, password=fstppassword)

        sftp.chdir(ftpdir)
        result = sftp.exists(self.filename + self.filedate.strftime("%Y%m%d") + self.filetype)

        # Close the connection
        sftp.close()

        return result


class GetsftpFile(luigi.Task):
    '''
    Get the file from SFTP
    '''
    filename = luigi.Parameter()
    filedate = luigi.DateParameter(default=(datetime.date.today() - datetime.timedelta(1)))
    filetype = luigi.Parameter(default='.csv')

    def requires(self):
        return CheckFileExists(self.filename, self.filedate, self.filetype)

    def output(self):
        return luigi.LocalTarget(self.filename + self.filedate.strftime("%Y%m%d") + self.filetype)

    def run(self):
        '''
        Connect to the FTP, check to see if file exists
        '''

        sftp = pysftp.Connection(sftphost, username, password=sftppassword)

        sftp.chdir(ftpdir)

        try:
            sftp.get(self.filename + self.filedate.strftime("%Y%m%d") + self.filetype, preserve_mtime=True)
        except Exception:
            return Exception

        # Close the connection
        sftp.close()


class MoveFileToS3(luigi.Task):
    filename = luigi.Parameter()
    filedate = luigi.DateParameter(default=(datetime.date.today() - datetime.timedelta(1)))
    filetype = luigi.Parameter(default='.csv')

    def requires(self):
        return GetsftpFile(self.filename, self.filedate, self.filetype)

    def run(self):
        s3_client = S3Client(aws_access_key_id, aws_access_secret_key)
        s3_client.put(self.input().path, self.output().path)

    def output(self):
        s3_client = luigi.s3.S3Client(aws_access_key_id, aws_access_secret_key)
        print(self.input().path)
        return S3Target('s3://' + s3bucket + '/' + self.filename + self.filedate.strftime("%Y%m%d") + self.filetype,
                        client=s3_client)


if __name__ == '__main__':
    luigi.run()