trying to run fact_gl with workers, get an error
news group suggests run as admin
admin can't see shared folders
net use z: "\\vmware-host\Shared Folders"
made the folders accessibl

but running with workers still results in:
AttributeError: Can't pickle local object 'Worker._create_task_process.<locals>.update_tracking_url'

same issue:
https://github.com/spotify/luigi/issues/1683 (Python 3.5 breaks pickling)

made a python27 virtualenv for luigi, this will not run with workers either
pickle.PicklingError: Can't pickle <function update_tracking_url at 0x029E3430>: it's not found as luigi.worker.update_tracking_url
looks like it is an outstandint windows issue
https://github.com/spotify/luigi/issues/1423