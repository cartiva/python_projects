# https://threadsoftechnology.com/2016/07/19/how-to-create-a-data-pipeline-using-luigi/
# struggling with the same old inablility to run this shit
# fuck me
# got it to work by putting parameters in Run->configuration
# append timestamp to file name, this would enable scheduling and running this
# as frequently as i want
# not so sure how this would work when the output filename is required as the
# input for a subsequent task

# failed scheduled task

# this worked in command window, although it wrote the file to Z:\E\python_venvs\luigi\Scripts
# Z:\E\python_venvs\luigi\Scripts>python "z:\e\python projects\luigi\condla\pipe.py"
# CountIt --local-scheduler --input-path "z:\e\python projects\luigi\condla\test.txt"


import luigi
import time


"""
Define the input file for our job:
    The output method of this class defines
    the input file of the class in which FileInput is
    referenced in "requires"
"""

# Parameter definition: input file path
input_path = luigi.Parameter()


def output(self):
    """
    As stated: the output method defines a path.
    If the FileInput  class is referenced in a
    "requires" method of another task class, the
    file can be used with the "input" method in that
    class.
    """
    return luigi.LocalTarget(self.input_path)


class CountIt(luigi.Task):
    """
    Counts the words from the input file and saves the
    output into another file.
    """

    input_path = luigi.Parameter()

    def requires(self):
        """
        Requires the output of the previously defined class.
        Can be used as input in this class.
        """
        return FileInput(self.input_path)

    def output(self):
        """
        count.txt is the output file of the job. In a more
        close-to-reality job you would specify a parameter for
        this instead of hardcoding it.
        """
        timestr = time.strftime("%Y%m%d-%H%M%S")
        # return luigi.LocalTarget('count.txt')
        return luigi.LocalTarget('count' + '_' + timestr + '.txt')

    def run(self):
        """
        This method opens the input file stream, counts the
        words, opens the output file stream and writes the number.
        """
        word_count = 0
        with self.input().open('r') as ifp:
            for line in ifp:
                word_count += len(line.split(' '))
        with self.output().open('w') as ofp:
            # ofp.write(unicode(word_count))
            ofp.write(str(word_count))

if __name__ == "__main__":
    luigi.run(["--local-scheduler --input_path test.txt"], main_task_cls=CountIt)
    # luigi.run()
