# Filename: run_luigi.py
# https://marcobonzanini.com/2015/10/24/building-data-pipelines-with-python-and-luigi/
import luigi


class PrintNumbers(luigi.Task):
    def requires(self):
        return []

    def output(self):
        return luigi.LocalTarget("numbers_up_to_10.txt")

    def run(self):
        with self.output().open('w') as f:
            for i in range(1, 11):
                f.write("{}\n".format(i))


class SquaredNumbers(luigi.Task):
    def requires(self):
        return [PrintNumbers()]

    def output(self):
        return luigi.LocalTarget("squares.txt")

    def run(self):
        with self.input()[0].open() as fin, self.output().open('w') as fout:
            for line in fin:
                n = int(line.strip())
                out = n * n
                fout.write("{}:{}\n".format(n, out))


if __name__ == '__main__':
    # borrowed from gouthamanbalaraman.com, this works such that this script will run
    # as is from the pycharm ide
    luigi.run(["--local-scheduler"], main_task_cls=SquaredNumbers)
    # as opposed to the original script which required setting the task and scheduler
    # in pycharhm Run->configuration
    # ie Script Parameters: SquaredNumbers --local-scheduler