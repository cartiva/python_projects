"""
http://stackoverflow.com/questions/34072872/use-luigi-dateparameter-in-a-run-function-of-a-luigi-orchestrator-task/34091261#34091261

from terminal:
cd to Z:\E\python projects\luigi\date_parameter
$ python foo.py Test1 --local-scheduler

original line 22: squawks DateParameter has no strftime attribute
can't quite figure out the formatting deal

not only generates a foobar.txt file, it also generates a foobar.txt-luigi-tmp-9xxxxxxxx89 file whether it passes or
fails, what the fuck is that ???
"""
import datetime
import luigi


class WTF(luigi.Task):

    date = luigi.DateParameter(default=datetime.date.today() - datetime.timedelta(days=1))

    def output(self):
        return luigi.LocalTarget("foobar.txt")

    def run(self):
        with self.output().open('w') as out_file:
            # out_file.write(self.date.strftime("%Y%m%d") + "\n")
            out_file.write(str(self.date))


if __name__ == "__main__":
    luigi.run(main_task_cls=WTF)
