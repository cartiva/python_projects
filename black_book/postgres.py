import json
import db_cnx
import csv

with open('files/2016-2017.json') as data_file:
    data = json.load(data_file)

with db_cnx.win_local_pg() as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = """
            insert into bb.raw_json values('%s','%s')
        """ % (json.dumps(data), json.dumps(data))
        pg_cur.execute(sql)
# http://stackoverflow.com/questions/34317680/postgresql-querying-double-nested-json
#
# import json
# import db_cnx
#
# with open('files/double_nested.json') as data_file:
#     data = json.load(data_file)
#
# with db_cnx.win_local_pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         # sql = """
#         #     create table bb._list(_data jsonb)
#         # """
#         # pg_cur.execute(sql)
#         sql = """
#             insert into bb._list values('%s')
#         """ % json.dumps(data)
#         pg_cur.execute(sql)
#
# write pretty print to file
# with db_cnx.win_local_pg() as pg_con:
#     with pg_con.cursor() as pg_cur:
#         sql = """
#             select jsonb_pretty(jsonb_data) from bb.raw_json
#         """
#         pg_cur.execute(sql)
#         with open('files/acura_pp.json', 'w') as f:
#             csv.writer(f).writerows(pg_cur)