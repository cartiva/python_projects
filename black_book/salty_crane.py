# http://www.saltycrane.com/blog/2011/10/some-more-python-recursion-examples/
# this one is VERY INTERESTING
# EXAMPLE_DATA = {
#     'jobs': [{'frequency': '* * * * *',
#               'jobconfig': [{'config': [('*',
#                                          {'maxspeed': 1048576,
#                                           'password': 'onesecretpassword',
#                                           'port': 22,
#                                           'url': 'basset://basset1.domain.com/tootsiepop/123.csv',
#                                           'username': 'myusername'})],
#                              'hasbro': 'basset'},
#                             {'config': [('*',
#                                          {'field_delim': ',',
#                                           'field_line': True,
#                                           'no_blanks': True,
#                                           'quote_char': '"'})],
#                              'hasbro': 'pen'},
#                             {'config': [('*',
#                                          {'db_database': 'mydatabase',
#                                           'db_host': 'myhost',
#                                           'db_password': 'anothersecretpassword',
#                                           'db_table': 'mytable',
#                                           'db_user': 'myuser'})],
#                              'hasbro': 'dart'}],
#               'jobdesc': 'Data from tootsiepop',
#               'jobname': 'tootsiepop',
#               'max_records_fail': '110%',
#               'min_failure_time': '1000y'}],
#     'vendor': 'tootsiepop'}

EXAMPLE_DATA = {
    "year_list": [{
        "make_list": [{
            "model_list": [{
                "series_list": [{
                    "style_list": [{
                        "uvc": "2017020104",
                        "name": "4D Sdn w/Prem&A-SPEC"
                    }, {
                        "uvc": "2017020106",
                        "name": "4D Sdn w/Tech&A-SPEC"
                    }, {
                        "uvc": "2017020100",
                        "name": "4D Sedan"
                    }, {
                        "uvc": "2017020101",
                        "name": "4D Sedan w/Prem Pkg"
                    }, {
                        "uvc": "2017020105",
                        "name": "4D Sedan w/Tech Pkg"
                    }],
                    "name": ""
                }],
                "name": "ILX"
            }, {
                "series_list": [{
                    "style_list": [{
                        "uvc": "2017020520",
                        "name": "4D SUV FWD"
                    }],
                    "name": "Advance"
                }, {
                    "style_list": [{
                        "uvc": "2017020084",
                        "name": "4D SUV AWD"
                    }, {
                        "uvc": "2017020085",
                        "name": "4D SUV AWD w/Tech"
                    }, {
                        "uvc": "2017020059",
                        "name": "4D SUV FWD"
                    }, {
                        "uvc": "2017020064",
                        "name": "4D SUV FWD w/Tech"
                    }],
                    "name": "Base"
                }],
                "name": "RDX"
            }],
            "name": "Acura"
        }],
        "name": "2017"
    }]
}


def print_all_leaf_nodes(data):
    if isinstance(data, dict):
        for item in data.values():
            print_all_leaf_nodes(item)
    elif isinstance(data, list) or isinstance(data, tuple):
        for item in data:
            print_all_leaf_nodes(item)
    else:
        print data


def get_all_leaf_nodes(data):
    class Namespace(object):
        pass
    ns = Namespace()
    ns.results = []

    def inner(data):
        if isinstance(data, dict):
            for item in data.values():
                inner(item)
        elif isinstance(data, list) or isinstance(data, tuple):
            for item in data:
                inner(item)
        else:
            ns.results.append(data)

    inner(data)
    return ns.results

from pprint import pprint
pprint(get_all_leaf_nodes(EXAMPLE_DATA))

def get_all_key_value_pairs_where_values_are_simple(data):
    class Namespace(object):
        pass
    ns = Namespace()
    ns.results = []

    def inner(data):
        if isinstance(data, dict):
            for k, v in data.iteritems():
                if (isinstance(v, dict) or
                    isinstance(v, list) or
                    isinstance(v, tuple)
                    ):
                    inner(v)
                else:
                    ns.results.append((k, v))
        elif isinstance(data, list) or isinstance(data, tuple):
            for item in data:
                inner(item)

    inner(data)
    return ns.results

from pprint import pprint
pprint(get_all_key_value_pairs_where_values_are_simple(EXAMPLE_DATA))