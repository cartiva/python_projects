# coding=utf-8

import adsdb
import psycopg2
import csv
pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pgCursor = pgCon.cursor()
pgCursor_2 = pgCon.cursor()
schema = 'bb'

pgCursor.execute("""
  select replace('bb_' || blackbookpubdate::text, '-','_') as pub_date
  from bb.blackbookpubdates
  where blackbookpubdate in ('09/21/2015','11/10/2014','04/08/2013');
""")
# sql = ''
# for x in xrange(0, pub_dates):
#     row = pgCursor.fetchone()
for pub_date in pgCursor:
    table_name = pub_date[0]
    sql = """create TABLE bb.""" + table_name + """(  blackbookadtid bigint NOT NULL,
      vin citext,
      vinyear citext,
      year citext,
      plants citext,
      groupnumbers citext,
      modelnumber citext,
      manufacturer citext,
      make citext,
      model citext,
      series citext,
      bodystyle citext,
      wholesaleextraclean integer,
      wholesaleclean integer,
      wholesaleaverage integer,
      wholesalerough integer,
      retailextraclean integer,
      retailclean integer,
      retailaverage integer,
      retailrough integer,
      msrp integer,
      loanvalue integer,
      enginedescription citext,
      makenumber citext,
      uvc citext,
      regionaladjustment citext,
      blackbookpubdate date);"""
    pgCursor_2.execute(sql)
    pgCon.commit()
pgCursor.close()
pgCon.close()
