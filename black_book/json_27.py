import json
from collections import OrderedDict
from pprint import pprint
with open('files/2016-2017.json') as data_file:
    data = json.load(data_file)
# pprint(data)

# # this prints as a single row
# # {u'year_list': [{u'make_list': [{u'model_list': [{u'series_list': [{u'style_list': [{u'uvc': u'2017020104', u'na ...
print (data)

# # this prints 2017
# print data['year_list'][0]['name']

# KeyError: 'make_list'
# print data['make_list'][0]['name']

# # holy shit this works
print data['year_list'][1]['name']
print data['year_list'][1]['make_list'][12]['name']
print data['year_list'][1]['make_list'][12]['model_list'][3]['name']
print data['year_list'][1]['make_list'][12]['model_list'][3]['series_list'][0]['style_list'][0]['name']
print data['year_list'][1]['make_list'][12]['model_list'][3]['series_list'][0]['style_list'][0]['uvc']

# # this works
# with open('files/acura.json', 'r') as fp:
#     json_value = fp.read()
#     print json_value
#     raw_data = json.loads(json_value)
#     print raw_data

# # http://chimera.labs.oreilly.com/books/1230000000393/ch07.html
# # # this works but not interesing
# with open('files/acura.json') as data_file:
#     data2 = json.load(data_file, object_pairs_hook=OrderedDict)
# print data2

# # very interesting
# class JSONObject:
#     def __init__(self, d):
#         self.__dict__ = d
#
# with open('files/blackbook_2017.json') as data_file:
#     data3 = json.load(data_file, object_hook=JSONObject)
# print data3.year_list[0].make_list[0].model_list[0].name
# print data3.year_list[0].make_list[0].model_list[0].series_list[0].style_list[1].name
# print data3.year_list[0].make_list[0].model_list[0].series_list[0].style_list[1].uvc
# print data3.year_list[0].make_list[0].model_list[1].series_list[1].style_list[3].name
# print data3.year_list[0].make_list[0].model_list[1].series_list[1].style_list[3].uvc

# # ok but can't get make model etc
# for row in data['year_list']:
#     print row['name']

# # http://stackoverflow.com/questions/12966308/decoding-json-with-python-and-storing-nested-object
# # damnit this feels like it should be useful
# def printKeyVals(data, indent=0):
#     if isinstance(data, list):
#         print
#         for item in data:
#             printKeyVals(item, indent+1)
#     elif isinstance(data, dict):
#         print
#         for k, v in data.iteritems():
#             print "    " * indent, k + ":",
#             printKeyVals(v, indent + 1)
#     else:
#         print data
#
# printKeyVals(data)

# def printKeyVals(data, indent=0):
#     if isinstance(data, list):
#         print
#         for item in data:
#             printKeyVals(item)
#     elif isinstance(data, dict):
#         print
#         for k, v in data.iteritems():
#             print  k + ":",
#             printKeyVals(v)
#     else:
#         print data
#
# printKeyVals(data)

# http://stackoverflow.com/questions/21028979/recursive-iteration-through-nested-json-for-specific-key-in-python
#$meh
# def id_generator(dic):
#       for k, v in dic.items():
#             # if k == "id":
#             if k.find('_'):
#                  yield v
#             elif isinstance(v, dict):
#                  for id_val in id_generator(v):
#                        yield id_val
# for _ in id_generator(data):
#      print(_)

for k in data.keys():
    print("Got key", k, "which maps to value", data[k])

ks = list(data.keys())
print(ks)