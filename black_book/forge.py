# http://stackoverflow.com/questions/35669227/get-method-for-nested-json-doesnt-work
# Data is a valid JSON formatted string. JSON contains four basic elements:
#
# Object: defined with curly braces {}
# Array: defined with braces []
# Value: can be a string, a number, an object, an array, or the literals true, false or null
# String: defined by double quotes and contain Unicode characters or common backslash escapes
#
# Using json.loads will convert the string into a python object recursively.
# It means that every inner JSON element will be represented as a python object.
#
# Therefore: jfile.get('Study') ---> python list
#
#
# To retrieve Field you should iterate over the study list:


import json
data = """
   {"location":{"town":"Rome","groupe":"Advanced",
        "school":{"SchoolGroupe":"TrowMet", "SchoolName":"VeronM"}},
        "id":"145",
        "Mother":{"MotherName":"Helen","MotherAge":"46"},"NGlobalNote":2,
        "Father":{"FatherName":"Peter","FatherAge":"51"},
         "Study":[{
        "Teacher":["MrCrock","MrDaniel"],
       "Field":{"Master1":["Marketing", "Politics", "Philosophy"],
       "Master2":["Economics", "Management"], "ExamCode": "1256"}
         }],
         "season":["summer","spring"]}
"""

jfile = json.loads(data.strip())
study_list = jfile.get('Study', [])  # don't set default value with different type
print study_list
for item in study_list:
  print item.get('Field')