# https://gist.github.com/douglasmiranda/5127251
def find(key, dictionary):
    for k, v in dictionary.items():
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                for result in find(key, d):
                    yield result

d = {
    "year_list": [{
        "make_list": [{
            "model_list": [{
                "series_list": [{
                    "style_list": [{
                        "uvc": "2017020104",
                        "name": "4D Sdn w/Prem&A-SPEC"
                    }, {
                        "uvc": "2017020106",
                        "name": "4D Sdn w/Tech&A-SPEC"
                    }, {
                        "uvc": "2017020100",
                        "name": "4D Sedan"
                    }, {
                        "uvc": "2017020101",
                        "name": "4D Sedan w/Prem Pkg"
                    }, {
                        "uvc": "2017020105",
                        "name": "4D Sedan w/Tech Pkg"
                    }],
                    "name": ""
                }],
                "name": "ILX"
            }, {
                "series_list": [{
                    "style_list": [{
                        "uvc": "2017020520",
                        "name": "4D SUV FWD"
                    }],
                    "name": "Advance"
                }, {
                    "style_list": [{
                        "uvc": "2017020084",
                        "name": "4D SUV AWD"
                    }, {
                        "uvc": "2017020085",
                        "name": "4D SUV AWD w/Tech"
                    }, {
                        "uvc": "2017020059",
                        "name": "4D SUV FWD"
                    }, {
                        "uvc": "2017020064",
                        "name": "4D SUV FWD w/Tech"
                    }],
                    "name": "Base"
                }],
                "name": "RDX"
            }],
            "name": "Acura"
        }],
        "name": "2017"
    }]
}

print(list(find('uvc',d)))
