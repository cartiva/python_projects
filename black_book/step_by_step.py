# http://stackoverflow.com/questions/19490856/navigating-multi-dimensional-json-arrays-in-python

import json
with open('files/2016-2017.json') as data_file:
    json_data = json.load(data_file)
# print data
print type(json_data) # dict
print json_data.keys() # [u'year_list']
print type(json_data['year_list']) # list
print type(json_data['year_list'][0]) # dict
# print json_data['year_list'][0].keys() # [u'make_list', u'name']
# print type(json_data['year_list'][0]['name']) # unicode
# print json_data['year_list'][0]['name'] # 2017


