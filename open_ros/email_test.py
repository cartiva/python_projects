# encoding=utf-8
import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

# http://robertwdempsey.com/python3-email-with-attachments-using-gmail/
# finally, this fucking worked
COMMASPACE = ', '


def main():
    sender = 'jandrews@cartiva.com'
    # gmail_password = 'YOUR GMAIL PASSWORD'
    recipients = ['jandrews@cartiva.com', 'test@cartiva.com', 'test1@cartiva.com']

    # Create the enclosing (outer) message
    outer = MIMEMultipart()
    outer['Subject'] = 'Open ROs'
    outer['To'] = COMMASPACE.join(recipients)
    outer['From'] = sender
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'

    # List of attachments
    attachments = ['files/open_ros.xlsx']

    # Add the attachments to the message
    for file in attachments:
        try:
            with open(file, 'rb') as fp:
                msg = MIMEBase('application', "octet-stream")
                msg.set_payload(fp.read())
            encoders.encode_base64(msg)
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file))
            outer.attach(msg)
        except:
            # print("Unable to open one of the attachments. Error: ", sys.exc_info()[0])
            raise

    composed = outer.as_string()

    # Send the email
    try:
        with smtplib.SMTP('mail.cartiva.com') as s:
            # s.ehlo()
            # s.starttls()
            # s.ehlo()
            # s.login(sender, gmail_password)
            s.sendmail(sender, recipients, composed)
            s.close()
        print("Email sent!")
    except:
        # print("Unable to send the email. Error: ", sys.exc_info()[0])
        raise


if __name__ == '__main__':
    main()
