DROP TABLE open_ros_report;
CREATE TABLE open_ros_report (
  open_date date,
  store cichar(3),
  ro cichar(9),
  customer cichar(80),
  writer cichar(80),
  censusdept cichar(3),
  rostatus cichar(24),
  age integer,
  warranty logical);

/*
DELETE FROM open_ros_report;
INSERT INTO open_ros_report
SELECT c.thedate AS open_date, c.storecode, c.ro, d.fullname AS customer, 
  f.name AS writer, 
  CASE -- handlw XX censusdept
    WHEN f.name IN ('BEAR, JEFFERY L','LONGORIA, BEVERLEY A','WOINAROWICZ, EMILEE M') THEN 'MR'
	WHEN f.name = 'POWELL, GAYLA R' THEN 'BS'
    ELSE e.censusdept
  END AS censusdept, g.rostatus, curdate() - c.thedate AS age,
  CASE 
    WHEN h.ro IS NOT NULL THEN true
	ELSE false
  END AS warranty
FROM ( 
  SELECT c.thedate, a.storecode, a.ro, a.customerkey, a.servicewriterkey, 
    a.rostatuskey
  FROM factrepairorder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
    AND b.thedate > curdate()
  INNER JOIN day c on a.opendatekey = c.datekey
  GROUP BY c.thedate, a.storecode, a.ro, a.customerkey, a.servicewriterkey, 
    a.rostatuskey) c
INNER JOIN dimcustomer d on c.customerkey = d.customerkey	
INNER JOIN dimservicewriter e on c.servicewriterkey = e.servicewriterkey
INNER JOIN edwEmployeeDim f on e.employeenumber = f.employeenumber
  AND f.currentrow = true
INNER JOIN dimrostatus g on c.rostatuskey = g.rostatuskey  
LEFT JOIN (--warranty
  SELECT d.thedate, a.ro
  FROM factrepairorder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
    AND b.thedate > curdate()
  INNER JOIN day d on a.opendatekey = d.datekey
    AND d.thedate > '12/31/2015'
  INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
    AND c.paymenttype = 'Warranty'
  GROUP BY ro, d.thedate) h on c.ro = h.ro
WHERE c.thedate > '12/31/2015'
  AND c.ro <> '19226040'
--ORDER BY curdate() - c.thedate
*/


--http://stackoverflow.com/questions/17684610/python-convert-csv-to-xlsx

SELECT 1 AS seq, 'GM CASHIER STATUS' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 2, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus = 'Cashier'
  AND store = 'ry1'
-- ORDER BY seq, age DESC 
UNION 
SELECT 3 AS seq, 'GM SERVICE: RED > 30 days' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 4, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept IN ('MR','AM')
  AND store = 'ry1'
  AND age > 30
UNION 
SELECT 5 AS seq, 'GM SERVICE: YELLOW 14 - 30 days' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 6, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept IN ('MR','AM')
  AND store = 'ry1'
  AND age BETWEEN 14 AND 30
UNION 
SELECT 7 AS seq, 'GM BS RED > 60 days' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 8, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept IN ('BS')
  AND store = 'ry1'
  AND age > 60
UNION 
SELECT 9 AS seq, 'GM BS YELLOW > between 30 and 60' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 10, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept IN ('BS')
  AND store = 'ry1'
  AND age BETWEEN 30 AND 60
UNION 
SELECT 11 AS seq, 'GM PDQ RED > 1 day' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 12, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept = 'QL'
  AND store = 'ry1'
  AND age > 1
  AND warranty = false
UNION 
SELECT 13 AS seq, 'GM PDQ WARRANTY RED > 7 day' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 14, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept = 'QL'
  AND store = 'ry1'
  AND age > 7  
  AND warranty = true
UNION
SELECT 15 AS seq, 'HONDA CASHIER STATUS' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 16, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus = 'Cashier'
  AND store = 'ry2'  
UNION
SELECT 17 AS seq, 'HONDA SERVICE RED > 30 days' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 18, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept = 'MR'
  AND store = 'ry2'
  AND age > 30
UNION
SELECT 19 AS seq, 'HONDA SERVICE YELLOW BETWEEN 14 and 30 days' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 20, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept = 'MR'
  AND store = 'ry2'
  AND age BETWEEN 14 AND 30
UNION
SELECT 21 AS seq, 'HONDA PDQ RED > 1 day' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 22, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept = 'QL'
  AND store = 'ry2'
  AND age > 1
  AND warranty = false
UNION 
SELECT 23 AS seq, 'HONDA PDQ WARRANTY RED > 7 days' AS category, '' AS store, '' AS ro, CAST(NULL AS sql_date) AS open_date, 
  '' AS customer, '' AS writer, '' as status, CAST(NULL AS sql_integer) AS age
FROM system.iota  
UNION 
select 24, '' , a.store, a.ro, a.open_date, a.customer, a.writer, a.rostatus, a.age
FROM open_ros_report a
WHERE rostatus <> 'Cashier'
  AND censusdept = 'QL'
  AND store = 'ry2'
  AND age > 7  
  AND warranty = true
  
ORDER BY seq, age DESC 

