﻿drop table greg.used_vehicle_daily_snapshot cascade;
create table greg.used_vehicle_daily_snapshot (
  store_code citext NOT NULL,
  the_date date NOT NULL,
  stocknumber citext NOT NULL,
  vin citext NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  model_year citext NOT NULL,
  miles integer NOT NULL DEFAULT 0,  
  body_style citext,
  trim_level citext,
  interior_color citext,
  exterior_color citext,
  engine citext,
  transmission citext,
  shape citext,
  size citext,
  shape_and_size citext,
  cab citext,
  drive citext,

  from_date date NOT NULL,
  status citext NOT NULL,
  sale_date date NOT NULL,
  sale_type citext NOT NULL,
  date_priced date NOT NULL,
  best_price integer NOT NULL,
  invoice integer NOT NULL,
  days_since_priced integer NOT NULL,
  price_band citext NOT NULL,
  days_owned integer NOT NULL,
  days_avail integer NOT NULL, 
  sold_from_status citext NOT NULL,
  disposition citext,

  sale_amount integer NOT NULL DEFAULT 0,
  cost_of_sale integer NOT NULL DEFAULT 0,
  sales_gross integer NOT NULL DEFAULT 0,
  fi_sale_amount integer NOT NULL DEFAULT 0,
  fi_cost integer NOT NULL DEFAULT 0,
  fi_gross integer NOT NULL DEFAULT 0,
  sd_labor_sales integer NOT NULL DEFAULT 0,
  sd_labor_cogs integer NOT NULL DEFAULT 0,
  sd_labor_gross integer NOT NULL DEFAULT 0,
  bs_labor_sales integer NOT NULL DEFAULT 0,
  bs_labor_cogs integer NOT NULL DEFAULT 0,
  bs_labor_gross integer NOT NULL DEFAULT 0,
  bs_paint_mat_sales integer NOT NULL DEFAULT 0,
  bs_paint_mat_cogs integer NOT NULL DEFAULT 0,
  bs_paint_mat_gross integer NOT NULL DEFAULT 0,
  re_labor_sales integer NOT NULL DEFAULT 0,
  re_labor_cogs integer NOT NULL DEFAULT 0,
  re_labor_gross integer NOT NULL DEFAULT 0,
  ql_labor_sales integer NOT NULL DEFAULT 0,
  ql_labor_cogs integer NOT NULL DEFAULT 0,
  ql_labor_gross integer NOT NULL DEFAULT 0,
  parts_sales integer NOT NULL DEFAULT 0,
  parts_cogs integer NOT NULL DEFAULT 0,
  parts_gross integer NOT NULL DEFAULT 0,
  recon_sales integer NOT NULL DEFAULT 0,
  recon_cogs integer NOT NULL DEFAULT 0,
  recon_gross integer NOT NULL DEFAULT 0,

 

  vdp_views bigint NOT NULL DEFAULT 0,
  CONSTRAINT used_vehicle_daily_snapshot_pkey PRIMARY KEY (the_date, stocknumber)
);

create index on greg.used_vehicle_daily_snapshot(store_code);
create index on greg.used_vehicle_daily_snapshot(store_code);
create index on greg.used_vehicle_daily_snapshot(the_date);
create index on greg.used_vehicle_daily_snapshot(stocknumber);
create index on greg.used_vehicle_daily_snapshot(vin);
create index on greg.used_vehicle_daily_snapshot(shape_and_size);
create index on greg.used_vehicle_daily_snapshot(status);
create index on greg.used_vehicle_daily_snapshot(shape);
create index on greg.used_vehicle_daily_snapshot(size);

/*
SELECT array_agg(column_name::text order by ordinal_position)
-- select *
FROM information_schema.columns
WHERE table_schema = 'greg'
  AND table_name   = 'used_vehicle_daily_snapshot'
 
-- convert that array into a comma separated string
select array_to_string(array_agg(column_name::text order by ordinal_position), ',')
FROM information_schema.columns
WHERE table_schema = 'greg'
  AND table_name   = 'used_vehicle_daily_snapshot'
*/

delete from greg.used_vehicle_daily_snapshot;
insert into greg.used_vehicle_daily_snapshot 
  (store_code,the_date,stocknumber,vin,make,
  model,model_year,miles,body_style,trim_level,interior_color,exterior_color,engine,
  transmission,shape,size,shape_and_size,cab,drive,from_date,status,sale_date,
  sale_type,date_priced,best_price,invoice,days_since_priced,price_band,days_owned,
  days_avail,sold_from_status,disposition,sale_amount,cost_of_sale,sales_gross,
  fi_sale_amount,fi_cost,fi_gross,sd_labor_sales,sd_labor_cogs,sd_labor_gross,
  bs_labor_sales,bs_labor_cogs,bs_labor_gross,bs_paint_mat_sales,bs_paint_mat_cogs,
  bs_paint_mat_gross,re_labor_sales,re_labor_cogs,re_labor_gross,ql_labor_sales,
  ql_labor_cogs,ql_labor_gross,parts_sales,parts_cogs,parts_gross,recon_sales,
  recon_cogs,recon_gross,vdp_views)
select a.storecode, a.thedate, a.stocknumber, 
  b.vin, b.make, b.model, b.model_year, b.miles, 
  b.body_style, b.trim_level, b.interior_color, b.exterior_color,
  b.engine, b.transmission, b.shape, b.size, b.shape_and_size,
  b.cab, b.drive,
  a.from_date, a.status, a.sale_date, a.sale_Type, a.date_priced, a.best_price, a.invoice,
  a.days_since_priced, a.price_band, a.days_owned, a.days_avail, 
  a.sold_from_status, a.disposition,  
  coalesce(c.sale_amount, 0), coalesce(c.cost_of_sale, 0), coalesce(c.sales_gross, 0), 
  coalesce(c.fi_sale_amount, 0),
  coalesce(c.fi_cost, 0), coalesce(c.fi_gross, 0), coalesce(c.sd_labor_sales, 0), 
  coalesce(c.sd_labor_cogs, 0),
  coalesce(c.sd_labor_gross, 0), coalesce(c.bs_labor_sales, 0), coalesce(c.bs_labor_cogs, 0), 
  coalesce(c.bs_labor_gross, 0),
  coalesce(c.bs_paint_mat_sales, 0), coalesce(c.bs_paint_mat_cogs, 0), coalesce(c.bs_paint_mat_gross, 0),
  coalesce(c.re_labor_sales, 0), coalesce(c.re_labor_cogs, 0), coalesce(c.re_labor_gross, 0),
  coalesce(c.ql_labor_sales, 0), coalesce(c.ql_labor_cogs, 0), coalesce(c.ql_labor_gross, 0), 
  coalesce(c.parts_sales, 0),
  coalesce(c.parts_cogs, 0), coalesce(c.parts_gross, 0), coalesce(c.recon_sales, 0),
  coalesce(c.recon_cogs, 0), coalesce(c.recon_gross, 0),
  coalesce(d.views, 0)
-- select *
from greg.ucinv_tmp_avail_4 a
left join greg.ucinv_tmp_vehicles b on a.stocknumber = b.stocknumber
left join greg.ucinv_tmp_sales_activity c on a.stocknumber = c.stocknumber
left join ga.page_views d on a.thedate = d.thedate
  and b.vin = d.vin
 --where d.thedate is not null   
 --limit 100   

/*  ok
select *
from (
  select stocknumber, sale_date
  --select *
  from greg.ucinv_tmp_avail_4 
  group by stocknumber, sale_date) a
inner join greg.ucinv_tmp_sales_activity b on a.stocknumber = b.stocknumber
  and a.sale_date <> b.sale_date  
*/


