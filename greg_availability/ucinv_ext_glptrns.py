# encoding=utf-8
import csv
import db_cnx
import datetime
import uuid

pg_con = None
db2_con = None
file_name = 'files/ucinv_ext_glptrns.csv'
lvl1 = None
lvl2 = None
from_time = None
thru_time = None
log_id = None
the_date = None
try:
    from_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
    lvl1 = 'pg'
    lvl2 = 'ucinv_ext_glptrns'
    log_id = uuid.uuid4().urn[9:]
    the_date = datetime.date.today().strftime("%m/%d/%Y")
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into greg.ucinv_log(log_id,the_date,lvl1,lvl2,from_time)
                values('%s','%s', '%s', '%s', '%s')
            """ % (log_id, the_date, lvl1, lvl2, from_time)
            pg_cur.execute(sql)
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  gtco#, gttrn#, gtseq#, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
                  gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
                  trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
                  trim(gtdesc), gttamt, gtcost, gtctlo,gtoco#, rrn(rydedata.glptrns)
                FROM  rydedata.glptrns
                where trim(gtacct) in ('124100','124000')
                    and gtdate > '12/31/2013'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate greg.ucinv_ext_glptrns")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy greg.ucinv_ext_glptrns from stdin with csv encoding 'latin-1'""", io)
            thru_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
            sql = """
                update greg.ucinv_log
                set thru_time = '%s',
                    pass_fail = 'Pass'
                where log_id = '%s'
                  and the_date = '%s'
                  and lvl1 = '%s'
                  and lvl2 = '%s';
            """ % (thru_time, log_id, the_date, lvl1, lvl2)
            pg_cur.execute(sql)
except Exception, error:
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
