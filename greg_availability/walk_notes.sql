﻿
select a.*, b.make, b.model, b.model_year, b.body_style, b.miles, sold_from_status, sale_type, sales_gross, fi_gross, 
  recon_gross
from (
  select walk_date, walker, evaluator,stocknumber,notes
  from greg.walk_notes 
  group by walk_date, walker, evaluator,stocknumber,notes) a
left join (
  select *
  from greg.used_vehicle_daily_snapshot 
  where 
    CASE 
      when sale_date <= current_date - 1 then the_date = sale_date
      else the_date = current_date -1
    end) b on a.stocknumber = b.stocknumber
where a.walk_date > '01/01/2014'    



select min(the_date) from greg.used_vehicle_daily_snapshot 



select *
from greg.used_vehicle_daily_snapshot 
where 
  CASE 
    when sale_date <= current_date - 1 then the_date = sale_date
    else the_date = current_date -1
  end 
order by sale_Date desc  

select * from greg.walk_notes where stocknumber in (
select stocknumber from greg.walk_notes group by stocknumber having count(*) > 1)
order by stocknumber