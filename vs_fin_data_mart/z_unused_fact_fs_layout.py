# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'fact_fs_layout'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_eisglobal_sypfflout.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select trim(flcode), flcyy,flpage,flseq,flline,flflne,flflsq,trim(fldata),trim(flcont)
                from eisglobal.sypfflout
                where trim(flcode) = 'GM'
                  and flcyy > 2010
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_eisglobal_sypfflout")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_eisglobal_sypfflout from stdin with csv encoding 'latin-1 '""", io)
            pg_con.commit()
            pg_cur.execute("truncate dds.xfm_eisglobal_sypfflout")
            sql = """
                insert into dds.xfm_eisglobal_sypfflout
                select flcyy, flpage, flflne, trim(left(line_label, position('|' in line_label) - 1)) as line_label
                from (
                  select flcyy, flpage, flflne,
                  case
                    when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
                    else trim(fldata)
                  end as line_label
                  from dds.ext_eisglobal_sypfflout
                  where flcode = 'GM'
                    and flcyy > 2010
                    and flflsq = 0
                    and flflne > 0
                    and flpage < 18) x
                group by flcyy, flpage, flflne, left(line_label, position('|' in line_label) - 1)
            """
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                update dds.xfm_eisglobal_sypfflout
                set line_label = 'ASSETS'
                where line_label = 'ASSETS                 AMOUNT';

                update dds.xfm_eisglobal_sypfflout
                set line_label = 'EXPENSES'
                where line_label = 'EXPENSES                                                                                                 NEW SALES/PN';
            """
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                select count (1)
                from ((
                    select *
                    from fin.fact_fs_layout d
                    except
                    select *
                    from dds.xfm_eisglobal_sypfflout e)
                  union (
                    select *
                    from dds.xfm_eisglobal_sypfflout f
                    except
                    select *
                    from fin.fact_fs_layout g)) x
            """
            pg_cur.execute(sql)
            the_count = pg_cur.fetchone()[0]
            if the_count != 0:
                raise Exception('there are differences between dds.ext_eisglobal_sypfflout and fin.fact_fs_layout')
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    ops.email_error(task, run_id, error)
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
