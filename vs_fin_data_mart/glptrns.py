# encoding=utf-8
"""
    Filter GLPTRNS: where GTADJUST <> 'Y': these are the 13th month entries
"""
import csv
import db_cnx
import ops
import string

task = 'glptrns'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_glptrns_tmp.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  gtco#, gttrn#, gtseq#, gtdtyp, gttype, trim(gtpost), gtrsts, gtadjust, gtpsel,
                  trim(gtjrnl), gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
                  trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
                  trim(upper(gtdesc)), gttamt, gtcost, gtctlo,gtoco#
                from rydedata.glptrns a
                where gtdate between current_date - 45 day and current_date
                  and gtadjust <> 'Y'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_glptrns_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_glptrns_tmp from stdin with csv encoding 'latin-1 '""", io)
            pg_con.commit()
            pg_cur.execute("truncate arkona.xfm_glptrns")
            sql = """
                insert into arkona.xfm_glptrns
                select a.*, md5(a::text) as hash
                from (
                  select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct,
                    gtctl_, gtdoc_, gtref_, gttamt::numeric(12,2), coalesce(gtdesc, 'NONE')
                  from arkona.ext_glptrns_tmp
                  where gtco_ = 'RY1'
                    and gtpost in ('Y','V')
                    and left(gtacct, 1) <> '3'
                  group by gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_,
                    gtdoc_, gtref_, gttamt, gtdesc) a;
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
