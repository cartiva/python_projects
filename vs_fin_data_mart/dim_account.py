# encoding=utf-8
# import csv
import db_cnx
import ops
import string

task = 'dim_account'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/xfm_glpmast_tmp.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.xfm_glpmast")
            sql = """
                insert into arkona.xfm_glpmast
                select account_number, account_type, account_desc,
                  case account_sub_type
                    when 'A' then 'RY1'
                    when 'B' then 'RY2'
                    else 'XXX'
                  end,
                  department,
                  case typical_balance
                    when 'D' then 'Debit'
                    when 'C' then 'Credit'
                    else 'X'
                  end
                from arkona.ext_glpmast
                where account_sub_type <> 'C' -- crookston accounts
                group by account_number, account_type, account_desc, account_sub_type,
                  department, typical_balance
            """
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                select count(1)
                from (
                    (select account, account_type, description, store_code, department_code, typical_balance
                    from arkona.xfm_glpmast
                    except
                    select account, account_type_code, description, store_code, department_code, typical_balance
                    from fin.dim_account)
                union
                    (select account, account_type_code, description, store_code, department_code, typical_balance
                    from fin.dim_account
                    where current_row = true
                        and account not in ('none', '*E*', '*VOID', '*SPORD')
                    except
                    select account, account_type, description, store_code, department_code, typical_balance
                    from arkona.xfm_glpmast)) x
            """
            pg_cur.execute(sql)
            the_count = pg_cur.fetchone()[0]
            if the_count != 0:
                raise Exception('there are differences between xfm_glpmast and dim_account')
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    ops.email_error(task, run_id, error)
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
