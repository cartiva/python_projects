﻿ext_tablename: one time task to populate the base scrape data

subsequently, daily scrapes will go into ext_tablename_tmp, which will then be processed, 
  adding rows to ext_tablename as required

DROP TABLE IF EXISTS dds.ext_glpdept;
CREATE TABLE IF NOT EXISTS dds.ext_glpdept(
    COMPANY_NUMBER CITEXT,
    DEPARTMENT_CODE CITEXT,
    DEPT_DESCRIPTION CITEXT,
    constraint ext_glpdept_pk primary key (company_number,department_code))
WITH (OIDS=FALSE);
COMMENT ON COLUMN dds.ext_GLPDEPT.COMPANY_NUMBER IS 'GDCO# : Company Number';
COMMENT ON COLUMN dds.ext_GLPDEPT.DEPARTMENT_CODE IS 'GDDEPT : Department Code';
COMMENT ON COLUMN dds.ext_GLPDEPT.DEPT_DESCRIPTION IS 'GDDESC : Dept Description';
-- {TRIM(COMPANY_NUMBER),TRIM(DEPARTMENT_CODE),TRIM(DEPT_DESCRIPTION)}'

insert into ops.tasks values('ext_glpdept','Daily');
delete from ops.task_log where task = 'ext_glpdept';
delete from ops.tasks where task = 'ext_glpdept';

insert into ops.tasks values('glpdept','Daily');

DROP TABLE IF EXISTS dds.ext_glpdept_tmp;
CREATE TABLE IF NOT EXISTS dds.ext_glpdept_tmp(
    COMPANY_NUMBER CITEXT,
    DEPARTMENT_CODE CITEXT,
    DEPT_DESCRIPTION CITEXT,
    constraint ext_glpdept_tmp_pk primary key (company_number,department_code))
WITH (OIDS=FALSE);
COMMENT ON COLUMN dds.ext_GLPDEPT_tmp.COMPANY_NUMBER IS 'GDCO# : Company Number';
COMMENT ON COLUMN dds.ext_GLPDEPT_tmp.DEPARTMENT_CODE IS 'GDDEPT : Department Code';
COMMENT ON COLUMN dds.ext_GLPDEPT_tmp.DEPT_DESCRIPTION IS 'GDDESC : Dept Description';
-- {TRIM(COMPANY_NUMBER),TRIM(DEPARTMENT_CODE),TRIM(DEPT_DESCRIPTION)}'

-- if there is a new row in _tmp, this does not expose it

select (
  select count(*) 
  from (
    select * 
    from dds.ext_glpdept
    except
    select *
    from dds.ext_glpdept_tmp) a)
+ 
-- but this does
(
  select count(*) 
  from (
    select * 
    from dds.ext_glpdept_tmp 
    except
    select *
    from dds.ext_glpdept) a)

select count (1) 
from ((
    select * 
    from dds.ext_glpdept d
    except
    select *
    from dds.ext_glpdept_tmp e)    
  union (
    select * 
    from dds.ext_glpdept_tmp f
    except
    select *
    from dds.ext_glpdept g)) x 

select * from ops.task_log where task = 'glpdept'    

drop table if exists dds.xfm_glpdept cascade;
create table dds.xfm_glpdept (
  department_code citext not null primary key,
  department citext not null);

insert into dds.xfm_glpdept
select department_code, dept_description 
from dds.ext_glpdept
group by department_code, dept_description;

-- 10/3/16
-- refactorying
1. change schema to arkona from dds
2. this is a small look up table used in the etl process only, to provide textual description of department codes in
    dim_account
3. change schema in glpdept.py and deploy  