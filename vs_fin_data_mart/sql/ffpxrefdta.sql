﻿select * from dds.ext_ffpxrefdta -- 144850

select * from dds.ext_ffpxrefdta  -- 16975
where coalesce(consolidation_grp, '1')  <> '3'
  and factory_code = 'GM'
  and factory_account <> '331A'
  and factory_financial_year > 2010
  and g_l_acct_number is not null limit 100


select factory_financial_year as the_year, 
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  g_l_acct_number as gl_account, factory_account as gm_account,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null -- eliminates the XFCOPY... rows



select the_year, gl_account, gm_account from (
select factory_financial_year as the_year, 
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  g_l_acct_number as gl_account, factory_account as gm_account,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null -- eliminates the XFCOPY... rows
) x group by the_year, gl_account,gm_account having count(*) > 1  

drop table if exists dds.xfm_ffpxrefdta cascade;
create table dds.xfm_ffpxrefdta (
  the_year integer not null,
  gl_account citext not null,
  gm_account citext not null,
  store_code citext not null,
  multiplier numeric(2,1) not null default 1,
  constraint xfm_ffpxrefdta_pk primary key (the_year,gl_account,gm_account));

insert into dds.xfm_ffpxrefdta
select factory_financial_year as the_year, 
  g_l_acct_number as gl_account, factory_account as gm_account,
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null; -- eliminates the XFCOPY... rows  

  
create table dds.xfm_ffpxrefdta_tmp (
  the_year integer not null,
  gl_account citext not null,
  gm_account citext not null,
  store_code citext not null,
  multiplier numeric(2,1) not null default 1,
  constraint xfm_ffpxrefdta_tmp_pk primary key (the_year,gl_account,gm_account));  



insert into ops.tasks values ('ffpxrefdta','Daily');

select * from dds.xfm_ffpxrefdta 


                select *
                from ((
                    select *
                    from dds.xfm_ffpxrefdta d
                    except
                    select *
                    from dds.xfm_ffpxrefdta_tmp e)
                  union (
                    select *
                    from dds.xfm_ffpxrefdta_tmp f
                    except
                    select *
                    from dds.xfm_ffpxrefdta g)) x

-- 8/23/16 
-- routing for the new marketing holding accounts (12315,12415,12515,12715,12915)    
insert into dds.xfm_ffpxrefdta (the_year,gl_account,gm_account,store_code,multiplier)
select *
from dds.xfm_ffpxrefdta_tmp f
except
select *
from dds.xfm_ffpxrefdta g


-- 10/6/16
move to arkona schema     

select *
from arkona.xfm_ffpxrefdta_tmp f
except
select *
from arkona.xfm_ffpxrefdta g

-- get out of dds
alter table dds.ext_ffpxrefdta
rename to z_unused_ext_ffpxrefdta;
alter table dds.xfm_ffpxrefdta
rename to z_unused_xfm_ffpxrefdta;
alter table dds.xfm_ffpxrefdta_tmp
rename to z_unused_xfm_ffpxrefdta_tmp;

in september, jeri added one new account: 16603 (pdq contributions)
add that to ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta g

-- -- changes in ffpxrefdta:
--   update arkona.ext_ffpxrefdta
--   update fin.dim_fs

select * from arkona.ext_Ffpxrefdta where g_l_acct_number = '16603'

select * from arkona.ext_eisglobal_sypffxmst where fxmact = '066d'

select * from ops.task_dependencies where successor = 'sypffxmst' or predecessor = 'sypffxmst'

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/12/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '16603'


-- 1/3/17
-- 3 new accounts added for December
-- 230700
-- 285003
-- 130700
                select *
                from ((
                    select *
                    from arkona.ext_ffpxrefdta d
                    except
                    select *
                    from arkona.ext_ffpxrefdta_tmp e)
                  union (
                    select *
                    from arkona.ext_ffpxrefdta_tmp f
                    except
                    select *
                    from arkona.ext_ffpxrefdta g)) x

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '230700';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '130700';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '285003';


-- 1/13/17
-- 2 new accounts added in January, this time, jeri added the routing immediately after adding the new accounts in glpmast
-- 180601
-- 180801
                select *
                from ((
                    select *
                    from arkona.ext_ffpxrefdta d
                    except
                    select *
                    from arkona.ext_ffpxrefdta_tmp e)
                  union (
                    select *
                    from arkona.ext_ffpxrefdta_tmp f
                    except
                    select *
                    from arkona.ext_ffpxrefdta g)) x

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '01/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('180601','180801');
