﻿drop table if exists tmp_fs;
-- year, page, line, col, gm_account, label
create temp table tmp_fs as
select a.fxmcyy, a.fxmact, a.fxmpge, a.fxmlne, a.fxmcol , b.*
from dds.ext_eisglobal_sypffxmst a
left join (
  select flcyy,flpage,flflne,left(fldata_1, position('|' in fldata_1) - 1) as line_label
  from (
    select flcyy,flpage,flflne,fldata, length(trim(fldata)),
      case
        when left(fldata, 1) = '|' then right(fldata, length(fldata) - 1)
        else fldata
      end as fldata_1
    from dds.ext_eisglobal_sypfflout
    where trim(flcode) = 'GM'
      and flcyy > 2010
      and flflsq = 0) a) b on a.fxmcyy = b.flcyy and a.fxmpge = b.flpage and a.fxmlne = b.flflne
where a.fxmcyy > 2010      
order by a.fxmcyy, a.fxmpge, a.fxmlne, a.fxmcol

-- ok, are there multiple labels per line
-- only on page 18 line 39
select fxmcyy, fxmpge, fxmlne
from (
  select fxmcyy, fxmpge, fxmlne, line_label 
  from tmp_fs
  group by fxmcyy, fxmpge, fxmlne, line_label) x
group by fxmcyy, fxmpge, fxmlne
having count(*) > 1

select *
from tmp_fs
where fxmpge = 16
  and fxmlne = 21

  

-- this gives me a bunch of shit with null values from the xref table  
-- because of the effect of the "formula" accounts, gl_account correlation 
-- form layout and form population are separate i think
-- or the form is a separate process,
-- the values are a separate proces with just the accounts from form that are not formulas (1 - 9)
select *
from tmp_fs a
left join (
  select factory_financial_year, factory_account, g_l_acct_number, fact_account_, coalesce(a.consolidation_grp, '1') as store 
  from dds.ext_ffpxrefdta a
  where  coalesce(a.consolidation_grp, 'X') <> '3'
    and a.factory_account <> '331A'
    and a.factory_financial_year > 2010
    and a.factory_code = 'GM') b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
where a.fxmcyy = 2016    
  and a.fxmpge = 17
order by a.fxmcyy, a.fxmpge, a.fxmlne, a.fxmcol    


-- first the component parts nightly
-- many multiple line rows
select fxmcyy, fxmpge, fxmlne
from dds.ext_eisglobal_sypffxmst
group by fxmcyy, fxmpge, fxmlne
having count(*) > 1
order by fxmcyy, fxmpge, fxmlne

select fxmcyy, fxmpge, fxmlne, fxmcol
from dds.ext_eisglobal_sypffxmst
group by fxmcyy, fxmpge, fxmlne, fxmcol
having count(*) > 1
order by fxmcyy, fxmpge, fxmlne

-- struggling with the glpmast department conundrum ----------------------------------------------
select coalesce(a.consolidation_grp, '1'), a.factory_financial_year,
  a.g_l_acct_number, a.factory_Account, a.fact_account_ ,
  b.description, b.department_code
-- select *  
from dds.ext_ffpxrefdta  a
left join fin.dim_account b on a.g_l_acct_number = b.account
where a.factory_financial_year = 2016
  and coalesce(a.consolidation_grp, '1') <> '3'
  and a.factory_code = 'GM'
  and a.factory_account <> '331A'

select * from fin.dim_fs_layout where page = 3 and the_year = 2016

  select *
  from dds.ext_eisglobal_sypffxmst a
  where a.fxmcyy = 2016
  and a.fxmpge = 1
  order by fxmlne, fxmcol, fxmstr
  
  select a.fxmcyy, a.fxmpge, a.fxmlne, a.fxmcol
  from dds.ext_eisglobal_sypffxmst a
  where a.fxmcyy = 2016
  group by a.fxmcyy, a.fxmpge, a.fxmlne, a.fxmcol
  order by a.fxmcyy, a.fxmpge, a.fxmlne, a.fxmcol
  
-- distinct columns per page
  select a.fxmcyy, a.fxmpge, a.fxmcol, a.fxmstr, count(*)
  from dds.ext_eisglobal_sypffxmst a
  where a.fxmcyy = 2016
  group by a.fxmcyy, a.fxmpge, a.fxmcol, a.fxmstr
  order by a.fxmcyy, a.fxmpge, a.fxmcol, a.fxmstr

----------------------------------------------------------------------------------------------------

-- aha some page/line/col resolve to multiple accounts
select fxmpge, fxmlne, fxmcol, fxmstr
from (
  select fxmact, fxmpge, fxmlne, fxmcol, fxmstr 
  from dds.ext_eisglobal_sypffxmst a 
  where left(a.fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0') 
    and fxmcyy = 2016 
  group by fxmact, fxmpge, fxmlne, fxmcol, fxmstr) b
group by  fxmpge, fxmlne, fxmcol, fxmstr
having count(*) > 1
order by fxmpge, fxmlne, fxmcol



select fxmpge, fxmlne
from (
  select fxmact, fxmpge, fxmlne
  from dds.ext_eisglobal_sypffxmst a 
  where left(a.fxmact, 1) in ('1','2','3','4','5','6','7','8','9','0') 
    and fxmcyy = 2016 
  group by fxmact, fxmpge, fxmlne) b
group by  fxmpge, fxmlne
having count(*) > 1
order by fxmpge, fxmlne


select *
from dds.ext_eisglobal_sypffxmst
where fxmcyy = 2016
and fxmpge = 4
and fxmlne = 24

-- hmm, derive year, store, page,line,column label
-- will need to manually edit the ry2 new car pages
select fxmcyy, 'RY1', fxmpge, fxmlne, fxmcol
from dds.ext_eisglobal_sypffxmst
where fxmcyy = 2016
group by fxmcyy, fxmpge, fxmlne, fxmcol 


select fxmcyy, fxmpge, fxmlne, fxmcol
from (
  select fxmcyy, fxmpge, fxmlne, fxmcol, fxmact
  from dds.ext_eisglobal_sypffxmst
  where fxmcyy > 2010
  group by fxmcyy, fxmpge, fxmlne, fxmcol, fxmact) a
group by fxmcyy, fxmpge, fxmlne, fxmcol
having count(*) > 1  

select *
from dds.ext_eisglobal_sypffxmst
where fxmcyy = 2016 and fxmpge = 4 and fxmlne = 59 and fxmcol = 11

-- 7/11 -- just go ahead with the bridge, need to see if it is going to work
-- the whole column deal may resolve with the bridge
-- tempted to just do the non formula stuff for now

-- multiple account_key = null:: 2 issues: 
1: no row in ffpxrefdta correlating to row in sypffxmst
select fxmcyy, count(*)
-- select * 
from dds.ext_eisglobal_sypffxmst a 
left join dds.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_account <> '331A'
  and b.factory_code = 'GM'
where left(a.fxmact, 1) not in ('<','&','*')
  and a.fxmcyy > 2010
  and b.factory_account is null
group by fxmcyy  

2. those ffpxrefdta rows where the g_l_acct_number is like SPLIT
  
select * 
from dds.ext_ffpxrefdta 
where g_l_acct_number like 'SPLT%' 
  and factory_financial_year = 2016
  and coalesce(consolidation_grp, '1') <> '3'
  and factory_code = 'GM'

select factory_Financial_year, count(*) from dds.ext_ffpxrefdta where g_l_acct_number like 'SPLT%' group by factory_Financial_year



drop table if exists tmp_bridge;
create temp table tmp_bridge as 
select a.*, b.g_l_acct_number, b.fact_account_, 
  case 
    when coalesce(b.consolidation_grp, '1') = '1' then 'RY1'
    when coalesce(b.consolidation_grp, '1') = '2' then 'RY2'
  END as store_code, 
  coalesce(c.description, cc.description) as description, 
  coalesce(c.department_code, cc.department_code) as department_Code, 
  coalesce(c.department, cc.department) as department, 
  coalesce(c.account_key, cc.account_key) as account_key,
  coalesce(c.account, cc.account) as account
from dds.ext_eisglobal_sypffxmst a
left join dds.ext_ffpxrefdta b on a.fxmact = b.factory_account
  and a.fxmcyy = b.factory_financial_year
  and coalesce(b.consolidation_grp, '1') <> '3'
  and b.factory_account <> '331A'
  and b.factory_code = 'GM'
left join fin.dim_account c on b.g_l_acct_number = c.account  
  and c.current_row = true
left join fin.dim_account cc on 1 = 1
  and cc.account = 'none'
WHERE left(a.fxmact, 1) not in ('<','&','*')
--  AND a.fxmcyy = 2016
  -- no row in ffpxrefdta correlating to row in sypffxmst
  and b.factory_account is not null 
  -- temporarily leave out parts split
  and b.g_l_acct_number not like 'SPLT%'


select * from tmp_bridge where account = 'none' order by fxmpge, fxmlne

select * from tmp_bridge where fxmpge = 4 and fxmlne = 8

select account_key from tmp_bridge group by account_key having count(*) > 1

select * from tmp_bridge where g_l_acct_number = '147701'

select * from tmp_bridge where fact_account_ <> 1

select * from tmp_bridge where fxmcyy = 2016

-- parts split -- later -----------------------------------------------------------------
select *
from fin.dim_Account
where account in(
select replace(g_l_acct_number,'SPLT','')
from tmp_bridge where account = 'none')
order by account

select b.yearmonth, c.account, sum(a.amount), count(*)
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.theyear = 2016
inner join (
  select account, account_key
  from fin.dim_Account
  where account in(
  select replace(g_l_acct_number,'SPLT','')
  from tmp_bridge where account = 'none')) c on a.account_key = c.account_key  
group by b.yearmonth, c.account  

--------------------------------------------------------------------------------------------

select * from tmp_bridge where fxmcyy = 2016

select distinct department from tmp_bridge where fxmcyy = 2016

select * from fin.dim_fs_layout where the_year = 2015

select * from fin.dim_fs_org

select count(*)
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201605


drop table if exists wtf;
create temp table wtf as
select a.*, 
  b.fxmcol, b.fxmact, b.g_l_acct_number, b.account_key, b.description, b.department
from fin.dim_fs_layout a
left join tmp_bridge b on a.the_year = b.fxmcyy
  and a.store_code = b.store_code
  and a.page = b.fxmpge
  and a.line = b.fxmlne
where a.the_year = 2016
--  and a.store_code = 'RY1'
order by a.page, a.line;

select * from wtf

drop table if exists wtf_1;
create temp table wtf_1 as
select b.page, b.line, b.line_label, b.fxmcol, b.fxmact, b.g_l_acct_number, b.account_key, b.description, 
  c.store, c.area, c.department, c.sub_department 
from wtf b
left join fin.dim_fs_org c on b.store_code = c.store
  and
    case 
      when b.department = 'Body Shop' then c.department = 'body shop'
      when b.department = 'Car Wash' then c.sub_Department = 'car wash'
      when b.department = 'Detail' then c.sub_department = 'detail'
      when b.department = 'Finance' and b.line < 10 then c.department = 'finance' and c.sub_department = 'new'
      when b.department = 'Finance' and b.line > 10 then c.department = 'finance' and c.sub_department = 'used'
      when b.department = 'New Vehicle' then c.department = 'sales' and c.sub_department = 'new'
      when b.department = 'Parts' then c.department = 'parts'
      when b.department = 'Quick Lane' then c.sub_department = 'quick lane'
      when b.department = 'Service' then c.sub_department = 'mechanical'
      when b.department = 'Used Vehicle' then c.department = 'sales' and c.sub_department = 'used'
    end 
where b.page > 2   
order by b.page, b.line

select * from wtf_1

select page, line, fxmcol, line_label, store, area, department, sub_department, sum(amount) as amount 
from wtf_1 a
left join fin.fact_gl b on a.account_key = b.account_key
  and b.date_key in (
    select datekey
    from dds.day
    where yearmonth = 201605)
where a.account_key is not null 
  and a.store is not null 
group by page, line, fxmcol, line_label, store, area, department, sub_department
order by page, line

-- ok, kind of getting close
-- -- fin.fact_fs table for each month should there be a row for every page/line regardless 
-- -- that would mean, kind of a place holder for eventually populating formula based lines
-- -- i kind of like that
-- -- a row for every store/year_month/page/line/gl_account whether there is an amount (or even an account) or not
-- -- not sure how this addresses multiple departments or account types (sales/cogs/profit) per line
-- -- guess we will see
-- -- hmmm manually determing for each line the column characteristics
-- -- do not know

in the intial build, allow nulls (and leave out FK referencing) as i progressively populate it
create table fin.fact_fs (
  year_month integer,
  fs_layout_key integer,
  fs_org_key integer,
  fs_account_key integer,
  amount numeric(12,2));

select the_year, store_code, count(*) 
from fin.dim_fs_layout
-- where store_code = 'ry1'
--   and the_year = 2016
group by the_year, store_code  
order by the_year, store_code

select * from fin.dim_fs_layout order by line



drop table if exists temp_1;
create table temp_1 as
select year_month, fs_layout_key
from (
  select *, the_year * 100 + generate_series as year_month
  from (
    select distinct the_year as the_year
    from fin.dim_fs_layout) a
  cross join (
    select * from generate_Series(1, 12, 1)) b) c
left join fin.dim_fs_layout d on c.the_year = d.the_year;
create index on temp_1(year_month);
create index on temp_1(fs_layout_key);
create unique index on temp_1(year_month,fs_layout_key);

select *
from temp_1 a
left join fin.dim_fs_layout b on a.fs_layout_key = b.fs_layout_key
left join tmp_bridge c on b.the_year = c.fxmcyy
  and b.page = c.fxmpge
  and b.line = c.fxmlne
where a.year_month = 201605
order by b.store_code, page, line


-- routing is specific to year
select g_l_acct_number
from (
select g_l_acct_number, factory_account 
from dds.ext_ffpxrefdta a
where coalesce(a.consolidation_grp, '1') <> '3'
  and a.factory_account <> '331A'
  and a.factory_code = 'GM'
group by g_l_acct_number, factory_account 
) x
group by g_l_acct_number
having count(*) > 1



select * 
from (
  select factory_financial_year, coalesce(consolidation_grp, '1') as store_code, g_l_acct_number, factory_account, fact_account_
  from dds.ext_ffpxrefdta a
  where coalesce(a.consolidation_grp, '1') <> '3'
    and a.factory_account <> '331A'
    and a.factory_code = 'GM'
    and factory_financial_year > 2010) e
full outer join (
  select fxmcyy, fxmact, fxmpge, fxmlne, fxmcol
  from dds.ext_eisglobal_sypffxmst
  where fxmcyy > 2010) f on e.factory_financial_year = f. fxmcyy and e.factory_account = f.fxmact


-- are there any ffpxref accounts not in sypff  
-- nope, so ffpxref becomes the basis
-- generate the bridge
-- as i build this i worry about the none/unknown
-- hmm should be ok for the "formula" accounts, account_key is simply none, 
-- another good reason for exposing gm acct in bridge except the fucking formula accounts aren't in this
drop table if exists fin.br_fs_account;
create table fin.br_fs_account (
  fs_account_key serial primary key,
  account_key integer not null references fin.dim_account(account_key),
  gm_account citext not null,
  the_year integer not null,
  page integer not null,
  line numeric(4,1) not null,
  col integer not null, 
  multiplier numeric(2,1) not null);

insert into fin.br_fs_account(account_key, gm_account, the_year, page, line, col, multiplier)
select account_key, factory_account, factory_financial_year, fxmpge, fxmlne, fxmcol, fact_account_
from (
  select factory_financial_year, coalesce(consolidation_grp, '1') as store_code, g_l_acct_number, factory_account, fact_account_
  from dds.ext_ffpxrefdta a
  where coalesce(a.consolidation_grp, '1') <> '3'
    and a.factory_account <> '331A'
    and a.factory_code = 'GM'
    and factory_financial_year > 2010
    -- excludes the goofy XFCOPY factory accounts
    and g_l_acct_number is not null
    -- leave out parts splits for now
    and g_l_acct_number not like 'SPLT%') e
left join (   
  select fxmcyy, fxmact, fxmpge, fxmlne, fxmcol
  from dds.ext_eisglobal_sypffxmst
  where fxmcyy > 2010) f on e.factory_financial_year = f. fxmcyy and e.factory_account = f.fxmact
left join fin.dim_account g on e.g_l_acct_number = g.account  
union
select b.account_key, fxmact, fxmcyy, fxmpge, fxmlne, fxmcol, 0
from dds.ext_eisglobal_sypffxmst a
inner join fin.dim_account b on 1 = 1
  and b.account = 'none'
where fxmcyy > 2010
  and left(a.fxmact, 1) in ('<','&','*')

select * from br_fs_account where gm_account = '<ADV'

select a.*, left(a.year_month::text, 4)::integer
from temp_1 a


select *
from temp_1 a
left join fin.dim_fs_layout b on a.fs_layout_key = b.fs_layout_key
left join br_fs_account c on b.the_year = c.the_year
  and b.page = c.page
  and b.line = c.line
where a.year_month = 201605
  and b.store_code = 'RY1'
order by b.store_code, b.page, b.line

select *
from fin.br_fs_account a
inner join fin.dim_account b on a.account_key = b.account_key
where a.gm_Account = '650b'

-- 7/13 ----- starting over page/line/col in fact table ------------------------------------
-- this actually seems a bit promising - but it always does at first 

TEMP1
manually generated
  the_year
  year_month

sypfflout
  page
  line

sypffxmst
  col
  gm_account  

7-13
left out those rows where fxmact/fxmcol are null
this still includes "formula" lines
grain is now 1 row per year_month/fxmact

select * from temp1 limit 2000

select year_month, fxmact from temp1 group by year_month, fxmact having count(*) > 1

-- -- -- this is all from when i was trying to include everything, pointless for now
-- -- unique where fxmact is not null 
-- select year_month, fxmact from temp1 where fxmact is not null group by year_month,fxmact having count(*) > 1
-- 
-- -- mostly the fxmact is null on lines that are simply labels: section headings, totals/subtotals, etc 
-- select the_year, flpage, flflne from temp1 where fxmact is null group by the_year, flpage, flflne order by the_year, flpage, flflne
-- 
-- -- there are none of these, they are both null
-- select * from temp1 where fxmact is null and fxmcol is not null
-- select * from temp1 where fxmcol is null and fxmact is not null
-- 
-- -- year_month, page, line unique only when col is null, there are multiple
-- select year_month, flpage, flflne, fxmcol from temp1 where fxmcol is null group by year_month, flpage, flflne, fxmcol having count(*) > 1
-- 
-- select year_month, flpage, flflne, coalesce(fxmcol, -1) from temp1 where fxmcol <> 0 group by year_month, flpage, flflne, coalesce(fxmcol, -1) having count(*) > 1


drop table if exists temp1;
create temp table temp1 as
select c.the_year, c.year_month, d.flpage as page, d.flflne as line, 
  d.fxmcol as col, d.fxmact as gm_account
from ( -- year and year_month
  select a.the_year, a.the_year * 100 + the_month as year_month
  from (
    select generate_series(2011, 2016, 1) as the_year) a
  cross join (
    select * from generate_Series(1, 12, 1) as the_month) b) c
left join ( -- pages and lines
  select a.*, b.fxmcol, b.fxmact
  from (
    select flcyy,flpage,flflne
    from dds.ext_eisglobal_sypfflout
    where trim(flcode) = 'GM'
      and flcyy > 2010
      and flflsq = 0
      and flflne > 0
    group by flcyy,flpage,flflne) a
  left join ( -- columns and gm_accounts  
    select fxmcyy, fxmact, fxmpge, fxmlne, fxmcol
    from dds.ext_eisglobal_sypffxmst
    where fxmcyy > 2010) b  on a.flcyy = b.fxmcyy and a.flpage = b.fxmpge and a.flflne = b.fxmlne) d on c.the_year = d.flcyy   
where c.year_month < 201607
  and d.fxmcol is not null; 

--147701 / 477S fix: s/b reouted to page 16 line 49 col 2, not split, all body shop
--167701 /677S fix: s/b reouted to page 16 line 49 col 3, not split, all body shop
-- update temp1
-- set page = 6,
--     line = 49, 
--     col = 2
-- where gm_account = '477S';    
-- update temp1
-- set page = 6,
--     line = 49, 
--     col = 3
-- where gm_account = '677S'; 
-- TODO
-- so it appears that there will have to be 2 rows for each of the rows with a formula account
-- one for ry1 and one for ry2 becuase there is no routing record in ffpxrefdta, which is where the store is coming from
-- so for each formula account row, store is null

TEMP2
should i eliminate unused accounts
eg those with no routing in ffpxrefdta

Grain: year_month/fxmact/coalesce(account_key) 

select year_month, fxmact, coalesce(account_key, 0) from temp2 group by year_month, fxmact, coalesce(account_key, 0)   having count(*) > 1

select * from temp2 where store_code = 'none' and left(fxmact, 1) NOT in ('<','&','*')

select * from temp2 where account_key is null and store_code is not null limit 2000

select * from dds.ext_ffpxrefdta where factory_account like '&%'

select * from temp2 limit 1000

temp1
  the_year
  year_month
  page
  line
  col
  gm_account

ffpxrefdta
  store_code
  gl_account
  multiplier (multiplier for split accounts)

dim_account
  gl_department
  account_key

select * from temp1 limit 100  
  
drop table if exists temp2;
create temp table temp2 as
-- select b.*, c.store_code, c.g_l_acct_number, c.fact_account_, d.department, d.account_key
select b.*, 
  coalesce(c.store_code, 'none') as store_code, coalesce(c.g_l_acct_number, 'none') as gl_account, 
  coalesce(c.fact_account_, 1) as multiplier, d.department as gl_department, d.account_key
from temp1 b
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from dds.ext_ffpxrefdta a
  where coalesce(a.consolidation_grp, '1') <> '3'
    and a.factory_account <> '331A'
    and a.factory_code = 'GM'
    and factory_financial_year > 2010
    -- excludes the goofy XFCOPY factory accounts
    and g_l_acct_number is not null) c on b.the_year = c.factory_financial_year and b.gm_account = c.factory_account
    -- leave out parts splits for now
    -- parts split is part of the etl process not the data model
--      and g_l_acct_number not like 'SPLT%') c on b.the_year = c.factory_financial_year and b.gm_account = c.factory_account
left join fin.dim_account d on coalesce(c.g_l_acct_number, 'none') = d.account;    
-- eliminate the unused gm accounts, leave the formula accounts
delete from temp2 where store_code = 'none' and left(gm_account, 1) NOT in ('<','&','*');

select * from temp2 where page = 4 and line = 59

select * from temp2 where store_code = 'none'

TEMP3
-- just add the dim_fs_org info, which of course will be null for "formula" accounts
-- grain is still year_month/fxmact/coalesce(account_key)

-- insert into fin.dim_fs_org (market,store,area,department,sub_department)
-- values('none','none','none','none','none');
drop table if exists temp3;
create temp table temp3 as
select b.the_year, b.year_month, b.page, b.line, b.col, b.gm_account, b.store_code,
  b.gl_account, b.multiplier, b.gl_department, b.account_key, 
  coalesce(c.fs_org_key, cc.fs_org_key) as fs_org_key
from temp2 b 
left join fin.dim_fs_org c on b.store_code = c.store
  and
    case 
      when b.gl_department = 'Body Shop' then c.department = 'body shop'
      when b.gl_department = 'Car Wash' then c.sub_Department = 'car wash'
      when b.gl_department = 'Detail' then c.sub_department = 'detail'
      when b.gl_department = 'Finance' and b.line < 10 then c.department = 'finance' and c.sub_department = 'new'
      when b.gl_department = 'Finance' and b.line > 10 then c.department = 'finance' and c.sub_department = 'used'
      when b.gl_department = 'New Vehicle' then c.department = 'sales' and c.sub_department = 'new'
      when b.gl_department = 'Parts' then c.department = 'parts'
      when b.gl_department = 'Quick Lane' then c.sub_department = 'quick lane'
      when b.gl_department = 'Service' then c.sub_department = 'mechanical'
      when b.gl_department = 'Used Vehicle' then c.department = 'sales' and c.sub_department = 'used'
    end
left join fin.dim_fs_org cc on 1 = 1
  and cc.market = 'none';
   
select * from temp3 limit 2000  


drop table if exists may_16;
create temp table may_16 as
select c.*, coalesce(d.amount, 0)
from temp3 c
left join (
  select b.yearmonth, a.account_key, round(sum(a.amount), 0) as amount
  from fin.fact_gl a
  inner join dds.day b on a.date_key = b.datekey
  where a.post_status = 'Y'
    and b.yearmonth = 201605
  group by b.yearmonth, a.account_key) d on c.account_key = d.account_key and c.year_month = d.yearmonth
where c.store_code = 'RY1'
  and c.year_month = 201605
order by c.year_month, c.page, c.line, c.col;

select * from may_16;
select page, line,col from may_16 group by page, line, col having count(*)> 1

select * from may_16 where gl_Account like '%468%'


select flpage,flflne,fxmcol, round(sum(amount), 0) 
from may_16 a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'RY1'
  and b.area = 'fixed'
--  and b.department = 'service'
where flpage = 16 
group by flpage,flflne,fxmcol  
order by flpage,flflne,fxmcol 

select flpage,flflne,fxmcol, round(sum(amount), 0) 
from may_16 a
inner join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
  and b.store = 'RY1'
  and b.area = 'fixed'
  and b.department = 'body shop'
where flpage = 16  
  and flflne between 35 and 42
group by flpage,flflne,fxmcol
order by flpage,flflne,fxmcol

-- hmm do i add rows to dim_account?
select * 
from may_16 a

left join fin.dim_account b on a.g_l_acct_number = b.account
where multiplier <> 1

select * from temp3 where flpage = 1 order by flflne, the_year
select * from temp2 where flpage = 1 order by flflne
select * from temp1 where flpage = 1 order by flflne
select * from dds.ext_eisglobal_sypfflout where flpage = 6 and flflne = 2
select * from dds.ext_eisglobal_sypffxmst where fxmpge = 6 and fxmlne = 2

-- 7/14 -- get the 147701 and parts split shit fixed ------------------------------------------------
select * from may_16 where flpage = 16 and flflne = 49
select * 
from may_16 a
where gl_account = '147701'
union
select * 
from may_16 a
where g_l_acct_number like 'SPLT%'

select * from temp1 where gm_account = '477s'
select * from temp1 where gm_account like '477%'
select * from dds.ext_ffpxrefdta where factory_account like '477%';
select * from fin.dim_account where account in ('147700','147701')
select * from temp2 where gm_account like '677%' order by year_month, gl_account

select * from may_16 where page = 16 and line = 49
select * from temp2 where page = 16 and line = 49 and year_month = 201605 and store_code = 'RY1'

select * from dds.ext_ffpxrefdta where g_l_acct_number = '147701'

select * from temp2 where gl_account in ('147701','167701')

-- this gives me the correct values for p6 l49: 147700 + 147701, 167700 + 167701
-- all parts dept, but fucking chart of accounts shows 147701 as body shop
-- fs shows p6 l49 as acct 477
-- FUCKING PARTS SPLIT
-- 
--   the only place parts split shows up is on page 4 line 59 as an addendum to profit (L61)
-- 
--   on the statement it is not part of gross profit !
-- 
--   gm account 477S is routed to both 147700 and 147701
--  
--   parts split is included in budget as part of gross profit
--   
--   budget gross profit = fs p4 L2 (gross profit) + p4 L59 parts split
--   
--   doc is just weird
-- 
-- so on the statemnt 147701 all goes to parts sales
-- on the budget it all goes to body shop sales
-- on neither is it split
-- 
-- starting to thing it is an etl procedure not a data model attribute
-- ( this means i can dispense with those SPLT... gm_account rows from ffpxrefdta )
-- 
-- so that process needs to:
-- acct 147701/167701 : route to P16/L49/C2-C3
-- 
-- acct 146700/166700 : sum sales + cogs and 1/2 to parts half to mechanical
-- acct 147800/167800 : sum sales + cogs and 1/2 to parts half to quick lane
-- acct 147700/167700 : sum sales + cogs and 1/2 to parts half to body shop
-- and assign those values to P4/L59/C1(mech & ql) P4/L59/C11 (body shop)
-- sypffxmst does not include parts columns for P4/L59
  
bingo  
select year_month, gm_Account, account_key from temp3 group by year_month, gm_account, account_key having count(*)> 1

drop table if exists fin.br_fs_account;
create table fin.br_fs_account (
  fs_account_key serial primary key,
  account_key integer not null references fin.dim_account(account_key),
  gm_account citext not null);
create unique index on fin.br_fs_account (account_key,gm_account);

insert into fin.br_fs_account (account_key, gm_account)
select account_key, gm_account from temp3 group by gm_account, account_key;

create table fin.dim_fs_account(
  fs_account_key integer primary key references fin.br_fs_account(fs_account_key));

insert into fin.dim_fs_account
select fs_account_key from fin.br_fs_account;  
  
select * from temp3 a left join fin.br_fs_account b on a.account_key = b.account_key and a.gm_account = b.gm_account where a.year_month = 201605 order by page,line,col, store_code

-- 7/15 ---- bright and shiny, configure the parts split ( & routing correction) etl and get this shit going

select * from temp3 where page = 16 and line = 49 and year_month = 201605
select * from temp3 where gl_account = '147701' and year_month = 201605

update temp3
set page = 16,
    line = 49,
    col = 2
where gl_account = '147701';    

update temp3
set page = 16,
    line = 49,
    col = 3
where gl_account = '167701';    

drop table if exists may_16;
create temp table may_16 as
select c.*, coalesce(d.amount, 0) as amount
from temp3 c
left join (
  select b.yearmonth, a.account_key, round(sum(a.amount), 0) as amount
  from fin.fact_gl a
  inner join dds.day b on a.date_key = b.datekey
  where a.post_status = 'Y'
    and b.yearmonth = 201605
  group by b.yearmonth, a.account_key) d on c.account_key = d.account_key and c.year_month = d.yearmonth
where c.store_code = 'RY1'
  and c.year_month = 201605
order by c.year_month, c.page, c.line, c.col;

select * from may_16 where page = 16 and line = 49

-- that was the easy part
select * from temp1 where page = 4 and line = 59
select * from temp3 where page = 4 and line = 59
-- ah shit, excluding the gl_accounts like SPLT in the generation of temp2
-- ok, left them in, now just try to fix them?

select * from temp3 limit 100

-- parts split accurate and complete
drop table if exists test1;
create temp table test1 as
select a.*, replace(a.gl_account,'SPLT','') ,
  b.account_key as new_account_key, b.department, 
  c.department as department_1, c.sub_department,
  case
    when b.account in ('147701','167701') then d.amount
    else d.amount * .5 
  end as the_amount
from temp3 a 
left join fin.dim_account b on  replace(a.gl_account,'SPLT','') = b.account
left join fin.dim_fs_org c on a.store_code = c.store
  and
    case
      when a.gm_account in ('467s','667s', '468s','668s' ) then c.sub_department = 'mechanical'
      when a.gm_account in ('478s','678s') then c.sub_department = 'quick lane'
      when a.gm_account in ('477s','677s') then c.department = 'body shop'
      when a.gm_account in ('477s','677s') then c.department = 'body shop'
      when a.gl_account = '246700D' then c.sub_department = 'mechanical' 
      when a.gl_account = '246701D' then c.sub_department = 'quick lane'
    end
left join (
  select b.yearmonth, a.account_key, round(sum(a.amount), 0) as amount
  from fin.fact_gl a
  inner join dds.day b on a.date_key = b.datekey
  where a.post_status = 'Y'
    and b.yearmonth = 201605
  group by b.yearmonth, a.account_key) d on b.account_key = d.account_key and a.year_month = d.yearmonth  
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('246700D','246701D')))
  and a.year_month = 201605
  order by a.store_code, b.account

select * from test1

select store_code, department_1, sub_department, sum(the_amount)
from test1
group by store_code, department_1, sub_department

select * from temp3 limit 100
select distinct the_year, year_month 
from temp3

-- 7/16 -- ok, that works, change it into a temp3 mod


select a.*, replace(a.gl_account,'SPLT','') ,
  b.account_key as new_account_key, b.department, 
  c.department as department_1, c.sub_department
--   case
--     when b.account in ('147701','167701') then d.amount
--     else d.amount * .5 
--   end as the_amount
-- delete from temp3 where page = 4 and line = 59;
-- insert into temp3
drop table if exists temp4;
create temp table temp4 as
select a.the_year, a.year_month, 4 as page, 59 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, a.store_code, replace(a.gl_account,'SPLT','')::citext as gl_account,
  multiplier, b.department as gl_department, b.account_key, c.fs_org_key
-- select *  
from temp3 a 
left join fin.dim_account b on  replace(a.gl_account,'SPLT','') = b.account
left join fin.dim_fs_org c on a.store_code = c.store
  and
    case
      when a.gm_account in ('467S','667S', '468S','668S' ) then c.sub_department = 'mechanical'
      when a.gm_account in ('478s','678s') then c.sub_department = 'quick lane'
      when a.gm_account in ('477s','677s') then c.department = 'body shop'
      when a.gl_account = '246700D' then c.sub_department = 'mechanical' 
      when a.gl_account = '246701D' then c.sub_department = 'quick lane'
    end
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('246700D','246701D')));

delete from temp3 where page = 4 and line = 59;
insert into temp3
select * from temp4;

select * from temp3 where year_month = 201605 and page = 4 and line = 59

drop table if exists may_16;
create temp table may_16 as
select c.*,-- coalesce(d.amount, 0)
--   case
--     when c.gl_account in ('147701','167701') then d.amount
--     else round(d.amount * .5, 0)
--   end as the_amount
  case 
    when page = 4 and line = 59 and c.gl_account not in ('147701','167701') then round(coalesce(d.amount, 0) * multiplier, 0) 
    else coalesce(d.amount, 0)
  end as amount
from temp3 c
left join (
  select b.yearmonth, a.account_key, round(sum(a.amount), 0) as amount
  from fin.fact_gl a
  inner join dds.day b on a.date_key = b.datekey
  where a.post_status = 'Y'
    and b.yearmonth = 201605
  group by b.yearmonth, a.account_key) d on c.account_key = d.account_key and c.year_month = d.yearmonth
--where c.store_code = 'RY1'
where c.year_month = 201605
order by c.year_month, c.page, c.line, c.col;

select store, department, sub_department, sum(amount) 
from may_16 a
left join fin.dim_fs_org b on a.fs_org_key = b.fs_org_key
where page = 4 and line = 59
group by store, department, sub_department

select a.*, b.profit 
from (
  select page, line, col, sum(amount) as amount
  from may_16
  where store_code = 'ry1'
  group by page, line, col) a
left join (
  select page, line, sum(amount) as profit
  from may_16
  where store_code = 'ry1'
  group by page, line) b on a.page = b.page and a.line = b.line
order by a.page, a.line, a.col


select * from may_16 order by page, line, col

close but appear to be goofing up on 147701, current state is it not contributing to parts split, i think
wrong, this morning (7/17) it looks ok, must have been tired

--- 7/17 ----------------------------------------------------------------
-- 8/2 july fs done, let's update all this shit
-- 8/30/16 changed dim_fs_org quick lane to pdq
-- JUST DO THE RELEVANT MONTH
-- 9/2 aug statement done, let's do it

drop table if exists temp1;
create temp table temp1 as
select c.the_year, c.year_month, d.flpage as page, d.flflne as line, 
  d.fxmcol as col, d.fxmact as gm_account
from ( -- year and year_month
  select a.the_year, a.the_year * 100 + the_month as year_month
  from (
    select generate_series(2011, 2016, 1) as the_year) a
  cross join (
    select * from generate_Series(1, 12, 1) as the_month) b) c
left join ( -- pages and lines
  select a.*, b.fxmcol, b.fxmact
  from (
    select flcyy,flpage,flflne
    from dds.ext_eisglobal_sypfflout
    where trim(flcode) = 'GM'
      and flcyy > 2010
      and flflsq = 0
      and flflne > 0
    group by flcyy,flpage,flflne) a
  left join ( -- columns and gm_accounts  
    select fxmcyy, fxmact, fxmpge, fxmlne, fxmcol
    from dds.ext_eisglobal_sypffxmst
    where fxmcyy > 2010) b  on a.flcyy = b.fxmcyy and a.flpage = b.fxmpge and a.flflne = b.fxmlne) d on c.the_year = d.flcyy   
-- WHERE c.year_month = 201608
--   and d.fxmcol is not null; 
where d.fxmcol is not null 
  and c.year_month < 201608

/*  9/6/16 refactoring to accomodate the 684/692 assignment problem */

1. 1 row per year_month,page/line/col/gm_account
can be more than one gm account per page/line/col, eg 16/1/3

select year_month, gm_account from temp1 group by year_month, gm_account having count(*) > 1

select * from temp1 where gm_account is null and year_month = 201608 order by page,line
select * from temp1 
limit 100

select year_month, page, line, col from temp1 where year_month = 201608 group by year_month, page, line, col having count(*) > 1
order by year_month, page, line

select * from temp1 where year_month = 201310 and page = 16 and line between 1 and 10 and col = 3

drop table if exists temp2;
create temp table temp2 as
-- select b.*, c.store_code, c.g_l_acct_number, c.fact_account_, d.department, d.account_key
select b.*, 
  coalesce(c.store_code, 'none') as store_code, coalesce(c.g_l_acct_number, 'none') as gl_account, 
  coalesce(c.fact_account_, 1) as multiplier, d.department as gl_department, d.account_key
from temp1 b
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from dds.ext_ffpxrefdta a
  where coalesce(a.consolidation_grp, '1') <> '3'
    and a.factory_account <> '331A'
    and a.factory_code = 'GM'
    and factory_financial_year > 2010
    -- excludes the goofy XFCOPY factory accounts
    and g_l_acct_number is not null-- ) c on b.the_year = c.factory_financial_year and b.gm_account = c.factory_account
    -- leave out parts splits for now
    -- parts split is part of the etl process not the data model
    and g_l_acct_number not like 'SPLT%') c on b.the_year = c.factory_financial_year and b.gm_account = c.factory_account
left join fin.dim_account d on coalesce(c.g_l_acct_number, 'none') = d.account;    
-- eliminate the unused gm accounts, leave the formula accounts
delete from temp2 where store_code = 'none' and left(gm_account, 1) NOT in ('<','&','*'); 
--
-- dds.ext_ffpxrefdta is not longer being updated
-- ffpxrefdta.py downloads into dds.xfm_fffpxrefdta_tmp and compares it to dds.xfm_ffpxrefdta
-- so, when, like on 9/2/16, routing is changed, xfm_ffpxrefdta will be updated
-- so, this needs to use xfm_ffpxrefdta
-- -- -- drop table if exists temp2;
-- -- -- create temp table temp2 as
-- -- -- -- select b.*, c.store_code, c.g_l_acct_number, c.fact_account_, d.department, d.account_key
-- -- -- select b.*, 
-- -- --   coalesce(c.store_code, 'none') as store_code, coalesce(c.gl_account, 'none') as gl_account, 
-- -- --   coalesce(c.multiplier, 1) as multiplier, d.department as gl_department, d.account_key
-- -- -- from temp1 b
-- -- -- left join dds.xfm_ffpxrefdta c on b.the_year = c.the_year
-- -- --   and b.gm_account = c.gm_account
-- -- -- left join fin.dim_account d on coalesce(c.gl_account, 'none') = d.account;    
-- -- -- -- eliminate the unused gm accounts, leave the formula accounts
-- -- -- delete from temp2 where store_code = 'none' and left(gm_account, 1) NOT in ('<','&','*'); 



drop table if exists temp3;
create temp table temp3 as
select b.the_year, b.year_month, b.page, b.line, b.col, b.gm_account, b.store_code,
  b.gl_account, b.multiplier, b.gl_department, b.account_key, 
  coalesce(c.fs_org_key, cc.fs_org_key) as fs_org_key
from temp2 b 
left join fin.dim_fs_org c on b.store_code = c.store
  and
    case 
      when b.gl_department = 'Body Shop' then c.department = 'body shop'
      when b.gl_department = 'Car Wash' then c.sub_Department = 'car wash'
      when b.gl_department = 'Detail' then c.sub_department = 'detail'
      when b.gl_department = 'Finance' and b.line < 10 then c.department = 'finance' and c.sub_department = 'new'
      when b.gl_department = 'Finance' and b.line > 10 then c.department = 'finance' and c.sub_department = 'used'
      -- next 2 lines  correctly categorize these accounts as finance rather than sales, which is how they are categorized in COA
      when b.gl_department = 'New Vehicle' and b.page = 17 and b.line < 10 then c.department = 'finance' and c.sub_department = 'new'
      when b.gl_department = 'Used Vehicle' and b.page = 17 and b.line between 11 and 19 then c.department = 'finance' and c.sub_department = 'used'      
      when b.gl_department = 'New Vehicle' then c.department = 'sales' and c.sub_department = 'new'
      when b.gl_department = 'Parts' then c.department = 'parts'
      when b.gl_department = 'Quick Lane' then c.sub_department = 'pdq'
      when b.gl_department = 'Service' then c.sub_department = 'mechanical'
      when b.gl_department = 'Used Vehicle' then c.department = 'sales' and c.sub_department = 'used'
    end
left join fin.dim_fs_org cc on 1 = 1
  and cc.market = 'none';

update temp3
set page = 16,
    line = 49,
    col = 2
where gl_account = '147701';    

update temp3
set page = 16,
    line = 49,
    col = 3
where gl_account = '167701';       

drop table if exists temp4;
create temp table temp4 as
select a.the_year, a.year_month, 4 as page, 59 as line, 
  case 
    when a.gm_account in ('477s','677s') then 11 -- body shop
    else 1
  end as col, 
  a.gm_account, a.store_code, replace(a.gl_account,'SPLT','')::citext as gl_account,
  multiplier, b.department as gl_department, b.account_key, c.fs_org_key
-- select *  
from temp3 a 
left join fin.dim_account b on  replace(a.gl_account,'SPLT','') = b.account
left join fin.dim_fs_org c on a.store_code = c.store
  and
    case
      when a.gm_account in ('467S','667S', '468S','668S' ) then c.sub_department = 'mechanical'
      when a.gm_account in ('478s','678s') then c.sub_department = 'pdq'
      when a.gm_account in ('477s','677s') then c.department = 'body shop'
      when a.gl_account = '246700D' then c.sub_department = 'mechanical' 
      when a.gl_account = '246701D' then c.sub_department = 'pdq'
    end
where ((a.gm_account in('467s','667s','478s','678s','477s','677s','468s','668s'))
  or (a.gl_account in ('246700D','246701D')));

delete from temp3 where page = 4 and line = 59;
insert into temp3
select * from temp4;

/* 9/6/16 figure out how to update this shit better */
first issue: br_fs_account: needs to be year specific
in fact thinking the whole bridge concept here is superfluous
it is simply a dimension dim_fs_account
comprised of a gm_Account, gl_account
select * from temp3 limit 1000

select * from temp3 where gm_account in ('684','692') and store_code = 'ry1' and year_month in (201212, 201607)

/*****************************************************************************************************/
 -----------------------------------------------------------------------------------------------------
/*****************************************************************************************************/
8/26/16 need to generate report card data
have done the july update: ran all the above (temp1 - temp4)
lets see if temp3 looks ok
-- ry2 p3 line5 off
-- new acct 21310C is $50 off:: duh, forgot voids

-- ry1 p3 line 32 $500 hi both new and used :: 8/28 fixed: updated fact_gl with retroactive voids
--             44 no values in query

select a.*, b.col, b.amount
from (
  select page, line, col, sum(amount) as amount
  from temp3 a
  inner join (
    select account_key, sum(amount)::integer as amount
    from fin.fact_gl a
    inner join dds.day b on a.date_key = b.datekey
      and b.yearmonth = 201608
    where a.post_status = 'Y'
    group by account_key) c on a.account_key = c.account_key
  where a.year_month = 201608
    and a.page = 8
    and a.store_code = 'ry1'
    and a.col = 1
  group by page, line, col) a
left join (
  select page, line, col, sum(amount) as amount
  from temp3 a
  inner join (
    select account_key, sum(amount)::integer as amount
    from fin.fact_gl a
    inner join dds.day b on a.date_key = b.datekey
      and b.yearmonth = 201608
    where a.post_status = 'Y'
    group by account_key) c on a.account_key = c.account_key
  where a.year_month = 201608
    and a.page = 8
    and a.store_code = 'ry1'
    and a.col <> 1
  group by page, line, col) b on a.line = b.line 
order by page,line


-- might as well generate the account amounts i am going to need

drop table if exists august;
create temp table august as
select c.account, a.account_key, sum(a.amount)::integer as amount
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth = 201608
inner join fin.dim_account c on a.account_key = c.account_key  
where post_status = 'Y'
group by c.account, a.account_key

select * from august where account like '1401%'

/*****************************************************************************************************/
 -----------------------------------------------------------------------------------------------------
/*****************************************************************************************************/  

9/2 no need to reconstruct the whole thing, in fact it is a bad idea, surrogate keys change ?!?!?
just new or changed rows
drop table if exists fin.br_fs_account cascade;
create table fin.br_fs_account (
  fs_account_key serial primary key,
  account_key integer not null references fin.dim_account(account_key),
  gm_account citext not null);
create unique index on fin.br_fs_account (account_key,gm_account);

insert into fin.br_fs_account (account_key, gm_account)
select account_key, gm_account 
from temp3 
where account_key is not null 
group by gm_account, account_key;

-- new rows
select account_key, gl_account, gm_account 
from temp3 
where account_key is not null
group by account_key, gl_account, gm_account 
except 
select a.account_key, b.account, a.gm_account 
from fin.br_fs_account a
inner join fin.dim_account b on a.account_key = b.account_key

union 
-- changes holy shit 
select a.account_key, b.account, a.gm_account 
from fin.br_fs_account a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account <> 'none'
except
select account_key, gl_account, gm_account 
from temp3 
where account_key is not null
group by account_key, gl_account, gm_account 


select * from temp3 where account_key = 907
select * from fin.dim_Account where account_key = 907

-- holy shit, here is the fucking problem
select * from fin.br_fs_account where account_key = 907



-- don't need this
-- drop table if exists fin.dim_fs_account cascade;
-- create table fin.dim_fs_account(
--   fs_account_key integer primary key references fin.br_fs_account(fs_account_key));
-- 
-- insert into fin.dim_fs_account
-- select fs_account_key from fin.br_fs_account;  

-- 8/28 oops, no need to fucking recreate the entire fucking table, which i mistakenly did
-- just insert the values for the new month
-- -- -- drop table if exists fin.fact_fs cascade;
-- -- -- create table fin.fact_fs (
-- -- --   year_month integer not null,
-- -- --   page integer not null,
-- -- --   line numeric(4,1) not null,
-- -- --   col integer not null,
-- -- --   fs_org_key integer not null references fin.dim_fs_org(fs_org_key),
-- -- --   fs_account_key integer not null references fin.dim_fs_account(fs_account_key),
-- -- --   amount integer);
-- -- -- create index on fin.fact_fs(year_month);  
-- -- -- create index on fin.fact_fs(page);
-- -- -- create index on fin.fact_fs(line);
-- -- -- create index on fin.fact_fs(fs_org_key);
-- -- -- create index on fin.fact_fs(fs_account_key);

insert into fin.fact_fs
-- drop table if exists fs;
-- create temp table fs as
select a.year_month, a.page, a.line, a.col, a.fs_org_key, 
  b.fs_account_key,
  round( -- this is the parts split
    case 
      when a.page = 4 and a.line = 59 and a.gl_account not in ('147701','167701') then round(coalesce(c.amount, 0) * multiplier, 0) 
      else coalesce(c.amount, 0)
    end, 0) as amount  
from temp3 a
left join fin.br_fs_account b on a.gm_account = b.gm_account
  and a.account_key = b.account_key
left join (
  select b.yearmonth, a.account_key, sum(a.amount) as amount
  from fin.fact_gl a
  inner join dds.day b on a.date_key = b.datekey  
  where a.post_status = 'Y'
  group by b.yearmonth, a.account_key) c on b.account_key = c.account_key and a.year_month = c.yearmonth
where gl_account not like 'SPLT%'  ; 

 

select col, sum(amount)
-- select *
from fin.fact_fs
where year_month = 201606
and page = 4
and line = 59
group by col

