﻿/*
1/26/17
  i have been making the from_date the first of the month for any new account
*/  
10/4/16
pussied out on autmating change processing for dim_account
so do it here

-- differences

select account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
except
select account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account



select 'xfm' as source, account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
where account in ('126105','231201','16603')
union 
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where account in ('126105','231201','16603')
order by account

-- new account

insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.department,
  a.typical_balance, 
  true, '09/12/2016', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
left join dds.xfm_glpdept b on a.department_code = b.department_code
where a.account = '16603';


-- new account
-- 12/16/16 285003
-- glpdept now arkona.ext_glpdept
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '12/15/2016', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
-- left join dds.xfm_glpdept b on a.department_code = b.department_code
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
where a.account = '285003';


-- changed accounts
select 'xfm' as source, account, account_type, description, store_code, department_code, typical_balance
from arkona.xfm_glpmast  
where account in ('126105','231201')
union 
select 'dim', account, account_type_code, description, store_code, department_code, typical_balance
from fin.dim_account
where account in ('126105','231201')
order by account

select *
from fin.dim_fs_account
where gl_Account = '231201'

select *
from fin.fact_fs a
inner join fin.dim_Fs b on a.fs_key = b.fs_key
where fs_Account_key in (216,3352)

select *
from fin.dim_account
where account = '126105'


-- update old row
update fin.dim_account
  set current_row = false,
      thru_date = '08/31/2016'      
where account = '126105';
-- new row
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.department,
  a.typical_balance, 
  true, '09/01/2016', '12/31/9999', 'description changed'
from arkona.xfm_glpmast a -- new rows in glpmast
left join dds.xfm_glpdept b on a.department_code = b.department_code
where a.account = '126105';


-- new account
-- 12/30/16 130700, 230700
-- glpdept now arkona.ext_glpdept
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '12/30/2016', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
-- left join dds.xfm_glpdept b on a.department_code = b.department_code
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
where a.account = '230700';



-- new account
-- 1/13/17 180601, 180801
-- glpdept now arkona.ext_glpdept
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '01/01/2017', '12/31/9999', 'new account created by controller'
from arkona.xfm_glpmast a -- new rows in glpmast
-- left join dds.xfm_glpdept b on a.department_code = b.department_code
left join arkona.ext_glpdept b on a.department_code = b.department_code
  and a.store_code = b.company_number
where a.account in ('180601','180801');


-- 1/25/17
-- new accounts 25104D,I,T,R,M,F,P
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  row_from_date, row_thru_date, row_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.dept_description,
  a.typical_balance, 
  true, '01/01/2017', '12/31/9999', 'new account created by controller'
from (
  select account, account_type, description, store_code, department_code, typical_balance
  from arkona.xfm_glpmast
  except
  select account, account_type_code, description, store_code, department_code, typical_balance
  from fin.dim_account) a
 left join arkona.ext_glpdept b on a.department_code = b.department_code
   and a.store_code = b.company_number;