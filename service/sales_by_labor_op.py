# coding=utf-8
import db_cnx

pg_con = None
file_name = 'files/sales_by_labor_op.csv'

try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.sales_by_labor_op")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.sales_by_labor_op from stdin with csv header
                    encoding 'latin-1 '""", io)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
finally:
    if pg_con:
        pg_con.close()