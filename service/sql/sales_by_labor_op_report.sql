﻿create table dds.sales_by_labor_op (
  service_writer citext,
  ro citext,
  labor_op citext,
  tech_number citext,
  close_date date,
  payment_method citext,
  vin citext,
  labor numeric(6,2),
  labor_gross numeric(6,2),
  parts numeric(6,2),   
  parts_gross numeric(6,2),
  sublet numeric(6,2),
  sublet_gross numeric(6,2),
  sp citext);

select *
from dds.sales_by_labor_op
where vin = 'shop'
order by tech_number



select tech_number, sum(labor_gross) as labor_gross,
  string_agg(ro, ',')
-- select  sum(labor_gross)  
from dds.sales_by_labor_op
where vin = 'shop'
group by tech_number
order by tech_number
  