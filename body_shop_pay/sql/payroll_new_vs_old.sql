﻿do
$$
/*
evavold: 316.66
ostlund:  45.86
hill: 37.85
lueker: 236.83
steinke: 103.40
yem: 49.24


*/
declare 
  _user_name citext := 'aostlund@rydellcars.com';
  _pay_period_indicator integer := -1;
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
--   _from_date date := (
--     select distinct biweekly_pay_period_start_date
--     from dds.dim_date
--     where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
--   _thru_date date := (
--     select distinct biweekly_pay_period_end_date
--     from dds.dim_date
--     where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator); 
--  
-- total pay difference, excluding first pay period (which was fucked up because alt_writer assumed to be team mate)
  _from_date date := '03/05/2017';
  _thru_date date := '04/01/2017'; 
begin
    drop table if exists old_way;
    create temp table old_way as
    select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
    from ( -- x all the individual's pay data elements:  single row
        select b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate, 
          a.pto_rate,
          a.commission_perc as commission_percentage, 
          coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
          coalesce(c.overtime_hours, 0) as overtime_hours,
          coalesce(c.pto_hours, 0) as pto_hours,
          coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
          coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
          coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
          coalesce(d.team_parts_and_labor_sales, 0) as team_parts_and_labor_sales,
          coalesce(d.team_parts_total, 0) as team_parts_total,
          coalesce(d.team_labor_total, 0) as team_labor_total,
          coalesce(round(a.commission_perc * d.team_parts_and_labor_sales, 2), 0) as commission_pay,
          case
            when c.regular_clock_hours + c.overtime_hours = 0 then 0
            else coalesce(round((.5 * c.overtime_hours * 
              (a.commission_perc * d.team_parts_and_labor_sales))/(c.regular_clock_hours + c.overtime_hours), 2), 0)
           end as ot_variance
        from bspp.personnel a
        inner join bspp.personnel_role b on a.user_name = b.user_name
        LEFT join ( -- users clock hours
          select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
            sum(pto_hours + hol_hours) as pto_hours
          from bspp.clock_hours
          where the_date between _from_date and _thru_date
          group by user_name) c on a.user_name = c.user_name
        left join ( -- total team sales for team & pay period
          select sum(total_sales) as team_parts_and_labor_sales, sum(total_parts) as team_parts_total,
            sum(labor) as team_labor_total
          from bspp.sales a
          inner join bspp.personnel b on (a.writer_id = b.writer_id
            or a.alt_writer_id = b.writer_id)
            and b.user_name = _user_name
          where the_date between _from_date and _thru_date) d on 1 = 1
        where a.user_name = _user_name) x;

    drop table if exists new_way;
    create temp table new_way as
    select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay 
    from ( -- x all the individual's pay data elements:  single row
        select b.base_pay_value as hourly_rate, round(1.5 * b.base_pay_value, 2) as overtime_rate, 
          a.pto_rate,
          a.commission_perc as commission_percentage, 
          coalesce(c.regular_clock_hours, 0) as regular_clock_hours,
          coalesce(c.overtime_hours, 0) as overtime_hours,
          coalesce(c.pto_hours, 0) as pto_hours,
          coalesce(round(c.regular_clock_hours * b.base_pay_value, 2), 0) as regular_pay,
          coalesce(round(c.overtime_hours * 1.5 * b.base_pay_value, 2), 0) as overtime_pay,
          coalesce(round(a.pto_rate * c.pto_hours, 2), 0) as pto_pay,
          coalesce(d.team_parts_and_labor_sales, 0) as team_parts_and_labor_sales,
          coalesce(d.team_parts_total, 0) as team_parts_total,
          coalesce(d.team_labor_total, 0) as team_labor_total,
          coalesce(round(a.commission_perc * d.team_parts_and_labor_sales, 2), 0) as commission_pay,
          case
            when c.regular_clock_hours + c.overtime_hours = 0 then 0
            else coalesce(round((.5 * c.overtime_hours * 
              (a.commission_perc * d.team_parts_and_labor_sales))/(c.regular_clock_hours + c.overtime_hours), 2), 0)
           end as ot_variance
        from bspp.personnel a
        inner join bspp.personnel_role b on a.user_name = b.user_name
        LEFT join ( -- users clock hours
          select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
            sum(pto_hours + hol_hours) as pto_hours
          from bspp.clock_hours
          where the_date between _from_date and _thru_date
          group by user_name) c on a.user_name = c.user_name
        left join ( -- total team sales for team & pay period
          select sum(total_sales) as team_parts_and_labor_sales, sum(total_parts) as team_parts_total,
            sum(labor) as team_labor_total
          from bspp.writer_sales a
          inner join bspp.personnel b on (a.writer_id = b.writer_id
            or a.alt_writer_id = b.writer_id)
            and b.user_name = _user_name
          where the_date between _from_date and _thru_date) d on 1 = 1
        where a.user_name = _user_name) x;        
end
$$;
-- select 'old', a.* from old_way a       
-- union        
-- select 'new', a.* from new_way a;

select (select total_pay from new_way) - (select total_pay from old_way)



