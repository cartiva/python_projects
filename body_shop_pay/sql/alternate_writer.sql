﻿-- individual stats
-- add pto, total & string_agg dates
drop table if exists step_1;
create temp table step_1 as
select a.user_name, a.first_name, a.last_name, a.employee_number, a.writer_id,
  c.the_role, a.commission_perc, b.base_pay_value, d.team_name,
  e.reg_hours, e.ot_hours, 
  f.parts, f.labor, f.total_sales, 
  e.pto_hours, g.pto_dates
from bspp.personnel a
inner join bspp.personnel_role b on a.user_name = b.user_name
inner join bspp.roles c on b.the_role = c.the_role
  and c.the_role in ('vda','csr')
inner join bspp.teams d on a.user_name = d.vda_user_name or a.user_name = d.csr_user_name
inner join (
  select user_name, sum(ot_hours) as ot_hours, sum(reg_hours) as reg_hours, 
    sum(pto_hours) as pto_hours
  from bspp.clock_hours
  where the_date between '02/19/2017' and current_date
  group by user_name) e on a.user_name = e.user_name
inner join (
  select writer_id, sum(parts) as parts, sum(labor) as labor, 
    sum(parts + labor) as total_sales
  from bspp.sales
  where the_date between '02/19/2017' and current_date
  group by writer_id) f on a.writer_id = f.writer_id
left join (
  select user_name, string_agg(to_char(the_date, 'Mon DD'), ',' order by the_date) as pto_dates
  from bspp.clock_hours
  where pto_hours + hol_hours <> 0
    AND the_date between '02/19/2017' and current_date
  group by user_name) g on a.user_name = g.user_name
order by d.team_name, c.the_role;

select * from step_1

drop table ads.ext_dim_opcode;
create TABLE ads.ext_dim_opcode(
    OpCodeKey serial,
    StoreCode citext NOT NULL,
    OpCode citext NOT NULL,
    Description citext NOT NULL,
    PDQcat1 citext NOT NULL DEFAULT 'N/A',
    PDQcat2 citext NOT NULL DEFAULT 'N/A',
    PDQcat3 citext DEFAULT 'N/A',
    PRIMARY KEY (OpCodeKey)) WITH (OIDS=FALSE);

alter table bspp.sales_tmp
add column alt_writer_id citext default 'none';

alter table bspp.sales
add column alt_writer_id citext default 'none';

-- this looks ok
-- ros for a pay period with an alternate writer
drop table if exists step_2;
create temp table step_2 as
select * 
from (
  select n.writernumber, m.the_date, m.control, m.parts, m.labor, m.parts + m.labor as total_sales, --, coalesce(alt_writer_id, 'none')
      coalesce ((
        select 
          case opcodekey
            when 26302 then '312'
            when 26303 then '311'
            when 26304 then '303'
            when 26305 then '317'
            when 26306 then '712'
            when 26307 then '403'
          end as alt_writer_id
        from ads.ext_fact_repair_order
        where ro = m.control
          and opcodekey between 26302 and 26307
          and ro <> '18049420'), 'none') as alt_writer_id
  from ( -- m
    select (
      select distinct servicewriterkey
      from ads.ext_fact_repair_order
      where ro = a.control),
      e.the_date, a.control,
      sum(case when c.category = 'parts' then -1 * a.amount else 0 end) as parts,
      sum(case when c.category = 'labor' then -1 * a.amount else 0 end) as labor
    from fin.fact_gl a
    inner join fin.dim_account b on a.account_key = b.account_key
    inner join bspp.sale_accounts c on b.account = c.account
    inner join dds.dim_date e on a.date_key = e.date_key
      and e.biweekly_pay_period_sequence = (
        select biweekly_pay_period_sequence
        from dds.dim_date
        where the_date = current_date)
    where a.post_status = 'Y'
    group by servicewriterkey, e.the_date, a.control) m
  left join ads.ext_dim_service_writer n on m.servicewriterkey = n.servicewriterkey) x where alt_writer_id <> 'none'


select * from step_2 order by control
-- writer & alt writer w/ dough
select a.*, b.last_name as writer, b.the_role as writer_role, c.last_name as alt_writer, 
  c.the_role as alt_writer_role
from step_2 a
left join step_1 b on a.writernumber = b.writer_id
left join step_1 c on a.alt_writer_id = c.writer_id

-- total dough in alt_writers --$83,755.61
select sum(parts + labor) from step_2

select * from step_1

-- writer & alt writer
select a.the_date, a.control, b.last_name as writer, b.team_name, 
  b.the_role as writer_role, c.last_name as alt_writer, c.the_role as alt_writer_role
from step_2 a
left join step_1 b on a.writernumber = b.writer_id
left join step_1 c on a.alt_writer_id = c.writer_id
order by a.control


-- ros where a writer had pto time in the ro interval
select g.ro, g.open_date, g.acct_date, g.writer_id, h.last_name,
  sum(h.pto_hours) as pto_hours, string_agg(to_char(h.pto_date, 'Mon DD'), ',' order by pto_date) as pto_dates
from ( -- g: ro open dates on sales ros
  select a.ro, b.the_date as open_date, e.the_date as acct_date, d.writer_id
  from ads.ext_Fact_repair_order a
  inner join dds.dim_date b on a.opendatekey = b.date_key
  --   and b.the_date between '02/19/2017' and current_date
  inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
  inner join bspp.personnel d on c.writernumber = d.writer_id
    inner join (
    select  *
    from bspp.sales
    where the_date between '02/19/2017' and current_date) e on a.ro = e.ro
  group by a.ro, b.the_date, e.the_date, d.writer_id) g
left join ( -- h: pto info
  select b.writer_id, b.last_name, a.the_date as pto_date, a.pto_hours + a.hol_hours as pto_hours
  from bspp.clock_hours a
  inner join bspp.personnel b on a.user_name = b.user_name
  where pto_hours + hol_hours <> 0) h on g.writer_id = h.writer_id
    and h.pto_date between g.open_date and g.acct_date
group by g.ro, g.open_date, g.acct_date, g.writer_id, h.last_name    
order by g.ro    

-- ros with alternate writers and ros with pto time for writer
select *
from ( -- k: ros with alt writers
  select a.the_date, a.control, b.last_name as writer, b.team_name, 
    b.the_role as writer_role, c.last_name as alt_writer, c.the_role as alt_writer_role
  from step_2 a
  left join step_1 b on a.writernumber = b.writer_id
  left join step_1 c on a.alt_writer_id = c.writer_id) k
left join (-- l: ros where a writer had pto time in the ro interval
  select g.ro, g.open_date, g.acct_date, g.writer_id, h.last_name,
    sum(h.pto_hours) as pto_hours, string_agg(to_char(h.pto_date, 'Mon DD'), ',' order by pto_date) as pto_dates
  from ( -- g: ro open dates on sales ros
    select a.ro, b.the_date as open_date, e.the_date as acct_date, d.writer_id
    from ads.ext_Fact_repair_order a
    inner join dds.dim_date b on a.opendatekey = b.date_key
    --   and b.the_date between '02/19/2017' and current_date
    inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
    inner join bspp.personnel d on c.writernumber = d.writer_id
      inner join (
      select  *
      from bspp.sales
      where the_date between '02/19/2017' and current_date) e on a.ro = e.ro
    group by a.ro, b.the_date, e.the_date, d.writer_id) g
  left join ( -- h: pto info
    select b.writer_id, b.last_name, a.the_date as pto_date, a.pto_hours + a.hol_hours as pto_hours
    from bspp.clock_hours a
    inner join bspp.personnel b on a.user_name = b.user_name
    where pto_hours + hol_hours <> 0) h on g.writer_id = h.writer_id
      and h.pto_date between g.open_date and g.acct_date
  group by g.ro, g.open_date, g.acct_date, g.writer_id, h.last_name) l on k.control = l.ro


select *
from bspp.sales a
where a.the_date between '02/19/2017' and current_date
order by a.ro


