﻿/*
create or replace function bspp.get_pay_periods()
returns setof text as
$BODY$
declare
  _thru_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_pay_period_seq integer := _thru_pay_period_seq - 7; 
begin  
return query
select '[' || row_to_json(x) || ']'
from (
  select distinct to_char(biweekly_pay_period_start_date, 'Mon') 
    || ' ' || to_char(biweekly_pay_period_start_date, 'DD')
    || ' - ' 
    || to_char(biweekly_pay_period_end_date, 'Mon') 
    || ' ' || to_char(biweekly_pay_period_end_date, 'DD')
    || ', ' || to_char(biweekly_pay_period_end_date, 'YYYY') as pay_period_display,
    biweekly_pay_period_sequence - _thru_pay_period_seq as pay_period_indicator
  from dds.dim_date 
  where biweekly_pay_period_sequence between _from_pay_period_seq and _thru_pay_period_seq
    and the_date > '02/04/2017'
  order by biweekly_pay_period_sequence - _thru_pay_period_seq desc) x;
end 
$BODY$
language plpgsql;  


select * from bspp.get_pay_periods()
*/
/*
-- using anonymous block to develop bspp.get_estimator_pay_summary(citext,integer)
do 
$$
declare _user_name citext := 'aostlund@rydellcars.com';
declare _team citext := (
  select team_name
  from bspp.teams
  where writer_1 = (select writer_id from bspp.personnel where user_name = _user_name)
    or writer_2 = (select writer_id from bspp.personnel where user_name = _user_name)); 
begin
drop table if exists test;
create temp table test as
select row_to_json (z)
from (
  select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay
  from (
    select b.base_pay_value as hourly_rate, round(.5 * b.base_pay_value, 2) as overtime_rate, 
      a.pto_rate,
      round(100 * a.commission_perc, 2) as commission_percentage, c.regular_clock_hours,
      c.overtime_hours, c.pto_hours,
      round(c.regular_clock_hours * b.base_pay_value, 2) as regular_pay,
      round(c.overtime_hours * 1.5 * b.base_pay_value, 2) as overtime_pay,
      round(a.pto_rate * c.pto_hours, 2) as pto_pay,
      d.team_parts_and_labor_sales,
      round(a.commission_perc * d.team_parts_and_labor_sales, 2) as commission_pay,
      round((.5 * c.overtime_hours * 
        (a.commission_perc * d.team_parts_and_labor_sales))/(c.regular_clock_hours + c.overtime_hours), 2) as ot_variance
    from bspp.personnel a
    inner join bspp.personnel_role b on a.user_name = b.user_name
    inner join (
      select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
        sum(pto_hours + hol_hours) as pto_hours
      from bspp.clock_hours
      where the_date between '02/05/2017' and '02/18/2017'
      group by user_name) c on a.user_name = c.user_name
    inner join (
      select sum(parts + labor) as team_parts_and_labor_sales
      from bspp.sales a
      inner join bspp.teams b on a.writer_id in (b.writer_1, b.writer_2)
        and b.team_name = _team
      where the_date between '02/05/2017' and '02/18/2017') d on 1 = 1
    where a.user_name = 'aostlund@rydellcars.com') x) z;

end
$$;

select * from test;
*/ 

select * from bspp.get_estimator_pay_summary('aostlund@rydellcars.com',0)

/*
drop function if exists bspp.get_estimator_pay_summary(citext,integer);
create or replace function bspp.get_estimator_pay_summary(
  _user_name citext,
  _pay_period_indicator integer)
returns setof json as  

$BODY$
declare
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date date := (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);  
  _team citext := (
    select team_name
    from bspp.teams
    where writer_1 = (select writer_id from bspp.personnel where user_name = _user_name)
      or writer_2 = (select writer_id from bspp.personnel where user_name = _user_name));     
begin  
return query
select row_to_json (z)
from (
  select x.*, regular_pay + overtime_pay + pto_pay + commission_pay + ot_variance as total_pay
  from (
    select b.base_pay_value as hourly_rate, round(.5 * b.base_pay_value, 2) as overtime_rate, 
      a.pto_rate,
      round(100 * a.commission_perc, 2) as commission_percentage, c.regular_clock_hours,
      c.overtime_hours, c.pto_hours,
      round(c.regular_clock_hours * b.base_pay_value, 2) as regular_pay,
      round(c.overtime_hours * 1.5 * b.base_pay_value, 2) as overtime_pay,
      round(a.pto_rate * c.pto_hours, 2) as pto_pay,
      d.team_parts_and_labor_sales,
      round(a.commission_perc * d.team_parts_and_labor_sales, 2) as commission_pay,
      round((.5 * c.overtime_hours * 
        (a.commission_perc * d.team_parts_and_labor_sales))/(c.regular_clock_hours + c.overtime_hours), 2) as ot_variance
    from bspp.personnel a
    inner join bspp.personnel_role b on a.user_name = b.user_name
    inner join (
      select user_name, sum(reg_hours) as regular_clock_hours, sum(ot_hours) as overtime_hours,
        sum(pto_hours + hol_hours) as pto_hours
      from bspp.clock_hours
      where the_date between _from_date and _thru_date
      group by user_name) c on a.user_name = c.user_name
    inner join (
      select sum(parts + labor) as team_parts_and_labor_sales
      from bspp.sales a
      inner join bspp.teams b on a.writer_id in (b.writer_1, b.writer_2)
        and b.team_name = _team
      where the_date between _from_date and _thru_date) d on 1 = 1
    where a.user_name = _user_name) x) z;
end 
$BODY$
language plpgsql;  

-- select * from bspp.get_estimator_pay_summary('aostlund@rydellcars.com', 0);
*/

-- anonymous block format: 
-- do 
-- $$
-- declare _user_name citext := 'aostlund@rydellcars.com';
-- begin
-- drop table if exists test;
-- create temp table test as
-- select row_to_json (z)
-- ...
-- where a.user_name = 'aostlund@rydellcars.com') x) z;
-- end
-- $$;



drop function if exists bspp.get_estimator_pay_break_down(citext,integer);
create or replace function bspp.get_estimator_pay_break_down(
  _user_name citext,
  _pay_period_indicator integer)
returns setof json as  
$BODY$
declare
  _current_pay_period_seq integer := (
    select distinct biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date);
  _from_date date := (
    select distinct biweekly_pay_period_start_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);
  _thru_date date := (
    select distinct biweekly_pay_period_end_date
    from dds.dim_date
    where biweekly_pay_period_sequence = _current_pay_period_seq + _pay_period_indicator);  
  _team citext := (
    select team_name
    from bspp.teams
    where writer_1 = (select writer_id from bspp.personnel where user_name = _user_name)
      or writer_2 = (select writer_id from bspp.personnel where user_name = _user_name));     
begin  
return query
select row_to_json(g)
from ( 
  select e.*,
    ( -- ros: array of ro objects
      select coalesce(array_to_json(array_agg(row_to_json(x))), '[]')
      from ( -- x one row per row
        select a.ro, sum(a.parts) as parts, sum(a.labor) as labor, sum(a.parts + labor) as total
        from bspp.sales a
        inner join bspp.teams b on a.writer_id in (b.writer_1, b.writer_2)
          and b.team_name = _team
        where the_date between _from_date and _thru_date
        group by a.ro) x) as ros  
      
  from ( -- e: writer info
    select b.base_pay_value as hourly_rate, round(.5 * b.base_pay_value, 2) as overtime_rate, 
      a.pto_rate,
      round(100 * a.commission_perc, 2) as commission_percentage    
    from bspp.personnel a
    inner join bspp.personnel_role b on a.user_name = b.user_name
    where a.user_name = _user_name) e) g;
end 
$BODY$
language plpgsql;  


-- select * from bspp.get_estimator_pay_break_down('aostlund@rydellcars.com', 0);