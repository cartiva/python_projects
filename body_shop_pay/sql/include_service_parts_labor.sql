﻿-- all transactions for the current pay period
-- all transactions for the current pay period
-- start off retroactive, pay plan started on 2/19/17
drop table if exists pay_period_transactions;
create temp table pay_period_transactions as
select a.control
from fin.fact_gl a
inner join dds.dim_date e on a.date_key = e.date_key
  and e.the_date between '02/19/2017' and current_date
where a.post_status = 'Y'
group by a.control;

-- hmm include date, account (account_type: sale)
-- 
-- drop table if exists pay_period_transactions;
-- create temp table pay_period_transactions as

insert into bspp.pay_period_transactions
select e.the_date, a.control, b.account, b.department, b.description, sum(a.amount) as amount
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and b.account_type = 'sale'
  and b.current_row = true
inner join dds.dim_date e on a.date_key = e.date_key
  and e.biweekly_pay_period_sequence = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date -1)
where a.post_status = 'Y'
group by a.control, b.account, e.the_date, b.description, b.department;

create table bspp.pay_period_transactions (
  the_date date not null,
  control citext not null,
  account citext not null,
  department citext not null,
  description citext not null,
  amount numeric(8,2),
  constraint pay_period_transactions_pkey primary key(the_date,control,account));

select max(the_date) from bspp.pay_period_transactions limit 10

-- -- which of them are the relevant ros based on writer
-- drop table if exists writer_transactions;
-- create temp table writer_transactions as
-- select d.ro, d.writernumber writer_id, 
--   e.account, e.the_date, sum(e.amount) as amount,
--   e.description, e.department, 
--   case 
--     when f.category is not null then f.category
--     when e.department = 'parts' then 'parts'
--     else 'labor'
--   end as category, 
--   case 
--     when f.multiplier is not null then f.multiplier
--     when e.department = 'parts' and e.description not like '%NON-GM%' then 0.5
--     else 1.0
--   end as multiplier
-- from (
--   select a.ro, c.writernumber
--   from ads.ext_fact_repair_order  a
--   inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
--     and c.writernumber in ('312','311','303','317','712','403')
--   group by a.ro, c.writernumber) d
-- inner join pay_period_transactions e on d.ro = e.control
-- left join bspp.sale_accounts f on e.account = f.account
-- group by d.ro, d.writernumber, e.account, e.the_date, e.department, 
--   f.category, e.description, f.multiplier;


-- which of them are the relevant ros based on writer
-- include alt_writer
-- this looks good
drop table if exists writer_transactions;
create temp table writer_transactions as
select d.ro, d.writer_id, alt_writer_id,
  e.account, e.the_date, sum(e.amount) as amount,
  e.description, e.department, 
  case 
    when f.category is not null then f.category
    when e.department = 'parts' then 'parts'
    else 'labor'
  end as category, 
  case 
    when f.multiplier is not null then f.multiplier
    when e.department = 'parts' and e.description not like '%NON-GM%' then 0.5
    else 1.0
  end as multiplier
from (
select m.ro, m.writernumber as writer_id, coalesce(n.alt_writer_id, 'none') as alt_writer_id
from ( -- writer and alt_writer where bs est is writer
    select a.ro, c.writernumber
    from ads.ext_fact_repair_order  a
    inner join ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey 
      and c.writernumber in ('312','311','303','317','712','403')
    left join ads.ext_dim_opcode d on a.opcodekey = d.opcodekey
      and d.opcodekey BETWEEN 26302 AND 26307
    group by a.ro, c.writernumber) m
  left join ( -- alt_writer where bs est is writer
    select a.ro, 
      case a.opcodekey
        when 26302 then '312'
        when 26303 then '311'
        when 26304 then '303'
        when 26305 then '317'
        when 26306 then '712'
        when 26307 then '403'
        else 'none'
      end as alt_writer_id
    from ads.ext_fact_repair_order  a
--     inner join pay_period_transactions b on a.ro = b.control
    inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
      and c.opcodekey between 26302 and 26307
    where a.servicewriterkey in (
      select servicewriterkey
      from ads.ext_dim_service_Writer
      where  writernumber in ('312','311','303','317','712','403'))
    and ro <> '18049420'  
    group by a.ro, a.opcodekey) n on m.ro = n.ro) d
inner join pay_period_transactions e on d.ro = e.control
left join bspp.sale_accounts f on e.account = f.account
group by d.ro, d.writer_id, d.alt_writer_id, e.account, e.the_date, e.department, 
  f.category, e.description, f.multiplier;


select a.ro, b.writer_id as old_writer, b.alt_writer_id as old_alt, 
  a.writer_id as new_writer, a.alt_writer_id as new_alt,
  a.the_date, a.ro, a.gm_parts, a.non_gm_parts,
  a.gm_parts + a.non_gm_parts as total_parts, a.labor,
  b.gm_parts as old_gm, b.non_gm_parts as old_non_gm, b.labor as old_labor,
  b.gm_parts + b.non_gm_parts + b.labor as old_total,
  a.gm_parts + a.non_gm_parts + a.labor as total_sales
from(  
  select writer_id, alt_writer_id, the_date, ro,
    coalesce(round(sum(case when category = 'parts' and multiplier = .5 then -1 * amount * multiplier end), 2), 0) as gm_parts,
    coalesce(round(sum(case when category = 'parts' and multiplier = 1 then -1 * amount * multiplier end), 2), 0) as non_gm_parts,
    coalesce(round(sum(case when category = 'labor' then -1 * amount * multiplier end), 2), 0) as labor
  from writer_transactions
  group by ro, writer_id, alt_writer_id, the_date) a
left join bspp.sales b on a.ro = b.ro  
where (a.writer_id = '403' or a.alt_writer_id = '403') and a.the_date between '04/02/2017' and current_date
order by a.ro


select * from writer_transactions limit 10

-- this looks good for identifying the bs lines
-- where writer <> bs estimator but there is an alt_writer and bs service type line(s)
-- only those lines where c is not null
select a.control, a.amount, b.account, b.description, b.department, c.*
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
  and account_type = 'sale'
left join bspp.sale_accounts c on b.account = c.account  
where control in ( -- alt writer and servicetype
  select a.ro -- 31 rows  -- 10 distinct
  from ads.ext_fact_repair_order  a
  inner join pay_period_transactions b on a.ro = b.control
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    and c.opcodekey between 26302 and 26307
  where a.servicewriterkey not in (
    select servicewriterkey
    from ads.ext_dim_service_Writer
    where  writernumber in ('312','311','303','317','712','403'))
  and exists (
    select 1
    from ads.ext_fact_repair_order
    where ro = a.ro
      and servicetypekey = 3)) 
order by a.control

-- ok, let's make it conform to writer_transactions
-- add dimservicewriter
-- looks good
select a.control, d.writer_id, d.alt_writer_id, b.account, aa.the_date, 
  a.amount, b.description, b.department, c.category, c.multiplier
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and account_type = 'sale'
inner join bspp.sale_accounts c on b.account = c.account  
inner join (
  select a.ro, d.writernumber as writer_id,
    max(
      case a.opcodekey
        when 26302 then '312'
        when 26303 then '311'
        when 26304 then '303'
        when 26305 then '317'
        when 26306 then '712'
        when 26307 then '403'
        else 'none'
      end) as alt_writer_id
  from ads.ext_fact_repair_order a
  inner join pay_period_transactions b on a.ro = b.control
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    and c.opcodekey between 26302 and 26307
  left join ads.ext_dim_service_writer d on a.servicewriterkey = d.servicewriterkey
  where d.writernumber not in ('312','311','303','317','712','403')
    and exists (
      select 1
      from ads.ext_fact_repair_order
      where ro = a.ro
        and servicetypekey = 3) 
  group by a.ro, d.writernumber) d on a.control = d.ro


-- ok, put it all together
drop table if exists writer_sales;
create temp table writer_sales as
select * from writer_transactions
union
select a.control, d.writer_id, d.alt_writer_id, b.account, aa.the_date, 
  a.amount, b.description, b.department, c.category, c.multiplier
from fin.fact_gl a
inner join dds.dim_date aa on a.date_key = aa.date_key
inner join fin.dim_account b on a.account_key = b.account_key
  and account_type = 'sale'
inner join bspp.sale_accounts c on b.account = c.account  
inner join (
  select a.ro, d.writernumber as writer_id,
    max(
      case a.opcodekey
        when 26302 then '312'
        when 26303 then '311'
        when 26304 then '303'
        when 26305 then '317'
        when 26306 then '712'
        when 26307 then '403'
        else 'none'
      end) as alt_writer_id
  from ads.ext_fact_repair_order a
  inner join pay_period_transactions b on a.ro = b.control
  inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
    and c.opcodekey between 26302 and 26307
  left join ads.ext_dim_service_writer d on a.servicewriterkey = d.servicewriterkey
  where d.writernumber not in ('312','311','303','317','712','403')
    and exists (
      select 1
      from ads.ext_fact_repair_order
      where ro = a.ro
        and servicetypekey = 3) 
  group by a.ro, d.writernumber) d on a.control = d.ro

-- update source to manual
update bspp.writer_sales a
set source = x.source
from (
  select *
  from bspp.sales) x
where a.ro = x.ro
  and a.source <> x.source  
  
-- alt_writer different -- 284 rows 
-- i understand where source is manual, but wtf with source is pipeline
-- all the pipeline ones are from the first payrole, remember, it was fucked up
-- assumed team mate as alt_writer
-- SO,  only fix alt_writer for source = manual: 58 rows
update bspp.writer_sales a
set alt_writer_id = x.alt_writer_id
from (
  select a.*
  from bspp.sales a
  inner join bspp.personnel b on a.writer_id = b.writer_id
  inner join bspp.personnel_role c on b.user_name = c.user_name
    and c.the_role in ('vda','csr')
  inner join bspp.writer_sales d on a.ro = d.ro
    and a.writer_id = d.writer_id
    and a.alt_writer_id <> d.alt_writer_id  
  where a.the_date > '03/04/2017') x
where a.ro = x.ro  



-- missing ros THERE ARE NONE !
select a.*
from bspp.sales a
inner join bspp.personnel b on a.writer_id = b.writer_id
inner join bspp.personnel_role c on b.user_name = c.user_name
  and c.the_role in ('vda','csr')
left join bspp.writer_sales d on a.ro = d.ro
where a.the_date > '03/04/2017'  
  and d.ro is null 



-- difference by ro
select a.ro, b.writer_id as old_writer, b.alt_writer_id as old_alt, 
  a.writer_id as new_writer, a.alt_writer_id as new_alt,
  a.the_date, a.ro, a.gm_parts, a.non_gm_parts,
  a.gm_parts + a.non_gm_parts as total_parts, a.labor,
  b.gm_parts as old_gm, b.non_gm_parts as old_non_gm, b.labor as old_labor,
  b.gm_parts + b.non_gm_parts + b.labor as old_total,
  a.gm_parts + a.non_gm_parts + a.labor as total_sales
from(  
  select writer_id, alt_writer_id, the_date, ro,
    coalesce(round(sum(case when category = 'parts' and multiplier = .5 then -1 * amount * multiplier end), 2), 0) as gm_parts,
    coalesce(round(sum(case when category = 'parts' and multiplier = 1 then -1 * amount * multiplier end), 2), 0) as non_gm_parts,
    coalesce(round(sum(case when category = 'labor' then -1 * amount * multiplier end), 2), 0) as labor
  from writer_sales
  group by ro, writer_id, alt_writer_id, the_date) a
left join bspp.sales b on a.ro = b.ro  
where (a.writer_id = '403' or a.alt_writer_id = '403') and a.the_date between '03/05/2017' and '03/18/2017'
order by a.ro


-- difference by ro
select a.ro, b.writer_id as old_writer, b.alt_writer_id as old_alt, 
  a.writer_id as new_writer, a.alt_writer_id as new_alt,
  a.the_date, a.ro, a.gm_parts, a.non_gm_parts,
  a.gm_parts + a.non_gm_parts as total_parts, a.labor,
  b.gm_parts as old_gm, b.non_gm_parts as old_non_gm, b.labor as old_labor,
  b.gm_parts + b.non_gm_parts + b.labor as old_total,
  a.gm_parts + a.non_gm_parts + a.labor as total_sales
from bspp. writer_sales a
left join bspp.sales b on a.ro = b.ro  
where (a.writer_id = '403' or a.alt_writer_id = '403') and a.the_date between '03/05/2017' and '03/18/2017'
order by a.ro





select b.last_name, b.writer_id, sum(amount)
from writer_sales a
inner join bspp.personnel b on (a.writer_id = b.writer_id
  or a.alt_writer_id = b.writer_id)
where a.the_date between '04/02/2017' and current_date
group by b.last_name, b.writer_id



insert into bspp.writer_sales
select writer_id, alt_writer_id, the_date, ro, gm_parts, non_gm_parts,
  gm_parts + non_gm_parts, labor, gm_parts + non_gm_parts + labor, 'pipeline'
from(  
  select writer_id, alt_writer_id, the_date, ro,
    coalesce(round(sum(case when category = 'parts' and multiplier = .5 then -1 * amount * multiplier end), 2), 0) as gm_parts,
    coalesce(round(sum(case when category = 'parts' and multiplier = 1 then -1 * amount * multiplier end), 2), 0) as non_gm_parts,
    coalesce(round(sum(case when category = 'labor' then -1 * amount * multiplier end), 2), 0) as labor
  from writer_sales
  group by ro, writer_id, alt_writer_id, the_date) x


-- ros in bspp.sales not in writer_transactions
-- mostly internal
select *
from bspp.sales a
left join writer_transactions b on a.ro = b.ro
where b.ro is null 


-- turn it into a single query
-- see function  bspp.update_writer_sales()

select * from bspp.pay_period_transactions

select * from bspp.writer_sales where the_date between '04/02/2017' and '04/15/2017'