﻿720,403,2017-04-29,16273951,0,41.57,41.57,126.67,168.24,pipeline

1. the data being returned is for a previous pay period
2. it is not returning the data for the current pay period:
2017-05-02,16273951,148004,SLS-PTS WARR CAR,SCA,2.81,ADJ PARTS MARKUP

the problem is in the unioned query that includes bs est not the writer, has alt_writer & bs line(s)

-- this is the original form: returning the previous pay period data only
    select a.control, d.writer_id, d.alt_writer_id, b.account, aa.the_date, 
      a.amount, b.description, b.department, c.category, c.multiplier
    from fin.fact_gl a
    inner join dds.dim_date aa on a.date_key = aa.date_key
    inner join fin.dim_account b on a.account_key = b.account_key
      and account_type = 'sale'
    inner join bspp.sale_accounts c on b.account = c.account  
    inner join (
      select a.ro, d.writernumber as writer_id,
        max(
          case a.opcodekey
            when 26302 then '312'
            when 26303 then '311'
            when 26304 then '303'
            when 26305 then '317'
            when 26306 then '712'
            when 26307 then '403'
            else 'none'
          end) as alt_writer_id
      from ads.ext_fact_repair_order a
      inner join bspp.pay_period_transactions b on a.ro = b.control
      inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
        and c.opcodekey between 26302 and 26307
      left join ads.ext_dim_service_writer d on a.servicewriterkey = d.servicewriterkey
      -- where d.writernumber not in ('312','311','303','317','712','403')
      where d.writernumber not in (
        select writer_id
        from bspp.personnel b 
        inner join bspp.personnel_role c on b.user_name = c.user_name
          and c.the_role in ('vda','csr'))      
        and exists (
          select 1
          from ads.ext_fact_repair_order
          where ro = a.ro
            and servicetypekey = 3
        and a.ro = '16273951') 
      group by a.ro, d.writernumber) d on a.control = d.ro


-- and the corrected version that returns just the data for the current pay period
    select a.control, d.writer_id, d.alt_writer_id, b.account, aa.the_date, 
      a.amount, b.description, b.department, c.category, c.multiplier
    from fin.fact_gl a
    inner join dds.dim_date aa on a.date_key = aa.date_key
    inner join fin.dim_account b on a.account_key = b.account_key
      and account_type = 'sale'
    -- change this to a left outer join now includes the correct pay period data
    left join bspp.sale_accounts c on b.account = c.account  
    inner join ( -- include the_date from bspp.pay_period_transactions
      select a.ro, d.writernumber as writer_id, b.the_date,
        max(
          case a.opcodekey
            when 26302 then '312'
            when 26303 then '311'
            when 26304 then '303'
            when 26305 then '317'
            when 26306 then '712'
            when 26307 then '403'
            else 'none'
          end) as alt_writer_id
      from ads.ext_fact_repair_order a
      inner join bspp.pay_period_transactions b on a.ro = b.control
      inner join ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
        and c.opcodekey between 26302 and 26307
      left join ads.ext_dim_service_writer d on a.servicewriterkey = d.servicewriterkey
      -- where d.writernumber not in ('312','311','303','317','712','403')
      where d.writernumber not in (
        select writer_id
        from bspp.personnel b 
        inner join bspp.personnel_role c on b.user_name = c.user_name
          and c.the_role in ('vda','csr'))      
        and exists (
          select 1
          from ads.ext_fact_repair_order
          where ro = a.ro
            and servicetypekey = 3) 
      -- include the_date in the grouping (d) and join to a join
      group by a.ro, d.writernumber, b.the_date) d on a.control = d.ro and aa.the_date = d.the_date


