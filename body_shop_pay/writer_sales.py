# coding=utf-8
"""
ads: factrepairorder (180 days), dimservicewriter, dimopcode
all into pg::ads.

"""
import db_cnx
import ops
import string
import csv

pg_con = None
ads_con = None

try:
    # ext_fact_repair_order
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            file_name = 'files/ext_fact_repair_order.csv'
            sql = """
                SELECT a.*
                from factrepairorder a
                inner join day b on a.opendatekey = b.datekey
                where b.thedate > curdate() - 180
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_fact_repair_order_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_fact_repair_order_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                delete
                from ads.ext_fact_repair_order
                where ro in (
                  select ro
                  from ads.ext_fact_repair_order_tmp);
            """
            pg_cur.execute(sql)
            sql = """
                insert into ads.ext_fact_repair_order
                select * from ads.ext_fact_repair_order_tmp
            """
            pg_cur.execute(sql)
    # ext_dim_opcode
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT opcodekey,storecode,opcode,
                  case when description = '' then 'NONE' else description end,
                  pdqcat1,pdqcat2,pdqcat3
                from dimopcode
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_opcode")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_opcode from stdin with csv encoding 'latin-1 '""", io)
    # ext_dim_service_writer
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT ServiceWriterKey,StoreCode,EmployeeNumber,Name,Description,WriterNumber,dtUserName,
                    active,DefaultServiceType,CensusDept,CurrentRow,RowChangeDate,RowChangeDateKey,
                    RowFromTS,RowThruTS,RowChangeReason,ServiceWriterKeyFromDate,ServiceWriterKeyFromDateKey,
                    ServiceWriterKeyThruDate,ServiceWriterKeyThruDateKey
                from dimServiceWriter
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate ads.ext_dim_service_writer")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy ads.ext_dim_service_writer from stdin with csv encoding 'latin-1 '""", io)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            # decided to go with functions rather than a shitload of sql in the script
            sql = "select * from bspp.update_pay_period_transactions();"
            pg_cur.execute(sql)
            sql = "select * from bspp.update_writer_sales();"
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
