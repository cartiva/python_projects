# coding=utf-8
"""
ads factRepairOrder
pg  ads.ext_fact_repair_order_tmp
pg  ads.ext_fact_repair_order
Not 100% confident - do a nightly check of the final product in pg vs ads

"""
import db_cnx
import ops
import string
import csv

task = 'sales'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_fact_repair_order.csv'
try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select a.control
                from fin.fact_gl a
                inner join fin.dim_account b on a.account_key = b.account_key
                inner join bspp.sale_accounts c on b.account = c.account
                inner join dds.dim_date e on a.date_key = e.date_key
                  and e.biweekly_pay_period_sequence = (
                    select biweekly_pay_period_sequence
                    from dds.dim_date
                    where the_date = current_date -1)
                where a.post_status = 'Y'
                group by a.control
            """
            pg_cur.execute(sql)
            file_name = 'files/bs_ro_numbers.csv'
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(pg_cur.fetchall())  # is the fetchall required?
    csv_data = csv.reader(file(file_name))
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            ads_cur.execute("delete from tmp_body_shop_ros")
            for row in csv_data:
                sql = """
                    insert into tmp_body_shop_ros
                    values('%s')
                """ % (row[0])
                ads_cur.execute(sql)
            sql = """
                SELECT count(*)
                FROM factrepairorder a
                INNER JOIN  tmp_body_shop_ros b on a.ro = b.ro
                INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
                  AND c.opcodekey BETWEEN 26302 AND 26307
                GROUP BY a.ro
                HAVING COUNT(*) > 1
            """
            ads_cur.execute(sql)
            # 3/6/17 all of a sudden this throws 'NoneType' object has no attribute '__getitem__'
            # the_count = ads_cur.fetchone()[0]
            # if the_count != 0:
            #     ops.email_error('abc','def','danger')
            # TODO this fixes the error, but still will generate an email every night in the pay period
            the_count = ads_cur.fetchone()
            if the_count:
                ops.email_error('abc', 'def', 'danger')
            sql = """
                SELECT ro, writer_id,
                  coalesce((
                    select
                      case opcodekey
                        when 26302 then '312'
                        when 26303 then '311'
                        when 26304 then '303'
                        when 26305 then '317'
                        when 26306 then '712'
                        when 26307 then '403'
                      end as alt_writer_id
                    from factrepairorder
                    where ro = d.ro
                      and opcodekey between 26302 and 26307), 'none') as alt_writer_id
                FROM (
                  SELECT a.ro, c.writernumber AS writer_id
                  FROM factrepairorder a
                  INNER JOIN  tmp_body_shop_ros b on a.ro = b.ro
                  INNER JOIN dimservicewriter c on a.servicewriterkey = c.servicewriterkey
                  WHERE a.ro NOT IN ( -- exclude ros with > 2 writers
                    SELECT a.ro
                    FROM factrepairorder a
                    INNER JOIN  tmp_body_shop_ros b on a.ro = b.ro
                    INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
                      AND c.opcodekey BETWEEN 26302 AND 26307
                    GROUP BY a.ro
                    HAVING COUNT(*) > 1)
                  GROUP BY a.ro, c.writernumber) d
            """
            ads_cur.execute(sql)
            file_name = 'files/bs_ros.csv'
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate bspp.bs_ros")
            file_name = 'files/bs_ros.csv'
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy bspp.bs_ros from stdin with csv encoding 'latin-1 '""", io)
            # delete current pay periods ros where source <> manual
            sql = """
                delete
                from bspp.sales
                where source <> 'manual'
                  and the_date between
                    (select biweekly_pay_period_start_date from dds.dim_date where the_date = current_date - 1)
                    and
                    (select biweekly_pay_period_end_date from dds.dim_date where the_date = current_date - 1)
            """
            pg_cur.execute(sql)
            # exclude manual ros from bspp.bs_ros
            sql = """
                delete
                from bspp.bs_ros
                where ro in (
                  select ro
                  from bspp.sales
                  where source = 'manual')
            """
            pg_cur.execute(sql)
            sql = """
                insert into bspp.sales (writer_id,alt_writer_id,the_date,ro,gm_parts,
                  non_gm_parts,total_parts,labor,total_sales)
                select s.writer_id, s.alt_writer_id, r.the_date, r.control, r.gm_parts, r.non_gm_parts,
                  r.gm_parts + r.non_gm_parts as total_parts, r.labor,
                  r.gm_parts + r.non_gm_parts + r.labor as total_sales
                from ( -- r: date, ro, amounts; 1 row per ro/date; 147700(GM Parts): multiplier = .5
                  select e.the_date, a.control,
                    round(sum(case when c.category = 'parts' and b.account = '147700'
                      then -1 * a.amount * c.multiplier else 0 end), 2) as gm_parts,
                    round(sum(case when c.category = 'parts' and b.account <> '147700'
                      then -1 * a.amount * c.multiplier else 0 end), 2) as non_gm_parts,
                    round(sum(case when c.category = 'labor' then -1 * a.amount * c.multiplier else 0 end), 2) as labor
                  from fin.fact_gl a -- same query that generates bs_ro_numbers.csv
                  inner join fin.dim_account b on a.account_key = b.account_key
                  inner join bspp.sale_accounts c on b.account = c.account
                  inner join dds.dim_date e on a.date_key = e.date_key
                    and e.biweekly_pay_period_sequence = (
                      select biweekly_pay_period_sequence
                      from dds.dim_date
                      where the_date = current_date - 1)
                  where a.post_status = 'Y'
                  group by a.control, e.the_date) r
                inner join bspp.bs_ros s on r.control = s.ro
            """
            pg_cur.execute(sql)
        ops.log_pass(run_id)
        print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()