# encoding=utf-8
import csv
import db_cnx
import ops
import string
"""
glpdept
glpmast
dim_account
"""
task = 'dim_account'
pg_con = None
db2_con = None
run_id = None
# file_name = 'files/ext_glpdept_tmp.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    file_name = 'files/ext_glpdept_tmp.csv'
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(DEPARTMENT_CODE),TRIM(DEPT_DESCRIPTION)
                from rydedata.glpdept a
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_glpdept_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_glpdept_tmp from stdin with csv encoding 'latin-1 '""", io)
            pg_con.commit()
            sql = """
                select count (1)
                from ((
                    select *
                    from arkona.ext_glpdept d
                    except
                    select *
                    from arkona.ext_glpdept_tmp e)
                  union (
                    select *
                    from arkona.ext_glpdept_tmp f
                    except
                    select *
                    from arkona.ext_glpdept g)) x
            """
            pg_cur.execute(sql)
            the_count = pg_cur.fetchone()[0]
            if the_count != 0:
                raise Exception('there are differences between ext_glpdept and ext_glpdept_tmp')
    # glpmast
    file_name = 'files/ext_glpmast.csv'
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(FISCAL_ANNUAL),YEAR,TRIM(ACCOUNT_NUMBER),TRIM(ACCOUNT_TYPE),
                  TRIM(ACCOUNT_DESC),TRIM(ACCOUNT_SUB_TYPE),TRIM(ACCOUNT_CTL_TYPE),TRIM(DEPARTMENT),
                  TRIM(TYPICAL_BALANCE),TRIM(DEBIT_OFFSET_ACCT),TRIM(CRED_OFFSET_ACCT),OFFSET_PERCENT,
                  AUTO_CLEAR_AMOUNT,TRIM(WRITE_OFF_ACCOUNT),TRIM(SCHEDULE_BY),TRIM(RECONCILE_BY),
                  TRIM(COUNT_UNITS),TRIM(CONTROL_DESC1),TRIM(VALIDATE_STOCK_NUMBER),TRIM(OPTION_3),
                  TRIM(OPTION_4),TRIM(OPTION_5),BEGINNING_BALANCE,JAN_BALANCE01,FEB_BALANCE02,MAR_BALANCE03,
                  APR_BALANCE04,MAY_BALANCE05,JUN_BALANCE06,JUL_BALANCE07,AUG_BALANCE08,SEP_BALANCE09,
                  OCT_BALANCE10,NOV_BALANCE11,DEC_BALANCE12,ADJ_BALANCE13,UNITS_BEG_BALANCE,JAN_UNITS01,
                  FEB_UNITS02,MAR_UNITS03,APR_UNITS04,MAY_UNITS05,JUN_UNITS06,JUL_UNITS07,AUG_UNITS08,
                  SEP_UNITS09,OCT_UNITS10,NOV_UNITS11,DEC_UNITS12,ADJ_UNITS13,TRIM(ACTIVE)
                from rydedata.glpmast
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_glpmast")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_glpmast from stdin with csv encoding 'latin-1 '""", io)
    # dim_account
            pg_cur.execute("truncate arkona.xfm_glpmast")
            sql = """
                insert into arkona.xfm_glpmast
                select account_number, account_type, account_desc,
                  case account_sub_type
                    when 'A' then 'RY1'
                    when 'B' then 'RY2'
                    else 'XXX'
                  end,
                  department,
                  case typical_balance
                    when 'D' then 'Debit'
                    when 'C' then 'Credit'
                    else 'X'
                  end
                from arkona.ext_glpmast
                where account_sub_type <> 'C' -- crookston accounts
                group by account_number, account_type, account_desc, account_sub_type,
                  department, typical_balance
            """
            pg_cur.execute(sql)
            pg_con.commit()
            sql = """
                select count(*)
                from (
                    (select account, account_type_code, description, store_code, department_code, typical_balance
                    from arkona.xfm_glpmast
                    except
                    select account, account_type_code, description, store_code, department_code, typical_balance
                    from fin.dim_account)
                union
                    (select account, account_type_code, description, store_code, department_code, typical_balance
                    from fin.dim_account
                    where current_row = true
                        and account not in ('none', '*E*', '*VOID', '*SPORD')
                    except
                    select account, account_type_code, description, store_code, department_code, typical_balance
                    from arkona.xfm_glpmast)) x
            """
            pg_cur.execute(sql)
            the_count = pg_cur.fetchone()[0]
            if the_count != 0:
                raise Exception('there are differences between xfm_glpmast and dim_account')
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception as error:
    ops.log_error(str(run_id), str.replace(str(error), "'", ""))
    ops.email_error(task, run_id, error)
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
