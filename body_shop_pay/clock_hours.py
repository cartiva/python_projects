# coding=utf-8
import db_cnx
import ops
import string
import csv

task = 'clock_hours'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_ads_edwClockHoursFact.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        'Failed dependency check'
        exit()
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT c.employeenumber,b.thedate,clockhours,regularhours,overtimehours,
                  vacationhours,ptohours,holidayhours
                from edwClockHoursFact a
                inner join day b on a.datekey = b.datekey
                  and b.thedate between curdate() -18 and curdate()
                inner join edwEmployeeDim c on a.employeekey = c.employeekey
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate bspp.xfm_fact_clock_hours")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy bspp.xfm_fact_clock_hours from stdin with csv encoding 'latin-1 '""", io)
            #  populate bspp.clock_hours
            sql = """
                delete
                from bspp.clock_hours
                where the_date in (
                  select the_date
                  from bspp.xfm_fact_clock_hours);
            """
            pg_cur.execute(sql)
            sql = """
                insert into bspp.clock_hours(the_date,user_name,reg_hours,ot_hours,pto_hours,hol_hours)
                select a.the_date, b.user_name, sum(a.regularhours), sum(a.overtimehours),
                  sum(a.ptohours + a.vacationhours) , sum(a.holidayhours)
                from bspp.xfm_fact_clock_hours a
                inner join bspp.personnel b on a.employee_number = b.employee_number
                group by a.the_date, b.user_name;
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
