﻿create table rv.hr_employee_profile (
  store_code citext not null,
  first_name citext not null,
  middle_name citext not null,
  last_name citext not null, 
  employee_number citext primary key,
  email citext,
  phone_extension citext, 
  department citext not null, 
  address citext,
  city citext,
  state citext,
  zip citext);

drop table rv.ext_pymast_tmp;
 create table rv.ext_pymast_tmp (
  store_code citext not null,
  employee_name citext not null,
  employee_number citext primary key,
  department citext not null, 
  address citext,
  city citext,
  state citext,
  zip citext);

create table rv.ext_dim_employee_tmp (
  store_code citext,
  employee_number citext,
  first_name citext,
  last_name citext,
  middle_name citext);
  