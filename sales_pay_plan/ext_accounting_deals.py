# coding=utf-8
'''
4/23/17
    limit to transactions between 2/1/17 and the_date

'''
import db_cnx
import ops
import string
# TODO 30635 issue, capped and uncapped on same day (3/8/17) resulting in 2 rows in xfm_glptrs
pg_con = None
run_id = None
# the_date = str(datetime.datetime.now() - datetime.timedelta(days=1))[:10]
# month_day = ((datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%m") +
#             (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%d"))
# print the_date
# print month_day
month_day = '0602'
the_date = '2017-06-02'
try:
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into sls.ext_accounting_deals
                select {0}, coalesce(the_date, '12/31/9999')::date as the_date, control, amount,
                  coalesce(case when amount < 0 then 1 else -1 end, 0) as the_count,
                  account, account_description, gl_description
                from (
                  select d.the_date, a.control, sum(a.amount) as amount,
                    b.account, b.description as account_description, max(e.description) as gl_description
                  from fin.fact_gl a
                  inner join fin.dim_account b on a.account_key = b.account_key
                    and b.current_row = true
                  inner join fin.dim_journal c on a.journal_key = c.journal_key
                  inner join dds.dim_date d on a.date_key = d.date_key
                  inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
                  where b.account_type = 'sale'
                    and b.department_code in ('nc','uc')
                    and c.journal_code in ('vsn','vsu')
                    and a.post_status = 'Y'
                    and d.the_date between '02/01/2017' and {0}
                  group by d.the_date, a.control, b.account, b.description
                  having sum(amount) <> 0) x
                where not exists (
                    select 1
                    from sls.ext_accounting_deals
                    where control = x.control
                      and gl_date = x.the_date);
            """.format("'" + the_date + "'")
            pg_cur.execute(sql)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
