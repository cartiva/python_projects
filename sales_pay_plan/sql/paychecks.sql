﻿

select e.last_name, e.first_name, coalesce(ee.total_pay, eee.total_pay) as vision, f.arkona_total_gross, arkona_comm_draw, commission, draw, overtime, pto_pay_out, lease, vacation, spiffs
from sls.folks e
left join sls.consultant_pay ee on e.employee_number = ee.employee_number
left join sls.team_leader_pay eee on e.employee_number = eee.employee_number
left join (
  select m.*, n.arkona_comm_draw, n.commission, draw, overtime, pto_pay_out, lease, vacation, spiffs
  from ( -- pyhshdta
    select a.employee_, check_month, 201600 + check_month as year_month, sum(a.total_gross_pay) as arkona_total_gross
    from arkona.ext_pyhshdta a
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
      and a.payroll_ending_month = 4
      and a.company_number = 'RY1'
    group by a.employee_, check_month) m
  left join ( -- pyhscdta
    select employee_, check_month, 
      sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
      sum(case when q.description = 'commissions' then amount else 0 end) as commission,
      sum(case when q.description = 'draws' then amount else 0 end) as draw,
      sum(case when q.description = 'overtime pay' then amount else 0 end) as overtime,
      sum(case when q.description = 'pay out pto' then amount else 0 end) as pto_pay_out,
      sum(case when q.description = 'lease program' then amount else 0 end) as lease,
      sum(case when q.description = 'sales vacation' then amount else 0 end) as vacation,
      sum(case when q.description = 'spiff pay outs' then amount else 0 end) as spiffs
--       string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
    from ( 
      select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount, b.code_id
      -- select distinct b.description
      from arkona.ext_pyhshdta a
      inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
        and a.company_number = b.company_number
        and a.employee_ = b.employee_number
        and b.code_type in ('0','1') -- 0,1: income, 2: deduction
      where trim(a.distrib_code) in ('TEAM','SALE')
        and a.payroll_cen_year = 117
        and a.payroll_ending_month = 4
        and a.company_number = 'RY1'
      group by a.employee_name, a.employee_, a.check_month, b.description, b.code_id) q
    group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month) f on e.employee_number = f.employee_
order by last_name    


all i really need for payroll is the draw amount paid in the current month
but since i have all the data, lets generate a pay by month table
one row per employee per month with the total of the different pay categories




select company_number, code_id, description, count(*)
from arkona.ext_pyhscdta
where payroll_cen_year = 117
  and code_type in ('0','1')
group by company_number, code_id, description
order by code_id, company_number


select company_number, code_id, description, count(*)
from (
select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
  a.employee_name, a.employee_, a.check_year, a.check_month,
  ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay, 
  b.code_type, b.code_seq_, b.code_id, b.code_sw, b.description, b.amount, b.entered_amount
-- select distinct b.description
from arkona.ext_pyhshdta a
inner join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_type in ('0','1')
  and b.code_id in ('400','500','74','78','79','82','OVT')
where trim(a.distrib_code) in ('TEAM','SALE')
  and a.payroll_cen_year = 117
--   and a.company_number = 'RY1'
  and a.seq_void = '00') x
group by company_number, code_id, description
order by code_id, company_number


-- these codes are all wendi wheeler, etc
select *
from (
select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
  a.employee_name, a.employee_, a.check_year, a.check_month,
  ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay, 
  b.code_type, b.code_seq_, b.code_id, b.code_sw, b.description, b.amount, b.entered_amount
-- select distinct b.description
from arkona.ext_pyhshdta a
inner join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_type in ('0','1')
where trim(a.distrib_code) in ('TEAM','SALE')
  and a.payroll_cen_year > 115
--   and a.company_number = 'RY1'
  and a.seq_void = '00') x
where code_id in ('hol','vac','70')  



-- beware of premature grouping !!!!!!!!!!!!!!!!!
select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
  a.employee_name, a.employee_, a.check_year, a.check_month,
  ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay, 
  b.code_type, b.code_seq_, b.code_id, b.code_sw, b.description, b.amount, b.entered_amount
-- select distinct b.description
from arkona.ext_pyhshdta a
inner join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_type in ('0','1')
  and b.code_id in ('400','500','74','78','79','82','OVT')
where trim(a.distrib_code) in ('TEAM','SALE')
  and a.payroll_cen_year = 117
--   and a.company_number = 'RY1'
  and a.seq_void = '00'
order by ((2000 + a.check_year) *100) + a.check_month, a.employee_name, b.code_Seq_





      sum(case when b.code_id = '79' then amount else 0 end) as commission,
      sum(case when b.code_id = '78' then amount else 0 end) as draw,
      sum(case when b.code_id = 'OVT' then amount else 0 end) as overtime,
      sum(case when b.code_id = '400' then amount else 0 end) as pto_pay_out,
      sum(case when b.code_id = '500' then amount else 0 end) as lease,
      sum(case when b.code_id = '74' then amount else 0 end) as vacation,
      sum(case when b.code_id = '82' then amount else 0 end) as spiffs


select company_number, year_month, employee_name, employee_, 
  sum(total_gross_pay) as total_gross_pay,
  sum(case when code_id = '79' then amount else 0 end) as commission,
  sum(case when code_id = '78' then amount else 0 end) as draw,
  sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
  sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
  sum(case when code_id = '500' then amount else 0 end) as lease,
  sum(case when code_id = '74' then amount else 0 end) as vacation,
  sum(case when code_id = '82' then amount else 0 end) as spiffs  
from (  
  select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
    a.employee_name, a.employee_, a.check_year, a.check_month,
    ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay, 
    b.code_type, b.code_seq_, b.code_id, b.code_sw, b.description, b.amount, b.entered_amount
  -- select distinct b.description
  from arkona.ext_pyhshdta a
  inner join arkona.ext_pyhscdta b on a.company_number = b.company_number
    and a.payroll_cen_year = b.payroll_cen_year
    and a.payroll_run_number = b.payroll_run_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1')
    and b.code_id in ('400','500','74','78','79','82','OVT')
  where trim(a.distrib_code) in ('TEAM','SALE')
    and a.payroll_cen_year = 117
  --   and a.company_number = 'RY1'
    and a.seq_void = '00') g
group by year_month, company_number, employee_name, employee_    
order by year_month, employee_name





select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
  a.employee_name, a.employee_, a.check_year, a.check_month,
  ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay, 
  b.code_type, b.code_seq_, b.code_id, b.code_sw, b.description, b.amount, b.entered_amount
-- select distinct b.description
from arkona.ext_pyhshdta a
inner join arkona.ext_pyhscdta b on a.company_number = b.company_number
  and a.payroll_cen_year = b.payroll_cen_year
  and a.payroll_run_number = b.payroll_run_number
  and a.employee_ = b.employee_number
  and b.code_type in ('0','1')
  and b.code_id in ('400','500','74','78','79','82','OVT')
where trim(a.distrib_code) in ('TEAM','SALE')
  and a.payroll_cen_year = 117
--   and a.company_number = 'RY1'
  and a.seq_void = '00'
order by ((2000 + a.check_year) *100) + a.check_month, a.employee_name, b.code_Seq_  


-- due to the different grains the above is fucked up

do separate queries then join the results on employee_number, year_month

-- pyhshdta
select company_number, employee_name, employee_, year_month, sum(total_gross_pay) as total_gross_pay
from (
  select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
    a.employee_name, a.employee_, a.check_year, a.check_month,
    ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay
  -- select distinct b.description
  from arkona.ext_pyhshdta a
  where trim(a.distrib_code) in ('TEAM','SALE')
    and a.payroll_cen_year = 117
    and a.seq_void = '00') b
group by company_number, employee_name, employee_, year_month    


-- pyhscdta
select employee_name, employee_, year_month, 
  sum(case when code_id = '79' then amount else 0 end) as commission,
  sum(case when code_id = '78' then amount else 0 end) as draw,
  sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
  sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
  sum(case when code_id = '500' then amount else 0 end) as lease,
  sum(case when code_id = '74' then amount else 0 end) as vacation,
  sum(case when code_id = '82' then amount else 0 end) as spiffs  
from (
  select a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month as year_month,
    sum(b.amount) as amount, b.code_id
  -- select distinct b.description
  from arkona.ext_pyhshdta a
  inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
  where trim(a.distrib_code) in ('TEAM','SALE')
    and a.payroll_cen_year = 117
    and a.payroll_ending_month = 4
    and a.company_number = 'RY1'
  group by a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
group by employee_name, employee_, year_month


-- this looks good
select d.*, e.commission, e.draw, e.overtime, e.pto_pay_out, e.lease, e.vacation, e.spiffs
from ( -- pyhshdta
  select company_number, employee_name, employee_, year_month, sum(total_gross_pay) as total_gross_pay
  from (
    select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
      a.employee_name, a.employee_, a.check_year, a.check_month,
      ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay
    -- select distinct b.description
    from arkona.ext_pyhshdta a
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
      and a.seq_void = '00') b
  group by company_number, employee_name, employee_, year_month) d
left join ( -- pyhscdta
  select company_number, employee_name, employee_, year_month, 
    sum(case when code_id = '79' then amount else 0 end) as commission,
    sum(case when code_id = '78' then amount else 0 end) as draw,
    sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
    sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
    sum(case when code_id = '500' then amount else 0 end) as lease,
    sum(case when code_id = '74' then amount else 0 end) as vacation,
    sum(case when code_id = '82' then amount else 0 end) as spiffs  
  from (
    select a.company_number, a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month as year_month,
      sum(b.amount) as amount, b.code_id
    -- select distinct b.description
    from arkona.ext_pyhshdta a
    inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
    group by a.company_number, a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
  group by company_number, employee_name, employee_, year_month) e on d.company_number = e.company_number
    and d.employee_ = e.employee_ and d.year_month = e.year_month

drop table if exists sls.paid_by_month cascade;
create table sls.paid_by_month (
  store_code citext not null,
  employee_number citext not null,
  year_month integer not null,
  total_gross_pay numeric (8,2) not null default 0,
  commission numeric(8,2) not null default 0,
  draw numeric(8,2) not null default 0,
  overtime numeric(8,2) not null default 0,
  pto_pay_out numeric(8,2) not null default 0,
  lease numeric(8,2) not null default 0,
  vacation numeric(8,2) not null default 0,
  spiffs numeric(8,2) not null default 0,
  constraint paid_by_month_pkey primary key (employee_number,year_month));

create index on sls.paid_by_month(store_code);
create index on sls.paid_by_month(employee_number);
create index on sls.paid_by_month(year_month);  

insert into sls.paid_by_month
select d.company_number, d.employee_, d.year_month, 
  coalesce(e.commission, 0), coalesce(e.draw, 0), coalesce(e.overtime, 0),
  coalesce(e.pto_pay_out, 0), coalesce(e.lease, 0), coalesce(e.vacation, 0),
  coalesce(e.spiffs, 0)
from ( -- pyhshdta
  select company_number, employee_name, employee_, year_month, sum(total_gross_pay) as total_gross_pay
  from (
    select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
      a.employee_name, a.employee_, a.check_year, a.check_month,
      ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay
    -- select distinct b.description
    from arkona.ext_pyhshdta a
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
      and a.seq_void = '00') b
  group by company_number, employee_name, employee_, year_month) d
left join ( -- pyhscdta
  select company_number, employee_name, employee_, year_month, 
    sum(case when code_id = '79' then amount else 0 end) as commission,
    sum(case when code_id = '78' then amount else 0 end) as draw,
    sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
    sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
    sum(case when code_id = '500' then amount else 0 end) as lease,
    sum(case when code_id = '74' then amount else 0 end) as vacation,
    sum(case when code_id = '82' then amount else 0 end) as spiffs  
  from (
    select a.company_number, a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month as year_month,
      sum(b.amount) as amount, b.code_id
    -- select distinct b.description
    from arkona.ext_pyhshdta a
    inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
    group by a.company_number, a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
  group by company_number, employee_name, employee_, year_month) e on d.company_number = e.company_number
    and d.employee_ = e.employee_ and d.year_month = e.year_month;


delete from sls.paid_by_month where year_month = 201705


insert into sls.paid_by_month (store_code,employee_number,year_month,total_gross_pay,
  commission,draw,overtime,pto_pay_out,lease,vacation,spiffs)
select d.company_number, d.employee_, d.year_month, d.total_gross_pay,
  coalesce(e.commission, 0) as commission, coalesce(e.draw, 0) as draw, 
  coalesce(e.overtime, 0) as overtime,
  coalesce(e.pto_pay_out, 0) as pto_pay_out, coalesce(e.lease, 0) as lease, 
  coalesce(e.vacation, 0) as vacation,
  coalesce(e.spiffs, 0) as spiffs
from ( -- pyhshdta
  select company_number, employee_name, employee_, year_month, sum(total_gross_pay) as total_gross_pay
  from (
    select a.company_number, a.payroll_cen_year, a.payroll_run_number, 
      a.employee_name, a.employee_, a.check_year, a.check_month,
      ((2000 + a.check_year) *100) + a.check_month as year_month, a.total_gross_pay
    -- select distinct b.description
    from arkona.ext_pyhshdta a
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
      and a.seq_void = '00') b
  group by company_number, employee_name, employee_, year_month) d
left join ( -- pyhscdta
  select company_number, employee_name, employee_, year_month, 
    sum(case when code_id = '79' then amount else 0 end) as commission,
    sum(case when code_id = '78' then amount else 0 end) as draw,
    sum(case when code_id = 'OVT' then amount else 0 end) as overtime,
    sum(case when code_id = '400' then amount else 0 end) as pto_pay_out,
    sum(case when code_id = '500' then amount else 0 end) as lease,
    sum(case when code_id = '74' then amount else 0 end) as vacation,
    sum(case when code_id = '82' then amount else 0 end) as spiffs  
  from (
    select a.company_number, a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month as year_month,
      sum(b.amount) as amount, b.code_id
    -- select distinct b.description
    from arkona.ext_pyhshdta a
    inner join arkona.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) in ('TEAM','SALE')
      and a.payroll_cen_year = 117
    group by a.company_number, a.employee_name, a.employee_, ((2000 + a.check_year) *100) + a.check_month, b.code_id) c
  group by company_number, employee_name, employee_, year_month) e on d.company_number = e.company_number
    and d.employee_ = e.employee_ and d.year_month = e.year_month
where not exists (
  select 1
  from sls.paid_by_month
  where employee_number = d.employee_
    and year_month = d.year_month);





select * from sls.paid_by_month where year_month = 201704

















    
