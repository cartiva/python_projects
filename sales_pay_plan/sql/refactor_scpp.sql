﻿select max(run_date) from scpp.ext_deals  -- 4/1
select max(run_date) from scpp.xfm_deals  -- 4/1
select * from scpp.xfm_deals_for_update

alter table scpp.ext_deals
drop column gl_date,
add column bopmast_record_key integer;


alter table scpp.xfm_deals
rename column sc_change to psc_change;
alter table scpp.xfm_deals
add column ssc_change citext,
add column bopmast_record_key integer;

-- 800 rows with no match in ext_deals
-- 1. ext_deals does not include wholesale deals
-- 2. there is no deal for intra market wholesale
select a.row_type, a.run_date, a.store_code, a.stock_number, a.vin, a.customer_name,
  a.primary_sc, a.secondary_sc, a.record_status, a.date_approved, a.date_capped,
  a.gl_date, a.gl_count, b.*
from scpp.xfm_deals a
left join scpp.ext_deals b on a.stock_number = b.stock_number
where b.bopmast_record_key is null 
-- do this update before running xfm_deals for 4/2
update scpp.xfm_deals a
set bopmast_record_key = x.bopmast_record_key
from ( 
  select a.stock_number, b.bopmast_record_key
  from scpp.xfm_deals a
  left join scpp.ext_deals b on a.stock_number = b.stock_number) x
where a.stock_number = x.stock_number;


-- from xfm_deals, stock_number version
                select 'Update', /*current_date,*/
                  (select max(run_date) from scpp.ext_deals) as run_date,
                  a.store_code, a.stock_number, a.vin,
                  a.customer_name,
                  -- a.primary_sc, a.secondary_sc,
                  case a.primary_sc
                    when 'AMA' then 'HAN'
                    else a.primary_sc
                  end as primary_sc,
                  case a.secondary_sc
                    when 'AMA' then 'HAN'
                    else a.secondary_sc
                  end as seconday_sc,
                  a.record_status,
                  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
                  a.make, a.model,
                  (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
                  'No Change' as psc_change,
                  'Deleted' as status_change,
                  a.gl_date, a.gl_count,ssc_change,a.bopmast_record_key
                from (
                  select *
                  from (
                    select *
                    from scpp.xfm_deals x
                    where seq = (
                      select max(seq)
                      from scpp.xfm_deals
                      where stock_number = x.stock_number)) y
                  where status_change <> 'Deleted') a
                    -- disappeared stock numbers
--                     and stock_number not in ('28511','28873A','28108')) a
                left join scpp.ext_deals b on a.stock_number = b.stock_number
                where b.stock_number is null

'3GTU2NEC1GG261848'
'3GCUKREC0GG190066'
'1FTRW08L41KA41953'
'1GT12UE83GF221815'
'5GAKVCKD8HJ258544'
'1HGCR2F88GA179583'

only '5GAKVCKD8HJ258544'
'1HGCR2F88GA179583' are real deletes
                
-- convert to bopmast_record_key                
select *
from (
  select *
  from scpp.xfm_deals x
  where bopmast_record_key is not null
    and seq = (
      select max(seq)
      from scpp.xfm_deals
      where stock_number = x.stock_number)) y
left join scpp.ext_deals z on y.bopmast_record_key = z.bopmast_record_key      
where y.status_change = 'deleted'    
  and z.bopmast_record_key is null

-- 4/18/17
new approach
using PG schema sls
restructured ext_deals to include all the extra field i should need 
alter table sls.ext_deals
rename column store to store_code;

initially populate sls.ext_deals with ubuntu::text.ext_bopmast_0228 data where origination_date > 12/31/2016 (~900 rows)

sls.xfm_deals: all attributes from sls.ext_deals


