﻿/*
select *
from scpp.xfm_deals
where stock_number = '29340'

select *
from scpp.xfm_glptrns


select *
from scpp.deals
where year_month = 201703
order by stock_number


select deal_status, count(*)
from scpp.deals
group by deal_status

-- deals that at some time had a status of none
-- it will always be at least seq = 2, deal does not get into
-- deals until it is capped
select b.full_name, a.*
from scpp.deals a
left join scpp.sales_consultants b on a.employee_number = b.employee_number
where exists (
  select 1
  from scpp.deals
  where stock_number = a.stock_number
    and deal_status = 'none')
order by stock_number, seq 

*/

drop table if exists sls.deals cascade;
create table sls.deals (
-- from sls.xfm_deals  
  run_date date not null,
  store_code citext not null,
  bopmast_id integer not null,
  deal_status_code citext not null,
  deal_type_code citext not null,
  sale_type_code citext not null,
  sale_group_code citext not null,
  vehicle_type_code citext not null,
  stock_number citext not null,
  vin citext not null,
  odometer_at_sale integer not null,
  buyer_bopname_id integer not null,
  cobuyer_bopname_id integer not null,
  primary_sc citext not null,
  secondary_sc citext not null,
  fi_manager citext not null,
  origination_date date not null,
  approved_date date not null,
  capped_date date not null,
  delivery_date date not null,
  gap numeric(8,2) not null,
  service_contract numeric(8,2) not null,
  total_care numeric(8,2) not null,  
--   
  seq integer not null,
  year_month integer not null,
  unit_count numeric(3,1) not null,
  deal_date date not null, 
  deal_status citext not null,
  notes citext,
  constraint deals_pkey primary key (bopmast_id, seq));

  -- 4/25 add hash to match xfm
  -- 4/26 see xfm, hash can not be unique (29523R)
  alter table sls.deals 
  add column hash citext;
  create unique index on sls.deals(hash);

  drop index sls.deals_hash_idx;
  create index on sls.deals(hash);
  
update sls.deals a
set hash = x.hash
from (
  select store_code, bopmast_id, md5(g::text) as hash
from (
  select store_code, bopmast_id, deal_status_code,
    deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
    odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
    primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
    capped_date, delivery_date, gap, service_contract,total_care,
    deal_date, unit_count::integer
  from sls.deals) g) x
where a.store_code = x.store_code
  and a.bopmast_id = x.bopmast_id    
  
-- this is for the intial load only
-- 4/22
-- uh oh, split deals, 
-- so, i am currently thinking, sales cons deal count is a different entity
-- at this level, deal counts are 1, 0 or -1
-- 4/23 starting over again
truncate sls.deals;
insert into sls.deals
select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin, 
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care,
  1 as seq,  
  case
    when a.gl_date is null then
      (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
    else
      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
  end as year_month,
  gl_count unit_count,
  case -- delivery date may figure into this, eventually
    when a.gl_date is null then a.capped_date
    else a.gl_date
  end as deal_date,  
  case 
    when deal_status = 'U' then 'capped'
    when deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status
-- select *  
from sls.xfm_deals a 
where a.row_type = 'new'
  and a.deal_status = 'U'
  and (
    extract(month from delivery_date) = 2 or
    extract(month from gl_date) = 2);


-- 4/22 -------------------------------------------------------------------------------------------------
-- daily new rows
-- this is trickier than first blush, scpp relied on most recent run date which caused some to be missed

-- all new rows should have a seq of 1
select distinct seq
from sls.xfm_deals a
where a.row_type = 'new'

-- 3/1 
need rows from xfm that are capped and do not already exist in deals
but what do i do about all the xfm rows in which i am not interested
ok, for new rows the deal date (coalesce(gl_date,capped_date)) > 1/31 should work
ok, that is better, 5 rows
can not rely on row_type being new, eg 29577b, 29503b
rows can be in xfm as non capped, then add a capped row which will be an update

-- believe this will do it
-- 4/23 starting over (again)
-- wtf all the march new rows are generating none as deal status
-- fixed: deal_Status was casing deal_status_code, s/b  a.deal_status
select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin, 
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care,
  1 as seq,  
  case
    when a.gl_date is null then
      (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
    else
      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
  end as year_month,
  gl_count unit_count,
  case -- delivery date may figure into this, eventually
    when a.gl_date is null then a.capped_date
    else a.gl_date
  end as deal_date,  
  case 
    when a.deal_status = 'U' then 'capped'
    when a.deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status
from sls.xfm_deals a
left join sls.deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
where a.deal_status = 'U'
  and coalesce(a.gl_date, a.capped_date) > '01/31/2017' -- only feb and later deals
  and b.store_code is null;


-- changed rows
-- this detects them
drop table if exists changed_rows;
create temp table changed_rows as 
select k.bopmast_id
from (
  select store_code, bopmast_id, md5(h::text) as hash
  from (
    select store_code, bopmast_id, deal_status_code,
      deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
      primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
      capped_date, delivery_date, gap, service_contract,total_care,
      deal_date, unit_count::integer
    from sls.deals g
    where seq = (
      select max(seq)
      from sls.deals
      where store_code = g.store_code
        and bopmast_id = g.bopmast_id)) h) k
inner join (
  select store_code, bopmast_id, hash
  from sls.xfm_deals j
  where seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = j.store_code
      and bopmast_id = j.bopmast_id)) l on k.store_code = l.store_code
    and k.bopmast_id = l.bopmast_id
    and k.hash <> l.hash;     

-- and here are the details

select 'xfm' as source, run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, gl_count
-- select *  
from sls.xfm_deals a
where bopmast_id in (select * from changed_rows)   
  and seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id) 
union
select 'deal', run_date, store_code, bopmast_id, deal_status_code,
  deal_type_code, sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  deal_date, unit_count
from sls.deals g
where bopmast_id in (select * from changed_rows)   
  and seq = (
    select max(seq)
    from sls.deals
    where store_code = g.store_code
      and bopmast_id = g.bopmast_id)
order by bopmast_id, source desc        

-- 4/24 
any change except for deal status is type 1, straight update
deal status requires a new row in sls.deals, because the unit count changes
identify the "type 2", process, then update the rest
a type2 insert will include any and all incidental type changes, inserting from current xfm row
what is the deal deate when a deal is unwound
also, when gl_date changes, does the deal date change?  seems like if it is the same deal (vehicle & customer) it does not
so, if gl_date changes, but the customer does not, the deal date does not change

3/6 do it all manual (here in the sql script)
3/6     what changed
28855A  app_date  cap_date
29655   app_date  cap_date
30635   app_date  cap_date  deal_type   gl_date   gl_count !!!!
30245A  app_date  cap_date
28970R  app_date  cap_date
30745X  orig_date deal_status !!!!

??? maybe
create 2 tables: type1 & type2
then process them separately
type2: gl_date, gl_count, deal_status not EQUAL
type1: gl_date, gl_count, deal_status EQUAL

ok, so i can do the 2 insert statements (below) 
but the update seems iffy, 26 attribute update
the 2 table solution: 
or a single table with a change_type attribute
so type2 would be an insert
type 1 would be a delete andd insert
  

-- base changed rows: xfm_deals/deals, each max(seq), inner join on store = store, bopmast_id = bopmast_id AND HASH <> HASH
-- type 2 gl_date, gl_count, deal_status



-- 30635: xfm_deals.gl_date <> deals.deal_date and xfm_deals.gl_count <> deals.unit_count
-- new row: all values from xfm_deals except deal_date, year_month
-- new row: new values: run_date, unit_count
select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care,
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id),
--   case
--     when a.gl_date is null then
--       (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
--     else
--       (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
--   end as year_month,
  b.year_month,
  gl_count unit_count,
--   case 
--     when a.gl_date is null then a.capped_date
--     else a.gl_date
--   end as deal_date,
  b.deal_date, 
  case
    when a.deal_status = 'U' then 'capped'
    when a.deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status
from sls.xfm_deals a
inner join sls.deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
where a.gl_date <> b.deal_date
  and a.gl_count <> b.unit_count
  and a.seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id)    
  and b.seq = (
    select max(seq)
    from sls.deals
    where store_code = b.store_code
      and bopmast_id = b.bopmast_id);
        
-- 30745X: xfm_deals.deal_status <> deals.deal_status_code (deal_status)
-- new row: all values from xfm_deals 
select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
  a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
  a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
  a.total_care,
  (select max(seq) + 1 from sls.deals where store_code = b.store_code and bopmast_id = b.bopmast_id),
  case
    when a.gl_date is null then
      (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
    else
      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
  end as year_month,
  gl_count unit_count,
  case 
    when a.gl_date is null then a.capped_date
    else a.gl_date
  end as deal_date,
  case
    when a.deal_status = 'U' then 'capped'
    when a.deal_status = 'A' then 'accepted'
    else 'none'
  end as deal_status
from sls.xfm_deals a
inner join sls.deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
where a.deal_status <> b.deal_status_code
  and a.seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id)    
  and b.seq = (
    select max(seq)
    from sls.deals
    where store_code = b.store_code
      and bopmast_id = b.bopmast_id);

-- type2 rows might be individual for each unique trigger (combination of attributes) for a new row

-- and the type1 changes should just be a replacement of the the most recent deals row with the new xfm row
-- thinking the query

-- 4/25
base query, all rows with changes & all attributes from xfm_deals & deals, with a type1/type2 indicator
this might be easier if i already had a hash in deals that would match the hash in xfm_deals

-- -- count ----------------------------------------------------------------------------------------------------------------
-- -- count by store/new/used
-- select store_code, vehicle_type_code, count(*)
-- from sls.deals
-- group by store_code, vehicle_type_code;
-- 
-- -- intra market wholesales
-- -- ry1: 10  ry2: 8
-- select left(account, 1), count(*)
-- from sls.ext_accounting_deals a
-- left join sls.xfm_deals b on a.control = b.stock_number
-- where b.stock_number is null
--   and a.gl_date between '02/01/2017' and '02/28/2017'
--   and a.gl_description like '%w/s%'
-- group by left(account, 1)
  -- 
  -- fs:         ry1   ry2
  --       used  202    71
  --       new   145    50
  -- 
  -- sls.deals: incl intramarket ws
  --                    ry1   ry2
  --              used  202    72
  --              new   146    51     
  -- 
  -- 
  -- -- not too fucking bad, let's find the diffs
  -- 
  -- -- fuck it, close enough, work with the daily incrementals, don't sweat the initial load
  -- 
  -- ry1 new
  -- select *
  -- from sls.deals
  -- where store_code = 'ry1'
  --   and vehicle_type_code = 'n'
  -- 
  -- select store_code, vehicle_type_code, sale_group_code, sale_type_code, count(*)
  -- from sls.deals
  -- group by store_code, vehicle_type_code, sale_group_code, sale_type_code
  -- order by store_code, vehicle_type_code, sale_group_code, sale_type_code
  -- 
  -- 
  -- select store_code, vehicle_type_code, count(*)
  -- from sls.deals
  -- group by store_code, vehicle_type_code
  -- order by store_code, vehicle_type_code
  -- 
  -- 
  -- 
  -- select count(*)
  -- from fin.fact_fs a
  -- inner join fin.dim_fs b on a.fs_key = b.fs_key
  -- inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  -- inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  -- where year_month = 201702
  --   and store = 'ry1'
  --   and page between 5 and 15
  --   
  -- 
  -- 
  -- select *
  -- from (
  --   select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
  --     a.control, a.amount, b.year_month,  
  --     case when a.amount < 0 then 1 else -1 end as unit_count
  -- 
  -- -- select control    
  --   from fin.fact_gl a
  --   inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  --     and aa.journal_code in ('VSN','VSU')
  --   inner join dds.dim_date b on a.date_key = b.date_key
  --     and b.year_month = 201702
  --   inner join fin.dim_account c on a.account_key = c.account_key
  --   inner join ( -- d: fs gm_account page/line/acct description
  --     select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  --     from fin.fact_fs a
  --     inner join fin.dim_fs b on a.fs_key = b.fs_key
  --       and b.year_month = 201701 -- only need one year_month here to generate page/line/label/accounts
  -- --       and b.page between 5 and 15 and b.line between 1 and 45 -- new
  -- --       and b.page = 16 and b.line between 1 and 14 -- used total
  --       and b.page = 16 and b.line between 1 and 6 -- used retail
  -- --       and b.page = 16 and b.line between 1 and 14 -- used wholesale
  --     inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  --     inner join fin.dim_account e on d.gl_account = e.account
  --       and e.account_type_code = '4'
  --     inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  --       and f.store = 'ry1') d on c.account = d.gl_account      
  --   where a.post_status = 'Y') m   
  -- full outer join (
  -- 
  -- select stock_number, vin, deal_date
  -- from sls.deals
  -- where store_code = 'ry1'
  --   and vehicle_type_code = 'u'
  --   and sale_type_code <> 'w' -- exclude wholesale
  -- group by stock_number, vin, deal_date  
  -- ) n on m.control = n.stock_number
  -- order by m.control  
  -- 
  -- select d.the_date, b.account, c.description, e.journal_code, a.amount
  -- from fin.fact_gl a
  -- inner join fin.dim_account b on a.account_key = b.account_key
  -- inner join fin.dim_gl_description c on a.gl_description_key = c.gl_description_key
  -- inner join dds.dim_date d on a.date_key = d.date_key
  -- inner join fin.dim_journal e on a.journal_key = e.journal_key
  -- where control = '30040xxb'
  -- order by account, the_date
  -- 
  -- 
  -- 
  -- select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
  --   a.control, a.amount, b.year_month,
  --   case when a.amount < 0 then 1 else -1 end as unit_count
  -- from fin.fact_gl a
  -- inner join fin.dim_journal aa on a.journal_key = aa.journal_key
  --   and aa.journal_code in ('VSN','VSU')
  -- inner join dds.dim_date b on a.date_key = b.date_key
  --   and b.year_month = 201702
  -- inner join fin.dim_account c on a.account_key = c.account_key
  -- inner join ( -- d: fs gm_account page/line/acct description
  --   select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  --   from fin.fact_fs a
  --   inner join fin.dim_fs b on a.fs_key = b.fs_key
  --     and b.year_month = 201701 -- only need one year_month here to generate page/line/label/accounts
  --     and b.page between 5 and 15 and b.line between 1 and 45 -- used cars
  --   inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  --   inner join fin.dim_account e on d.gl_account = e.account
  --     and e.account_type_code = '4'
  --   inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
  --     and f.store = 'ry1') d on c.account = d.gl_account      
  -- where a.post_status = 'Y' 
  --   and control in ('29160','28850R','29908')
  -- 
  -- 
  -- select *
  -- from sls.ext_deals
  -- where stock_number = '29908'
-- -- count ----------------------------------------------------------------------------------------------------------------

               
-- misc observations -------------------------------------------------------------------------------------------
-- 3 with a non feb delivery_date
select *
from sls.xfm_deals a
where row_type = 'new'  
  and deal_status = 'U'
  and (
    extract(month from delivery_date) = 2 or
    extract(month from gl_date) = 2)
order by delivery_date


-- status A or none : null gl_date
select distinct deal_status from sls.xfm_deals where gl_date is null

select distinct deal_status from sls.xfm_deals where gl_date = '12/31/9999'

select * from sls.deals where stock_number = '30745X'

