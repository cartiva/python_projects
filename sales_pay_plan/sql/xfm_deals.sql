﻿/*
-- all attributes from sls.ext_deals
-- exclude vehicle details
change 
  hash becomes just the hash of the ext_deals attributes
add
  row_type: new/update
  seq
  primary_sc_change
  secondary_sc_change
  fi_manager_change
  status_change
  gl_date
  gl_count
  gl_date
will need to flag whether shit like gap changes, initial thought are that would be 
  a type 1 cha
  nge  
*/
drop table if exists sls.xfm_deals cascade;
create table sls.xfm_deals (
  run_date date not null,
  store_code citext not null,
  bopmast_id integer not null,
  deal_status citext not null,
  deal_type citext not null,
  sale_type citext not null,
  sale_group citext not null,
  vehicle_type citext not null,
  stock_number citext not null,
  vin citext not null,
  odometer_at_sale integer not null,
  buyer_bopname_id integer not null,
  cobuyer_bopname_id integer not null,
  primary_sc citext not null,
  secondary_sc citext not null,
  fi_manager citext not null,
  origination_date date not null,
  approved_date date not null,
  capped_date date not null,
  delivery_date date not null,
  gap numeric(8,2) not null,
  service_contract numeric(8,2) not null,
  total_care numeric(8,2) not null,
  hash citext,
  row_type citext not null,
  seq integer not null default 1,
  primary_sc_change citext not null default 'none'::citext,
  secondary_sc_change citext not null default 'none'::citext,
  fi_manager_change citext not null default 'none'::citext,
  status_change citext not null default 'none'::citext,
  gl_date date,
  gl_count integer default 0,
  gl_date_change boolean default false,
  constraint xfm_deals_pkey primary key (store_code, bopmast_id, seq));
COMMENT ON COLUMN sls.xfm_deals.run_date is 'script run date - 1';  
COMMENT ON COLUMN sls.xfm_deals.store_code IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.bopmast_id IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.deal_status IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.deal_type IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.sale_type IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.sale_group IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.vehicle_type IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.stock_number IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.vin IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.odometer_at_sale IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.buyer_bopname_id IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.cobuyer_bopname_id IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.primary_sc IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.secondary_sc IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.fi_manager IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.origination_date IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.approved_date IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.capped_date IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.delivery_date IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.gap IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.service_contract IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.total_care IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.hash IS 'source: sls.ext_deals';
COMMENT ON COLUMN sls.xfm_deals.row_type is 'new or update';
create index on sls.xfm_deals(hash);


-- 4/19/17 the first insert, everything is New
-- 4/23 starting over (again)
-- this might be fucked up, but am going to try it, limit to feb gl date
-- initial load
truncate sls.xfm_deals;
insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin, 
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id, 
  primary_sc, secondary_sc, fi_manager, origination_Date, approved_date, 
  capped_date, delivery_date, gap, service_contract, total_care, 
  row_type, seq, gl_date, gl_count, hash)
select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
  a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin, 
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id, 
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date, 
  a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care, 
  'new', 1, 
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0),
      ( -- generate the hash
        select md5(z::text) as hash
        from (
          select store_code, bopmast_id, deal_status,
            deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
            odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
            primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
            capped_date, delivery_date, gap, service_contract,total_care,
            coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
          from sls.ext_deals x
          left join sls.ext_accounting_deals y on x.stock_number = y.control
          where x.store_code = a.store_code  
            and x.bopmast_id = a.bopmast_id
            and coalesce(y.gl_date, '12/31/9999') = coalesce(b.gl_date, '12/31/9999')) z) 
from sls.ext_deals a 
left join sls.ext_accounting_deals b on a.stock_number = b.control
  and b.gl_date = (  -- this was just for the initial load
    select max(gl_date) 
    from sls.ext_accounting_deals
    where control = b.control)
where gl_date between '02/01/2017' and '02/28/2017';    


-- ok, now, what about multiple rows
-- there are only 2: 
--    29340: valid sale in february
--    H9825: valid sale in february
-- max gl_date for both of them
select *
from sls.ext_deals a 
left join sls.ext_accounting_deals b on a.stock_number = b.control 
inner join lateral (
  select control
  from sls.ext_accounting_deals
  where control = a.stock_number
  group by control
  having count(*) > 1) c on a.stock_number = c.control
order by a.stock_number  


truncate sls.ext_bopmast_partial;
truncate sls.ext_deals; 
truncate sls.ext_accounting_deals;
truncate sls.xfm_deals;


select * from sls.ext_bopmast_partial;
select * from sls.ext_deals; 
select * from sls.ext_accounting_deals;
select * from sls.xfm_deals;

-- matches statement for everything !
drop table if exists deals;
create temp table deals as
select account, description, sum(unit_count) as unit_count
from sls.ext_accounting_deals
where gl_date between '02/01/2017' and '02/28/2017'
group by account, description
order by account
-- details
select store, page, sum(unit_count)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
inner join deals e on c.gl_account = e.account
where year_month = 201702
group by store, page--, line, line_label
order by store, page--, line, line_label

-- store code and stock number are cool
select *
from sls.xfm_deals
where (
  (store_code = 'RY1' and stock_number like 'h%') or
  (store_code = 'ry2' and stock_number not like 'h%'))

select stock_number, capped_date, delivery_date, gl_date, gl_count
from sls.xfm_deals
where deal_status = 'u'  
  and capped_date <> delivery_date


-- deals in accounting but not in xfm
-- should be intramarket and unwound in feb (h8507b)
-- and that is the case
-- intramarket is going to be a bitch, there is no record in bopmast for them
-- so accounting will (of course) match the statement, but deals will not
-- it would be possible to mock up the deal... 
select *
from sls.ext_accounting_deals a
left join sls.xfm_deals b on a.control = b.stock_number
where b.stock_number is null
order by control

-- 4/20 ---------------------------------------------------------------------------------------
-- let's go with dailys
-- 1. separate sls.ext_accounting into its own scripts (sql & py)

-- 4/21 ------------------------------------------------------------------------------------------------------------------------
-- of course, gl_count can change
select * 
from scpp.xfm_deals
where stock_number in (
  select stock_number 
  from (
    select stock_number, gl_count
    from scpp.xfm_deals
    where gl_count is not null
    group by stock_number, gl_count) x group by stock_number having count(*) > 1)
order by stock_number, seq

-- in fact, almost anyfuckingthing can change
-- thinking, the hash needs to be at the xfm level, don't need it at the extract level
-- also, don't need the "change" attributes, there will be a more inclusive and specific
-- process for that later
alter table sls.xfm_deals
drop column hash;

alter table sls.xfm_deals
add column hash citext;

DROP INDEX sls.xfm_deals_hash_idx;
create unique index on sls.xfm_deals(hash);

-- 4/26 hash can not be unique
-- 3/21 xfm_deals.hash can not be unique: 29523R: capped 3/13, uncapped 3/14, capped 3/21: no changes
--            hash same as 3/13 row, legitimately
DROP INDEX sls.xfm_deals_hash_idx;
create index on sls.xfm_deals(hash);


alter table sls.xfm_deals
alter column hash set not null;

alter table sls.xfm_deals
drop column primary_sc_change,
drop column secondary_sc_change,
drop column fi_manager_change,
drop column gl_date_change,
drop column status_change;

update sls.xfm_deals a
set hash = x.hash
from (
  select store_code, bopmast_id, md5(c::text) as hash
  from (
    select a.store_code, a.bopmast_id, a.deal_status,
      a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
      a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
      coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0)
    from sls.ext_deals a
    left join sls.ext_accounting_deals b on a.stock_number = b.control
      and b.gl_date = (
        select max(gl_date)
        from sls.ext_accounting_deals
        where control = b.control)) c) x
where a.store_code = x.store_code
  and a.bopmast_id = x. bopmast_id;

-- new rows
-- 9 new rows  this looks like it will work
-- this will always include just one more days worth of data than the last scrape
--  so, for a new row, there will never be more data for a store/bopmast_id than a single day
--  no need to filter for most recent date in ext acct like below in the changed rows
-- 4/22
-- well fuck me, for 3/2 getting more than one row returned by a subquery
-- *a* guess i need to filter ext_Acct for most recent date after all, at least in the hash generator
-- 4/23 starting over (again)
-- might be fucked up, but limit gl_date >= 2/1
-- this is probably a bad idea, but fuck it, don't want to fuck with history
-- delete from sls.xfm_deals where store_code = 'ry2' and bopmast_id = 12852
--   delete from sls.xfm_deals where store_code = 'ry2' and bopmast_id =  12754
-- *b*since sls.ext_accounting_deals pk is control & date, i feel safe in limiting to the latest date
-- since earlier rows will have been processed earlier
select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
  a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
  a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
  'new', 1, 
  coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
  ( -- generate the hash
    select md5(z::text) as hash
    from (
      select store_code, bopmast_id, deal_status,
        deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
        odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
        primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
        capped_date, delivery_date, gap, service_contract,total_care,
        coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
      from sls.ext_deals x
      left join sls.ext_accounting_deals y on x.stock_number = y.control
        -- *a*
        and y.gl_date = (
          select max(gl_date)
          from sls.ext_accounting_deals
          where control = y.control)
      where x.store_code = a.store_code
        and x.bopmast_id = a.bopmast_id) z)
from sls.ext_deals a
left join sls.ext_accounting_deals b on a.stock_number = b.control
  and b.gl_date = ( -- *b*
    select max(gl_date)
    from sls.ext_accounting_deals
    where control = b.control)
where gl_date >= '02/01/2017'
  and not exists (
    select *
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id) 
    


     
-- change rows , 3 of them
-- 2 rows for 12668 (HH9825) do i need just the most recent row from ext_acc
-- yes only ever interested in the most recent change
-- after fixing the above update to use just the most recent row from ext_accounting, 3 changed rows
-- 4/22 need to add max(seq) to xfm_deals
-- inner join ext to xfm on store/bopmast_id
drop table if exists changed_rows;
create temp table changed_rows as
select 'run date'::text as run_date, e.store_code, e.bopmast_id, e.deal_status, e.deal_type,
  e.sale_type, e.sale_group, e.vehicle_type, e.stock_number, e.vin, 
  e.odometer_at_sale, e.buyer_bopname_id, e.cobuyer_bopname_id, 
  e.primary_sc, e.secondary_sc, e.fi_manager, e.origination_date, 
  e.approved_date, e.capped_date, e.delivery_date, e.gap, 
  e.service_contract, e.total_care,
  'update'::citext, 
  (
    select max(seq) + 1
    from sls.xfm_deals
    where store_code = e.store_code
      and bopmast_id = e.bopmast_id) as seq,
   e.gl_date, e.gl_count, e.hash
from (
  select z.*, md5(z::text) as hash
  from (
    select store_code, bopmast_id, deal_status,
      deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
      primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
      capped_date, delivery_date, gap, service_contract,total_care,
      coalesce(gl_date, '12/31/9999') as gl_date, coalesce(unit_count, 0) as gl_count
    from sls.ext_deals x
    left join sls.ext_accounting_deals y on x.stock_number = y.control
      and y.gl_date = (
        select max(gl_date)
        from sls.ext_accounting_deals
        where control = y.control)) z) e
inner join sls.xfm_deals f on e.store_code = f.store_code
  and f.seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = f.store_code
      and bopmast_id = f.bopmast_id) 
  and e.bopmast_id = f.bopmast_id
  and e.hash <> f.hash;

select * from changed_rows

-- this compares the rows for changed records
-- what changed
-- H9535A: olny gl_date and gl_count
-- thinking, for this stage, don't care what changed, just that something did
-- will filter/classify in next stage where i am comparing the latest version
-- of the xfm row to the latest version of the deal row
select 'xfm' as source, run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  gl_date, gl_count
-- select *  
from sls.xfm_deals a
where bopmast_id in (select bopmast_id from changed_rows)
union
select 'ext', x.run_date, store_code, bopmast_id, deal_status,
  deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
  odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
  primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
  capped_date, delivery_date, gap, service_contract,total_care,
  coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
from sls.ext_deals x
left join sls.ext_accounting_deals y on x.stock_number = y.control
  and y.gl_date = (
    select max(gl_date)
    from sls.ext_accounting_deals
    where control = y.control)
where bopmast_id in (select bopmast_id from changed_rows)
order by bopmast_id, source desc   
      
-- looks like i have to think about which attributes changing are of interest
-- hmm that might be the next phase (for update)

-- deleted rows ------------------------------------------------------------------------------------------------
-- deal (store/bopmast_id) no longer exist in ext
30157A missed it on 3/6, 

select a.*, c.*
from sls.xfm_deals a
left join sls.ext_deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
left join sls.ext_accounting_deals c on a.stock_number = c.control
  and c.gl_date = (
    select max(gl_date)
    from sls.ext_accounting_deals
    where control = c.control)
where b.store_code is null

change:
  deal_status -> none
  row_type = deleted
  seq = max(seq) + 1
  gl_date = c.gl_date
  gl_count = c.unit_count
  hash = new hash

-- insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
--   deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
--   odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
--   primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
--   capped_date, delivery_date, gap, service_contract, total_care,
--   row_type, seq,
--   gl_date, gl_count, hash) 

select '{0}', a.store_code, a.bopmast_id, 'deleted'::citext as deal_status,
  a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
  a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
  a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
  a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
  'update'::citext as row_type,
  (select max(seq) + 1 from sls.xfm_deals where store_code = a.store_code and bopmast_id = a.bopmast_id) as seq,
  c.gl_date, c.unit_count,
  ( -- generate a new hash
    select md5(z::text) as hash
    from (
      select store_code, bopmast_id, 'deleted',
        deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
        odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
        primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
        capped_date, delivery_date, gap, service_contract,total_care,
        gl_date, gl_count
      from sls.xfm_deals k
      where store_code = a.store_code
        and bopmast_id = a.bopmast_id
        and seq = (
          select max(seq)
          from sls.xfm_deals
          where store_code = k.store_code
            and bopmast_id = k.bopmast_id)) z)
from sls.xfm_deals a
left join sls.ext_deals b on a.store_code = b.store_code
  and a.bopmast_id = b.bopmast_id
left join sls.ext_accounting_deals c on a.stock_number = c.control
  and c.gl_date = (
    select max(gl_date)
    from sls.ext_accounting_deals
    where control = c.control)
where b.store_code is null -- no longer in extract
  and a.seq = (
    select max(seq)
    from sls.xfm_deals
    where store_code = a.store_code
      and bopmast_id = a.bopmast_id)
  and a.deal_status <> 'deleted';

                   
-- deleted rows ------------------------------------------------------------------------------------------------

-- 4/22 -------------------------------------------------------------------------------------
starting over (again)
3/1
  9 new rows
  6 changed rows
    H9535A: gl_date = 3/1, but no cap date, in UI, deal capped 3/3, deal_status still A
    29577B: deal status none to capped
    29503B: deal status none to capped, total_care from 25 to 0
    30404: only change is orig date, deal status still none
    29655: cap date & approv date change from 2-28 to 3-1, no change in gl_date
    30635: cap date & approv date change from 2-28 to 3-1, no change in gl_date

all changed rows go into xfm_deals    

3/2 new rows is throwing an error
duplicate key value violates unique constraint "xfm_deals_pkey"
DETAIL:  Key (store_code, bopmast_id, seq)=(RY2, 12895, 1) already exists.

-- 4/23
12895 = H8507B
the issue is 2 rows in ext_Acounting_deals
1/31 sold to varriano
2/6 unwound from varriano
3/2 sold to kurtti

the 3/2 sale is not yet in ext_accounting
why not?
somehow, the 3/2 row is not getting inserted into ext_accounting
ok figured that out, in the ext_accounting script in the exists clause (replace non existent the_date with gl_date)

but why is there no row in xfm fo H8507b
start over again

