﻿5/14/17
really having a hard time sorting this all out
april payroll went ok, but before the end of the month, i hope to have all the necessary data generating on a daily basis.
having a hard to sorting out what is stored and what gets generated
thought i was good with deals_for_display.sql which was to be  the canonical data for car deals for a given month 

so what about split deals and pay plan specific stats, new cadillac, etc
need to think, i believe, 

5/15
deals_for_display
  this is deal level, unit count is an integer

******
payroll, hoping to institute non batching for shit like pto, but fat chance of that, so the issue becomes
what "real time" processes need to be run to incorpsorate last minute changes into payroll
  clock hours: ads
  fact_gl
  split deals
  


maybe not deals_for_display, but deals_by_month
it is not a fact table, contains all the dimensional text, not keys
it will be the foundation table for generating payroll, i believe
need additional tables
  front gross
  fi gross

***
after deal recon: is that going to show up in VSN/VSU or a different journal, thinking about sfe 
showing up in gje
when i get to front gross/cogs, look for cogs in month after car deal month
28128XXZ: sold 201704, service work done in 201705: control 28128XXZ, acct 1647009, journal SVI

***
test for resuscitated deleted deal
-- to the possible likely extent that this query would show any resuscitated deleted deals, 29162A is the only one (41887)
-- and it has been fixed
select *
from (
  select bopmast_id, max(seq) as max_seq
  from sls.xfm_deals
  group by bopmast_id) a
inner join (
  select bopmast_id, seq
  from sls.xfm_deals
  where deal_status = 'deleted') b on a.bopmast_id = b.bopmast_id and a.max_seq <> b.seq

-- this might be an interesting test to run on an ongoing basis
-- checking for irregularites
-- compare xfm to deals month/count
-- 29951R
--   accounting:
--     4/29 sold - ramos
--     5/2  unwound - ramos
--     5/10 sold - lemar
-- shows as a 0 in xfm because there is a row in xfm for lemar on 5/10 with deal status A and unit-count = -1
-- do not know how i could have prevented this or even if i should, this is an example of an accounting row with
-- only a resolution of stocknumber being applied to a different deal
select b.*, c.*, (select distinct stock_number from sls.deals where bopmast_id = c.bopmast_id)
from (
  select bopmast_id, year_month, sum(gl_count) as unit_count
  from (
    select distinct bopmast_id, (extract(year from gl_Date) * 100) + extract(month from gl_date) as year_month, gl_count
    from sls.xfm_deals
    where stock_number <> '29951R') a
  group by bopmast_id, year_month) b
full outer join (
  select bopmast_id, year_month, sum(unit_count)::integer as unit_count
  from sls.deals
  group by bopmast_id, year_month) c on b.bopmast_id = c.bopmast_id and b.year_month = c.year_month
where b.unit_count <> c.unit_count


create table sls.open_month (
year_month integer primary key);  
insert into sls.open_month values(201705);

create table sls.payroll_guarantees (
employee_number citext not null,
year_month integer not null,
amount numeric(8,2) not null,
constraint payroll_guarantees_pkey primary key (employee_number,year_month));

insert into sls.payroll_guarantees(employee_number,year_month,amount) values
('137220',201705,6500),
('1124625',201705,6000),
('148080',201705,5000),
('17534',201705,10000),
('184625',201705,7000);

5/26/17
per Ben C, $3000 minimum for consultants


select * from sls.payroll_guarantees

insert into sls.payroll_guarantees (employee_number, year_month, amount)
select a.employee_number, (select year_month from sls.months where open_closed = 'open'), 3000
-- select a.*, b.payplan, c.amount as guarantee
from sls.personnel a
inner join sls.personnel_payplans b  on a.employee_number = b.employee_number
  and b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
  and thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')  
left join sls.payroll_guarantees c on a.employee_number = c.employee_number   
where payplan not in ('team_leader','hourly')
  and c.amount is null

***********************
if a consultant is being paid the guarantee and has pto for the month
go over it with ben
also: hourly: no guarantee


*****************************
new month proc:
  add row to sls.payroll_guarantees
  close old month, open new month in sls.months

insert into sls.payroll_guarantees (employee_number,year_month, amount)
select employee_number, 201706, amount 
from sls.payroll_guarantees  
where year_month = 201705



