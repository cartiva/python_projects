﻿need the most recent anniversary to determine the pto_interval which is the basis for the pto_rate
import the months table from scpp
need to add last_day_of_previous_month and first_day_of_next_month

/*
done
insert into sls.months
select * from scpp.months;

select * from sls.months

alter table sls.months
add column last_day_of_previous_month date,
add column first_day_of_next_month date;

update sls.months x
set last_day_of_previous_month = y.last_of_month,
    first_day_of_next_month = y.first_of_month
from (    
  select year_month, previous_year_month, next_year_month,
    (select first_of_month from sls.months where year_month = a.next_year_month),
     (select last_of_month from sls.months where year_month = a.previous_year_month)
  from sls.months a) y
where x.year_month = y.year_month  

update sls.months
set open_closed = 'closed';
update sls.months
set open_closed = 'open'
where year_month = 201704;
*/

-- start with what exists in scpp
-- hmm, where the fuck did i get sls.personnel.start_date?
-- /sales_people.sql: edwEmployeeDim.hiredate
-- feeling gitchy about hire dates, do pymast in ext_arkona, going to need it anyway
-- ok, start date is not necessarily the same as hire date
-- eg josh solie, hired 12/29/14, started in sales 3/20/17


-- 5/26
-- believe i should add a hire date to personnel for pto anniversary purposes
-- currently the best anniv source is ptoemployees
-- did an csv -> excel from ptoemployees
-- to get dates to work in excel needed:  =TEXT(B2,"mm/dd/yyyy"), otherwise the concat insert statement
-- represented the dates as an integer
/* DONE
create table sls.tmp_pto_anniversaries (
employee_number citext primary key,
pto_anniversary date, 
anniversary_type citext);

alter table sls.personnel
add column anniversary_date date;

update sls.personnel x
set anniversary_date = z.pto_anniversary
from (
  select b.*
  from sls.personnel a
  left join sls.tmp_pto_anniversaries b on a.employee_number = b.employee_number) z
where x.employee_number = z.employee_number;  
*/

-----------------------
-- most recent anniversary
-----------------------
update sls.months set open_closed = 'closed';
update sls.months set open_closed = 'open' where year_month = 201704;

-- pto period

drop table if exists sls.pto_interval_dates cascade;
create table sls.pto_interval_dates ( 
  year_month integer not null,
  employee_number citext not null,
  anniversary_date date not null,
  most_recent_anniversary date not null,
  from_date date not null,
  thru_date date not null,
  constraint pto_interval_dates_pkey primary key (year_month, employee_number));

-- how likely is the pto rate to change, not at all, it is based on old paychecks
-- so, do it like scpp, check team members for open month that do not have a row in pto_intervals for the open month
-- if there are none, we are done
-- if there are any, populate sls.pto_interval_dates, join that to paycheck info and insert into sls.pto_intervals

insert into sls.pto_interval_dates
select year_month, employee_number, anniversary_date, most_recent_anniversary,
 -- select v.*,  
  case
    when anniversary_date = most_recent_anniversary then anniversary_date
  else (
    select first_of_month
    from sls.months
    WHERE seq = (
      select seq - 12
      from sls.months
      where v.most_recent_anniversary between first_of_month and last_of_month))
  end as pto_period_from,
  case
    when anniversary_date = most_recent_anniversary then anniversary_date
  else (
    select last_of_month
    from sls.months
    WHERE seq = (
      select seq -1
      from sls.months
      where v.most_recent_anniversary between first_of_month and last_of_month))
  END as pto_period_thru  
from ( -- v: 1 row per employee with most recent anniversary and no row in pto_intervals for open month
  select (select year_month from sls.months where open_closed = 'open'),
    last_name, first_name, employee_number, anniversary_date, 
    case -- most recent anniversary
      when anniversary_date = to_date then anniversary_date
      when to_date < current_date then to_date
      when to_date > current_date then (to_date - interval '1 year'):: date
    end as most_recent_anniversary
  from (-- u: 1 row per employee with anniversary date for current year, with no row in pto_intervals for open month
    select c.*,
      to_date(extract(year from current_date)::text ||'-'||extract(month from c.anniversary_date)::text 
        ||'-'|| extract(day from c.anniversary_date)::text, 'YYYY MM DD' ) -- current year anniversary date
    from ( --c: personnel assigned to teams in open month
      select a.*
      from sls.personnel a
      inner join sls.team_personnel b  on a.employee_number = b.employee_number
      where b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
        and thru_date > (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
    left join sls.pto_intervals d on c.employee_number = d.employee_number
      and d.year_month = (select year_month from sls.months where open_closed = 'open')
    where d.employee_number is null) u) v;

------------------------------------------
-- pto rate
------------------------------------------
insert into sls.pto_intervals (employee_number,year_month,from_date,thru_date,most_recent_anniv,pto_rate)
select c.employee_number, c.year_month, c.from_date, c.thru_date, c.most_recent_anniversary,
  coalesce(x.pto_rate, 0) as pto_rate
from sls.pto_interval_dates c
left join sls.pto_intervals d on c.employee_number = d.employee_number
  and d.year_month = (select year_month from sls.months where open_closed = 'open')
left join (
  select employee_number, year_month, total_gross,
    -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
    round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate, adj_count
  from  (
    select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
      count(*),
      case
        when count(*) >= 24 then 24
        else count(*)
      end as adj_count
    from sls.pto_interval_dates a
    left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
      and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
        between a.from_date and a.thru_date
    where a.from_date <> a.thru_date
      and b.seq_void = '00'
    group by a.employee_number, a.year_month) e) x on  c.employee_number = x.employee_number
where d.employee_number is null;

























