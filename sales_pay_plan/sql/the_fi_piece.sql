﻿drop table if exists p7;
create temp table p7 as 
select d.*, b.description as account_description, 
  c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join fin.dim_account b on a.account_key = b.account_key
inner join dds.dim_date c  on a.date_key = c.date_key
  and c.year_month = 201703
inner join (
  select store, b.page, b.line, b.line_label, col, d.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201703
    and b.page = 17
    and b.line between 1 and 20
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
--     and c.store = 'ry1'
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  group by store, b.page, b.line, b.line_label, col, d.gl_account
  having sum(amount) <> 0) d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
where post_status = 'Y'  
order by page, line;

-- double check totals
select line, sum(amount)
from p7
where store = 'ry1'
group by line
order by line

select store, line, line_label, gl_account, account_description
from p7
group by store, line, line_label, gl_account, account_description
order by store, line, gl_account


select * 
from p7
where store = 'ry1'
  and line = 3
order by store, line, gl_account, control


select gl_Account, sum(amount) 
from p7
where store = 'ry1'
  and line = 3
group by gl_account  
order by store, line, gl_account, control

select *
from p7 a
left join sls.deals b on a.control = b.stock_number

-- by store/line/col 

select store, line, col, sum(amount)
from p7 a
group by store, line, col
order by store, line, col

-- believe this is it
-- looks ok compared to fs, total fi minus chargebacks (line 3,13) and fi comp(lines 9,19)
select store, sum(gross)
from (
  select store, control, 
    sum(case when line in (1,2,6,7,11,12,16,17) then amount end) as gross
  from p7 a
  group by store, control) x
where gross is not null  
group by store
order by store, control

select * 
from (
  select store, control, 
    sum(case when line in (1,2,6,7,11,12,16,17) then amount end) as gross
  from p7 a
  group by store, control) x
where gross is not null  
