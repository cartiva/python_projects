﻿-- -- -- -- drop table if exists step_1;
-- -- -- -- create temp table step_1 as
-- -- -- -- select a.*, b.deal_type_code, b.sale_type_code, b.sale_group_code, b.vehicle_type_code,
-- -- -- --     b.stock_number, b.vin, b.odometer_at_sale, b.buyer_bopname_id,
-- -- -- --     b.primary_sc, b.secondary_sc, b.fi_manager,
-- -- -- --     b.gap, b.service_contract, b.total_care
-- -- -- -- from (
-- -- -- --   select store_code, bopmast_id, max(seq) as seq, sum(unit_count) as unit_count
-- -- -- --   from sls.deals
-- -- -- --   where year_month = 201703
-- -- -- --   group by store_code, bopmast_id) a
-- -- -- -- left join (
-- -- -- -- select store_code, bopmast_id, seq, unit_count, deal_type_code, sale_type_code, sale_group_code, vehicle_type_code,
-- -- -- --     stock_number, vin, odometer_at_sale, buyer_bopname_id,
-- -- -- --     primary_sc, secondary_sc, fi_manager,
-- -- -- --     gap, service_contract, total_care
-- -- -- --   from sls.deals
-- -- -- --   where year_month = 201703) b on a.store_code = b.store_code
-- -- -- --     and a.bopmast_id = b.bopmast_id
-- -- -- --     and a.seq = b.seq 
-- -- -- -- order by a.bopmast_id, a.seq;
-- -- -- -- 
-- -- -- -- 
-- -- -- -- drop table if exists step_2;
-- -- -- -- create temp table step_2 as
-- -- -- -- select a.*, 
-- -- -- --   case when service_contract > 0 then true else false end as has_service_contract,
-- -- -- --   case when gap > 0 then true else false end as has_gap,
-- -- -- --   case when total_care > 1599 then true else false end as has_total_care,
-- -- -- --   case when sale_type_code = 'L' then true else false end as is_lease,
-- -- -- --   case when sale_group_code = 'CAC' then true else false end as is_new_cadillac,
-- -- -- --   case when odometer_at_sale < 100000 then true else false end as is_qualified
-- -- -- -- from step_1 a;
-- -- -- -- 
-- -- -- -- 
-- -- -- -- drop table if exists p7;
-- -- -- -- create temp table p7 as 
-- -- -- -- select d.*, b.description as account_description, 
-- -- -- --   c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
-- -- -- -- from fin.fact_gl a
-- -- -- -- inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- -- -- -- inner join fin.dim_account b on a.account_key = b.account_key
-- -- -- -- inner join dds.dim_date c  on a.date_key = c.date_key
-- -- -- --   and c.year_month = 201703
-- -- -- -- inner join (
-- -- -- --   select store, b.page, b.line, b.line_label, col, d.gl_account
-- -- -- --   from fin.fact_fs a
-- -- -- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- -- --     and b.year_month = 201703
-- -- -- --     and b.page = 17
-- -- -- --     and b.line between 1 and 20
-- -- -- --   inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
-- -- -- -- --     and c.store = 'ry1'
-- -- -- --   inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- -- -- --   group by store, b.page, b.line, b.line_label, col, d.gl_account
-- -- -- --   having sum(amount) <> 0) d on b.account = d.gl_account
-- -- -- -- inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
-- -- -- -- where post_status = 'Y'  
-- -- -- -- order by page, line;
-- -- -- -- 
-- -- -- -- drop table if exists fi;
-- -- -- -- create temp table fi as
-- -- -- -- select * 
-- -- -- -- from (
-- -- -- --   select store, control, 
-- -- -- --     sum(case when line in (1,2,6,7,11,12,16,17) then -1 * amount end) as fi_gross
-- -- -- --   from p7 a
-- -- -- --   group by store, control) x
-- -- -- -- where fi_gross is not null;  
-- -- -- -- 
-- -- -- -- drop table if exists step_3;
-- -- -- -- create temp table step_3 as
-- -- -- -- select a.*, coalesce(b.fi_gross, 0) as fi_gross, coalesce(c.front_gross, 0) as front_gross
-- -- -- -- from step_2 a
-- -- -- -- left join fi b on a.stock_number = b.control
-- -- -- -- left join ( -- front gross
-- -- -- --   select control, sum(-1 * a.amount) as front_gross
-- -- -- --   from fin.fact_gl a
-- -- -- --   inner join dds.dim_date b on a.date_key = b.date_key
-- -- -- --   inner join fin.dim_account c on a.account_key = c.account_key
-- -- -- --   inner join ( -- new car accounts
-- -- -- --     select distinct d.gl_account
-- -- -- --     from fin.fact_fs a
-- -- -- --     inner join fin.dim_fs b on a.fs_key = b.fs_key
-- -- -- --       and b.year_month = 201703
-- -- -- --       and (
-- -- -- --         (b.page between 5 and 15) or -- new cars
-- -- -- --         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- -- -- --     inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
-- -- -- --   where a.post_status = 'Y'
-- -- -- --     and b.year_month = 201703
-- -- -- --     and c.store_code = 'RY1'
-- -- -- --   group by control) c on a.stock_number = c.control;    
-- -- -- -- 
-- -- -- --  
-- -- -- -- -- personnel, teams, payplans
-- -- -- -- create temp table folks as
-- -- -- -- select a.team, b.employee_number, c.last_name, c.first_name, c.ry1_id, c.ry2_id, c.fi_id, d.payplan
-- -- -- -- from sls.teams a
-- -- -- -- inner join sls.team_personnel b on a.team = b.team
-- -- -- -- inner join sls.personnel c on b.employee_number = c.employee_number
-- -- -- -- inner join sls.personnel_payplans d on c.employee_number = d.employee_number
-- -- -- -- inner join sls.payplans e on d.payplan = e.payplan
-- -- -- -- order by team, payplan; 
-- -- 
-- -- 
-- -- select * from folks
-- -- 
-- -- select * from step_3 -- 501
-- -- 
-- -- select * from sls.deals where year_month = 201703 -- 511
-- -- 
-- -- select store_code, bopmast_id, seq from sls.deals where year_month = 201703
-- -- except
-- -- select store_code, bopmast_id, seq from step_3
-- -- 
-- -- select * 
-- -- from sls.deals a
-- -- inner join (
-- --   select store_code, bopmast_id, seq from sls.deals where year_month = 201703
-- --   except
-- --   select store_code, bopmast_id, seq from step_3) b on a.store_code = b.store_code and a.bopmast_id = b.bopmast_id and a.seq = b.seq
-- -- order by stock_number
-- -- 
-- -- select * from step_1 where bopmast_id = 41043
-- -- 
-- -- select *
-- -- from folks a
-- -- left join sls.deals b on b.primary_sc in (a.ry1_id, a.ry2_id)
-- --   or b.secondary_sc in  (a.ry1_id, a.ry2_id)
-- -- where a.payplan not in ('team_leader','hourly')
-- --   and a.ry2_id <> 'none'
-- -- order by team, last_name
-- -- 
-- -- drop table if exists step_1;
-- -- -- all 511 deals with personnel info
-- -- create temp table step_1 as
-- -- select a.store_code, a.bopmast_id, a.sale_type_code, a.sale_group_code, a.vehicle_type_code,
-- --   a.stock_number, a.vin, a.odometer_at_sale, a.buyer_bopname_id, 
-- --   coalesce(b.employee_number, 'none') as psc_emp, coalesce(b.last_name, 'none') as psc,
-- --   coalesce(c.employee_number, 'none') as ssc_emp, coalesce(c.last_name, 'none') as ssc,
-- --   coalesce(d.employee_number, 'none') as fi_emp, coalesce(d.last_name, 'none') as fi, 
-- --   a.seq, a.unit_count
-- -- from sls.deals a
-- -- left join sls.personnel b on a.primary_sc = 
-- --   case
-- --     when a.store_code = 'ry1' then b.ry1_id
-- --     when a.store_code = 'ry2' then b.ry2_id
-- --   end 
-- -- left join sls.personnel c on a.secondary_sc <> 'none'
-- --   and a.secondary_sc = 
-- --     case
-- --       when a.store_code = 'ry1' then c.ry1_id
-- --       when a.store_code = 'ry2' then c.ry2_id
-- --     end   
-- -- left join sls.personnel d on a.store_code = d.store_code 
-- --   and a.fi_manager = d.fi_id
-- -- where year_month = 201704
-- -- order by stock_number, seq;
-- -- 
-- -- select * from step_1
-- -- 
-- -- -- fi ---------------------------------------------------------------------------------------
-- -- drop table if exists p7;
-- -- create temp table p7 as 
-- -- select d.*, b.description as account_description, 
-- --   c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
-- -- from fin.fact_gl a
-- -- inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- -- inner join fin.dim_account b on a.account_key = b.account_key
-- -- inner join dds.dim_date c  on a.date_key = c.date_key
-- --   and c.year_month = 201703
-- -- inner join (
-- --   select store, b.page, b.line, b.line_label, col, d.gl_account
-- --   from fin.fact_fs a
-- --   inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --     and b.year_month = 201703
-- --     and b.page = 17
-- --     and b.line between 1 and 20
-- --   inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
-- --   inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- --   group by store, b.page, b.line, b.line_label, col, d.gl_account
-- --   having sum(amount) <> 0) d on b.account = d.gl_account
-- -- inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
-- -- where post_status = 'Y'  
-- -- 
-- -- 
-- -- drop table if exists fi;
-- -- create temp table fi as
-- -- select * 
-- -- from (
-- --   select store, control, 
-- --     sum(case when line in (1,2,6,7,11,12,16,17) then -1 * amount end) as fi_gross
-- --   from (
-- --     select d.*, b.description as account_description, 
-- --       c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
-- --     from fin.fact_gl a
-- --     inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- --     inner join fin.dim_account b on a.account_key = b.account_key
-- --     inner join dds.dim_date c  on a.date_key = c.date_key
-- --       and c.year_month = 201703
-- --     inner join (
-- --       select store, b.page, b.line, b.line_label, col, d.gl_account
-- --       from fin.fact_fs a
-- --       inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --         and b.year_month = 201703
-- --         and b.page = 17
-- --         and b.line between 1 and 20
-- --       inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
-- --       inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- --       group by store, b.page, b.line, b.line_label, col, d.gl_account
-- --       having sum(amount) <> 0) d on b.account = d.gl_account
-- --     inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
-- --     where post_status = 'Y') a
-- --   group by store, control) x
-- -- where fi_gross is not null;  
-- -- 
-- -- select * from folks
-- -- 
-- -- -- fi total by team leader (directly from sls.deals, don't need step 1)
-- -- select a.last_name, a.first_name, coalesce(sum(c.fi_gross), 0) as fi_gross
-- -- from folks a
-- -- left join sls.deals b on a.fi_id = b.fi_manager
-- --   and b.year_month = 201703
-- -- left join fi c on b.stock_number = c.control
-- -- where a.payplan = 'team_leader'
-- -- group by a.last_name, a.first_name
-- -- 
-- -- 
-- -- 
-- -- 
-- -- drop table if exists sls.sale_groups;
-- -- create table sls.sale_groups (
-- --   store_code citext not null,
-- --   code citext not null,
-- --   sale_group citext not null,
-- --   constraint sale_groups_pkey primary key (store_code,code));
-- -- 
-- -- insert into sls.sale_groups values
-- -- ('RY1','CAC','NEW CAD CAR'),         
-- -- ('RY1','CAT','NEW CAD TRUCK'),             
-- -- ('RY1','MV1','MV-1'),                       
-- -- ('RY1','NBC','NEW BUICK CAR'),              
-- -- ('RY1','NBT','NEW BUICK TRUCK'),            
-- -- ('RY1','NCC','NEW CHEV CAR'),               
-- -- ('RY1','NCT','NEW CHEV TRUCK'),             
-- -- ('RY1','NGT','NEW GMC'),                    
-- -- ('RY1','UC','USED CAR'),                   
-- -- ('RY1','UCC','USED CAR CERTIFIED'),         
-- -- ('RY1','UT','USED TRUCK'),                 
-- -- ('RY1','UTC','USED TRUCK CERTIFIED'),       
-- -- ('RY2','2HC','NEW HONDA CAR'),         
-- -- ('RY2','2HT','NEW HONDA TRUCK'),       
-- -- ('RY2','2NC','NEW NISSAN CAR'),        
-- -- ('RY2','2NT','NEW NISSAN TRUCK'),      
-- -- ('RY2','HCC','HONDA CERTIFIED CAR'),   
-- -- ('RY2','HCT','HONDA CERTIFIED TRK'),   
-- -- ('RY2','HUC','HONDA USED CAR'),        
-- -- ('RY2','HUT','HONDA USED TRUCK'),      
-- -- ('RY2','NCC','NISSAN CERTIFIED CAR'),  
-- -- ('RY2','NCT','NISSAN CERTIFIED TRK'),  
-- -- ('RY2','NUC','NISSAN USED CAR'),       
-- -- ('RY2','NUT','NISSAN USED TRUCK'),     
-- -- ('RY2','OUC','OTHER USED CAR'),        
-- -- ('RY2','OUT','OTHER USED TRUCK');      
-- -- 
-- -- 
-- -- -- not bad, a reasonable representation 
-- -- -- foundational dataset, both stores
-- -- -- add gross (front and f/i)
-- -- -- add team info
-- -- drop table if exists deals;
-- -- create temp table deals as
-- -- select a.store_code, a.bopmast_id, seq, unit_count,
-- --   case sale_type_code
-- --     when 'F' then 'fleet'
-- --     when 'I' then 'intra_market_ws'
-- --     when 'L' then 'lease'
-- --     when 'R' then 'retail'
-- --     when 'W' then 'wholesale'
-- --   end as sale_type,
-- --   b.sale_group, a.vehicle_type_code as new_used, 
-- --   a.stock_number, a.vin, e.model, a.odometer_at_sale, a.buyer_bopname_id, 
-- --   case 
-- --     when a.store_code = 'ry1' then (select employee_number from sls.personnel where ry1_id = a.primary_sc)
-- --     when a.store_code = 'ry2' then (select employee_number from sls.personnel where ry2_id = a.primary_sc)
-- --   end as primary_sc,
-- --   case 
-- --     when a.store_code = 'ry1' and a.secondary_sc <> 'none' then (select employee_number from sls.personnel where ry1_id = a.secondary_sc and store_code = a.store_code)
-- --     when a.store_code = 'ry2' and a.secondary_sc <> 'none' then (select employee_number from sls.personnel where ry2_id = a.secondary_sc and store_code = a.store_code)
-- --   end as secondary_sc,  
-- --   coalesce((select employee_number from sls.personnel where fi_id = a.fi_manager and store_code = a.store_code), 'none') as fi_manager,
-- --   gap, service_contract, total_care,
-- --   coalesce(c.fi_gross, 0) as fi_gross,
-- --   coalesce(d.front_gross, 0) as front_gross
-- -- -- select *  
-- -- from sls.deals a
-- -- left join sls.sale_groups b on a.store_code = b.store_code and a.sale_group_code = b.code
-- -- left join (-- fi
-- --   select * 
-- --   from (
-- --     select store, control, 
-- --       sum(case when line in (1,2,6,7,11,12,16,17) then -1 * amount end) as fi_gross
-- --     from (
-- --       select d.*, b.description as account_description, 
-- --         c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
-- --       from fin.fact_gl a
-- --       inner join fin.dim_journal aa on a.journal_key = aa.journal_key
-- --       inner join fin.dim_account b on a.account_key = b.account_key
-- --       inner join dds.dim_date c  on a.date_key = c.date_key
-- --         and c.year_month = 201704
-- --       inner join (
-- --         select store, b.page, b.line, b.line_label, col, d.gl_account
-- --         from fin.fact_fs a
-- --         inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --           and b.year_month = 201703
-- --           and b.page = 17
-- --           and b.line between 1 and 20
-- --         inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
-- --         inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
-- --         group by store, b.page, b.line, b.line_label, col, d.gl_account
-- --         having sum(amount) <> 0) d on b.account = d.gl_account
-- --       inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
-- --       where post_status = 'Y') a
-- --     group by store, control) x
-- --   where fi_gross is not null) c on a.store_code = c.store and a.stock_number = c.control
-- -- left join ( -- front gross
-- --   select control, sum(-1 * a.amount) as front_gross
-- --   from fin.fact_gl a
-- --   inner join dds.dim_date b on a.date_key = b.date_key
-- --   inner join fin.dim_account c on a.account_key = c.account_key
-- --   inner join ( -- new car accounts
-- --     select distinct d.gl_account
-- --     from fin.fact_fs a
-- --     inner join fin.dim_fs b on a.fs_key = b.fs_key
-- --       and b.year_month = 201703
-- --       and (
-- --         (b.page between 5 and 15) or -- new cars
-- --         (b.page = 16 and b.line between 1 and 14)) -- used cars
-- --     inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
-- --   where a.post_status = 'Y'
-- --     and b.year_month = 201704
-- -- --     and c.store_code = 'RY1'
-- --   group by control) d on a.stock_number = d.control  
-- -- -- add model for ben
-- -- left join dds.ext_inpmast e on a.stock_number = e.inpmast_stock_number  
-- -- where a.year_month = 201704
-- -- -- order by stock_number
-- -- -- limit 20
-- -- 
-- -- -- -- not sure what i am going to do with this at this time
-- -- -- select * from folks
-- -- -- select * 
-- -- -- from deals a
-- -- -- left join folks b on a.primary_sc = b.employee_number
-- -- 

4/30 just go with sls.payroll_deals

select * from sls.payroll_deals limit 10


-- fi total by team leader 
-- fi gross for any deal on which team leader is the fi manager
select a.last_name, a.first_name, coalesce(sum(b.fi_gross), 0) as fi_gross
from sls.folks a
left join sls.payroll_deals b on a.employee_number = b.fi_manager
where a.payplan = 'team_leader'
group by a.last_name, a.first_name

-- team avg sales per consultant
-- assumption: avg does not include hourly consultants in count of consultants/team
select team, count(*), sum(units) as units, round(sum(units)/count(*), 2) as avg_per_cons
from (
  select team, last_name, sum(unit_count) as units
  from sls.folks a
  left join sls.payroll_deals b on a.employee_number = b.primary_sc
    or a.employee_number = b.secondary_sc
  where b.store_code is not null  -- eliminates any team member with no deals
  group by team, last_name) c
group by team   

-- team front gross inc sfe
select team, sum(front_gross) as team_front_gross, sum(unit_count * c.amount) as team_sfe
from sls.folks a
left join sls.payroll_deals b on a.employee_number = b.primary_sc
  or a.employee_number = b.secondary_sc
left join sls.sfe c on b.vin = c.vin    
where b.store_code is not null
group by team

-- store front gross
-- instead of limiting the month's deals
-- this should be exact from the statement, all contributions to sales and cogs
select c.store_code, sum(-1 * a.amount) as front_gross
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- new car accounts
  select distinct d.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201703
    and (
      (b.page between 5 and 15) or -- new cars
      (b.page = 16 and b.line between 1 and 14)) -- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
where a.post_status = 'Y'
  and b.year_month = 201704
--     and c.store_code = 'RY1'
group by c.store_code

-- store sfe (adds to gross)
select sum(amount) as store_sfe
from sls.sfe a
where exists (
  select 1
  from sls.payroll_deals
  where vin = a.vin) 

-- team volume
select team, sum(unit_count) as units
from sls.folks a
left join sls.payroll_deals b on a.employee_number = b.primary_sc
  or a.employee_number = b.secondary_sc
where b.store_code is not null  
group by team

-- team new car volume
select team, sum(unit_count) as units
from sls.folks a
left join sls.payroll_deals b on a.employee_number = b.primary_sc
  or a.employee_number = b.secondary_sc
where b.store_code is not null  
  and b.new_used = 'N'
  and b.sale_type <> 'wholesale'
group by team

-- store_volume
select sum(unit_count)
-- select *
from sls.payroll_deals
where store_code = 'ry1'
  and sale_type <> 'wholesale'

drop table if exists team_leader_stats;
create temp table team_leader_stats as
select aa.team, aa.avg_per_cons, bb.fi_gross, cc.team_front_gross, cc.team_sfe,
  dd.store_front_gross, hh.store_sfe,
  ee.team_volume, ff.team_new_volume, gg.store_volume
from ( -- avg count/consultant
-- team avg sales per consultant
-- assumption: avg does not include hourly consultants in count of consultants/team
  select team, count(*), sum(units) as units, round(sum(units)/count(*), 2) as avg_per_cons
  from (
    select team, last_name, sum(unit_count) as units
    from sls.folks a
    left join sls.payroll_deals b on a.employee_number = b.primary_sc
      or a.employee_number = b.secondary_sc
    where b.store_code is not null  -- eliminates any team member with no deals
    group by team, last_name) c
  group by team) aa  
left join ( -- fi gross
  -- fi total by team leader 
  -- fi gross for any deal on which team leader is the fi manager
  select team, coalesce(sum(b.fi_gross), 0) as fi_gross
  from sls.folks a
  left join sls.payroll_deals b on a.employee_number = b.fi_manager
  where a.payplan = 'team_leader'
  group by team) bb on aa.team = bb.team
left join ( -- team front gross
  -- team front gross inc sfe
  select team, sum(front_gross) as team_front_gross, sum(unit_count * c.amount) as team_sfe
  from sls.folks a
  left join sls.payroll_deals b on a.employee_number = b.primary_sc
    or a.employee_number = b.secondary_sc
  left join sls.sfe c on b.vin = c.vin    
  where b.store_code is not null
  group by team) cc on aa.team = cc.team    
left join (-- store front gross
  select c.store_code, sum(-1 * a.amount) as store_front_gross
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- new car accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201703
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month = 201704
    and c.store_code = 'RY1'
  group by c.store_code) dd on 1 = 1
left join (-- team volume
  select team, sum(unit_count) as team_volume
  from sls.folks a
  left join sls.payroll_deals b on a.employee_number = b.primary_sc
    or a.employee_number = b.secondary_sc
  where b.store_code is not null  
  group by team) ee on aa.team = ee.team
left join ( -- team new car volume
  select team, sum(unit_count) as team_new_volume
  from sls.folks a
  left join sls.payroll_deals b on a.employee_number = b.primary_sc
    or a.employee_number = b.secondary_sc
  where b.store_code is not null  
    and b.new_used = 'N'
    and b.sale_type <> 'wholesale'
  group by team) ff on aa.team = ff.team
left join (-- store_volume
  select sum(unit_count) as store_volume
  from sls.payroll_deals
  where store_code = 'ry1'
    and sale_type <> 'wholesale') gg on 1 = 1
left join ( -- store sfe    
  -- store sfe (adds to gross)
  select sum(amount) as store_sfe
  from sls.sfe a
  where exists (
    select 1
    from sls.payroll_deals
    where vin = a.vin)) hh on 1 = 1;
      
select * from team_leader_stats

 create table sls.team_leader_pay (
  year_month integer,
  last_name citext,
  first_name citext, 
  employee_number citext,
  team citext,
  avg_per_cons numeric(4,2),
  fi_gross numeric(8,2),
  team_front_gross numeric(8,2),
  team_sfe integer,
  store_front_gross numeric(8,2),
  store_sfe integer,
  team_volume numeric(3,1),
  team_new_volume numeric(3,1),
  store_volume numeric(4,1),
  fi_pay numeric(8,2),
  team_front_gross_pay numeric(8,2),
  store_front_gross_pay numeric(8,2),
  team_volume_bonus numeric(8,2),
  team_new_volume_bonus numeric(8,2),
  store_volume_bonus numeric(8,2),
  total_pay numeric(8,2));

insert into sls.team_leader_pay
select 201704,
  (select last_name from sls.folks where team = b.team and payplan = 'team_leader'),
  (select first_name from sls.folks where team = b.team and payplan = 'team_leader'),
  (select employee_number from sls.folks where team = b.team and payplan = 'team_leader'),
  b.*, 
  coalesce(fi_pay, 0) + coalesce(team_front_gross_pay, 0) + coalesce(store_front_gross_pay, 0) + 
    coalesce(team_volume_bonus, 0) + coalesce(team_new_volume_bonus, 0) + coalesce(store_volume_bonus, 0) as total_pay
from (  
  select a.*,
    round(
      case
        when avg_per_cons < 12 then .07 * fi_gross 
        when avg_per_cons >= 12 then .08 * fi_gross
      end, 2) as fi_pay,
    round(
      case
        when avg_per_cons < 12 then .0225 * (team_front_gross + team_sfe)
        when avg_per_cons >= 12 then .0275 * (team_front_gross + team_sfe)
      end, 2) as team_front_gross_pay,
    round(.0025 * (store_front_gross + store_sfe), 2) as store_front_gross_pay,
    case
      when avg_per_cons < 12 then 25 * team_volume
      when avg_per_cons between 12 and 13 then 35 * team_volume
      when avg_per_cons > 13 then 50 * team_volume
    end as team_volume_bonus,
    30 * team_new_volume as team_new_volume_bonus,
    case
      when store_volume < 351 then 0
      when store_volume between 351 and 375 then 500
      when store_volume between 376 and 400 then 750
      when store_volume > 400 then 1250
    end as store_volume_bonus
  from team_leader_stats a) b
  
      

-- front gross not connected to a deal from this month
select * from deals where stock_number in ('h8864a','h8019a')

select * from sls.deals where year_month = 201704 order by sale_type

select * from sls.payroll_deals order by sale_type

select * from sls.deals where stock_number = '30377'


