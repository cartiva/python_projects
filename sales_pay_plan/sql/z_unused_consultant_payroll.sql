﻿
drop table if exists sls.payroll_deals;
create table sls.payroll_deals (
  store_code citext not null,
  bopmast_id integer not null, 
  seq integer not null, 
  unit_count integer not null,
  sale_type citext not null,
  sale_group citext not null, 
  new_used citext not null,
  stock_number citext not null,
  vin citext not null, 
  model citext,
  odometer_at_sale integer not null,
  buyer_bopname_id integer not null,
  primary_sc citext not null,
  secondary_sc citext,
  fi_manager citext not null,
  gap numeric(8,3) not null,
  service_contract numeric(8,2) not null,
  total_care numeric(8,2) not null,
  fi_gross numeric(8,2) not null,
  front_gross numeric(8,2) not null);

alter table sls.payroll_deals
add column year_month integer;  
update sls.payroll_deals
set year_month = 201704;
-- -- 
-- -- -- drop table if exists deals;
-- -- -- create temp table deals as
-- 4/30, this is important
-- all summed values need to be multiplied by unit_count, eg 30625a
insert into sls.payroll_deals
select a.store_code, a.bopmast_id, seq, unit_count,
  case sale_type_code
    when 'F' then 'fleet'
    when 'I' then 'intra_market_ws'
    when 'L' then 'lease'
    when 'R' then 'retail'
    when 'W' then 'wholesale'
  end as sale_type,
  b.sale_group, a.vehicle_type_code as new_used, 
  a.stock_number, a.vin, e.model, a.odometer_at_sale, a.buyer_bopname_id, 
  case 
    when a.store_code = 'ry1' then (select employee_number from sls.personnel where ry1_id = a.primary_sc)
    when a.store_code = 'ry2' then (select employee_number from sls.personnel where ry2_id = a.primary_sc)
  end as primary_sc,
  case 
    when a.store_code = 'ry1' and a.secondary_sc <> 'none' then (select employee_number from sls.personnel where ry1_id = a.secondary_sc and store_code = a.store_code)
    when a.store_code = 'ry2' and a.secondary_sc <> 'none' then (select employee_number from sls.personnel where ry2_id = a.secondary_sc and store_code = a.store_code)
  end as secondary_sc,  
  coalesce((select employee_number from sls.personnel where fi_id = a.fi_manager and store_code = a.store_code), 'none') as fi_manager,
  gap * unit_count, service_contract * unit_count, total_care * unit_count,
  coalesce(c.fi_gross * unit_count, 0) as fi_gross,
  coalesce(d.front_gross * unit_count, 0) as front_gross
-- select *  
from sls.deals a
left join sls.sale_groups b on a.store_code = b.store_code and a.sale_group_code = b.code
left join (-- fi
  select * 
  from (
    select store, control, 
      sum(case when line in (1,2,6,7,11,12,16,17) then -1 * amount end) as fi_gross
    from (
      select d.*, b.description as account_description, 
        c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
      from fin.fact_gl a
      inner join fin.dim_journal aa on a.journal_key = aa.journal_key
      inner join fin.dim_account b on a.account_key = b.account_key
      inner join dds.dim_date c  on a.date_key = c.date_key
        and c.year_month = 201704
      inner join (
        select store, b.page, b.line, b.line_label, col, d.gl_account
        from fin.fact_fs a
        inner join fin.dim_fs b on a.fs_key = b.fs_key
          and b.year_month = 201703
          and b.page = 17
          and b.line between 1 and 20
        inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
        inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
        group by store, b.page, b.line, b.line_label, col, d.gl_account
        having sum(amount) <> 0) d on b.account = d.gl_account
      inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
      where post_status = 'Y') a
    group by store, control) x
  where fi_gross is not null) c on a.store_code = c.store and a.stock_number = c.control
left join ( -- front gross
  select control, sum(-1 * a.amount) as front_gross
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- new car accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201703
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month = 201705
--     and c.store_code = 'RY1'
  group by control) d on a.stock_number = d.control  
-- add model for ben
left join dds.ext_inpmast e on a.stock_number = e.inpmast_stock_number  
where a.year_month = 201705;
-- -- 
select * from sls.payroll_deals where stock_number = '30625a'
select * from sls.payroll_deals a where exists (select 1 from sls.payroll_Deals where store_code = a.store_code and bopmast_id = a.bopmast_id and unit_count < 1) order by bopmast_id, seq

-- consultant base stats
select a.team, a.last_name, a.first_name, a.employee_number, a.payplan,
  sum(case when b.secondary_sc is null then b.unit_count else 0.5 * b.unit_count end) as unit_count, -- to cover split deal unwind
  sum(case when b.total_care > 1599 then unit_count else 0 end) as total_care,
  sum(case when b.service_contract > 0 then unit_count else 0 end) as service_contract,
  sum(case when b.gap > 0 then unit_count else 0 end) as gap, 
  sum(case when b.sale_type = 'lease' then unit_count else 0 end) as lease,
  sum(case when sale_group in ('new cad car','new cad truck') then unit_count else 0 end) as cadillac,
--     sum(
--       case 
--         when a.payplan <> 'executive' then unit_count * fi_gross 
--         when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('foster','seay')) then unit_count * fi_gross
--         when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('garceau','olderbak')) then (
--           select coalesce(sum(unit_count * fi_gross), 0)
--           from sls.payroll_deals
--           where fi_manager = a.employee_number
--             and store_code = b.store_code
--             and bopmast_id = b.bopmast_id
--             and seq = b.seq)
--         else 0 end) as fi_gross,
    sum(
      case 
        when a.payplan <> 'executive' then fi_gross 
        when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('foster','seay')) then fi_gross
        when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('garceau','olderbak')) then (
          select coalesce(sum(fi_gross), 0)
          from sls.payroll_deals
          where fi_manager = a.employee_number
            and store_code = b.store_code
            and bopmast_id = b.bopmast_id
            and seq = b.seq)
        else 0 end) as fi_gross,
  sum( -- this was tricky, but now works
    case 
      when odometer_at_sale < 100000 then
        case 
          when b.secondary_sc is null then b.unit_count 
          else unit_count * .5
        end
      else 0
    end) as qualified_unit_count,   
  sum(
    case 
      when odometer_at_sale >= 100000 then
        case 
          when b.secondary_sc is null then b.unit_count 
          else unit_count * .5
        end
      else 0
    end) as non_qualified_unit_count         
-- select *
from sls.folks a
left join sls.payroll_deals b on b.primary_sc = a.employee_number
  or b.secondary_sc = a.employee_number
where a.payplan not in ( 'team_leader', 'hourly')
group by a.team, a.last_name, a.first_name, a.employee_number, a.payplan
order by last_name 

drop table if exists sls.consultant_pay;
create table sls.consultant_pay (
  team citext,
  last_name citext,
  first_name citext,
  employee_number citext,
  payplan citext,
  unit_count numeric(3,1),
  unit_pay numeric(8,2),
  fi_pay numeric(8,2),
  total_care numeric(8,2),
  service_contract numeric(8,2),
  gap numeric(8,2),
  leases numeric(8,2),
  new_cadillac numeric(8,2),
  clock_hours numeric(8,2),
  hourly_pay numeric(8,2),
  pto_hours numeric(8,2),
  pto_pay numeric(8,2),
  total_pay numeric(8,2));

alter table sls.consultant_pay
add column year_month integer;

update sls.consultant_pay
set year_month = 201704;  

-- consultant base stats monetized: total pay  
insert into sls.consultant_pay
select team, last_name, first_name, employee_number, payplan, unit_count,
  case 
    when payplan = 'executive' then (200 * qualified_unit_count) + (280 * non_qualified_unit_count)
    when payplan <> 'executive' then 280 * unit_count 
  end as unit_pay,
  case
    when payplan = 'executive' then round(.16 * fi_gross, 2)
    else round(.035 * fi_gross, 2)
  end as fi_pay,
  25 * total_care as total_care, 25 * service_contract as service_contract,
  15 * gap as gap, 25 * leases as leases, 25 * new_cadillac as cadillac,
  0 as clock_hours, 0 as hourly_pay, 0 as pto_hours, 0 as pto_pay,
  case 
    when payplan = 'executive' then 200 * qualified_unit_count + 280 * non_qualified_unit_count
    else 280 * unit_count 
  end +
  case
    when payplan = 'executive' then round(.16 * fi_gross, 2)
    else round(.035 * fi_gross, 2)
  end +
  25 * total_care + 25 * service_contract + 15 * gap + 25 * leases + 25 * new_cadillac as total_pay
from (
  select a.team, a.last_name, a.first_name, a.employee_number, a.payplan,
    sum(case when b.secondary_sc is null then b.unit_count else 0.5 * b.unit_count end) as unit_count, -- to cover split deal unwind
    sum(case when b.total_care > 1599 then unit_count else 0 end) as total_care,
    sum(case when b.service_contract > 0 then unit_count else 0 end) as service_contract,
    sum(case when b.gap > 0 then unit_count else 0 end) as gap, 
    sum(case when b.sale_type = 'lease' then unit_count else 0 end) as leases,
    sum(case when sale_group in ('new cad car','new cad truck') then unit_count else 0 end) as new_cadillac,
--     sum(
--       case 
--         when a.payplan <> 'executive' then unit_count * fi_gross 
--         when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('foster','seay')) then unit_count * fi_gross
--         when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('garceau','olderbak')) then (
--           select coalesce(sum(unit_count * fi_gross), 0)
--           from sls.payroll_deals
--           where fi_manager = a.employee_number
--             and store_code = b.store_code
--             and bopmast_id = b.bopmast_id
--             and seq = b.seq)
--         else 0 end) as fi_gross,
    sum( -- negative unit counts are now accounted for in the generation of sls.payroll_deals
      case 
        when a.payplan <> 'executive' then fi_gross 
        when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('foster','seay')) then fi_gross
        when a.payplan = 'executive' and b.primary_sc in (select employee_number from sls.folks where last_name in ('garceau','olderbak')) then (
          select coalesce(sum(fi_gross), 0)
          from sls.payroll_deals
          where fi_manager = a.employee_number
            and store_code = b.store_code
            and bopmast_id = b.bopmast_id
            and seq = b.seq)
        else 0 end) as fi_gross,
    sum( -- this was tricky, but now works
      case 
        when odometer_at_sale < 100000 then
          case 
            when b.secondary_sc is null then b.unit_count 
            else unit_count * .5
          end
        else 0
      end) as qualified_unit_count,   
    sum(
      case 
        when odometer_at_sale >= 100000 then
          case 
            when b.secondary_sc is null then b.unit_count 
            else unit_count * .5
          end
        else 0
      end) as non_qualified_unit_count         
  -- select *
  from sls.folks a
  left join sls.payroll_deals b on b.primary_sc = a.employee_number
    or b.secondary_sc = a.employee_number
  where a.payplan not in ( 'team_leader', 'hourly') -- order by last_name
  group by a.team, a.last_name, a.first_name, a.employee_number, a.payplan) x;


select * from sls.consultant_pay order by team, last_name

**************************************
!!! hourly, do not forget overtime !!!
**************************************

update sls.consultant_pay -- 4557.66
set pto_hours = 32,
    pto_pay = 1291.84,
    total_pay = total_pay + 1291.84
where last_name = 'warmack';    

update sls.consultant_pay -- 3341.14
set pto_hours = 48,
    pto_pay = 1737.60,
    total_pay = total_pay + 3341.14
where last_name = 'carlson';    

insert into sls.consultant_pay (team,last_name,first_name,employee_number, 
  payplan,unit_count,unit_pay,fi_pay,total_care,service_contract,gap,
  leases,new_cadillac,clock_hours,hourly_pay,pto_hours,pto_pay,total_pay) values 
  ('team weber','Solie','Joshua','1130101', 
  'hourly',0,0,0,0,0,0,
  0,0,155.96,2495.36,0,0,2495.36);

insert into sls.consultant_pay (team,last_name,first_name,employee_number, 
  payplan,unit_count,unit_pay,fi_pay,total_care,service_contract,gap,
  leases,new_cadillac,clock_hours,hourly_pay,pto_hours,pto_pay,total_pay) values 
  ('team anthony','Eastman','Donovan','1212688', 
  'hourly',0,0,0,0,0,0,
  0,0,120,1920,0,0,1920);  

insert into sls.consultant_pay (team,last_name,first_name,employee_number, 
  payplan,unit_count,unit_pay,fi_pay,total_care,service_contract,gap,
  leases,new_cadillac,clock_hours,hourly_pay,pto_hours,pto_pay,total_pay) values 
  ('','Jeanotte','Jarrett','1112233', 
  'hourly',0,0,0,0,0,0,
  0,0,0,0,0,0,0);    


Donovan Eastman: clock shows 1.95 hours, actually clocked in 15 days