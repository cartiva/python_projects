﻿select store_code, bopmast_id, seq, unit_count, deal_type_code, sale_type_code, sale_group_code, vehicle_type_code,
  stock_number, vin, odometer_at_sale, buyer_bopname_id,
  primary_sc, secondary_sc, fi_manager,
  gap, service_contract, total_care
from sls.deals
where year_month = 201703
order by bopmast_id, seq

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
what is the effect the effect of grouping and just taking max(seq)
drop table if exists step_1;
-- max seq row from deals
-- for 201703: deals: 511 rows, step_1: 501 rows
create temp table step_1 as
select a.*, b.deal_type_code, b.sale_type_code, b.sale_group_code, b.vehicle_type_code,
    b.stock_number, b.vin, b.odometer_at_sale, b.buyer_bopname_id,
    b.primary_sc, b.secondary_sc, b.fi_manager,
    b.gap, b.service_contract, b.total_care
-- what is the effect the effect of grouping and just taking max(seq)    
from (
  select store_code, bopmast_id, max(seq) as seq, sum(unit_count) as unit_count
  from sls.deals
  where year_month = 201704
  group by store_code, bopmast_id) a
left join (
select store_code, bopmast_id, seq, unit_count, deal_type_code, sale_type_code, sale_group_code, vehicle_type_code,
    stock_number, vin, odometer_at_sale, buyer_bopname_id,
    primary_sc, secondary_sc, fi_manager,
    gap, service_contract, total_care
  from sls.deals
  where year_month = 201704) b on a.store_code = b.store_code
    and a.bopmast_id = b.bopmast_id
    and a.seq = b.seq 
order by a.bopmast_id, a.seq;


-- rydedata.sepfrns 
select * from sls.deals where sale_group_code = 'cac'

select total_care, count(*) from sls.deals group by total_care order by total_care

select * from step_1 where fi_manager in ('chr','jko')

select * from step_1 where has_total_care = true

  
drop table if exists step_2;
create temp table step_2 as
select a.*, 
  case when service_contract > 0 then true else false end as has_service_contract,
  case when gap > 0 then true else false end as has_gap,
  case when total_care > 1599 then true else false end as has_total_care,
  case when sale_type_code = 'L' then true else false end as is_lease,
  case when sale_group_code = 'CAC' then true else false end as is_new_cadillac,
  case when odometer_at_sale < 100000 then true else false end as is_qualified
from step_1 a;

-- fi ---------------------------------------------------------------------------------------
drop table if exists p7;
create temp table p7 as 
select d.*, b.description as account_description, 
  c.the_date, a.control, a.doc, a.ref, a.amount, aa.journal_code, e.description
from fin.fact_gl a
inner join fin.dim_journal aa on a.journal_key = aa.journal_key
inner join fin.dim_account b on a.account_key = b.account_key
inner join dds.dim_date c  on a.date_key = c.date_key
  and c.year_month = 201704
inner join (
  select store, b.page, b.line, b.line_label, col, d.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201703
    and b.page = 17
    and b.line between 1 and 20
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
--     and c.store = 'ry1'
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  group by store, b.page, b.line, b.line_label, col, d.gl_account
  having sum(amount) <> 0) d on b.account = d.gl_account
inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key  
where post_status = 'Y'  
order by page, line;

drop table if exists fi;
create temp table fi as
select * 
from (
  select store, control, 
    sum(case when line in (1,2,6,7,11,12,16,17) then -1 * amount end) as fi_gross
  from p7 a
  group by store, control) x
where fi_gross is not null;  

select a.*, coalesce(b.fi_gross, 0)
from step_2 a
left join fi b on a.stock_number = b.control

-- don't know what to do about these
select *
from fi a
left join step_2 b on a.control = b.stock_number
where b.stock_number is null 
-- fi ---------------------------------------------------------------------------------------

-- front gross ---------------------------------------------------------------------------------------
drop table if exists step_3;
create temp table step_3 as
select a.*, coalesce(b.fi_gross, 0) as fi_gross, coalesce(c.front_gross, 0) as front_gross
from step_2 a
left join fi b on a.stock_number = b.control
left join ( -- front gross
  select control, sum(-1 * a.amount) as front_gross
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- new car accounts
    select distinct d.gl_account
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201703
      and (
        (b.page between 5 and 15) or -- new cars
        (b.page = 16 and b.line between 1 and 14)) -- used cars
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key) f on c.account = f.gl_account
  where a.post_status = 'Y'
    and b.year_month = 201704
    and c.store_code = 'RY1'
  group by control) c on a.stock_number = c.control;

-- front gross ---------------------------------------------------------------------------------------


-- personnel, teams, payplans
create temp table folks as
select a.team, b.employee_number, c.last_name, c.first_name, c.ry1_id, c.ry2_id, c.fi_id, d.payplan
from sls.teams a
inner join sls.team_personnel b on a.team = b.team
inner join sls.personnel c on b.employee_number = c.employee_number
inner join sls.personnel_payplans d on c.employee_number = d.employee_number
inner join sls.payplans e on d.payplan = e.payplan
order by team, payplan;

select *
from folks a
left join step_3 b on b.primary_sc in (a.ry1_id, a.ry2_id)
  or b.secondary_sc in  (a.ry1_id, a.ry2_id)
where a.payplan not in ('team_leader','hourly')
  and a.ry2_id <> 'none'
order by last_name, stock_number

-- consultant base stats
  select a.team, a.last_name, a.first_name, a.employee_number, a.payplan,
    sum(case when b.secondary_sc = 'none' then b.unit_count else 0.5 * b.unit_count end) as unit_count, -- to cover split deal unwind
    sum(case when b.has_total_care = true then 1 else 0 end) as total_care,
    sum(case when b.has_service_contract = true then 1 else 0 end) as service_contract,
    sum(case when b.has_gap = true then 1 else 0 end) as gap,
    sum(case when b.is_lease = true then 1 else 0 end) as leases,
    sum(case when b.sale_group_code in ('CAC','CAT') then 1 else 0 end) as new_cadillac,
    sum(
      case 
        when a.payplan <> 'executive' then fi_gross 
        when a.payplan = 'executive' and primary_sc in ('fos','sea') then fi_gross
        when a.payplan = 'executive' and primary_sc in ('cga','jol')then (
          select coalesce(sum(fi_gross), 0)
          from step_3
          where fi_manager = a.fi_id
            and store_code = b.store_code
            and bopmast_id = b.bopmast_id
            and seq = b.seq)
        else 0 end) as fi_gross,
  sum( -- this was tricky, but now works
    case 
      when b.is_qualified = true then
        case 
          when b.secondary_sc = 'none' then b.unit_count 
          else .5
        end
      else 0
    end) as qualified_unit_count,   
  sum(
    case 
      when b.is_qualified = false then
        case 
          when b.secondary_sc = 'none' then b.unit_count 
          else .5
        end
      else 0
    end) as non_qualified_unit_count 
  -- select *
  from folks a
  left join step_3 b on b.primary_sc in (a.ry1_id, a.ry2_id)
    or b.secondary_sc in  (a.ry1_id, a.ry2_id)
  where a.payplan not in ('team_leader','hourly')
    and a.ry2_id <> 'none'
  group by a.team, a.last_name, a.first_name, a.employee_number, payplan

-- consultant base stats monetized: total pay  
select team, last_name, first_name, employee_number, payplan, unit_count,
  case 
    when payplan = 'executive' then (200 * qualified_unit_count) + (280 * non_qualified_unit_count)
    when payplan <> 'executive' then 280 * unit_count 
  end as unit_pay,
  case
    when payplan = 'executive' then round(.16 * fi_gross, 2)
    else round(.035 * fi_gross, 2)
  end as fi_pay,
  25 * total_care as total_care, 25 * service_contract as service_contract,
  15 * gap as gap, 25 * leases as leases, 25 * new_cadillac as cadillac,
  case 
    when payplan = 'executive' then 200 * qualified_unit_count + 280 * non_qualified_unit_count
    else 280 * unit_count 
  end +
  case
    when payplan = 'executive' then round(.16 * fi_gross, 2)
    else round(.035 * fi_gross, 2)
  end +
  25 * total_care + 25 * service_contract + 15 * gap + 25 * leases + 25 * new_cadillac as total_pay
from (
  select a.team, a.last_name, a.first_name, a.employee_number, a.payplan,
    sum(case when b.secondary_sc = 'none' then b.unit_count else 0.5 * b.unit_count end) as unit_count, 
    sum(case when b.has_total_care = true then 1 else 0 end) as total_care,
    sum(case when b.has_service_contract = true then 1 else 0 end) as service_contract,
    sum(case when b.has_gap = true then 1 else 0 end) as gap,
    sum(case when b.is_lease = true then 1 else 0 end) as leases,
    sum(case when b.sale_group_code in ('CAC','CAT') then 1 else 0 end) as new_cadillac,
    sum(
      case 
        when a.payplan <> 'executive' then fi_gross 
        when a.payplan = 'executive' and primary_sc in ('fos','sea') then fi_gross
        when a.payplan = 'executive' and primary_sc in ('cga','jol')then (
          select coalesce(sum(fi_gross), 0)
          from step_3
          where fi_manager = a.fi_id
            and store_code = b.store_code
            and bopmast_id = b.bopmast_id
            and seq = b.seq)
        else 0 end) as fi_gross,
  sum( -- this was tricky, but now works
    case 
      when b.is_qualified = true then
        case 
          when b.secondary_sc = 'none' then b.unit_count 
          else .5
        end
      else 0
    end) as qualified_unit_count,   
  sum(
    case 
      when b.is_qualified = false then
        case 
          when b.secondary_sc = 'none' then b.unit_count 
          else .5
        end
      else 0
    end) as non_qualified_unit_count 
  -- select *
  from folks a
  left join step_3 b on b.primary_sc in (a.ry1_id, a.ry2_id)
    or b.secondary_sc in  (a.ry1_id, a.ry2_id)
  where a.payplan not in ('team_leader','hourly')
    and a.ry2_id <> 'none'
  group by a.team, a.last_name, a.first_name, a.employee_number, payplan) x  
order by last_name


select * from step_3 where bopmast_id in (41494,41309,40999)