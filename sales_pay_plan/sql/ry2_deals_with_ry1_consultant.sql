﻿select employee_number, last_name, first_name, per_unit, count(*) as units, count(*) * per_unit as total
from (
  select stock_number, last_name, first_name, b.employee_number,
    case 
      when payplan <> 'executive' then 280
      when payplan = 'executive' and odometer_at_sale < 100000 then 200
      when payplan = 'executive' and odometer_at_sale >= 100000 then 280
    end as per_unit
  from sls.deals a
  inner join sls.personnel b on a.primary_sc = b.ry2_id
  inner join sls.personnel_payplans c on b.employee_number = c.employee_number
  where a.store_Code = 'RY2'
    and a.year_month = 201706) c
group by employee_number, last_name, first_name, per_unit  
order by last_name