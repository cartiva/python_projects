# encoding=utf-8
'''
4/23/17
    removed all null filtering
    fi mgr is in PD_POLICY_NUMBER (whatever), when there is a salesmanager assiged (eg 30259D, 30425XXB, 30784)
    that abbreviation is appended to the fi mgr in PD_POLICY_NUMBER (BENKLA)
    therefore, just take the left 3 of PD_POLICY_NUMBER
'''
import csv
import db_cnx
import datetime

task = 'ext_bopmast'
pg_con = None
ubuntu_con = None
run_id = None
file_name = 'files/ext_bopmast_partial.csv'
# the_date = str(datetime.datetime.now() - datetime.timedelta(days=1))[:10]
# month_day = ((datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%m") +
#             (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%d"))
# print the_date
# print month_day
# CHANGE THE FUCKING DATE
month_day = '0602'
the_date = '2017-06-02'
bopmast_table = 'test.ext_bopmast_' + month_day
try:
    # with db_cnx.arkona_report() as db2_con:
    #     with db2_con.cursor() as db2_cur:
    with db_cnx.postgres_ubuntu() as ubuntu_con:
        with ubuntu_con.cursor() as ubuntu_cur:
            sql = """
                select TRIM(BOPMAST_COMPANY_NUMBER),RECORD_KEY,TRIM(RECORD_STATUS),TRIM(RECORD_TYPE),
                  TRIM(SALE_TYPE),TRIM(FRANCHISE_CODE),TRIM(VEHICLE_TYPE),TRIM(BOPMAST_STOCK_NUMBER),
                  TRIM(BOPMAST_VIN),ODOMETER_AT_SALE,BUYER_NUMBER,CO_BUYER_NUMBER,
                  TRIM(PRIMARY_SALESPERS),TRIM(SECONDARY_SLSPERS2),trim(LEFT(PD_POLICY_NUMBER, 3)),
                  ORIGINATION_DATE,DATE_APPROVED,DATE_CAPPED,DELIVERY_DATE,
                  GAP_PREMIUM,SERV_CONT_AMOUNT,AMO_TOTAL
                from {0}
                where origination_date > 20170000
            """.format(bopmast_table)
            ubuntu_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ubuntu_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate sls.ext_bopmast_partial")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy sls.ext_bopmast_partial from stdin with csv encoding 'latin-1 '""", io)
            pg_cur.execute("truncate sls.ext_deals")
            sql = """
                insert into sls.ext_deals (run_date,store_code,bopmast_id,deal_status,deal_type,sale_type,
                  sale_group,vehicle_type,stock_number,vin,odometer_at_sale,buyer_bopname_id,
                  cobuyer_bopname_id,primary_sc,secondary_sc,fi_manager,origination_date,approved_date,
                  capped_date,delivery_date,gap,service_contract,total_care)
                select {0}::date, a.store,a.bopmast_id,coalesce(a.deal_status, 'none'),a.deal_type,a.sale_type,
                  coalesce(a.sale_group,'none'),a.vehicle_type,a.stock_number,a.vin,a.odometer_at_sale,
                  a.buyer_bopname_id, coalesce(a.cobuyer_bopname_id, -1),
                  case
                    when coalesce(a.primary_sc, 'none') = 'AMA' then 'HAN'
                    when coalesce(a.primary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                    when coalesce(a.primary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                    when coalesce(a.primary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                    when coalesce(a.primary_sc, 'none') = 'JKO' and a.store = 'RY1' then 'JOL'
                    when coalesce(a.primary_sc, 'none') = 'CBJ' and a.store = 'RY2' then 'CHB'
                    when coalesce(a.primary_sc, 'none') = 'SHI' and a.store = 'RY2' then 'NIC'
                    else coalesce(a.primary_sc, 'none')
                  end,
                  case
                    when coalesce(a.secondary_sc, 'none') = 'AMA' then 'HAN'
                    when coalesce(a.secondary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                    when coalesce(a.secondary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                    when coalesce(a.secondary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                    when coalesce(a.secondary_sc, 'none') = 'JKO' and a.store = 'RY1' then 'JOL'
                    when coalesce(a.secondary_sc, 'none') = 'CBJ' and a.store = 'RY2' then 'CHB'
                    when coalesce(a.secondary_sc, 'none') = 'SHI' and a.store = 'RY2' then 'NIC'
                    else coalesce(a.secondary_sc, 'none')
                  end,
                  coalesce(a.fi_manager,'none'),
                  (select dds.db2_integer_to_date(a.origination_date)),a.approved_date,
                  a.capped_date,a.delivery_date,a.gap,a.service_contract,a.total_care
                from sls.ext_bopmast_partial a
                --where a.vin is not null
                --  and stock_number is not null
                --  and stock_number in ( -- excludes dup stock numbers
                --    select stock_number
                --   from sls.ext_bopmast_partial
                --  group by stock_number
                --   having count(*) = 1)
                  where (select dds.db2_integer_to_date(a.origination_date)) > '12/31/2014'
            """.format("'" + the_date + "'")
            pg_cur.execute(sql)
                # union -- sanitized extract from dups: within date range and has a non null deal status
                # select *
                # from (
                #   select {0}::date,a.store,a.bopmast_id,a.deal_status,a.deal_type,a.sale_type,
                #     coalesce(a.sale_group,'none'),a.vehicle_type,a.stock_number,a.vin,a.odometer_at_sale,
                #     a.buyer_bopname_id,coalesce(a.cobuyer_bopname_id, -1),
                #     case
                #       when coalesce(a.primary_sc, 'none') = 'AMA' then 'HAN'
                #       when coalesce(a.primary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                #       when coalesce(a.primary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                #       when coalesce(a.primary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                #       else coalesce(a.primary_sc, 'none')
                #     end,
                #     case
                #       when coalesce(a.secondary_sc, 'none') = 'AMA' then 'HAN'
                #       when coalesce(a.secondary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                #       when coalesce(a.secondary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                #       when coalesce(a.secondary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                #       else coalesce(a.secondary_sc, 'none')
                #     end,
                #     coalesce(a.fi_manager,'none'),
                #     (select dds.db2_integer_to_date(a.origination_date)),a.approved_date,
                #     a.capped_date,a.delivery_date,a.gap,a.service_contract,a.total_care
                #   from sls.ext_bopmast_partial a
                #   inner join (
                #     select stock_number
                #     from sls.ext_bopmast_partial
                #     group by stock_number
                #     having count(*) > 1) b on a.stock_number = b.stock_number
                #   where (select dds.db2_integer_to_date(a.origination_date)) > '12/31/2015'
                #     and a.vin is not null) z
                # where z.deal_status is not null
                # union -- null stock numbers with non null vin & deal status
                # select *
                # from (
                #   select {0}::date,a.store,a.bopmast_id,a.deal_status,a.deal_type,a.sale_type,
                #     coalesce(a.sale_group,'none'),a.vehicle_type,
                #     coalesce(a.stock_number,'none'),a.vin,a.odometer_at_sale,
                #     a.buyer_bopname_id,coalesce(a.cobuyer_bopname_id, -1),
                #     case
                #       when coalesce(a.primary_sc, 'none') = 'AMA' then 'HAN'
                #       when coalesce(a.primary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                #       when coalesce(a.primary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                #       when coalesce(a.primary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                #       else coalesce(a.primary_sc, 'none')
                #     end,
                #     case
                #       when coalesce(a.secondary_sc, 'none') = 'AMA' then 'HAN'
                #       when coalesce(a.secondary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                #       when coalesce(a.secondary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                #       when coalesce(a.secondary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                #       else coalesce(a.secondary_sc, 'none')
                #     end,
                #     coalesce(a.fi_manager,'none'),
                #     (select dds.db2_integer_to_date(a.origination_date)),a.approved_date,
                #     a.capped_date,a.delivery_date,a.gap,a.service_contract,a.total_care
                # from sls.ext_bopmast_partial a
                # where (select dds.db2_integer_to_date(origination_date)) > '12/31/2015'
                #   and stock_number is null
                #   and vin is not null
                #   and deal_status is not null) x
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ubuntu_con:
        ubuntu_con.close()
    if pg_con:
        pg_con.close()

