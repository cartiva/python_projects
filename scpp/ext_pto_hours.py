# coding=utf-8
import db_cnx
import ops
import string
import csv

task = 'ext_pto_hours'
pg_con = None
ads_con = None
run_id = None
file_name = 'files/ext_pto_hours.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        'Failed dependency check'
        exit()
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT b.yearmonth, b.thedate, c.employeenumber,
                  a.ptohours + a.vacationhours
                FROM edwClockHoursFact a
                INNER JOIN day b on a.datekey = b.datekey
                INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
                WHERE ptohours + vacationhours <> 0
                  AND b.thedate > '01/01/2016'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate scpp.ext_pto_hours")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy scpp.ext_pto_hours from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
