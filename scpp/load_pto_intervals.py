# encoding=utf-8
import ops
import string
import db_cnx

# TODO what if an ops function fails

task = 'load_pto_intervals'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                select count(*) as required
                from scpp.xfm_pto_intervals
            """
            pg_cur.execute(sql)
            if pg_cur.fetchone()[0] == 0:
                ops.log_pass(run_id, 'Not Required, nothing in xfm_pto_intervals')
                exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into scpp.pto_intervals (employee_number,year_month,from_date,thru_date,
                  most_recent_anniv,pto_rate)
                select employee_number, year_month, pto_period_from, pto_period_thru, most_recent_anniv, pto_rate
                from scpp.xfm_pto_intervals
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
