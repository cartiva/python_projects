import psycopg2

with psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'") as pgCon:
    pgCur = pgCon.cursor()
file_name = 'files/test.csv'
with open(file_name, 'r') as io:
    pgCur.copy_expert("""copy scpp.zjon_csv from stdin with csv encoding 'latin-1'""", io)
pgCon.commit()