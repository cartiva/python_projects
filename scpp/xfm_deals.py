# coding=utf-8
import db_cnx
import ops
import string
# TODO 30635 issue, capped and uncapped on same day (3/8/17) resulting in 2 rows in xfm_glptrs
task = 'xfm_deals'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            # new rows
            sql = """
                insert into scpp.xfm_deals (row_type,run_date,store_code,stock_number,
                  vin,customer_name,primary_sc,secondary_sc,record_status,date_approved,
                  date_capped,origination_date,model_year,make,model,seq,sc_change,
                  status_change, gl_date, gl_count)
                select 'New', a.run_date, a.store_code, a.stock_number, a.vin,
                  a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
                  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
                  a.make, a.model,
                  1, 'No Change', 'No Change', b.gl_date, b.gl_count
                from scpp.ext_deals a
                left join scpp.xfm_glptrns b on a.run_date = b.run_date and a.stock_number = b.stock_number
                WHERE not exists (
                    select 1
                    from scpp.xfm_deals
                    where stock_number = a.stock_number);
            """
            pg_cur.execute(sql)
            # deals where sc or status have changed - inner join to scpp.xfm_deals
            sql = """
                insert into scpp.xfm_deals (row_type,run_date,store_code,stock_number,
                  vin,customer_name,primary_sc,secondary_sc,record_status,date_approved,
                  date_capped,origination_date,model_year,make,model,seq,sc_change,status_change,
                  gl_date, gl_count, gl_date_change)
                select 'Update', a.run_date, a.store_code, a.stock_number, a.vin,
                  a.customer_name,
                  a.primary_sc, a.secondary_sc, a.record_status,
                  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
                  a.make, a.model,
                  (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
                  case
                    when a.primary_sc <> b.primary_sc and a.secondary_sc <> b.secondary_sc then
                      'PSC from ' || b.primary_sc || ' to ' || a.primary_sc || ' and SSC from ' ||
                         b.secondary_sc || ' to ' || a.secondary_sc
                    when a.primary_sc <> b.primary_sc then
                      'PSC from ' || b.primary_sc || ' to ' || a.primary_sc
                    when a.secondary_sc <> b.secondary_sc then
                      'SSC from ' || b.secondary_sc || ' to ' || a.secondary_sc
                    else 'No Change'
                  end as sc_change,
                  case
                    when b.record_status = 'None' then
                      case
                        when a.record_status = 'None' then 'No Change'
                        when a.record_status = 'A' then 'None to Accepted'
                        when a.record_status = 'U' then 'None to Capped'
                      end
                    when b.record_status = 'A' then
                      case
                        when a.record_status = 'None' then 'Accepted to None'
                        when a.record_status = 'A' then 'No Change'
                        when a.record_status = 'U' then 'Accepted to Capped'
                      end
                    when b.record_status = 'U' then
                      case
                        when a.record_status = 'None' then 'Capped to None'
                        when a.record_status = 'A' then 'Capped to Accepted'
                        when a.record_status = 'U' then 'No Change'
                      end
                  end as status_change,
                  c.gl_date, c.gl_count,
                  case
                    when c.gl_date <> b.gl_date then true
                    else false
                  end
                from scpp.ext_deals a
                inner join (
                  select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
                  record_Status, date_approved, date_capped, seq, gl_date, gl_count
                  from scpp.xfm_deals x
                  where seq = (
                    select max(seq)
                    from scpp.xfm_deals
                    where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
                left join scpp.xfm_glptrns c on a.run_date = c.run_date and a.stock_number = c.stock_number
                  and c.stock_number <> '30635' -- goofy ben c deal, uncapped & capped on 3/8, ie 2 rows
                where (a.primary_sc <> b.primary_sc or a.secondary_sc <> b.secondary_sc
                  or a.record_status <> b.record_status
                  or (a.record_status <> 'U' AND c.gl_date = a.run_date and c.gl_count = 1)
                  or c.gl_date <> b.gl_date)
            """
            pg_cur.execute(sql)
            # deals that have been deleted from bopmast - left join to scpp.deals
            # if the stock_number no longer exists in ext_deals, it is a deleted deal
            # TODO currently hard coding deals where the stocknumber has been deleted, can't rely on stocknumber
            sql = """
                insert into scpp.xfm_deals (row_type,run_date,store_code,stock_number,
                  vin,customer_name,primary_sc,secondary_sc,record_status,date_approved,
                  date_capped,origination_date,model_year,make,model,seq,sc_change,status_change, gl_date, gl_count)
                select 'Update', /*current_date,*/
                  (select max(run_date) from scpp.ext_deals) as run_date,
                  a.store_code, a.stock_number, a.vin,
                  a.customer_name,
                  -- a.primary_sc, a.secondary_sc,
                  case a.primary_sc
                    when 'AMA' then 'HAN'
                    else a.primary_sc
                  end as primary_sc,
                  case a.secondary_sc
                    when 'AMA' then 'HAN'
                    else a.secondary_sc
                  end as seconday_sc,
                  a.record_status,
                  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
                  a.make, a.model,
                  (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
                  'No Change' as sc_change,
                  'Deleted' as status_change,
                  a.gl_date, a.gl_count
                from (
                  select *
                  from (
                    select *
                    from scpp.xfm_deals x
                    where seq = (
                      select max(seq)
                      from scpp.xfm_deals
                      where stock_number = x.stock_number)) y
                  where status_change <> 'Deleted'
                    -- disappeared stock numbers
                    and stock_number not in ('28511','28873A','28108')) a
                left join scpp.ext_deals b on a.stock_number = b.stock_number
                where b.stock_number is null
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
