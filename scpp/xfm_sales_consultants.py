# encoding=utf-8
import ops
import string
import db_cnx

# TODO what if an ops function fails

task = 'xfm_sales_consultants'
pg_con = None
run_id = None

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                truncate scpp.xfm_sales_consultants
            """
            pg_cur.execute(sql)
            sql = """
                insert into scpp.xfm_sales_consultants
                select 'New' as row_type, a.*, current_date
                from scpp.ext_sales_consultants a
                where not exists (
                  select 1
                  from scpp.sales_consultants
                  where employee_number = a.employee_number)
                union
                select 'Update', a.*, current_date
                from scpp.ext_sales_consultants a
                inner join scpp.sales_consultants b on a.employee_number = b.employee_number
                  and (
                    a.first_name <> b.first_name
                    or
                    a.last_name <> b.last_name
                    or
                    a.full_name <> b.full_name
                    -- or
                    -- a.term_date <> b.term_date
                    or
                    a.user_name <> b.user_name)
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if pg_con:
        pg_con.close()
