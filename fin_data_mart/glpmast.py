# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'glpmast'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_glpmast.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(FISCAL_ANNUAL),YEAR,TRIM(ACCOUNT_NUMBER),TRIM(ACCOUNT_TYPE),
                  TRIM(ACCOUNT_DESC),TRIM(ACCOUNT_SUB_TYPE),TRIM(ACCOUNT_CTL_TYPE),TRIM(DEPARTMENT),
                  TRIM(TYPICAL_BALANCE),TRIM(DEBIT_OFFSET_ACCT),TRIM(CRED_OFFSET_ACCT),OFFSET_PERCENT,
                  AUTO_CLEAR_AMOUNT,TRIM(WRITE_OFF_ACCOUNT),TRIM(SCHEDULE_BY),TRIM(RECONCILE_BY),
                  TRIM(COUNT_UNITS),TRIM(CONTROL_DESC1),TRIM(VALIDATE_STOCK_NUMBER),TRIM(OPTION_3),
                  TRIM(OPTION_4),TRIM(OPTION_5),BEGINNING_BALANCE,JAN_BALANCE01,FEB_BALANCE02,MAR_BALANCE03,
                  APR_BALANCE04,MAY_BALANCE05,JUN_BALANCE06,JUL_BALANCE07,AUG_BALANCE08,SEP_BALANCE09,
                  OCT_BALANCE10,NOV_BALANCE11,DEC_BALANCE12,ADJ_BALANCE13,UNITS_BEG_BALANCE,JAN_UNITS01,
                  FEB_UNITS02,MAR_UNITS03,APR_UNITS04,MAY_UNITS05,JUN_UNITS06,JUL_UNITS07,AUG_UNITS08,
                  SEP_UNITS09,OCT_UNITS10,NOV_UNITS11,DEC_UNITS12,ADJ_UNITS13,TRIM(ACTIVE)
                from rydedata.glpmast
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_glpmast")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_glpmast from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
