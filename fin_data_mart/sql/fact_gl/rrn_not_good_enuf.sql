﻿-- ubuntu cnx --
each one of the ext tables is for previous 45 days

create temp table _1110 as 
select *
from test.ext_glptrns_1110 a;
create index on _1110(gttrn_);
create index on _1110(gtseq_);
create index on _1110(gtpost);
create index on _1110(gtdate);
-- create index on ext(hash);

create temp table _1207 as 
select *
from test.ext_glptrns_1207 a;
create index on _1207(gttrn_);
create index on _1207(gtseq_);
create index on _1207(gtpost);
create index on _1207(gtdate);
-- create index on ext(hash);

select *
from _1110 a
where not exists (
  select 1
  from _1207
  where gttrn_ = a.gttrn_
    and gtseq_ = a.gtseq_)
limit 100    

-- disappearing transactions
select *
from _1110 a
where a.gtdate in (
  select distinct gtdate
  from _1207)
and not exists (
  select 1
  from _1207
  where gttrn_ = a.gttrn_
    and gtseq_ = a.gtseq_)  
limit 100    
  

create temp table ext _1207
select gttrn_, gtseq_, gtpost, md5(a::text) as hash
from dds.ext_glptrns_2016 a
where gtdate between '06/01/2016' and '10/30/2016'
create index on ext(gttrn_);
create index on ext(gtseq_);
create index on ext(gtpost);
create index on ext(hash);


-- pg cnx --
select *
from fin.fact_gl
limit 100

select *
from dds.ext_glptrns_2016
limit 100

create index on dds.ext_glptrns_2016(gttrn_);
create index on dds.ext_glptrns_2016(gtseq_);
create index on dds.ext_glptrns_2016(gtpost);
create index on dds.ext_glptrns_2016(gtdate);

-- trans/seq in fact_gl that are no longer in glptrns
-- mostly blank post_status records
select *
from fin.fact_gl a
inner join dds.day b on a.date_key = b.datekey
  and b.yearmonth between 201601 and 201610
where not exists (
  select 1
  from dds.ext_glptrns_2016
  where gttrn_ = a.trans
    and gtseq_ = a.seq)
limit 500

select * from fin.fact_gl where doc = 'REF070716' order by trans, seq

-- ok, i am conviced that rows get changed
select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
  d.thedate, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a 
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.day d on a.date_key = d.datekey
inner join fin.dim_account e on a.account_key = e.account_key
where doc = 'REF070716' 
union
select 'glptrns', gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, 
  gtdate, gtctl_, gtdoc_, gtref_, gttamt
from dds.ext_glptrns_2016
where gtdoc_ = 'REF070716'  
order by trans, seq

select gttamt, rrn
from dds.ext_glptrns_2016
where gtdoc_ = 'REF070716' and gttamt = 1215.46
-- oh yeah, shit is fucked up

select * from fin.fact_gl where rrn = 16374594

select * from fin.fact_gl where trans = 3509074

-- transactions that didn't get into fact_gl
select gttrn_, gtseq_, gtpost, gtdate, rrn
from dds.ext_glptrns_2016 a
where not exists (
  select 1
  from fin.fact_gl
  where trans = a.gttrn_ 
    and seq = a.gtseq_)
limit 100



select *
from fin.fact_gl a
where rrn in (16521298, 16521300)

select a.*
from fin.fact_gl a
inner join (
  select trans,seq
  from fin.fact_gl g
  inner join dds.day h on g.date_key = h.datekey
--     and h.yearmonth in (2015, 2016)
  group by trans, seq
  having count(*) > 1) b on b.trans = b.trans 
    and a.seq = b.seq

-- eliminate voids and no dups
select *
from dds.ext_Glptrns_2016 e
inner join (
select gttrn_, gtseq_
from dds.ext_glptrns_2016 a
  where gtpost <> 'V'
group by gttrn_, gtseq_
having count(*) > 1) f on e.gttrn_ = f.gttrn_
  and e.gtseq_ = f.gtseq_  

select *
from dds.ext_glptrns_2016 e
where gtpost not in ('y','v')

-- voids take different looks
-- this one, tran/seq = 1 void and not void
select *
from fin.fact_gl
where trans = 3327671

select count(distinct gtdesc) from arkona.ext_glptrns 987,963

-- cdc
-- has to be the nightly scrape of glptrns and compare trans/seq/md5 to fact_gl (i think)
select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
  d.thedate, a.control, a.doc, a.ref, a.amount
from fin.fact_gl a 
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.day d on a.date_key = d.datekey
inner join fin.dim_account e on a.account_key = e.account_key
where doc = 'REF070716' and trans = 3371202 and seq = 19
union
select 'glptrns', gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, 
  gtdate, gtctl_, gtdoc_, gtref_, gttamt
from dds.ext_glptrns_2016 
where gtdoc_ = 'REF070716' and gttrn_ = 3371202 and gtseq_ = 19
order by trans, seq

-- this will give me some idea of what i'm looking at with cdc
-- dim_doc_type: changed doc_type_code to uppercase, md5 is different for upper and
--   lower case, glptrns is all upper case
-- voids could get goofy: trans/seq 3213600/1 3390350/1, and there are going to be a lot of them
--   on pdq ros, the voids seem to be generally for seq 1 and have 0 amount
-- which raises the question of excluding 0 amount trans/seq
--   nah, rely on queries specifying post_Status = Y
-- a lot of gtdate changes - primarily pdq ros, seems like they are getting changed to december,
--   curious to see the effect on the statement
-- getting a lot of results from fact account = none, glptrns account is *VOID, or *SPORD or *E*
--   added rows in dim_account for these values
select *
from (
  select 'glptrns', gttrn_, gtseq_, md5(a::text) as hash
  from (
    select gttrn_, gtseq_, gtdtyp, 
      case 
        when gtpost = ' ' then ''
        else gtpost
      end as gtpost, coalesce(gtjrnl, 'none'), 
      gtdate, gtctl_, gtdoc_, gtref_, gtacct, gttamt
    from dds.ext_glptrns_2016
    where gtdoc_ <> 'REF070716') a) e
inner join (
  select 'fact', trans, seq, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
      d.thedate, a.control, a.doc, a.ref, e.account, a.amount
    from fin.fact_gl a 
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    where doc <> 'REF070716') a) f on e.gttrn_ = f.trans and e.gtseq_ = f.seq and e.hash <> f.hash
limit 500    


-- temporarily exclude the *accounts
-- this shows differences, not neccessarily changes

select trans, seq, 
  case when e.gtdtyp <> f.doc_type_code then 'X' end as doc_type,
  case when e.gtpost <> f.post_status then 'X' end as post_status,
  case when e.gtdate <> f.thedate then 'X' end as the_date,
  case when e.gtctl_ <> f.control then 'X' end as control,
  case when e.gtdoc_ <> f.doc then 'X' end as doc,
  case when e.gtref_ <> f.ref then 'X' end as ref,
  case when e.gtacct <> f.account and e.gtacct not like '*%' then 'X' end as account,
  case when e.gttamt <> f.amount then 'X' end as amount
from (
  select gttrn_, gtseq_, gtdtyp, 
    case 
      when gtpost = ' ' then ''
      else gtpost
    end as gtpost, coalesce(gtjrnl, 'none'), 
    gtdate, gtctl_, gtdoc_, gtref_, gtacct, gttamt
  from dds.ext_glptrns_2016) e
inner join (
  select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
    d.thedate, a.control, a.doc, a.ref, e.account, a.amount
  from fin.fact_gl a 
  inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.day d on a.date_key = d.datekey
  inner join fin.dim_account e on a.account_key = e.account_key) f on e.gttrn_ = f.trans 
    and e.gtseq_ = f.seq 
    and (
      (e.gtdtyp <> f.doc_type_code) or
      (e.gtpost <> f.post_status) or
      (e.gtdate <> f.thedate) OR
      (e.gtctl_ <> f.control) or
      (e.gtdoc_ <> f.doc) or
      (e.gtref_ <> f.ref) or
      (e.gtacct <> f.account and e.gtacct not like '*%') or
      (e.gttamt <> f.amount))
limit 1000    


