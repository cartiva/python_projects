﻿select *
from dds.ext_glpjrnd
where journal_code is not null 

select journal_code
from (
select journal_code, journal_Desc
from dds.ext_glpjrnd
where journal_code is not null 
group by journal_code, journal_Desc
) x group by journal_code having count(*) > 1

create table fin.dim_journal (
  journal_key serial primary key,
  journal_code citext not null unique,
  journal citext not null);

insert into fin.dim_journal(journal_code, journal)
select journal_code, journal_desc
from dds.ext_glpjrnd
where journal_code is not null 
group by journal_code, journal_desc;

drop table if exists dds.ext_glpjrnd_tmp cascade;
create table dds.ext_glpjrnd_tmp (
  company_number citext,
  journal_code citext,
  journal citext,
  constraint ext_glpjrnd_tmp_pk primary key (company_number, journal_code));

insert into ops.tasks values('dim_journal','Daily');  

select count(1)
from (
  select journal_code, journal 
  from fin.dim_journal 
  except (
  select journal_code, journal
  from dds.ext_glpjrnd_tmp 
  where journal_code is not null
  group by journal_code, journal) 
union (
  (select journal_code, journal
  from dds.ext_glpjrnd_tmp
  where journal_code is not null
  group by journal_code, journal) 
  except
  select journal_code, journal 
  from fin.dim_journal)) x

insert into fin.dim_journal (journal_code, journal) values ('none','none');

select * from fin.dim_journal

-- 9/30/16 update to conform with new dimension standards
alter table fin.dim_journal
add column row_from_date date,
add column row_thru_date date not null default '12/31/9999',
add column current_row boolean,
add column row_reason citext;

update fin.dim_journal
set row_from_date = '01/01/2009',
    current_row = true,
    row_reason = 'table creation';

alter table fin.dim_journal
alter column row_from_date set not null,
alter column current_row set not null,
alter column row_reason set not null;


  select * 
  from arkona.ext_glpjrnd
  order by journal_code, journal_desc
  
select journal_code
from (
  select journal_code, journal_desc
  from arkona.ext_glpjrnd
  group by journal_code, journal_desc) x
group by journal_code
having count(*) > 1


-- 10/3/16/
refactor
1. change schema to arkona (from dds)

if there are any changes in glprnd, fail and send an email, analyze changes, do not think
there is anything type 2 about dim_journal