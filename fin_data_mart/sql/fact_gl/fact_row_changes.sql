﻿--< 6/6/17, changed fact_gl rows ------------------------------------------------------------------------------------
create temp table trans_seq as
-- these are the changed rows
select e.trans, e.seq
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq
      and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        and e.post_status = f.post_status
        and e.hash <> f.hash;

select 'xfm' as source, a.trans, a.seq, a.doc_type_code, a.post_status, a.journal_code,
  a.the_date, a.account, a.control, a.doc, a.ref, a.amount, a.description   
from arkona.xfm_glptrns a                           
inner join trans_seq b on a.trans = b.trans and a.seq = b.seq
union           
select 'fact', a.trans, a.seq, doc_type_code, post_status, journal_code,
  the_date, account, control, doc, ref, amount, ee.description   
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
inner join trans_seq aa on a.trans = aa.trans and a.seq = aa.seq                    
order by trans, seq, source

select * from trans_seq

-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 1102900 to 1117500'
from fin.fact_gl a
where (
  (trans = 3693988 and seq = 21) or   
  (trans = 3702110 and seq = 9) or
  (trans = 3703875 and seq = 9) or
  (trans = 3707930 and seq = 11) or
  (trans = 3707931 and seq = 11) or
  (trans = 3711020 and seq = 4) or
  (trans = 3711022 and seq = 6));

update fin.fact_gl
set control = '1117500'
where (
  (trans = 3693988 and seq = 21) or   
  (trans = 3702110 and seq = 9) or
  (trans = 3703875 and seq = 9) or
  (trans = 3707930 and seq = 11) or
  (trans = 3707931 and seq = 11) or
  (trans = 3711020 and seq = 4) or
  (trans = 3711022 and seq = 6)); 
  
-- reran it all and came up with 2 more
update fin.fact_gl
set control = '1117500'
where trans = 3711022 and seq = 5;   

update fin.fact_gl
set control = '2117500'
where trans = 3711022 and seq = 6;  

-- shit keeps changing
update fin.fact_gl
set control = '2117180'
where trans = 3711022 and seq = 6; 

--/> 6/6/17, changed fact_gl rows ------------------------------------------------------------------------------------

-- 5/9/17, changed fact_gl rows ------------------------------------------------------------------------------------
select 'xfm', a.*
from arkona.xfm_glptrns a
where (
  (trans = 3661731 and seq = 4)
  or (trans = 3663517 and seq = 11))       
union all
select 'fact', f.*,  md5(f::text) 
from (
  select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
    d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
  from fin.fact_gl a
  inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.day d on a.date_key = d.datekey
  inner join fin.dim_account e on a.account_key = e.account_key
  inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key
  where (
  (trans = 3661731 and seq = 4)
  or (trans = 3663517 and seq = 11)) ) f 
order by trans, seq 



-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 113361 to 113363'
from fin.fact_gl a
where (
  (trans = 3661731 and seq = 4) or   
  (trans = 3663517 and seq = 11)) ;


update fin.fact_gl
set control = '113363'
where (
  (trans = 3661731 and seq = 4) or   
  (trans = 3663517 and seq = 11)) ;



  

-- 5/9/17, changed fact_gl rows ------------------------------------------------------------------------------------



-- 4/5/17, changed fact_gl rows ------------------------------------------------------------------------------------
-- detected prior to updating fact_fs for march
select 'xfm' as source, a.*
from arkona.xfm_glptrns a
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4))          
union
select 'fact',a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
  d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description, ''
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.day d on a.date_key = d.datekey
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key        
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4))     
order by trans, seq, source  



-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change)
select a.trans, a.seq, current_date, 'control changed from 127065 to 127190'
from fin.fact_gl a
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4)) ;


update fin.fact_gl
set control = '127190'
where (
  (trans = 3628950 and seq = 4) or   
  (trans = 3630027 and seq = 4) or
  (trans = 3630028 and seq = 4) or
  (trans = 3631134 and seq = 4) or
  (trans = 3643539 and seq = 6) or
  (trans = 3648075 and seq = 4)); 

-- 4/5/17, changed fact_gl rows ------------------------------------------------------------------------------------

2/10/17 
-- think i finally have the void sitch correct, 43 void rows today
-- these are the rows that have changed (xfm_glptrns different than the same trans/seq row of fact_gl)

-- attributes that are being monitored for change based on trans/seq being equal: 
--   doc_type_code, post_status, journal_code, the_Date, account, control, doc, ref, amount, description
-- drop table trans_seq;
create temp table trans_seq as
select e.trans, e.seq, e.post_status
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq -- and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        -- and e.post_status = f.post_status
        and e.hash <> f.hash;

-- union xfm_glptrs and fact_gl for those rows that have changed
select 'xfm' as source, a.*
from arkona.xfm_glptrns a
inner join trans_seq b on a.trans = b.trans
  and a.seq = b.seq
union                      
select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
  d.the_date, e.account, a.control, a.doc, a.ref, a.amount, f.description, ''
from fin.fact_gl a
inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
inner join fin.dim_journal c on a.journal_key = c.journal_key
inner join dds.dim_date d on a.date_key = d.date_key
inner join fin.dim_account e on a.account_key = e.account_key
inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
inner join trans_seq g on a.trans = g.trans
  and a.seq = g.seq
order by trans, seq, source

-- the only changed attribute is post_status
select j.trans, j.seq, 
  case when j.doc_type_code = k.doc_type_code then '' else 'X' end as doc_type_code,
  case when j.post_status = k.post_status then '' else 'X' end as post_status,
  case when j.journal_code = k.journal_code then '' else 'X' end as journal_code,
  case when j.the_date = k.the_date then '' else 'X' end as the_date,
  case when j.account = k.account then '' else 'X' end as account,
  case when j.control = k.control then '' else 'X' end as control,
  case when j.doc = k.doc then '' else 'X' end as v,
  case when j.ref = k.ref then '' else 'X' end as ref,
  case when j.amount = k.amount then '' else 'X' end as amount,
  case when j.description = k.description then '' else 'X' end as description
from (
  select 'xfm' as source, a.*
  from arkona.xfm_glptrns a
  inner join trans_seq b on a.trans = b.trans
    and a.seq = b.seq) j
inner join (
  select 'fact', a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code, 
    d.the_date, e.account, a.control, a.doc, a.ref, a.amount, f.description, ''
  from fin.fact_gl a
  inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
  inner join fin.dim_journal c on a.journal_key = c.journal_key
  inner join dds.dim_date d on a.date_key = d.date_key
  inner join fin.dim_account e on a.account_key = e.account_key
  inner join fin.dim_gl_description f on a.gl_description_key = f.gl_description_key
  inner join trans_seq g on a.trans = g.trans
    and a.seq = g.seq) k on j.trans = k.trans and j.seq = k.seq 


update fin.fact_gl a
set post_status = x.post_status
from (
  select *
  from trans_seq) X
where a.trans = x.trans
  and a.seq = x.seq;  

 -- 2/12/17 ---------------------------------------------------------------------------
the nightly script has to process void rows
only send an email if another attribute has changed
Due to the horror of actually modifying fact rows, i would like to keep a log of those
rows that get changed

create table fin.fact_gl_change_log (
  trans bigint not null, 
  seq integer not null, 
  the_date date not null,
  change citext not null,
  constraint fact_gl_change_log_pkey primary Key(trans,seq,the_date,change));

void rows:  

create index on arkona.xfm_glptrns(trans);
create index on arkona.xfm_glptrns(seq);
create index on arkona.xfm_glptrns(post_status);

create index on fin.fact_gl(trans);
create index on fin.fact_gl(seq);
create index on fin.fact_gl(post_status);

select e.trans, e.seq, e.post_status
from arkona.xfm_glptrns e
inner join (
  select 'fact', trans, seq, post_status, md5(a::text) as hash
  from (
    select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
      d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
    from fin.fact_gl a
    inner join arkona.xfm_glptrns aa on a.trans = aa.trans
      and a.seq = aa.seq -- and a.post_status = aa.post_status
    inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
    inner join fin.dim_journal c on a.journal_key = c.journal_key
    inner join dds.day d on a.date_key = d.datekey
    inner join fin.dim_account e on a.account_key = e.account_key
    inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
      on e.trans = f.trans
        and e.seq = f.seq
        -- and e.post_status = f.post_status
        and e.hash <> f.hash;


-- bizzaro test, goes from void to not void ---------------------
select b.trans, b.seq, b.post_status
from fin.fact_gl a
inner join arkona.xfm_glptrns b on a.trans = b.trans
    and a.seq = b.seq
    and a.post_status = 'V'
    and b.post_status <> 'V'


-- first update the fact_gl_log
insert into fin.fact_gl_change_log(trans,seq,the_date,change
select b.trans, b.seq, b.post_status, current_date, 'post_status to V'
from fin.fact_gl a
inner join arkona.xfm_glptrns b on a.trans = b.trans
  and a.seq = b.seq
  and a.post_status = 'Y'
  and b.post_status <> 'Y';
  
-- then update the voids
update fin.fact_gl a
set post_status = x.post_status
from (
  select b.trans, b.seq, b.post_status
  from fin.fact_gl a
  inner join arkona.xfm_glptrns b on a.trans = b.trans
      and a.seq = b.seq
      and a.post_status = 'Y'
      and b.post_status <> 'Y') x
where a.trans = x.trans
  and a seq = x.seq;      

    