﻿drop table if exists fin.dim_doc_type;
create table fin.dim_doc_type (
  doc_type_key serial primary key,
  doc_type_code citext not null unique,
  doc_type citext not null);

insert into fin.dim_doc_type (doc_type_code, doc_type)
select 'b', 'deals'
union
select 'c','checks'
union
select 'd','deposits'
union
select 'i','inventory purchase/stock in'
union
select 'j','conversion or gje'
union
select 'o','invoice/po entry'
union
select 'p','parts ticket'
union
select 'r','cash receipts'
union
select 's','service tickets'
union
select 'w','handwritten check'
union
select 'x','bank rec'
union
select 'h','i do not know'
union
select 't','i do not know'  



select * from fin.dim_doc_type

-- 9/30/16 update to conform with new dimension standards
alter table fin.dim_doc_type
add column row_from_date date,
add column row_thru_date date not null default '12/31/9999',
add column current_row boolean,
add column row_reason citext;

update fin.dim_doc_type
set row_from_date = '01/01/2009',
    current_row = true,
    row_reason = 'table creation';

alter table fin.dim_doc_type
alter column row_from_date set not null,
alter column current_row set not null,
alter column row_reason set not null;

select * from fin.dim_doc_type

-- 10/5/16
oops, forgot the none row
insert into fin.dim_doc_type (doc_type_code, doc_type, row_from_Date, row_Thru_date, current_row, row_reason)
values('none','none','01/01/2009','12/31/9999',true, 'table creation')

