﻿select year, account_number
from dds.ext_glpmast 
where account_number is not null 
group by year, account_number
having count(*) > 1

select left(account_number, 1), account_sub_type
from dds.ext_glpmast
group by left(account_number, 1), account_sub_type

from the work in cdc (postgres\changedatacapture\glptrns_2.sql)
account number get re-used (1427004, 1403006)
department can change - this is probably type 1 correction
accounts get added
accounts do not appear to get deleted, but made to be inactive 
accounts change from active to inactive
accounts change from inactive to active - this looks the the account number re-use case
descriptions change - tend to see this as type 1

-- account number is unique in this grouping
select account_number, account_type, account_desc, account_sub_type,
  department, typical_balance
-- select * 
from dds.ext_glpmast
where account_number is not null
  and account_sub_type <> 'C' -- crookston accounts
group by account_number, account_type, account_desc, account_sub_type,
  department, typical_balance

drop table if exists dds.xfm_glpmast;
create table dds.xfm_glpmast (
  account citext not null primary key,
  account_type_code citext not null,
  description citext not null,
  store_code citext not null,
  department_code citext not null,
  typical_balance citext not null);

insert into dds.xfm_glpmast  
select account_number, account_type, account_desc, 
  case account_sub_type
    when 'A' then 'RY1'
    when 'B' then 'RY2'
    else 'XXX'
  end,
  department, 
  case typical_balance
    when 'D' then 'Debit'
    when 'C' then 'Credit'
    else 'X'
  end
from dds.ext_glpmast
where account_number is not null
  and account_sub_type <> 'C' -- crookston accounts
group by account_number, account_type, account_desc, account_sub_type,
  department, typical_balance; 

select * from dds.ext_glpmast where account_number = '122001D'

select distinct account_number from dds.ext_glpmast where department is null and account_number is not null and year = 2016 order by account_number

select * from dds.xfm_glpmast limit 100

-- change date and change reason: when a row is added or changed (new row or type 2 change)
drop table if exists fin.dim_account cascade;
create table fin.dim_account (
  account_key serial primary key,
  account citext not null unique,
  account_type_code citext not null,
  account_type citext not null,
  description citext not null, 
  store_code citext not null,
  store citext not null,
  department_code citext not null,
  department citext not null,
  typical_balance citext not null,
  current_row boolean not null,
  from_date date not null,
  thru_date date not null default '12/31/9999',
  change_date date not null,
  change_reason citext not null);
create index on fin.dim_account(account_type_code);  
create index on fin.dim_account(account_type); 
create index on fin.dim_account(department_code); 
create index on fin.dim_account(department); 
create index on fin.dim_account(store_code); 
create index on fin.dim_account(store); 
create index on fin.dim_account(current_row); 

insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, change_date, change_reason)  
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.department,
  a.typical_balance, 
  true, '01/01/2009', '12/31/9999', current_date, 'table creation'
from dds.xfm_glpmast a
left join dds.xfm_glpdept b on a.department_code = b.department_code;

insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, change_date, change_reason) 
values('none','none','none','none','none','none','none','none','none',true, '01/01/2009', '12/31/9999', current_date, 'table creation')




  
drop table if exists dds.xfm_glpmast_tmp;
CREATE TABLE dds.xfm_glpmast_tmp(
  account citext NOT NULL primary key,
  account_type citext NOT NULL,
  description citext NOT NULL,
  store_code citext NOT NULL,
  department_code citext NOT NULL,
  typical_balance citext NOT NULL);

insert into ops.tasks values('dim_account','Daily');

insert into ops.task_dependencies values ('glpdept','dim_account');

-- 8/23/16 some new accounts
-- per Jeri, these are holding accounts, kind of like temp tables, 
-- for temporary holding of values
select *
from (
  (select *
  from dds.xfm_glpmast
  except
  select * 
  from dds.xfm_glpmast_tmp)
  union 
  (select *
  from dds.xfm_glpmast_tmp
  except
  select * 
  from dds.xfm_glpmast)) x
order by account   

-- first need to add new accounts to dds.xfm_glpmast
insert into dds.xfm_glpmast (account,account_type,description,store_Code,department_code,
  typical_balance)
select *
from dds.xfm_glpmast_tmp
except
select *
from dds.xfm_glpmast  

-- then into dds.dim_account  
insert into fin.dim_account (account, account_type_code, account_type, description,
  store_code, store, department_code, department, typical_balance, current_row,
  from_date, thru_date, change_date, change_reason) 
select a.account, a.account_type,
  case account_type
    when '1' then 'Asset'
    when '2' then 'Liability'
    when '3' then 'Equity'
    when '4' then 'Sale'
    when '5' then 'COGS'
    when '6' then 'Income'
    when '7' then 'Other Income'
    when '8' then 'Expense'
    when '9' then 'Other Expense'
  end as account_type,
  a.description, a.store_code,
  case a.store_code
    when 'RY1' then 'Rydell GM'
    when 'RY2' then 'Honda Nissan'
  end, 
  a.department_code, b.department,
  a.typical_balance, 
  true, '08/01/2016', '12/31/9999', '08/01/2016', 'new accounts created by controller'
from dds.xfm_glpmast_tmp a -- new rows in glpmast
left join dds.xfm_glpdept b on a.department_code = b.department_code
where a.account in (
  select account
  from dds.xfm_glpmast_tmp
  except
  select account 
  from dds.xfm_glpmast);


-- 9/30/16
-- have put this on hold until i finished with financial statement
-- that is now good thru 201606

-- some shit has changed
-- dim_account has failed since 9/13/16, due to a new account, some dept corrections and a 
-- description change (type 2 or correction, i don't know)

select 'xfm_tmp' as source, account, account_type,description,store_code,department_code,typical_balance
from dds.xfm_glpmast_tmp 
where account in ( 
  select account
  from (
    select *
    from dds.xfm_glpmast_tmp
    except
    select *
    from dds.xfm_glpmast) a)
union  
select 'xfm' as source, account, account_type,description,store_code,department_code,typical_balance
from dds.xfm_glpmast
where account in ( 
  select account
  from (
    select *
    from dds.xfm_glpmast_tmp
    except
    select *
    from dds.xfm_glpmast) a)
order by account, source    


in addition, i am rethinking the process, do i really need to save the ext_ or interrim xfr_ tables
today it feels like no.
so

the only thing  hanging me up at the moment is the notion of active
previously i was dismissive of active, did not care
but today, the transistion from inactive to active seems to matter in terms of determining
whether a changed description is type 1 or type 2
but do i really care
if i ever need to report using  the account description it could be wrong if i type 1 the description
but then, i do not have any history anyway
fuck it 
type 1 for now
10/4
fuck it
type 2 for now

move the extract from dds to arkona
extract glpmast to ext_
compare ext_ to dim
fail and email until i feel more confident about the types of changes

do the extract and compare in separate tasks?
yes

-- 10/3/16
do i track changes in glpmast or just those chnges in glpmast that directly affect the current
configuration of dim_account
fuck do i even need a full extract of glpmast

ok scenario 1
nightly full scrape of glpmast (overwrite)
in xfm , generate a simulation of dim_account from the nightly ext_glpmast and compare that to dim_account

select *
from ops.tasks
where task like '%glpmast%'

delete from ops.task_log where task = 'ext_glpmast';
update ops.tasks
set task = 'glpmast'
where task = 'ext_glpmast';


select *
from ops.task_log order by run_start_ts desc limit 1000

select *
from ops.task_dependencies
where successor = 'dim_account'

insert into ops.task_dependencies values ('glpmast','dim_account');

-- 10/4
attempting type 2 change (description) bombs on unique constraint on account
of course

ALTER TABLE fin.dim_Account DROP CONSTRAINT dim_account_account_key;

create unique index dim_account_account on fin.dim_account(account) where (current_row = true); 

need to add the source comments

COMMENT ON COLUMN fin.dim_account.account_key IS 'source: system generated surrogate key';
COMMENT ON COLUMN fin.dim_account.account IS 'source: arkona.glpmast.account_number';
COMMENT ON COLUMN fin.dim_account.account_type_code IS 'source: arkona.glpmast.account_type';
COMMENT ON COLUMN fin.dim_account.account_type IS 'source: hard coded based on account_type';
COMMENT ON COLUMN fin.dim_account.description IS 'source: arkona.glpmast.account_desc';
COMMENT ON COLUMN fin.dim_account.store_code IS 'source: arkona.glpmast.account_sub_type';
COMMENT ON COLUMN fin.dim_account.store IS 'source: ';
COMMENT ON COLUMN fin.dim_account.department_code IS 'source: arkona.glpmast.department';
COMMENT ON COLUMN fin.dim_account.department IS 'source: arkona.glpdept.dept_description';
COMMENT ON COLUMN fin.dim_account.typical_balance IS 'source: arkona.glpmast.typical_balance';



