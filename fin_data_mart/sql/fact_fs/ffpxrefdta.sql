﻿select * from dds.ext_ffpxrefdta -- 144850

select * from dds.ext_ffpxrefdta  -- 16975
where coalesce(consolidation_grp, '1')  <> '3'
  and factory_code = 'GM'
  and factory_account <> '331A'
  and factory_financial_year > 2010
  and g_l_acct_number is not null limit 100


select factory_financial_year as the_year, 
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  g_l_acct_number as gl_account, factory_account as gm_account,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null -- eliminates the XFCOPY... rows



select the_year, gl_account, gm_account from (
select factory_financial_year as the_year, 
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  g_l_acct_number as gl_account, factory_account as gm_account,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null -- eliminates the XFCOPY... rows
) x group by the_year, gl_account,gm_account having count(*) > 1  

drop table if exists dds.xfm_ffpxrefdta cascade;
create table dds.xfm_ffpxrefdta (
  the_year integer not null,
  gl_account citext not null,
  gm_account citext not null,
  store_code citext not null,
  multiplier numeric(2,1) not null default 1,
  constraint xfm_ffpxrefdta_pk primary key (the_year,gl_account,gm_account));

insert into dds.xfm_ffpxrefdta
select factory_financial_year as the_year, 
  g_l_acct_number as gl_account, factory_account as gm_account,
  CASE coalesce(consolidation_grp, '1') 
    when '1' then 'RY1'::citext
    when '2' then 'RY2'::citext
  end as store_code,
  fact_account_ as multiplier
from dds.ext_ffpxrefdta 
where coalesce(consolidation_grp, '1')  <> '3' -- ry1/ry2 only
  and factory_code = 'GM'
  and factory_account <> '331A' -- don't remember why, but this is necessary
  and factory_financial_year > 2010
  and g_l_acct_number is not null; -- eliminates the XFCOPY... rows  

  
create table dds.xfm_ffpxrefdta_tmp (
  the_year integer not null,
  gl_account citext not null,
  gm_account citext not null,
  store_code citext not null,
  multiplier numeric(2,1) not null default 1,
  constraint xfm_ffpxrefdta_tmp_pk primary key (the_year,gl_account,gm_account));  



insert into ops.tasks values ('ffpxrefdta','Daily');

select * from dds.xfm_ffpxrefdta 


                select *
                from ((
                    select *
                    from dds.xfm_ffpxrefdta d
                    except
                    select *
                    from dds.xfm_ffpxrefdta_tmp e)
                  union (
                    select *
                    from dds.xfm_ffpxrefdta_tmp f
                    except
                    select *
                    from dds.xfm_ffpxrefdta g)) x

-- 8/23/16 
-- routing for the new marketing holding accounts (12315,12415,12515,12715,12915)    
insert into dds.xfm_ffpxrefdta (the_year,gl_account,gm_account,store_code,multiplier)
select *
from dds.xfm_ffpxrefdta_tmp f
except
select *
from dds.xfm_ffpxrefdta g


-- 10/6/16
move to arkona schema     

select *
from arkona.xfm_ffpxrefdta_tmp f
except
select *
from arkona.xfm_ffpxrefdta g

-- get out of dds
alter table dds.ext_ffpxrefdta
rename to z_unused_ext_ffpxrefdta;
alter table dds.xfm_ffpxrefdta
rename to z_unused_xfm_ffpxrefdta;
alter table dds.xfm_ffpxrefdta_tmp
rename to z_unused_xfm_ffpxrefdta_tmp;

in september, jeri added one new account: 16603 (pdq contributions)
add that to ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta g

-- -- changes in ffpxrefdta:
--   update arkona.ext_ffpxrefdta
--   update fin.dim_fs

select * from arkona.ext_Ffpxrefdta where g_l_acct_number = '16603'

select * from arkona.ext_eisglobal_sypffxmst where fxmact = '066d'

select * from ops.task_dependencies where successor = 'sypffxmst' or predecessor = 'sypffxmst'

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '09/12/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '16603'


-- 1/3/17
-- 3 new accounts added for December
-- 230700
-- 285003
-- 130700
                select *
                from ((
                    select *
                    from arkona.ext_ffpxrefdta d
                    except
                    select *
                    from arkona.ext_ffpxrefdta_tmp e)
                  union (
                    select *
                    from arkona.ext_ffpxrefdta_tmp f
                    except
                    select *
                    from arkona.ext_ffpxrefdta g)) x

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '230700';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '130700';
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '12/01/2016', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number = '285003';


-- 1/13/17
-- 2 new accounts added in January, this time, jeri added the routing immediately after adding the new accounts in glpmast
-- 180601
-- 180801
                select *
                from ((
                    select *
                    from arkona.ext_ffpxrefdta d
                    except
                    select *
                    from arkona.ext_ffpxrefdta_tmp e)
                  union (
                    select *
                    from arkona.ext_ffpxrefdta_tmp f
                    except
                    select *
                    from arkona.ext_ffpxrefdta g)) x

1. add to arkona.ext_ffpxrefdta

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

2. add to fin.dim_fs_account

insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '01/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in ('180601','180801');

-- 2/2/17
-- 2017 added to ffpxrefdta
select max(factory_financial_year) from arkona.ext_ffpxrefdta
select max(factory_financial_year) from arkona.ext_ffpxrefdta_tmp

insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp f
except
select *
from arkona.ext_ffpxrefdta 


-- 3/3/17
-- new rows - added in february
-- need to be added arkona.ext_ffpxrefdta and to fin.dim_fs_account

select g_l_acct_number, factory_financial_year
from arkona.ext_ffpxrefdta
group by g_l_acct_number, factory_financial_year
having count(*) > 1

-- new rows in ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)
  
-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- rows missing from dim_fs_account
select a.account
from fin.dim_account a
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account)  

-- 
-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '02/02/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
      where gl_Account = a.account)); 




-- removed rows
select * 
from arkona.ext_ffpxrefdta a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta_tmp
  where g_l_acct_number = a.g_l_acct_number)

-- 5/4/17
-- On May 4, 2017, at 7:24 AM, Jon Andrews <jandrews@cartiva.com> wrote:
-- Why was account 145400 (SLS USED GM PROT PLANS) rerouted to 455 (Other Protection Plans) from 454
--  
-- And account 165400 (C/S USED GM PROT PLANS) rerouted to 655 (Other Protection Plans) from 654
-- 
-- Because only activity this month was an unwound deal and gm wouldn't let me show a negative count. It will be changed back now that statement is submitted. I can't get away with anything :) 
-- but that leaves me with 11301F

select *
from ((
    select *
    from arkona.ext_ffpxrefdta d
    except
    select *
    from arkona.ext_ffpxrefdta_tmp e)
  union (
    select *
    from arkona.ext_ffpxrefdta_tmp f
    except
    select *
    from arkona.ext_ffpxrefdta g)) x
  

-- changed rows
select *
from (
  select factory_financial_year, g_l_acct_number, md5(a::text) as hash
  from  arkona.ext_ffpxrefdta a) c
inner join (
  select factory_financial_year, g_l_acct_number, md5(b::text) as hash
  from  arkona.ext_ffpxrefdta_tmp b) d on c.g_l_acct_number = d.g_l_acct_number 
    and c.factory_financial_year = d.factory_financial_year
    and c.hash <> d.hash


select 'xfm' as source, company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
-- select *
from  arkona.ext_ffpxrefdta a  
where factory_financial_year = 2017
  and g_l_acct_number in ('11301F')
union
select 'ext', company_number, consolidation_grp, g_l_company_, factory_code, factory_financial_year,
  g_l_acct_number, factory_account
from  arkona.ext_ffpxrefdta_tmp a  
where factory_financial_year = 2017
  and g_l_acct_number in ('11301F')  
order by g_l_acct_number, source
  

select company_number, count(*) from arkona.ext_ffpxrefdta group by company_number

select company_number, count(*) from arkona.ext_ffpxrefdta_tmp group by company_number

can not figure out in 5250 where this goofy one off of 11301F / 202 with company number RY2 comes from
so just exclude it


-- 6/30/17
-- new rows - added in june: 16902W, 16102W, 16802W, 146901,166901, 246901, 266901
-- need to be added arkona.ext_ffpxrefdta and to fin.dim_fs_account

-- new rows in ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp a
where not exists (
  select 1
  from arkona.ext_ffpxrefdta
  where g_l_acct_number = a.g_l_acct_number)
  
-- 1. add to arkona.ext_ffpxrefdta
insert into arkona.ext_ffpxrefdta
select *
from arkona.ext_ffpxrefdta_tmp 
except
select *
from arkona.ext_ffpxrefdta;

-- rows missing from dim_fs_account
select a.account
from fin.dim_account a
where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
  and not exists (
    select 1
    from fin.dim_fs_account
    where gl_Account = a.account)  

-- !!!!!!!!!!!!!!!!!!! 6/30 - 7/2, done
-- jeri has also added 146901,166901, 246901, 266901 but not routed them yet
-- hold off on  updating until all have been routed
-- 
-- 2. add to fin.dim_fs_account
insert into fin.dim_fs_account(gm_account, gl_account, row_from_date, current_row, row_reason)
select factory_account, g_l_acct_number, '06/01/2017', true, 'new gl_account'
from arkona.ext_ffpxrefdta
where g_l_acct_number in (
  select a.account
  from fin.dim_account a
  where account not in ('none', '*E*', '*VOID', '*SPORD', '21', '999999')
    and not exists (
      select 1
      from fin.dim_fs_account
      where gl_Account = a.account)); 

-- 3. 7/4/17, page 2 failing accounts missing from fin.dim_fs_account for gm_account <EX061Z, <EX061V

select *
from fin.dim_fs_account
where gm_account in ('<EX061Z','<EX061V')

insert into fin.dim_fs_account (gm_account,gl_account,row_from_date,current_row,row_reason)values
('<G&AV','16102W',current_date,true,'Page 2'),
('<G&AV','16802W',current_date,true,'Page 2'),
('<G&AV','16902W',current_date,true,'Page 2');
-- ('P2L57C1','16102W',current_date,true,'Page 2'),
-- ('P2L57C1','16802W',current_date,true,'Page 2'),
-- ('P2L57C1','16902W',current_date,true,'Page 2'),
-- ('P2L57C7','16102W',current_date,true,'Page 2'),
-- ('P2L57C7','16802W',current_date,true,'Page 2'),
-- ('P2L57C7','16902W',current_date,true,'Page 2');
-- ('P2L56C1','16102W',current_date,true,'Page 2'),
-- ('P2L56C1','16802W',current_date,true,'Page 2'),
-- ('P2L56C1','16902W',current_date,true,'Page 2'),
-- ('P2L56C7','16102W',current_date,true,'Page 2'),
-- ('P2L56C7','16802W',current_date,true,'Page 2'),
-- ('P2L56C7','16902W',current_date,true,'Page 2');
-- ('P2L40C1','16102W',current_date,true,'Page 2'),
-- ('P2L40C1','16802W',current_date,true,'Page 2'),
-- ('P2L40C1','16902W',current_date,true,'Page 2');
-- ('P2L40C7','16102W',current_date,true,'Page 2'),
-- ('P2L40C7','16802W',current_date,true,'Page 2'),
-- ('P2L40C7','16902W',current_date,true,'Page 2');
-- ('<EX068V','16802W',current_date,true,'Page 2'),
-- ('<EX069Z','16902W',current_date,true,'Page 2'),
-- ('<EX068Z','16802W',current_date,true,'Page 2'),
-- ('<EX069V','16902W',current_date,true,'Page 2');
-- ('<EX061Z','16102W',current_date,true,'Page 2'),
-- ('<EX061v','16102W',current_date,true,'Page 2');
