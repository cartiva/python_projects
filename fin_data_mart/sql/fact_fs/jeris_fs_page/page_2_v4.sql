﻿page 2 all year_months

-- FIXED:SALES:<TFS  --done 3/25
select aa.year_month, store, sum(amount)
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701) aa 
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
inner join (
  select year_month, store, sum(a.amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
    and b.col in (1,2)
    and d.area = 'fixed'
   group by year_month, store) c on aa.year_month = c.year_month  
where a.fxmact = '<TFS'
  and a.fxmpge = 2
  and a.fxmlne in (1,2)
group by aa.year_month, store
order by store, aa.year_month

-- FIXED:SALES:<TFS  -- done 3/25
-- now, generate all the fact_fs rows 201101 -> 201701
-- need rows fact_fs
drop table if exists base_tfs;
create table base_tfs as
insert into fin.fact_fs
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701) aa 
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TFS'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
    and b.col in (1,2)
    and d.area = 'fixed') c on aa.year_month = c.year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TFS'   
inner join fin.dim_fs e on aa.year_month = e.year_month
  and e.page = 2
  and e.line = 1
  and e.col = 11
where a.fxmact = '<TFS'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);

-- VARIABLE:SALES:<TVS  -- done 3/25
-- now, generate all the fact_fs rows 201101 -> 201701
-- need rows fact_fs
-- drop table if exists base_tvs;
-- create table base_tvs as
insert into fin.fact_fs
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701) aa 
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TVS'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where (
    (b.page = 16 and b.line in (1,2,3,4,5,8,10) and b.col = 1) --uc
    or
    (b.page between 5 and 15 and b.col = 1) -- nc
    or
    (b.page = 17 and b.line between 1 and 21 and b.col = 1)) --f&i
    and d.area = 'variable') c on aa.year_month = c.year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TVS'   
inner join fin.dim_fs e on aa.year_month = e.year_month
  and e.page = 2
  and e.line = 1
  and e.col = 7
where a.fxmact = '<TVS'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);

-- STORE:SALES:<TNU  -- done 3/25
-- now, generate all the fact_fs rows 201101 -> 201701
-- need rows fact_fs
-- drop table if exists base_tnu;
-- create table base_tnu as
insert into fin.fact_fs
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701) aa 
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TNU'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where ((
    (b.page = 16 and b.line in (1,2,3,4,5,8,10) and b.col = 1) --uc
    or
    (b.page between 5 and 15 and b.col = 1) -- nc
    or
    (b.page = 17 and b.line between 1 and 21 and b.col = 1) --f&i
    and d.area = 'variable')
    or
    (b.page = 16 and b.line between 21 and 59 and b.col in (1,2) and d.area = 'fixed'))) c on aa.year_month = c.year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TNU'   
inner join fin.dim_fs e on aa.year_month = e.year_month
  and e.page = 2
  and e.line = 1
--   and e.col = 2
where a.fxmact = '<TNU'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);


-- FIXED:GROSS:<TFEG  -- done 3/26
-- now, generate all the fact_fs rows 201101 -> 201701
-- need rows fact_fs
-- drop table if exists base_TFEG;
-- create table base_TFEG as
insert into fin.fact_fs
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701) aa 
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TFEG'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
--     and b.col in (1,2)
    and d.area = 'fixed') c on aa.year_month = c.year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TFEG'   
inner join fin.dim_fs e on aa.year_month = e.year_month
  and e.page = 2
  and e.line = 2
  and e.col = 11
where a.fxmact = '<TFEG'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);


-- VARIABLE:GROSS:<TVEG  -- done 3/26
-- now, generate all the fact_fs rows 201101 -> 201701
-- need rows fact_fs
-- drop table if exists base_TVEG;
-- create table base_TVEG as
insert into fin.fact_fs
select e.fs_key, c.fs_org_key, d.fs_account_key, c.amount
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701) aa 
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
inner join (
  select b.year_month, a.fs_key, a.fs_org_key, cc.fs_account_key, a.amount, c.gl_account
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  left join fin.dim_fs_Account cc on c.gl_account = cc.gl_account 
    and cc.gm_account = '<TVEG'
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where (
    (b.page = 16 and b.line in (1,2,3,4,5,8,9,10)) --uc
    or
    (b.page between 5 and 15) -- nc
    or
    (b.page = 17 and b.line between 1 and 21)) --f&i
    and d.area = 'variable') c on aa.year_month = c.year_month  
inner join fin.dim_fs_account d on c.gl_account = d.gl_account
  and d.gm_Account = '<TVEG'   
inner join fin.dim_fs e on aa.year_month = e.year_month
  and e.page = 2
  and e.line = 2
  and e.col = 7
where a.fxmact = '<TVEG'
  and a.fxmpge = 2
  and a.fxmlne in (1,2);  



-- all months no missing accounts for <TFS
select distinct c.gl_account
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2015, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b  
  union
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( --201601 thru 201606
    select 2016 as the_year) a
    cross join (
    select * from generate_Series(1, 6, 1) as the_month) b ) aa  
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy   
left join (
  select factory_financial_year, 
  case
    when coalesce(consolidation_grp, '1')  = '1' then 'RY1'::citext
    when coalesce(consolidation_grp, '1')  = '2' then 'RY2'::citext
    else 'XXX'::citext
  end as store_code, g_l_acct_number, factory_account, fact_account_
  from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
inner join (
  select distinct gl_account, year_month
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
    and b.col in (1,2)
    and d.area = 'fixed') c on aa.year_month = c.year_month  
where a.fxmact = '<TFS'
  and a.fxmpge = 2
  and a.fxmlne in (1,2)
  and not exists (
    select 1
    from fin.dim_fs_account
    where gm_account = '<TFS'
      and gl_account = c.gl_account);

      
  select year_month, sum(a.amount) as amount
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
  inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
  inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
  where b.page = 16
    and b.line between 21 and 59
    and b.col in (1,2)
   group by year_month
order by year_month   

select year_month, 
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key




-- page 2 totals ok, but subdivide by departments not ok
-- service ok, parts & body shop: parts split confusion
select gl_account, department, sum(amount)
-- select sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.year_month = 201702
  and b.page = 2
  and b.line = 1
  and b.col = 11
  and d.store = 'ry1'
--   and d.department = 'service'
--   and d.sub_department = 'mechanical'
  and d.area = 'fixed'
group by department, gl_account

select department, sub_department, sum(amount)
-- select sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.year_month = 201702
  and b.page = 2
  and b.line = 1
  and b.col = 7
  and d.store = 'ry1'
--   and d.department = 'service'
--   and d.sub_department = 'mechanical'
  and d.area = 'variable'
group by department, sub_department


--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
P2 lines 4 - 54
try it it all in one
1. need to do the dim_fs_account
    delete existing <EX.../none accounts
    add the real deals


-- ok, have all the dim_fs rows
select year_month, count(*)
from fin.dim_fs
where page = 2
group by year_month
order by year_month

-- this is wrong, page 3/4
pages 3/4 cols: 1 5 10
page 2 cols 1 7 11

select *
from fin.dim_Fs 
where page = 2
  and year_month = 201702
  and line between 4 and 54
order by line, col  

select *
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701
  union 
    select 2017, 201702) aa
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy
-- left join (
--   select distinct gl_account, year_month, line, col
--   from fin.fact_fs a
--   inner join fin.dim_fs b on a.fs_key = b.fs_key
--   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
--   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
--   where b.page in (3,4)
--     and b.line between 4 and 54) c on aa.year_month = c.year_month and a.fxmlne = c.line and a.fxmcol = c.col
where a.fxmpge = 2
  and a.fxmlne between 4 and 54     
  and aa.year_month = 201702
order by fxmlne, fxmcol  


-- by year_month/store/page/line/col/gl_account::amount
select year_month, store, page, line, col, gl_account, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.page in (3,4)
  and b.line between 4 and 54
  and d.store <> 'none'
group by year_month, store, page, line, col, gl_account
order by year_month, store, page, line, col, gl_account

-- by year_month/store/page/col L57
-- spot checked amounts look ok
select year_month, store, page, col, sum(amount) as amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.page in (3,4)
  and b.line between 4 and 54
group by year_month, store, page, col
order by year_month, store, page, col

-- -- store none ??? last month is 201512 ---------------------------------------------------------------------
-- -- not going to worry about it, some old national stuff, honda non existent departments, 
-- select *
-- from (
--   select year_month, store, page, col, sum(amount) as amount
--   from fin.fact_fs a
--   inner join fin.dim_fs b on a.fs_key = b.fs_key
--   inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
--   inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
--   where b.page in (3,4)
--     and b.line between 4 and 54
--   group by year_month, store, page, col) x
-- where store = 'none'
--   and amount <> 0
-- order by year_month, store, page, col  
-- 
-- select *
-- from fin.fact_fs a
-- inner join fin.dim_fs b on a.fs_key = b.fs_key
-- inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
-- inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
-- where b.page = 3
--   and b.line between 4 and 54
--   and year_month = 201301
--   and store = 'none'
--   and amount <> 0
-- order by year_month, store, page, line, co
-- 
-- -- store none ??? ---------------------------------------------------------------------

drop table if exists base_1;
create temp table base_1 as
-- expense line of page 3 & 4 (line 4 - 54)
-- by year_month/store/page/line/col/gl_account::amount
select year_month, store, page, line, col, gl_account, d.fs_org_key, sum(amount) as amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where b.page in (3,4)
  and b.line between 4 and 54
  and d.store <> 'none'
group by year_month, store, page, line, col, gl_account, d.fs_org_key
order by year_month, store, page, line, col, gl_account;

drop table if exists base_2;
create temp table base_2 as 
-- the expense lines of page 2 (line 4 - 54)
select *
from (
  select a.the_year, a.the_year * 100 + the_month as year_month
  from ( -- 201101 thru 201512
    select generate_series(2011, 2016, 1) as the_year) a
    cross join (
    select * from generate_Series(1, 12, 1) as the_month) b
  union
    select 2017, 201701
  union 
    select 2017, 201702) aa
left join arkona.ext_eisglobal_sypffxmst a on aa.the_year = a.fxmcyy
where a.fxmpge = 2
  and a.fxmlne between 4 and 54     
order by fxmlne, fxmcol;


-- need dim_Fs_account rows

delete from fin.dim_Fs_account where gm_account like '<%' and gl_account = 'none'

select gl_account, page, line, col from base_1 group by gl_account, page, line, col order by line, col
select fxmact, fxmpge, fxmlne, fxmcol from base_2 group by fxmact, fxmpge, fxmlne, fxmcol order by fxmlne

insert into fin.dim_fs_account(gm_account,gl_account,row_from_date,current_row,row_reason)
select distinct a.fxmact, b.gl_account, current_date, true,'Page 2'
from (select fxmact, fxmpge, fxmlne, fxmcol from base_2 group by fxmact, fxmpge, fxmlne, fxmcol) a
left join (select gl_account, page, line, col from base_1 group by gl_account, page, line, col) b 
  on a.fxmlne = b.line
  and 
--     case 
--       when a.fxmlne < 8 then 1 = 1
--       when a.fxmcol = 1 then 1 = 1
--       when a.fxmcol = 7 then b.col = 5
--       when a.fxmcol = 11 then b.col between 10 and 13
--     end  
--     case 
--       when a.fxmlne < 8 then 1 = 1
--       when a.fxmcol = 1 then 1 = 1
--       when b.page = 3 and a.fxmcol = 7 then 1 = 1
-- --       when b.page = 4 and a.fxmcol = 7 then b.col = 11
--       when b.page = 4 and a.fxmcol = 11 then 1 = 1
--     end 
    case 
      when a.fxmlne < 8 then 1 = 1
      when a.fxmcol = 1 then 1 = 1
      when a.fxmcol = 7 then b.page = 3
      when a.fxmcol = 11 then b.page = 4
    end    
on conflict do nothing

looks like i fucked up the column matching
page 3 and 4 are different
3: 1 - 5 - 10  only 1 and 5 are relevant
4: 1 - 11 - 13

b:
select gl_account, page, line, col from base_1 group by gl_account, page, line, col order by page, line, col
a:
select fxmact, fxmpge, fxmlne, fxmcol from base_2 group by fxmact, fxmpge, fxmlne, fxmcol order by fxmpge, fxmlne, fxmcol

-- this is the fix
drop table if exists this_is_it;
create table this_is_it as
select c.fs_key, b.fs_org_key, d.fs_account_key, b.amount
from base_2 a
left join base_1 b on a.year_month = b.year_month
  and a.fxmlne = b.line
  and 
--     case 
--       when a.fxmlne < 8 then 1 = 1
--       when a.fxmcol = 1 then b.col = 1
--       when a.fxmcol = 7 then b.col = 5
--       when a.fxmcol = 11 then b.col between 10 and 13
--     end  
--     case 
--       when a.fxmlne < 8 then 1 = 1
--       when a.fxmcol = 1 then 1 = 1
--       when b.page = 3 and a.fxmcol = 7 then 1 = 1
-- --       when b.page = 4 and a.fxmcol = 7 then b.col = 11
--       when b.page = 4 and a.fxmcol = 11 then 1 = 1
--     end
    case 
      when a.fxmlne < 8 then 1 = 1
      when a.fxmcol = 1 then 1 = 1
      when a.fxmcol = 7 then b.page = 3
      when a.fxmcol = 11 then b.page = 4
    end
left join fin.dim_fs c on b.year_month = c.year_month 
  and a.fxmpge = c.page
  and a.fxmlne = c.line
  and a.fxmcol = c.col
left join fin.dim_fs_account d on a.fxmact = d.gm_account
  and b.gl_account = d.gl_account  
-- where a.year_month = 201701   
order by a.year_month, a.fxmpge, a.fxmlne, a.fxmcol

-- finally
select line, sum(amount)
-- select *
from this_is_it a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where year_month = 201701
  and page = 2
  and store = 'ry1'
  and col < 3
 group by line
 

delete
from fin.fact_fs 
where fs_key in (
  select fs_key
  from fin.dim_fs
  where page = 2
    and line between 4 and 54);

-- 4/12 after zapping and recreating, 103348 rows inserted
insert into fin.fact_fs
select *
from this_is_it a;

-- 4/12 Page 2 Line 59: Net Additions & Deductions
-- from Page 3 lines 63 - 68
-- here are the fact_fs rows for 201101 -> 201701
-- added dim_fs_org rows for grand forks-ry1-general & grand forks-ry2-general, essentially store level
insert into fin.fact_fs(fs_key, fs_org_key, fs_account_key, amount)
select g.fs_key, i.fs_org_key, h.fs_account_key, round(e.amount, 0) as amount
from (
  select store, the_year, year_month, gm_account, gl_account, sum(amount) as amount, department
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join fin.dim_fs_account c on b.account = c.gl_account
    and gm_account in ('902', '903', '905','909','910','952','953','955')
  inner join dds.dim_date d on a.date_key = d.date_key
    and d.year_month between 201101 and 201701
  where post_status = 'Y'  
  group by store, the_year, year_month, gm_account, gl_account, department) e
left join (
  select store_code, fxmact, g_l_acct_number, fxmpge, fxmlne, fxmcol
  from arkona.ext_eisglobal_sypffxmst a
  left join (
    select factory_financial_year, 
    case
      when coalesce(consolidation_grp, '1')  = '1' then 'Rydell GM'::citext
      when coalesce(consolidation_grp, '1')  = '2' then 'Honda Nissan'::citext
      else 'XXX'::citext
    end as store_code, g_l_acct_number, factory_account, fact_account_
    from arkona.ext_ffpxrefdta) b on a.fxmcyy = b.factory_financial_year and a.fxmact = b.factory_account
  where a.fxmcyy = 2017
    and fxmpge = 3
    and fxmlne between 63 and 68) f on e.store = f.store_code  
  and e.gl_account = f.g_l_acct_number  
  and e.gm_Account = f.fxmact 
left join fin.dim_fs g on e.year_month = g.year_month  
  and f.fxmpge = g.page
  and f.fxmcol = g.col
  and f.fxmlne = g.line
left join fin.dim_fs_account h on e.gm_account = h.gm_account
  and e.gl_account = h.gl_account  
left join fin.dim_fs_org i on i.area = 'general'
  and 
  case 
    when e.store = 'Rydell GM' then i.store = 'RY1'
    when e.store = 'Honda Nissan' then i.store = 'RY2'
  end;
-----------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
drop materialized view if exists fin.fact_fs_all cascade;
create materialized view fin.fact_fs_all as
select year_month, store, area, department, sub_department, page, line, col, line_label, gm_account, gl_account, amount
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
inner join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key
where store <> 'none';
create index on fin.fact_fs_all(year_month);
create index on fin.fact_fs_all(store);
create index on fin.fact_fs_all(area);
create index on fin.fact_fs_all(department);
create index on fin.fact_fs_all(sub_department);
create index on fin.fact_fs_all(page);
create index on fin.fact_fs_all(line);
create index on fin.fact_fs_all(col);
create index on fin.fact_fs_all(gm_account);
create index on fin.fact_fs_all(gl_account);


select store, area, department, sub_department, sum(amount)
from fin.fact_fs_all
where year_month = 201702
  and page in (3,4)
  and line  = 11
group by store, area, department, sub_department
order by  store, area, department, sub_department;


select a.control, max(d.the_date), sum(-amount)
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join (
  select gl_account
  from fin.fact_fs_all
  where year_month = 201702
    and page = 16
    and line between 1 and 12
    and store = 'ry1') c on b.account = c.gl_account
inner join dds.dim_date d on a.date_key = d.date_key
  and d.year_month > 201612  
inner join fin.dim_journal e on a.journal_key = e.journal_key
  and e.journal_code = 'VSU'
where a.post_status = 'Y'
group by a.control
order by max(d.the_date)

-- store good 201702, 201602
select
  sum(case when line = 2 then -amount else 0 end) as "Gross",
  sum(case when line between 4 and 6 then amount else 0 end) as "Total Variable",
  sum(case when line between 8 and 17 then amount else 0 end) as "Total Personnel",
  sum(case when line between 18 and 40 then amount else 0 end) as "Total Semi-Fixed",
  sum(case when line between 41 and 55 then amount else 0 end) as "Total Fixed",
  sum(case when line = 2 then -amount else 0 end) - sum(case when line between 4 and 55 then amount else 0 end) as "Operating Profit"
from fin.fact_fs_all
where year_month = 201701
  and store = 'ry1'
  and page = 2;

-- store area 201702, 201602
select area,
  sum(case when line = 2 then -amount else 0 end) as "Gross",
  sum(case when line between 4 and 6 then amount else 0 end) as "Total Variable",
  sum(case when line between 8 and 17 then amount else 0 end) as "Total Personnel",
  sum(case when line between 18 and 40 then amount else 0 end) as "Total Semi-Fixed",
  sum(case when line between 41 and 55 then amount else 0 end) as "Total Fixed",
  sum(case when line = 2 then -amount else 0 end) - sum(case when line between 4 and 55 then amount else 0 end) as "Operating Profit"
from fin.fact_fs_all
where year_month = 201701
  and store = 'ry1'
  and page = 2  
group by area;  


-- store department 201702, 201602 
-- body shop (too high) and parts (too low) gross off by 26334: 147701/167701 should not be part of P2L2
select area, department,
  sum(case when line = 2 then -amount else 0 end) as "Gross",
  sum(case when line between 4 and 6 then amount else 0 end) as "Total Variable",
  sum(case when line between 8 and 17 then amount else 0 end) as "Total Personnel",
  sum(case when line between 18 and 40 then amount else 0 end) as "Total Semi-Fixed",
  sum(case when line between 41 and 55 then amount else 0 end) as "Total Fixed",
  sum(case when line = 2 then -amount else 0 end) - sum(case when line between 4 and 55 then amount else 0 end) as "Operating Profit"
from fin.fact_fs_all
where year_month = 201702
  and store = 'ry1'
  and page = 2  
group by area, department;  


-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1111111
-- 4/7/17
P2 only used for store/area
department requires looking at the relevant other pages
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11111111111

select line, line_label, sum(amount)
from fin.fact_fs_all
where year_month = 201702
  and store = 'ry1'
  and page = 2  
group by line, line_label  
order by line  

-- lines in fact_fs with no values
select *
from fin.dim_fs a
where year_month = 201701
  and page = 2
  and not exists (
    select 1
    from fin.fact_fs
    where fs_key = a.fs_key)

select *
from fin.dim_fs a
where a.page = 2 
  and a.line = 7

select store, year_month, 
  sum(case when line between 4 and 6 then amount else 0 end) as line_3
from fin.fact_fs a  
inner join fin.dim_fs b on a.fs_key = b.fs_key
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key
  and c.store <> 'none'
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
where b.page = 2
group by store, year_month
order by year_month

select *
from fin.dim_fs
where year_month = 201702
  and page = 2


select page, line, col, gm_account
from fin.fact_fs a
left join fin.dim_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on a.fs_account_key = c.fs_account_key
left join fin.dim_fs_org d on a.fs_org_key = d.fs_org_key  
where year_month = 201701
  and page = 2
  and store = 'ry1'
group by page, line, col, gm_account


-- P2 line,col,label,gm_account
select a.line, a.col, a.line_label, c.gm_account
from fin.dim_fs a
left join fin.fact_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on b.fs_account_key = c.fs_account_key
left join fin.dim_fs_org d on b.fs_org_key = d.fs_org_key
  and d.store = 'ry1'
where a.year_month = 201701
  and a.page = 2
group by a.line, a.col, a.line_label, c.gm_account  
order by a.line, a.col  

-- try to add gl account and fucking ry2 stuff shows up
-- what's wrong with this model 
select d.store, a.line, a.col, a.line_label, c.gm_account, gl_account
from fin.dim_fs a
left join fin.fact_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on b.fs_account_key = c.fs_account_key
left join fin.dim_fs_org d on b.fs_org_key = d.fs_org_key
where a.year_month = 201701
  and a.page = 2
group by d.store, a.line, a.col, a.line_label, c.gm_account, c.gl_account  
order by d.store, a.line, a.col  

-- this works to eliminate ry2 accounts but takes 52 sec to run
select a.line, a.col, a.line_label, c.gm_account, gl_account
from fin.dim_fs a
left join fin.fact_fs b on a.fs_key = b.fs_key
left join fin.dim_fs_account c on b.fs_account_key = c.fs_account_key
-- left join fin.dim_fs_org d on b.fs_org_key = d.fs_org_key
--   and d.store = 'ry1'
where a.year_month = 201701
  and a.page = 2
  and not exists (
    select 1
    from fin.fact_fs t
    inner join fin.dim_fs_org u on t.fs_org_key = u.fs_org_key
      and u.store = 'ry2'
    where t.fs_org_key = b.fs_org_key)
group by a.line, a.col, a.line_label, c.gm_account, c.gl_account  
order by a.line, a.col  



-- this is fin, but does not include the subtotal lines
select *  
from fin.fact_fs_all
where year_month = 201701
--   and store = 'ry1'
  and page = 2
order by line, col  




