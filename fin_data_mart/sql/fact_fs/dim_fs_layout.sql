﻿-- -- -- this did not work because basing it on sypffxmst leaves out shit like page 3 line 3 (section headings) 
-- -- -- and page 3 line 7 (sub totals/totals lines)
-- ok, let's go down the multiple dim path
-- dim_fs_layout
drop table if exists fin.dim_fs_layout;
create table fin.dim_fs_layout (
  fs_layout_key serial primary key,
  the_year integer not null,
  store_code citext not null,
  page integer not null,
  line numeric(4,1) not null,
  line_label citext);
create unique index on fin.dim_fs_layout(the_year,store_code,page,line);  

-- insert into fin.dim_fs_layout (the_year,store_code,page,line,line_label)
-- select h.fxmcyy, 'RY1', h.fxmpge, h.fxmlne, i.line_label 
-- from (
--   select a.fxmcyy, a.fxmpge, a.fxmlne
--   from dds.ext_eisglobal_sypffxmst a
--   where a.fxmcyy > 2010
--   group by a.fxmcyy, a.fxmpge, a.fxmlne
--   order by a.fxmcyy, a.fxmpge, a.fxmlne) h
-- left join (
--   select flcyy, flpage, flflne, left(fldata, position('|' in fldata) - 1) as line_label
--   from (
--     select flcyy,flpage,flflne,
--       max(
--         case
--           when left(fldata,1) = '|' then right(fldata, length(fldata) - 1)
--           else fldata
--         end) as fldata
--     from dds.ext_eisglobal_sypfflout
--     where trim(flcode) = 'GM'
--       and flcyy > 2010
--       and flflsq = 0
--       and 
--         case -- the only goofy multiple label per line 18-39 
--           when flpage = 18 and flflne = 39 then fldata not like '|DIRECT EXPENSE AND INCOME%'
--           else 1 = 1
--         end 
--     group by flcyy,flpage,flflne,fldata) a) i on h.fxmcyy = i.flcyy and h.fxmpge = i.flpage and h.fxmlne = i.flflne  

select * from dds.ext_eisglobal_sypffxmst where fxmcyy = 2016 and fxmpge = 3 order by fxmlne, fxmcol

select flcyy, flpage, flflne, flflsq, fldata, flcont from dds.ext_eisglobal_sypfflout where flcyy = 2016 and flpage = 16 order by flpage, flflne, flflsq


drop table if exists fin.dim_fs_layout;
create table fin.dim_fs_layout (
  fs_layout_key serial primary key,
  the_year integer not null,
  store_code citext not null,
  page integer not null,
  line numeric(4,1) not null,
  line_label citext);
create unique index on fin.dim_fs_layout(the_year,store_code,page,line);  

insert into fin.dim_fs_layout (the_year,store_code,page,line,line_label)
select flcyy, 'RY1', flpage, flflne, trim(left(fldata, position('|' in fldata) - 1)) as line_label
from (
  select flcyy,flpage,flflne,
    max(
      case
        when left(fldata,1) = '|' then right(fldata, length(fldata) - 1)
        else fldata
      end) as fldata
  from dds.ext_eisglobal_sypfflout
  where trim(flcode) = 'GM'
    and flcyy > 2010
    and flflsq = 0
    and flflne > 0
    and 
      case -- the only goofy multiple label per line 18-39 
        when flpage = 18 and flflne = 39 then fldata not like '|DIRECT EXPENSE AND INCOME%'
        else 1 = 1
      end 
  group by flcyy,flpage,flflne,fldata) a;


