﻿2/1/17
-- sypffxmst ------------------------------------------------------------------
select *
from ((
    select *
    from arkona.ext_eisglobal_sypffxmst 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp)
  union (
    select *
    from arkona.ext_eisglobal_sypffxmst_tmp 
    except
    select *
    from arkona.ext_eisglobal_sypffxmst)) x

select 'archive' as source, a.*
from arkona.ext_eisglobal_sypffxmst a
where fxmcyy = 2017
  and fxmpge = 16
  and fxmlne in (3,6)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypffxmst_tmp a
where fxmcyy = 2017
  and fxmpge = 16
  and fxmlne in (3,6)
order by fxmlne, fxmcol, source 

2/7/17
looks like page 16 line 3/6, col 2/4 get added back
select * from arkona.ext_eisglobal_sypffxmst where fxmpge = 16 and fxmlne in( 3.0, 6.0) and fxmcol in (2,4) and fxmcyy = 2017

insert into arkona.ext_eisglobal_sypffxmst
select * from arkona.ext_eisglobal_sypffxmst_tmp where fxmpge = 16 and fxmlne in( 3.0, 6.0) and fxmcol in (2,4) and fxmcyy = 2017 

looks like line 3 col 2 & 4, line 6 col 2 & 4 get deleted

select *
from arkona.ext_eisglobal_sypffxmst a
left join arkona.ext_eisglobal_sypffxmst_tmp b on a.fxmcyy = b.fxmcyy
  and a.fxmpge = b.fxmpge
  and a.fxmlne = b.fxmlne
  and a.fxmcol = b.fxmcol
where b.fxmpge is null

delete 
-- select *
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2017 
  AND fxmpge = 16 
  and fxmlne in (3.0, 6.0)
  and fxmcol in (2,4)

update arkona.ext_eisglobal_sypffxmst a
set fxmstr = x.fxmstr
from (
  select *
  from arkona.ext_eisglobal_sypffxmst_tmp a
  where fxmcyy = 2017
    and fxmpge = 16
    and fxmlne = 3)  x
where a.fxmcyy = x.fxmcyy
  and a.fxmpge = x.fxmpge
  and a.fxmlne = x.fxmlne
  and a.fxmcol = x.fxmcol
      
--2/2/17

select 'archive' as source, a.*
from arkona.ext_eisglobal_sypffxmst a
where fxmcyy = 2017
  and fxmpge = 3
  and fxmlne in (52)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypffxmst_tmp a
where fxmcyy = 2017
  and fxmpge = 3
  and fxmlne in (52)
order by fxmlne, fxmcol   

delete 
-- select * 
from arkona.ext_eisglobal_sypffxmst
where fxmcyy = 2017
  and fxmpge = 3
  and fxmlne = 52.0
  and fxmcol = 2
-- sypfflout ------------------------------------------------------------------  

select *
from ((
    select *
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select *
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select *
    from arkona.ext_eisglobal_sypfflout f
    except
    select *
    from arkona.ext_eisglobal_sypfflout_tmp g)) x


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage = 16
  and flflne in (3,6)
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage = 16
  and flflne in (3,6)
order by flflne

-- data not currently being used for 2017 page 16 lines 3 & 6 changed
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage = 16
    and flflne in (3, 6)) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne    

-- 2/2/17 
select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage = 3
  and flflne in (52)
--   and flflsq = 0
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage = 3
  and flflne in (52)
--   and flflsq = 0
order by flflne 
-- flcont change (i don't currently use this data) page 3, 
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage = 3
    and flflne = 52) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne   
  and a.flflsq = x.flflsq
   

-- 4/29/17

select *
from ((
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout f
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
order by flpage    


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and flpage in (7,8,9,10,11,12,13,14)
  and flflne = 52
  and flflsq = 0
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and flpage in (7,8,9,10,11,12,13,14)
  and flflne = 52
  and flflsq = 0
order by flpage, flflne, flflsq, source, flflsq

-- trivial unused information from far right in fldata has changed
-- go ahead and update it
update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and flpage in (7,8,9,10,11,12,13,14)
    and flflne = 52
    and flflsq = 0) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne   
  and a.flflsq = x.flflsq;

-- 5/11/17
select *
from ((
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp d
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout e)
  union (
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout f
    except
    select flcode,flcyy,flpage,flseq,flline,flflne,flflsq,fldata
    from arkona.ext_eisglobal_sypfflout_tmp g)) x
order by flpage, flseq  


select 'archive' as source, a.*
from arkona.ext_eisglobal_sypfflout a
where flcyy = 2017
  and (
    (flpage = 3 and flflne = 2 and flflsq = 1)
    or
    (flpage = 16 and flflne = 3 and flflsq = 0)
    or
    (flpage = 16 and flflne = 6 and flflsq = 0)
    or
    (flpage = 16 and flflne = 9 and flflsq = 0))
union all
select 'extract', a.*
from arkona.ext_eisglobal_sypfflout_tmp a
where flcyy = 2017
  and (
    (flpage = 3 and flflne = 2 and flflsq = 1)
    or
    (flpage = 16 and flflne = 3 and flflsq = 0)
    or
    (flpage = 16 and flflne = 6 and flflsq = 0)
    or
    (flpage = 16 and flflne = 9 and flflsq = 0))
order by flpage, flflne, flflsq, source, flflsq


update arkona.ext_eisglobal_sypfflout a
set fldata = x.fldata,
    flcont = x.flcont
from (
  select *
  from arkona.ext_eisglobal_sypfflout_tmp
  where flcyy = 2017
    and (
      (flpage = 3 and flflne = 2 and flflsq = 1)
      or
      (flpage = 16 and flflne = 3 and flflsq = 0)
      or
      (flpage = 16 and flflne = 6 and flflsq = 0)
      or
      (flpage = 16 and flflne = 9 and flflsq = 0))) x
where a.flcyy = x.flcyy
  and a.flpage = x.flpage
  and a.flflne = x.flflne   
  and a.flflsq = x.flflsq;



