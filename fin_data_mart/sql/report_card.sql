﻿select *
from (
  select a.department, year_month,
    line_label, budgeted_value, budgeted_percent_of_gross 
  from fin.ext_budget a
  where a.store_code = 'ry1'
    and a.year_month = 201601 
    and a.display_seq in(2,6,15,38,52,54)) aa
left join (
  select a.department || '-' || year_month::citext,
    line_label, budgeted_value, budgeted_percent_of_gross 
  from fin.ext_budget a
  where a.store_code = 'ry1'
    and a.year_month = 201601 
    and a.display_seq in(2,6,15,38,52,54))
order by department, display_seq  

select b.*, 
  (select amount from fin.ext_budget a where line_label = 'gross profit')
from (
  select line_label, display_seq, sum(budgeted_value) as amount
  from fin.ext_budget a
  where a.store_code = 'ry1'
    and a.year_month = 201601 
    and a.display_seq in(2,6,15,38,52,54)
  group by line_label, display_seq ) b 
order by display_seq


with Jan as (
  select line_label, display_seq, sum(budgeted_value) as amount
  from fin.ext_budget a
  where a.store_code = 'ry1'
    and a.year_month = 201601 
    and a.display_seq in(2,6,15,38,52,54)
    group by line_label, display_seq)
select line_label, display_seq, amount,
  round(amount::numeric(12,4)/(select amount from jan where display_seq = 2), 4)
from jan
order by display_seq    