﻿--create schema jon;
--this use to point to a VM server called ops
--that server has been removed
--to use this stuff, need to create the db stuff somewhere

drop table jon.arkona_production_vs_report_tables cascade;
create table jon.arkona_production_vs_report_tables (
  table_name citext primary key,
  date_format citext not null,
  date_field citext);
insert into  jon.arkona_production_vs_report_tables values 
('rydedata.pdppdet', 'the_date_decimal', 'ptdate'),
('rydedata.pdpphdr', 'the_date_decimal', 'ptdate'),
('rydedata.sdprdet', 'the_date_decimal', 'ptdate'),
('rydedata.sdprhdr', 'the_date_decimal', 'ptdate'),
('rydedata.glptrns', 'the_date', 'gtdate'),
('rydedata.pypclockin', 'the_date', 'yiclkind');

select * from jon.arkona_production_vs_report_tables;


drop table jon.arkona_production_vs_report;

create table jon.arkona_production_vs_report (
  the_date date not null,
  ts timestamptz not null default current_timestamp,
  table_name citext not null references jon.arkona_production_vs_report_tables,
  production_count bigint not null default 0,
  report_count bigint not null default 0,
  constraint arkona_production_vs_report_pk primary key(the_date,table_name));

select *
-- delete 
from jon.arkona_production_vs_report 
--order by ts
where the_date = current_date

