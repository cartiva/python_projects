# encoding=utf-8
import db_cnx
import ops
import csv
import datetime
import string

pg_con = None
db2_con = None
run_id = None
task = 'ext_glpmast_cdc'
file_name = 'files/ext_glpmast_cdc.csv'
# today
# table_name = ('test.ext_glptrns_' + str(datetime.date.today().strftime("%m")) +
#               str(datetime.date.today().strftime("%d")))
# yesterday: for running script past midnight, but data if for previous day
# to be sure and get all of michelle's west coast/EOM postings
# eg, run at 04/22/16 2:30 AM, table name is 0421
# 5/6/16 fuckers changed attribute VALIDATE_STOCK_ to VALIDATE_STOCK_NUMBER
table_name = ('test.ext_glpmast_' + str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%m")) +
              str((datetime.date.today() - datetime.timedelta(days=1)).strftime("%d")))

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(FISCAL_ANNUAL),YEAR,TRIM(ACCOUNT_NUMBER),
                  TRIM(ACCOUNT_TYPE),TRIM(ACCOUNT_DESC),TRIM(ACCOUNT_SUB_TYPE),TRIM(ACCOUNT_CTL_TYPE),
                  TRIM(DEPARTMENT),TRIM(TYPICAL_BALANCE),TRIM(DEBIT_OFFSET_ACCT),TRIM(CRED_OFFSET_ACCT),
                  OFFSET_PERCENT,AUTO_CLEAR_AMOUNT,TRIM(WRITE_OFF_ACCOUNT),TRIM(SCHEDULE_BY),
                  TRIM(RECONCILE_BY),TRIM(COUNT_UNITS),TRIM(CONTROL_DESC1),TRIM(VALIDATE_STOCK_NUMBER),
                  TRIM(OPTION_3),TRIM(OPTION_4),TRIM(OPTION_5),BEGINNING_BALANCE,JAN_BALANCE01,
                  FEB_BALANCE02,MAR_BALANCE03,APR_BALANCE04,MAY_BALANCE05,JUN_BALANCE06,JUL_BALANCE07,
                  AUG_BALANCE08,SEP_BALANCE09,OCT_BALANCE10,NOV_BALANCE11,DEC_BALANCE12,ADJ_BALANCE13,
                  UNITS_BEG_BALANCE,JAN_UNITS01,FEB_UNITS02,MAR_UNITS03,APR_UNITS04,MAY_UNITS05,
                  JUN_UNITS06,JUL_UNITS07,AUG_UNITS08,SEP_UNITS09,OCT_UNITS10,NOV_UNITS11,DEC_UNITS12,
                  ADJ_UNITS13,TRIM(ACTIVE)
                from rydedata.glpmast
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.postgres_ubuntu() as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                CREATE TABLE """ + table_name + """ (
                    COMPANY_NUMBER CITEXT,
                    FISCAL_ANNUAL CITEXT,
                    YEAR INTEGER,
                    ACCOUNT_NUMBER CITEXT,
                    ACCOUNT_TYPE CITEXT,
                    ACCOUNT_DESC CITEXT,
                    ACCOUNT_SUB_TYPE CITEXT,
                    ACCOUNT_CTL_TYPE CITEXT,
                    DEPARTMENT CITEXT,
                    TYPICAL_BALANCE CITEXT,
                    DEBIT_OFFSET_ACCT CITEXT,
                    CRED_OFFSET_ACCT CITEXT,
                    OFFSET_PERCENT NUMERIC (3,2),
                    AUTO_CLEAR_AMOUNT NUMERIC (7,2),
                    WRITE_OFF_ACCOUNT CITEXT,
                    SCHEDULE_BY CITEXT,
                    RECONCILE_BY CITEXT,
                    COUNT_UNITS CITEXT,
                    CONTROL_DESC1 CITEXT,
                    VALIDATE_STOCK_NUMBER CITEXT,
                    OPTION_3 CITEXT,
                    OPTION_4 CITEXT,
                    OPTION_5 CITEXT,
                    BEGINNING_BALANCE NUMERIC (11,2),
                    JAN_BALANCE01 NUMERIC (11,2),
                    FEB_BALANCE02 NUMERIC (11,2),
                    MAR_BALANCE03 NUMERIC (11,2),
                    APR_BALANCE04 NUMERIC (11,2),
                    MAY_BALANCE05 NUMERIC (11,2),
                    JUN_BALANCE06 NUMERIC (11,2),
                    JUL_BALANCE07 NUMERIC (11,2),
                    AUG_BALANCE08 NUMERIC (11,2),
                    SEP_BALANCE09 NUMERIC (11,2),
                    OCT_BALANCE10 NUMERIC (11,2),
                    NOV_BALANCE11 NUMERIC (11,2),
                    DEC_BALANCE12 NUMERIC (11,2),
                    ADJ_BALANCE13 NUMERIC (11,2),
                    UNITS_BEG_BALANCE INTEGER,
                    JAN_UNITS01 INTEGER,
                    FEB_UNITS02 INTEGER,
                    MAR_UNITS03 INTEGER,
                    APR_UNITS04 INTEGER,
                    MAY_UNITS05 INTEGER,
                    JUN_UNITS06 INTEGER,
                    JUL_UNITS07 INTEGER,
                    AUG_UNITS08 INTEGER,
                    SEP_UNITS09 INTEGER,
                    OCT_UNITS10 INTEGER,
                    NOV_UNITS11 INTEGER,
                    DEC_UNITS12 INTEGER,
                    ADJ_UNITS13 INTEGER,
                    ACTIVE CITEXT)
                WITH (OIDS=FALSE);
            """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """
                    from stdin with csv encoding 'latin-1'""", io)
    ops.log_pass(run_id)
    print 'Passssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    print error
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
