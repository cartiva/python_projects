# encoding=utf-8
# http://trac.edgewall.org/wiki/TracDev/DatabaseApi#RulesforDBAPIUsage
# Parameter passing
# Always use the "format" parameter style, and always with %s (because that's the only type that pyPgSQL supports).
# Statement parameters always need to be passed into execute as an actual sequence (list or tuple).
#
# So the following statements are okay:
# cursor.execute("SELECT author,ipnr,comment FROM wiki WHERE name=%s", [thename])
# cursor.execute("SELECT id FROM ticket WHERE time>=%s AND time<=%s", (start, stop))
#
# The following uses are not okay:
# cursor.execute("SELECT author,ipnr,comment FROM wiki WHERE name=?", thename)
# cursor.execute("SELECT id FROM ticket WHERE time>=%i AND time<=%i", start, stop)
# At any cost, try avoiding the use of ​string formatting to get values into the SQL statement. The database
# automatically escapes values you pass using execute() arguments, the same is not true if you use string formatting,
# opening your code up to ​SQL injection attacks.
#
# On the other hand, you must use string formatting to dynamically specify names of tables or columns, i.e.
# anything that is not a value as such:
# cursor.execute("SELECT time FROM %s WHERE name=%%s" % db.quote(table), (thename,))import psycopg2
import psycopg2
# employee_number = """'16425'"""
# employee_number = repr('16425')
employee_number = '16425'
current_row = 'true'
pgCon = psycopg2.connect("host='localhost' dbname='Test1' user='postgres' password='cartiva'")
pgCursor = pgCon.cursor()
print psycopg2.paramstyle

# sql = "select * from dds.edwemployeedim where employeenumber = '16425'"
# this works
# sql = "select * from dds.edwemployeedim where employeenumber = %s"
# sql = sql % employee_number
# these work (sql.format) if assignment is : employee_number = '16425'
# pgCursor.execute("select * from dds.edwemployeedim where employeenumber = '{0}'".format(employee_number))
# sql = "select * from dds.edwemployeedim where employeenumber = '{0}' and currentrow = '{1}'".format(employee_number, current_row)
# fails with TypeError: not all arguments converted during string formatting
# regardless of the assignment style
# this is the "format" paramter style, assignment is employee_number = '16425'
# all of these work
# http://initd.org/psycopg/docs/usage.html#passing-parameters-to-sql-queries
# pgCursor.execute("select * from dds.edwemployeedim where employeenumber = %s", (employee_number,))
# pgCursor.execute("select * from dds.edwemployeedim where employeenumber = %s", (employee_number,))
# pgCursor.execute("select * from dds.edwemployeedim where employeenumber = %s", [employee_number])
# to print the actual query sent to db when not using intermediate variables for the sql
print pgCursor.mogrify("select * from dds.edwemployeedim where employeenumber = %s", [employee_number])
pgCursor.execute("select * from dds.edwemployeedim where employeenumber = %s", [employee_number])
# sql = "select * from dds.edwemployeedim where employeenumber = %s"
# pgCursor.execute(sql, [employee_number])
# pyformat
# works with assignment: employee_number = '16425'
# pgCursor.execute("select * from dds.edwemployeedim where employeenumber = %(employee_number)s", {'employee_number' :employee_number})
# pgCursor.execute(sql, employee_number)
# http://bobby-tables.com/python.html
# fails with TypeError: string indices must be integers, not str, regardless of assignment
# sql = "select * from dds.edwemployeedim where employeenumber=%(employee_number)s"
# pgCursor.execute(sql, employee_number)
# print sql
# pgCursor.execute(sql)
the_result = pgCursor.fetchall()
for t in the_result:
    print 't: ' + str(type(t))
    print 't: ' + str(t)
print 'the_result:  ' + str(type(the_result))
print 'the_result: ' + str(the_result)
pgCursor.close()
pgCon.close()
