# encoding=utf-8
# http://trac.edgewall.org/wiki/TracDev/DatabaseApi#RulesforDBAPIUsage
# Parameter passing
# Always use the "format" parameter style, and always with %s (because that's the only type that pyPgSQL supports).
# Statement parameters always need to be passed into execute as an actual sequence (list or tuple).
#
# So the following statements are okay:
# cursor.execute("SELECT author,ipnr,comment FROM wiki WHERE name=%s", [thename])
# cursor.execute("SELECT id FROM ticket WHERE time>=%s AND time<=%s", (start, stop))
#
# The following uses are not okay:
# cursor.execute("SELECT author,ipnr,comment FROM wiki WHERE name=?", thename)
# cursor.execute("SELECT id FROM ticket WHERE time>=%i AND time<=%i", start, stop)
# At any cost, try avoiding the use of ​string formatting to get values into the SQL statement. The database
# automatically escapes values you pass using execute() arguments, the same is not true if you use string formatting,
# opening your code up to ​SQL injection attacks.
#
# On the other hand, you must use string formatting to dynamically specify names of tables or columns, i.e.
# anything that is not a value as such:
# cursor.execute("SELECT time FROM %s WHERE name=%%s" % db.quote(table), (thename,))
import adsdb
ads_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
                        userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                        TrimTrailingSpaces='TRUE')
stocknumber = '25409XXR'
# stocknumber = repr('25409XXR')
body = ''
master_cursor = ads_con.cursor()
query = """
    SELECT b.stocknumber,
      trim(stocknumber) + ':  Inspected by ' + trim(c.fullname),
      trim(f.yearmodel) + ' ' + trim(f.make) + ' ' + trim(f.model) + ' '
          + coalesce(trim(f.TRIM), '') + ' ' + TRIM(coalesce(f.bodystyle)),
        'Engine: ' + trim(coalesce(f.engine))
    FROM vehicleinspections a
    INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    LEFT JOIN organizations bb on b.owninglocationid = bb.partyid
      AND bb.name = 'Rydells'
    LEFT JOIN people c on a.technicianid = c.partyid
    LEFT JOIN VehicleItems f on a.VehicleItemID = f.VehicleItemID
    LEFT JOIN VehicleWalks g on a.VehicleInventoryItemID = g.VehicleInventoryItemID
    WHERE b.stocknumber = '{}'""".format(stocknumber)
print 'query: '
print query
master_cursor.execute(query)
master_result = master_cursor.fetchall()
for t in master_result:
    print t[0] + '  ' + t[1]
    print 't: ' + str(type(t))
    print 't: ' + str(t)
print 'master_result:  ' + str(type(master_result))
print 'master_result: ' + str(master_result)
for t in master_result:
    body += t[1] + '\n' + t[2] + '\n' + t[3] + '\n'
    detail_cursor = ads_con.cursor()
    stocknumber = "'" + t[0] + "'"
    detail_query = """
        SELECT b.stocknumber, d.description,
          d.totalpartsamount, d.laboramount, ee.description AS Area
        FROM vehicleinspections a
        INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
        INNER JOIN organizations bb on b.owninglocationid = bb.partyid
          AND bb.name = 'Rydells'
        INNER JOIN people c on a.technicianid = c.partyid
        INNER JOIN VehicleReconItems d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
          AND d.typ <> 'MechanicalReconItem_Inspection'
        INNER JOIN typcategories e on d.typ = e.typ
          AND e.category = 'MechanicalReconItem'
        INNER JOIN typdescriptions ee on e.typ = ee.typ
        WHERE b.stocknumber = {}""".format(stocknumber)
    print detail_query
    detail_cursor.execute(detail_query)
    detail_result = detail_cursor.fetchall()
    print 'detail result:  ' + str(detail_result)
    if detail_cursor.rowcount == 0:
        body += 'No mechanical recon' + '\n'
    else:
        for k in detail_result:
            body += k[4] + ': ' + k[1] + '  Parts: ' + str(k[2]) + ' Labor ' + str(k[3]) + '\n'
    body += '\n'
    detail_cursor.close()
print body
master_cursor.close()
ads_con.close()
