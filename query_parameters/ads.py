# encoding=utf-8
# http://trac.edgewall.org/wiki/TracDev/DatabaseApi#RulesforDBAPIUsage
# Parameter passing
# Always use the "format" parameter style, and always with %s (because that's the only type that pyPgSQL supports).
# Statement parameters always need to be passed into execute as an actual sequence (list or tuple).
#
# So the following statements are okay:
# cursor.execute("SELECT author,ipnr,comment FROM wiki WHERE name=%s", [thename])
# cursor.execute("SELECT id FROM ticket WHERE time>=%s AND time<=%s", (start, stop))
#
# The following uses are not okay:
# cursor.execute("SELECT author,ipnr,comment FROM wiki WHERE name=?", thename)
# cursor.execute("SELECT id FROM ticket WHERE time>=%i AND time<=%i", start, stop)
# At any cost, try avoiding the use of ​string formatting to get values into the SQL statement. The database
# automatically escapes values you pass using execute() arguments, the same is not true if you use string formatting,
# opening your code up to ​SQL injection attacks.
#
# On the other hand, you must use string formatting to dynamically specify names of tables or columns, i.e.
# anything that is not a value as such:
# cursor.execute("SELECT time FROM %s WHERE name=%%s" % db.quote(table), (thename,))import psycopg2
# employee_number = """'16425'"""
# employee_number = repr('16425')import adsdb
import adsdb
ads_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
                        userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                        TrimTrailingSpaces='TRUE')
# both of these meams of setting stock number work
# stocknumber = """'25409XXR'"""
# stocknumber = repr('25409XXR')
stocknumber = '25409XXR'
ads_cursor = ads_con.cursor()
print adsdb.paramstyle
# query = """
#     select *
#     from VehicleInventoryItems
#      where stocknumber = %s"""
# print 'query: ' + query
# sql = query % dict(x=stocknumber)
# print 'sql: ' + sql
# the_cursor.execute(query, (stocknumber))

# as a base, this works
# the_cursor.execute("""select * from vehicleInventoryItems where stocknumber = '25409XXR'""")

# http://stackoverflow.com/questions/775296/python-mysql-with-variables
# this works
# sql = "select * from vehicleInventoryItems where stocknumber = %s"
# sql = sql % stocknumber
# these both work when assignment is: stocknumber = '25409XXR'
sql = "select * from vehicleInventoryItems where stocknumber = '{0}'".format(stocknumber)
# the_cursor.execute("select * from vehicleInventoryItems where stocknumber = '{0}'".format(stocknumber))
# this fails with TypeError: unhashapble type
# the_cursor.execute("select * from vehicleInventoryItems where stocknumber=:stocknumber", {"stocknumber": stocknumber})
# this works
# sql = "select * from vehicleInventoryItems where stocknumber = %s" % stocknumber
# this works if assignment is: stocknumber = '25409XXR'
# sql = "select * from vehicleInventoryItems where stocknumber = '%s'" % stocknumber
# actually adsdb.py specifies paramstyle = qmark
# but this returns an empty set
# the_cursor.execute("select * from vehicleInventoryItems where stocknumber=?", stocknumber)
print sql
ads_cursor.execute(sql)
the_result = ads_cursor.fetchall()
for t in the_result:
    print t[0] + '  ' + t[1]
    print 't: ' + str(type(t))
    print 't: ' + str(t)
print 'the_result:  ' + str(type(the_result))
print 'the_result: ' + str(the_result)
ads_cursor.close()
ads_con.close()

# import adsdb
# the dictionary method
# ads_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
#                         userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
#                         TrimTrailingSpaces='TRUE')
# # both of these meams of setting stock number work
# # stocknumber = """'25409XXR'"""
# stocknumber = repr('25409XXR')
# the_cursor = ads_con.cursor()
# query = """
#     select *
#     from VehicleInventoryItems
#      where stocknumber = %(x)s"""
# print 'query: ' + query
# sql = query % dict(x=stocknumber)
# print 'sql: ' + sql
# the_cursor.execute(sql)
# the_result = the_cursor.fetchall()
# for t in the_result:
#     print t[0] + '  ' + t[1]
#     print 't: ' + str(type(t))
#     print 't: ' + str(t)
# print 'the_result:  ' + str(type(the_result))
# print 'the_result: ' + str(the_result)
# ads_cursor.close()
# ads_con.close()