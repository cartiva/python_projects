﻿/*
create table if not exists chr.maint_nvd(
    table_name citext unique not null,
    file_name citext unique not null,
    load_sequence integer unique not null,
    truncate_sequence integer unique not null)
with (OIDS=FALSE);   
*/
truncate chr.maint_nvd;
insert into chr.maint_nvd(table_name,file_name,load_sequence,truncate_sequence)
values
('ci_types','CITypes.txt',1,26),
('norm_ci_labels','NormCILabels.txt',2,25),
('mkt_class','MktClass.txt',3,24),
('opt_kinds','OptKinds.txt',4,23),
('tech_title_header','TechTitleHeader.txt',5,22),
('opt_headers','OptHeaders.txt',6,21),
('std_headers','StdHeaders.txt',7,20),
('category_headers','CategoryHeaders.txt',8,19),
('manufacturers','Manufacturers.txt',9,18),
('tech_titles','TechTitles.txt',10,17),
('divisions','Divisions.txt',11,16),
('categories','Categories.txt',12,15),
('subdivisions','Subdivisions.txt',13,14),
('models','Models.txt',14,13),
('styles','Styles.txt',15,12),
('jpgs','JPGs.txt',16,11),
('options','Options.txt',17,10),
('prices','Prices.txt',18,9),
('norm_cons_info','NormConsInfo.txt',19,8),
('cons_info','ConsInfo.txt',20,7),
('colors','Colors.txt',21,6),
('body_styles','BodyStyles.txt',22,5),
('standards','Standards.txt',23,4),
('style_cats','StyleCats.txt',24,3),
('tech_specs','TechSpecs.txt',25,2),
('version','Version.txt',26,1);

ALTER TABLE chr.maint_nvd ADD PRIMARY KEY (table_name);

alter table chr.maint_nvd
add column tmp_table_name citext;
update chr.maint_nvd
set tmp_table_name = 'tmp_'||table_name;

alter table chr.maint_nvd
ADD column primary_key citext,
ADD column has_style_id boolean;


select *
from chr.maint_nvd a
left join information_schema.tables b on a.table_name = b.table_name
order by a.table_name

select *
from information_schema.key_column_usage
where constraint_schema = 'chr'
  and constraint_name not like 'tmp%'


SELECT * 
from chr.maint_nvd a
left join information_schema.columns b on a.table_name = b.table_name
  and column_name = 'style_id'
WHERE table_schema = 'chr'
order by a.load_sequence



-- from : http://solaimurugan.blogspot.com/2010/10/list-out-all-forien-key-constraints.html
select a.table_name, b.column_name as primary_key, 
  case when c.column_name = 'style_id' then true else false end as has_style_id
from chr.maint_nvd a
left join (
  SELECT tc.constraint_name,tc.constraint_type,tc.table_name,kcu.column_name,
    tc.is_deferrable,tc.initially_deferred,rc.match_option AS match_type,
    rc.update_rule AS on_update,rc.delete_rule AS on_delete,ccu.table_name AS references_table,
    ccu.column_name AS references_field
  FROM information_schema.table_constraints tc
  LEFT JOIN information_schema.key_column_usage kcu ON tc.constraint_catalog = kcu.constraint_catalog
    AND tc.constraint_schema = kcu.constraint_schema
    AND tc.constraint_name = kcu.constraint_name
  LEFT JOIN information_schema.referential_constraints rc ON tc.constraint_catalog = rc.constraint_catalog
    AND tc.constraint_schema = rc.constraint_schema
    AND tc.constraint_name = rc.constraint_name
  LEFT JOIN information_schema.constraint_column_usage ccu ON rc.unique_constraint_catalog = ccu.constraint_catalog
    AND rc.unique_constraint_schema = ccu.constraint_schema
    AND rc.unique_constraint_name = ccu.constraint_name
  where constraint_type = 'PRIMARY KEY') b on a.table_name = b.table_name
left join information_schema.columns c on a.table_name = c.table_name
  and c.column_name = 'style_id'  
order by load_sequence

update chr.maint_nvd
set primary_key = x.primary_key,
    has_style_id = x.has_style_id
from (    
  select a.table_name, b.column_name as primary_key, 
    case when c.column_name = 'style_id' then true else false end as has_style_id
  from chr.maint_nvd a
  left join (
    SELECT tc.constraint_name,tc.constraint_type,tc.table_name,kcu.column_name,
      tc.is_deferrable,tc.initially_deferred,rc.match_option AS match_type,
      rc.update_rule AS on_update,rc.delete_rule AS on_delete,ccu.table_name AS references_table,
      ccu.column_name AS references_field
    FROM information_schema.table_constraints tc
    LEFT JOIN information_schema.key_column_usage kcu ON tc.constraint_catalog = kcu.constraint_catalog
      AND tc.constraint_schema = kcu.constraint_schema
      AND tc.constraint_name = kcu.constraint_name
    LEFT JOIN information_schema.referential_constraints rc ON tc.constraint_catalog = rc.constraint_catalog
      AND tc.constraint_schema = rc.constraint_schema
      AND tc.constraint_name = rc.constraint_name
    LEFT JOIN information_schema.constraint_column_usage ccu ON rc.unique_constraint_catalog = ccu.constraint_catalog
      AND rc.unique_constraint_schema = ccu.constraint_schema
      AND rc.unique_constraint_name = ccu.constraint_name
    where constraint_type = 'PRIMARY KEY') b on a.table_name = b.table_name
  left join information_schema.columns c on a.table_name = c.table_name
    and c.column_name = 'style_id') x
where chr.maint_nvd.table_name = x.table_name

select *
from chr.maint_nvd
order by load_sequence





    



         