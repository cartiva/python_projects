﻿create TABLE chr.tmp_ci_types(   
    type_id integer primary key,
    ci_type citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_norm_ci_labels(
    label_id integer primary key,
    norm_ci_label citext,
    type_id integer not null references chr.tmp_ci_types,
    label_sequence integer not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_mkt_class(
    mkt_class_id integer primary key,
    market_class citext not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_opt_kinds(
    option_kind_id integer primary key,
    option_kind citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_tech_title_header(
    tech_title_header_id integer primary key,
    tech_title_header_text citext NOT NULL,
    tech_title_header_sequence integer not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_opt_headers(
    header_id integer primary key,
    opt_header citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_std_headers(
    header_id integer primary key,
    std_header citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_category_headers(
    category_header_id integer primary key,
    category_header citext,
    category_header_sequence integer not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_manufacturers(
    manufacturer_id integer primary key,
    manufacturer_name citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_tech_titles(
    title_id integer primary key,
    tech_title_sequence integer not null,
    title citext NOT NULL,
    tech_title_header_id integer not null references chr.tmp_tech_title_header)
WITH (OIDS=FALSE);

create TABLE chr.tmp_divisions(
    division_id integer primary key,
    manufacturer_id integer not null references chr.tmp_manufacturers,
    division_name citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_categories(
    category_id integer primary key,
    category citext,
    category_type_filter citext,
    category_header_id integer references chr.tmp_category_headers,
    user_friendly_name citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_subdivisions(
    model_year integer not null,
    division_id integer not null references chr.tmp_divisions,
    subdivision_id integer primary key,
    hist_subdivision_id integer,
    subdivision_name citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_models(
    model_id integer primary key,
    hist_model_id integer,
    model_year integer not null,
    division_id integer not null references chr.tmp_divisions,
    subdivision_id integer not null references chr.tmp_subdivisions,
    model_name citext,
    effective_date date,
    model_comment citext,
    availability citext not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_Styles(
    style_id integer primary key,
    hist_style_id integer,
    model_id integer not null references chr.tmp_models,
    model_year integer,
    style_sequence integer not null,
    style_code citext not null,
    full_style_code citext,
    style_name citext,
    tru_base_price citext not null,
    invoice numeric (12,2),
    msrp numeric (12,2),
    destination numeric (12,2),
    style_cvc_list citext,
    mkt_class_id integer not null references chr.tmp_mkt_class,
    style_name_wo_trim citext,
    trim_level citext,
    passenger_capacity integer,
    passenger_doors integer,
    manual_trans citext,
    auto_trans citext,
    front_wd citext,
    rear_wd citext,
    all_wd citext,
    four_wd citext,
    step_side citext,
    caption citext,
    availability citext not null,
    price_State citext not null,
    auto_builder_style_id citext not null,
    cf_model_name citext,
    cf_style_name citext,
    cf_drive_train citext,
    cf_body_type citext)
 WITH (OIDS=FALSE);

create TABLE chr.tmp_jpgs(
    style_id integer not null references chr.tmp_Styles,
    jpg_name citext NOT NULL,
    subdivision_name citext not null,
    model_name citext not null,
    style_name citext not null,
    model_year integer not null)
WITH (OIDS=FALSE);

 create TABLE chr.tmp_Options(
    style_id integer not null references chr.tmp_Styles,
    header_id integer not null references chr.tmp_opt_headers,
    option_sequence integer not null,
    opction_code citext not null,
    option_desc citext,
    option_kind_id integer not null references chr.tmp_opt_kinds,
    category_list citext,
    availability citext not null,
    pon citext,
    ext_description citext,
    supported_logic citext,
    unsupported_logic citext,
    price_notes citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_prices(
    style_id integer not null references chr.tmp_Styles,
    price_sequence integer not null,
    option_code citext not null,
    price_rule_desc citext,
    condition citext,
    invoice numeric (12,2),
    msrp numeric (12,2),
    price_state citext not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_norm_cons_info(
    style_id integer not null references chr.tmp_Styles,
    label_id integer not null references chr.tmp_norm_ci_labels,
    norm_cons_info_value citext,
    qualifier citext,
    qualifier_sequence integer,
    norm_cons_info_group integer not null)
WITH (OIDS=FALSE);

create TABLE chr.tmp_cons_info(
    style_id integer not null references chr.tmp_Styles,
    type_id integer not null references chr.tmp_ci_types,
    cons_info_text citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_Colors(
    style_id integer not null references chr.tmp_Styles,
    ext1_code citext not null,
    ext_code citext,
    int_code citext,
    ext1_man_code citext,
    ext2_man_code citext,
    int_man_code citext,
    order_code citext,
    as_two_tone citext not null,
    ext1_Desc citext,
    ext2_desc citext,
    int_Desc citext,
    condition citext,
    generic_ext_color citext,
    generic_ext2_color citext,
    ext1_rgb_hex citext,
    ext2_rgb_hex citext,
    ext1_mfr_full_code citext,
    ext2_mfr_full_code citext)
WITH (OIDS=FALSE);

create TABLE chr.tmp_body_styles(
    style_id integer not null references chr.tmp_Styles,
    body_style citext,
    is_primary citext not null)
WITH (OIDS=FALSE);

 create TABLE chr.tmp_standards(
    style_id integer not null references chr.tmp_Styles,
    header_id integer not null references chr.tmp_std_headers,
    standards_sequence integer NOT NULL,
    Standard citext,
    CategoryList citext) 
WITH (OIDS=FALSE);

create TABLE chr.tmp_style_cats(
    style_id integer not null references chr.tmp_Styles,
    category_id integer not null references chr.tmp_categories,
    feature_type citext not null,
    style_cats_sequence integer not null,
    state citext)
WITH (OIDS=FALSE); 
 
create TABLE chr.tmp_tech_specs(
    style_id integer not null references chr.tmp_Styles,
    title_id integer not null references chr.tmp_tech_titles,
    tech_specs_sequence integer not null,
    tech_specs_text citext,
    condition citext)
WITH (OIDS=FALSE);


