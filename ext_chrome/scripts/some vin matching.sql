﻿select *
from chr.year_make_model_style
limit 1000 

select *
from chr.styles
-- where style_code = 'CK25903'
limit 1000 

select *
from chr.models


1GC0KUEG8FZ521690


-- 15 msec
DECLARE @vin string;
@vin = '2GTEK19K6S1501685';
SELECT *
FROM VinPattern
WHERE LEFT(VinPattern, 8) = LEFT(@vin, 8)
AND @vin LIKE REPLACE(vinpattern, '*', '_');









with dkey as (select 1 as wtf)
select *
from dds.day 
where datekey = (select wtf from dkey)

with vin as (select '2GTEK19K6S1501685'::text as the_vin)
SELECT *
FROM chr.vin_pattern
WHERE LEFT(vin_pattern, 8) = (select LEFT(the_vin,8) from vin)
AND (select the_vin from vin) LIKE REPLACE(vin_pattern, '*', '_');


-- aha ymms.mfr_style_code = arkona model code
SELECT *
FROM chr.vin_pattern a
inner join chr.vin_pattern_style_mapping b on a.vin_pattern_id = b.vin_pattern_id
inner join chr.year_make_model_style c on b.chrome_style_id = c.chrome_style_id
inner join chr.styles d on c.chrome_style_id = d.style_id
left join chr.models e on d.model_id = e.model_id
inner join chr.body_styles f on d.style_id = f.style_id
WHERE '1GKS2AKC0GR144125'::text LIKE REPLACE(vin_pattern, '*', '_');

-- chrome new car body styles
select body_style, count(*)
from chr.body_styles
group by body_style



select *
from chr.year_make_model_style
where mfr_style_code = '4HP69'


there should be vins that resolve to multiple chrome vinpatterns

-- this vin: 1GCVKREC1FZ226902 in eval gives 2 trim options
with vin as (select '1GCVKREC1FZ226902'::text as the_vin)
SELECT *
FROM chr.vin_pattern
WHERE LEFT(vin_pattern, 8) = (select LEFT(the_vin,8) from vin)
AND (select the_vin from vin) LIKE REPLACE(vin_pattern, '*', '_');
-- yep, 2 
select * from chr.vin_pattern_style_mapping where vin_pattern_id = 3489194
select * from chr.year_make_model_style where chrome_style_id in (select chrome_style_id from chr.vin_pattern_style_mapping where vin_pattern_id = 3489194)
select * from chr.styles where style_id in (select chrome_style_id from chr.vin_pattern_style_mapping where vin_pattern_id = 3489194)
-- but what is goofy: tool: LT or LT Z71
-- chrome: w/1LT or w/2LT


select distinct cf_body_type from chr.styles


select category_type, count(*) from chr.category group by category_type

select a.vin, m.*
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
 
  

select a.vin, a.yearmodel as "bb model year", a.make as "bb make", a.model as "bb model", 
  a.bodystyle as "bb body style", a.trim as "bb trim",
  a.engine as "bb engine", a.vinresolved, 
  substring(b.vehicletype from position('_' in b.vehicletype) + 1 for 12) as shape, 
  substring(b.vehiclesegment from position('_' in b.vehiclesegment) + 1 for 12) as size, 
  m.vin_pattern, m.model_year, m.vin_division_name, m.vin_model_name, 
  m.vin_style_name, m.engine_size, n.description,
  p.*
from dps.vehicleitems a
left join dps.makemodelclassifications b on a.make = b.make
  and a.model = b.model  
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.category n on m.engine_type_category_id = n.category_id  
left join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id
left join chr.year_make_model_style p on o.chrome_style_id = p.chrome_style_id
limit 100 



-- 11/9 segregate vins with single vs multiple chrome style ids
select vin from (
select a.vin, m.*
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
) x group by vin having count(*) > 1

-- only one anomalous vin that decodes to 2 vin_patterns
select a.vin, m.*
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
where a.vin = '2CNDL13F186301602'


select a.vin, m.*, n.*
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.vin_pattern_style_mapping n on m.vin_pattern_id = n.vin_pattern_id   

-- how many chrome_style_ids per vin_pattern_id
select chrome_style_count, count(*) as how_many
from (
  select a.vin_pattern_id, count(*) as chrome_style_count
  from chr.vin_pattern a
  left join chr.vin_pattern_style_mapping b on a.vin_pattern_id = b.vin_pattern_id
  group by a.vin_pattern_id) x
group by chrome_style_count
order by chrome_style_count


select a.vin, m.vin_pattern_id, n.chrome_style_id, count(*)
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.vin_pattern_style_mapping n on m.vin_pattern_id = n.vin_pattern_id 
group by a.vin, m.vin_pattern_id, n.chrome_style_id

select a.vin, count(*)
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.vin_pattern_style_mapping n on m.vin_pattern_id = n.vin_pattern_id 
group by a.vin


select a.vin, m.vin_pattern_id, n.chrome_style_id
-- select *
from dps.vehicleitems a
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.vin_pattern_style_mapping n on m.vin_pattern_id = n.vin_pattern_id 
left join chr.year_make_model_style o on n.chrome_style_id = o.chrome_style_id
where a.vin = '4T1BK36B47U205591' -- a vin with 10 chrome_style_ids


-- i don't fucking exactly know what i am trying to produce

select a.vin, a.yearmodel as "bb model year", a.make as "bb make", a.model as "bb model", 
  a.bodystyle as "bb body style", a.trim as "bb trim",
  a.engine as "bb engine", a.vinresolved, 
  substring(b.vehicletype from position('_' in b.vehicletype) + 1 for 12) as shape, 
  substring(b.vehiclesegment from position('_' in b.vehiclesegment) + 1 for 12) as size, 
  m.vin_pattern, m.model_year, m.vin_division_name, m.vin_model_name, 
  m.vin_style_name, m.engine_size, n.description,
  p.*,
  q.*, 
  r.*
from dps.vehicleitems a
left join dps.makemodelclassifications b on a.make = b.make
  and a.model = b.model  
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.category n on m.engine_type_category_id = n.category_id  
left join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id
left join chr.year_make_model_style p on o.chrome_style_id = p.chrome_style_id
left join chr.styles q on p.chrome_style_id = q.style_id
left join chr.mkt_class r on q.mkt_class_id = r.mkt_class_id
limit 100 


style_name designations:
PZEV: Partial Zero Emissions Vehicle
SD: Southeast
GS: Gulf States
Natl: Nationwide


select *
from (
  select yearmodel, make, model, bodystyle, trim, max(vin) as vin, left(max(vin), 8) as left_vin_8
  from dps.vehicleitems 
  where vinresolved = true
  group by yearmodel, make, model, bodystyle, trim) a
left join dps.makemodelclassifications b on a.make = b.make
  and a.model = b.model  
left join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
left join chr.category n on m.engine_type_category_id = n.category_id  
left join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id
left join chr.year_make_model_style p on o.chrome_style_id = p.chrome_style_id


-- allright god damnit
-- vins with a single chrome_style_id
select a.vin
from dps.vehicleitems a
inner join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
  and a.vin like replace(m.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id  
where a.vinresolved = true
group by a.vin 
having count(*) = 1

select u.*,
  substring(v.vehicletype from position('_' in v.vehicletype) + 1 for 12) as shape, 
  substring(v.vehiclesegment from position('_' in v.vehiclesegment) + 1 for 12) as size
from (  
  select s.yearmodel, s.make, s.model, s.bodystyle, s.trim, max(s.vin) as vin, left(max(s.vin), 8) as left_vin_8
  from dps.vehicleitems s
  inner join (
    select a.vin
    from dps.vehicleitems a
    inner join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
      and a.vin like replace(m.vin_pattern, '*', '_')
    inner join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id  
    where a.vinresolved = true
    group by a.vin 
    having count(*) = 1) t on s.vin = t.vin
  group by s.yearmodel, s.make, s.model, s.bodystyle, s.trim) u
inner join dps.makemodelclassifications v on u.make = v.make
  and u.model = v.model  

create table chr.z_tool_1 (
  vin citext primary key,
  model_year integer not null,
  make citext not null,
  model citext not null,
  body_style citext not null,
  trim_level citext,
  left_vin_8 citext not null,
  shape citext not null,
  size citext not null);
COMMENT ON TABLE chr.z_tool_1 IS 'tool vins with single chrome_style_id';  
CREATE INDEX ON chr.z_tool_1(left_vin_8);

insert into chr.z_tool_1
select u.vin, u.yearmodel::integer, u.make, u.model, u.bodystyle, u.trim, u.left_vin_8,
  substring(v.vehicletype from position('_' in v.vehicletype) + 1 for 12) as shape, 
  substring(v.vehiclesegment from position('_' in v.vehiclesegment) + 1 for 12) as size
from (  
  select s.yearmodel, s.make, s.model, s.bodystyle, s.trim, max(s.vin) as vin, left(max(s.vin), 8) as left_vin_8
  from dps.vehicleitems s
  inner join (
    select a.vin
    from dps.vehicleitems a
    inner join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
      and a.vin like replace(m.vin_pattern, '*', '_')
    inner join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id  
    where a.vinresolved = true
    group by a.vin 
    having count(*) = 1) t on s.vin = t.vin
  group by s.yearmodel, s.make, s.model, s.bodystyle, s.trim) u
inner join dps.makemodelclassifications v on u.make = v.make
  and u.model = v.model;  


select distinct body_style from chr.body_styles

select a.vin, a.model_year, a.make, a.model, a.body_style, a.trim_level, a.shape, a.size,  
  b.vin_model_name, b.vin_style_name, c.description, b.engine_size,
  e.model_name, e.style_name, e.trim_name, e.mfr_style_code,
  f.style_name, f.style_name_wo_trim, f.trim_level, f.cf_model_name, f.cf_style_name,
  f.cf_drive_train, f.cf_body_type,
  g.market_class
from chr.z_tool_1 a
left join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
  and a.vin like replace(b.vin_pattern, '*', '_')
left join chr.category c on b.engine_type_category_id = c.category_id 
left join chr.vin_pattern_style_mapping d on b.vin_pattern_id = d.vin_pattern_id
left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id
left join chr.styles f on e.chrome_style_id = f.style_id
left join chr.mkt_class g on f.mkt_class_id = g.mkt_class_id
order by a.shape, a.size, make, model





select a.vin, a.model_year, a.make, a.model, a.body_style, a.trim_level, a.shape, a.size,  
  b.vin_style_name, 
  e.style_name, e.trim_name, 
  f.style_name, f.style_name_wo_trim, f.trim_level, f.cf_model_name, f.cf_style_name,
  f.cf_drive_train, f.cf_body_type,
  g.market_class
from chr.z_tool_1 a
left join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
  and a.vin like replace(b.vin_pattern, '*', '_')
left join chr.category c on b.engine_type_category_id = c.category_id 
left join chr.vin_pattern_style_mapping d on b.vin_pattern_id = d.vin_pattern_id
left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id
left join chr.styles f on e.chrome_style_id = f.style_id
left join chr.mkt_class g on f.mkt_class_id = g.mkt_class_id
-- left join chr.style_wheel_base h on e.chrome_style_id = h.chrome_style_id
 where a.model like 'silvera%'
-- where cf_style_name is null   -- new car info on '97 and newer only
order by a.shape, a.size, make, model

--11/11
chrome body_styles include nothing regarding size
select distinct body_style from chr.body_styles

-- don't care, initially, about multiple chromestyleids, instead looking for single
rows that include the categorization info that i want
-- add style_wheel_base -- nah, doesn't really add anything
look at seating capacity, length, 
so, what are the attributes i want
1. only the new car data from chrome, eliminate year_make_model_style
select a.vin, a.model_year, a.make, a.model, a.body_style, a.trim_level, a.shape, a.size,  
  b.vin_style_name, 
  --e.style_name, e.trim_name, 
  f.style_name, f.style_name_wo_trim, f.trim_level, f.cf_model_name, f.cf_style_name,
  f.cf_drive_train, f.cf_body_type, f.passenger_capacity,
  g.market_class,
  h.tech_specs_text
from chr.z_tool_1 a
left join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
  and a.vin like replace(b.vin_pattern, '*', '_')
left join chr.category c on b.engine_type_category_id = c.category_id 
left join chr.vin_pattern_style_mapping d on b.vin_pattern_id = d.vin_pattern_id
-- left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id
left join chr.styles f on d.chrome_style_id = f.style_id
left join chr.mkt_class g on f.mkt_class_id = g.mkt_class_id
--left join chr.tech_specs h on f.style_id = h.style_id -- nah
--  and h.title_id = 179 -- length
-- left join chr.style_wheel_base h on e.chrome_style_id = h.chrome_style_id
 where a.model like 'silvera%'
-- where cf_style_name is null   -- new car info on '97 and newer only
order by a.shape, a.size, make, model

-- no immediately recognizable help from categories or tech specs
select * 
from chr.category_headers a
left join chr.categories b on a.category_header_id = b.category_header_id
order by a.category_header_id

drivetrain: categoryid: 1041,1040,1042,1043

select * from chr.style_cats where category_id in (1041,1040,1042,1043) limit 1000

select * from chr.tech_title_header -- id 2 = exterior dimensions
select * from chr.tech_Titles where tech_Title_header_id = 2 -- id 304: length, overall (in), 269: body length default value (ft)
select * from chr.tech_specs where title_id = 304 limit 1000

back to narrowing down the list of attributes
do not care about multiple chrome style ids IF IF IF the new car attributes that i want are the same for all the ids
select a.vin, a.model_year, a.make, a.model, a.body_style, a.trim_level, a.shape, a.size,  
  b.vin_style_name, 
  --e.style_name, e.trim_name, 
  f.style_name, f.style_name_wo_trim, f.trim_level, f.cf_model_name, f.cf_style_name,
  f.cf_drive_train, f.cf_body_type, f.passenger_capacity,
  g.market_class
from chr.z_tool_1 a
left join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
  and a.vin like replace(b.vin_pattern, '*', '_')
left join chr.category c on b.engine_type_category_id = c.category_id 
left join chr.vin_pattern_style_mapping d on b.vin_pattern_id = d.vin_pattern_id
-- left join chr.year_make_model_style e on d.chrome_style_id = e.chrome_style_id
left join chr.styles f on d.chrome_style_id = f.style_id
left join chr.mkt_class g on f.mkt_class_id = g.mkt_class_id
--left join chr.tech_specs h on f.style_id = h.style_id -- nah
--  and h.title_id = 179 -- length
-- left join chr.style_wheel_base h on e.chrome_style_id = h.chrome_style_id
 --where a.model like 'silvera%'
-- where cf_style_name is null   -- new car info on '97 and newer only
order by a.shape, a.size, make, model



select u.*,
  substring(v.vehicletype from position('_' in v.vehicletype) + 1 for 12) as shape, 
  substring(v.vehiclesegment from position('_' in v.vehiclesegment) + 1 for 12) as size
from (  
  select s.yearmodel, s.make, s.model, s.bodystyle, s.trim, max(s.vin) as vin, left(max(s.vin), 8) as left_vin_8
  from dps.vehicleitems s
  inner join (
    select a.vin
    from dps.vehicleitems a
    inner join chr.vin_pattern m on left(a.vin, 8) = left(m.vin_pattern, 8)
      and a.vin like replace(m.vin_pattern, '*', '_')
    inner join chr.vin_pattern_style_mapping o on m.vin_pattern_id = o.vin_pattern_id  
    where a.vinresolved = true
    group by a.vin 
    having count(*) = 1) t on s.vin = t.vin
  group by s.yearmodel, s.make, s.model, s.bodystyle, s.trim) u
inner join dps.makemodelclassifications v on u.make = v.make
  and u.model = v.model  

select a.vin, a.yearmodel, a.make, a.model, d.style_id, d.style_name_wo_trim, d.trim_level, cf_style_name, cf_drive_train, cf_body_type
from dps.vehicleitems a
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id      
where a.vinresolved = true 


-- 11/13

drop table chr.z_vehicle_items cascade;
create table chr.z_vehicle_items (
  vin citext primary key,
  model_year integer NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  body_style citext NOT NULL,
  trim_level citext,
  engine citext,
  shape citext NOT NULL,
  size citext NOT NULL,
  left_vin_8 citext NOT NULL);
Comment on table chr.z_vehicle_items is 'all vin resolved vehicle items 1997 and newer';
create index on chr.z_vehicle_items(left_vin_8);

insert into chr.z_vehicle_items
select a.vin, a.yearmodel::integer, a.make, a.model, a.bodystyle, a.trim, a.engine, 
  substring(b.vehicletype from position('_' in b.vehicletype) + 1 for 12) as shape, 
  substring(b.vehiclesegment from position('_' in b.vehiclesegment) + 1 for 12) as size,
  left(a.vin, 8) as left_vin_8
from dps.vehicleitems a
inner join dps.makemodelclassifications b on a.make = b.make
  and a.model = b.model
where a.vinresolved = true
  and a.yearmodel::integer > 1996;

select a.*, d.*, e.*
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id  
inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id

-- ok, let's narrow this shit down   

select a.*, d.trim_level, cf_drive_train, cf_body_type, market_class
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id  
inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id

select a.model_year, a.make, a.model, a.body_style, coalesce(a.trim_level,'') as trim_level,
  a.engine, a.shape, a.size, d.cf_drive_Train, d.cf_body_type, e.market_class
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id  
inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id  
group by a.model_year, a.make, a.model, a.body_style, coalesce(a.trim_level,'') ,
  a.engine, a.shape, a.size, d.cf_drive_Train, d.cf_body_type, e.market_class;


create table chr.z_grouped_make_models (
  model_year integer,
  make citext,
  model citext,
  body_style citext,  
  trim_level citext,  
  engine citext,  
  shape citext,  
  size citext,  
  cf_drive_train citext,  
  cf_Body_type citext,  
  market_class citext);
Comment on table chr.z_grouped_make_models is 'grouped makes models and chrome info';
--truncate chr.z_grouped_make_models;  
insert into chr.z_grouped_make_models
select a.model_year, a.make, a.model, a.body_style, coalesce(a.trim_level,'') as trim_level,
  a.engine, a.shape, a.size, d.cf_drive_Train, d.cf_body_type, e.market_class
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id  
inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id  
group by a.model_year, a.make, a.model, a.body_style, coalesce(a.trim_level,'') ,
  a.engine, a.shape, a.size, d.cf_drive_Train, d.cf_body_type, e.market_class;

select *
from chr.z_grouped_make_models  
order by model_year, make, model

-- there is no make, model that is categorized by multiple shape/size
select make, model
from (
  select make, model, shape, size
  from chr.z_grouped_make_models  
  group by make, model, shape, size) x
group by make, model
having count(*) > 1

select make, model
from (
  select make, model, cf_body_type, market_class
  from chr.z_grouped_make_models  
  group by make, model, cf_body_type, market_class) x
group by make, model
having count(*) > 1 

select *
from chr.z_grouped_make_models a --limit 1000
inner join (
  select make, model
  from (
    select make, model, cf_body_type, market_class
    from chr.z_grouped_make_models  
    group by make, model, cf_body_type, market_class) x
  group by make, model
  having count(*) > 1) b on a.make = b.make and a.model = b.model
order by a.make, a.model, model_year

-- 11/24 don't need all year_models
select shape, size, make, model, cf_drive_train, cf_body_type, market_class
from chr.z_grouped_make_models  
group by shape, size, make, model, cf_drive_train, cf_body_type, market_class
order by shape, size, make, model

-- 11/24 don't need all year_models, throw in body_style, trim_level -- 3297 rows
select shape, size, make, model, body_style, trim_level, cf_drive_train, cf_body_type, market_class
from chr.z_grouped_make_models  
group by shape, size, make, model, body_style, trim_level,cf_drive_train, cf_body_type, market_class
order by shape, size, make, model

-- 11/24 don't need all year_models, throw in body_style, leave out trim_level -- 1567 rows
select shape, size, make, model, body_style, cf_drive_train, cf_body_type, market_class
from chr.z_grouped_make_models  
group by shape, size, make, model, body_style, cf_drive_train, cf_body_type, market_class
order by make, model

-- so, which bb make model body_stye have multiple rows
-- some are bb.engines, some are trim_level, some market_class, some cf_drive_trime
-- ie it's all over the fucking place
select *
from ( -- mult make/model/body_style
  select make, model, body_style
  from chr.z_grouped_make_models
  group by make, model, body_style
  having count(*) > 1) a
left join chr.z_grouped_make_models b on a.make = b.make and a.model = b.model and a.body_style = b.body_style
order by a.make, a.model, a.body_style


  


-- sebrings s
select model_year, style_name, cf_drive_train, cf_body_type, market_class
from chr.styles a
inner join chr.mkt_class b on a.mkt_class_id = b.mkt_class_id
where cf_model_name = 'sebring'
order by model_year, style_name

-- spectra, some are'4-door Compact Passenger Car', some are '4-door Mid-Size Passenger Car'
-- seem to be separated by model_year
select model_year, style_name, cf_drive_train, cf_body_type, market_class
from chr.styles a
inner join chr.mkt_class b on a.mkt_class_id = b.mkt_class_id
where cf_model_name = 'spectra'
order by model_year, style_name

-- 11/24 don't need all model_years
-- throw in min/max model_years
select shape, size, make, model, cf_drive_train, cf_body_type, market_class, min(model_year), max(model_year)
from chr.z_grouped_make_models  9
group by shape, size, make, model, cf_drive_train, cf_body_type, market_class
order by make, model, shape, size





select model_year, style_name, cf_model_name, cf_style_name, cf_drive_train, cf_body_type, market_class
from chr.styles a
inner join chr.mkt_class b on a.mkt_class_id = b.mkt_class_id
where cf_model_name = 'spectra'
order by model_year, style_name



-- models, count is the number of model_ids for each model
-- min/max model year is not necessarily congiguous, ie, 2006/2012 does
-- not mean that the model existed in each of the years between 2006 and 2012
select a.manufacturer_name, b.division_name, c.subdivision_name, 
  d.model_name, 
  min(c.model_year), max(c.model_year), count(*) as the_count
-- select *
from chr.manufacturers a
left join chr.divisions b on a.manufacturer_id = b.manufacturer_id
left join chr.subdivisions c on b.division_id = c.division_id
left join chr.models d on b.division_id  = d.division_id 
  and c.model_year = d.model_year
  and c.subdivision_id = d.subdivision_id
group by a.manufacturer_name, b.division_name, c.subdivision_name, 
  d.model_name --order by count(*) desc 
order by a.manufacturer_name, b.division_name, c.subdivision_name

--11-25
-- did i ever fucking do the multiple vehicles per vin
-- no dup vins
select vin
from chr.z_vehicle_items
group by vin having count(*) > 1

-- only one dup !?!?!
select vin 
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
group by vin having count(*) > 1

-- 2CNDL13F186301602: 2008 equinoxs, one is AWD, one is FWD
select *
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id
where vin =  '2CNDL13F186301602'

-- that's more like it: 19531 
select a.vin
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id
group by a.vin having count(*) > 1

-- shit for those 19531 vins there are 86074 chrome style_ids 
-- what the fuck am i going to about those
select a.vin, d.*, e.*
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id
inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id
where a.vin in (
  select a.vin
  from chr.z_vehicle_items a  
  inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
     and a.vin like replace(b.vin_pattern, '*', '_')
  inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
  group by a.vin having count(*) > 1)
   

-- for example '1FTFW1ET7DKG30237' a 2013 F150 with 13 different styleids, combination of wheel base and trim
-- year_make_model_style
select a.*, d.*
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.year_make_model_style d on c.chrome_Style_id = d.chrome_style_id
where a.vin = '1FTFW1ET7DKG30237'
-- styles
select a.vin, d.*, e.*
from chr.z_vehicle_items a  
inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
   and a.vin like replace(b.vin_pattern, '*', '_')
inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
inner join chr.styles d on c.chrome_style_id = d.style_id
inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id
where a.vin = '1FTFW1ET7DKG30237'
order by invoice

select * from chr.categories where category_header_id = 13


select * from chr.tech_Title_header

select * from chr.tech_Titles where tech_Title_header_id = 13

select *
from chr.tech_specs a
inner join (
  select a.vin, d.*, e.*
  from chr.z_vehicle_items a  
  inner join chr.vin_pattern b on left(a.vin, 8) = left(b.vin_pattern, 8)
     and a.vin like replace(b.vin_pattern, '*', '_')
  inner join chr.vin_pattern_style_mapping c on b.vin_pattern_id = c.vin_pattern_id
  inner join chr.styles d on c.chrome_style_id = d.style_id
  inner join chr.mkt_class e on d.mkt_class_id = e.mkt_class_id
  where a.vin = '1FTFW1ET7DKG30237') b on a.style_id = b.style_id
inner join chr.tech_titles c on a.title_id = c.title_id
  and c.tech_title_header_id = 13  

-- BB designates a 3.5L Ecoboost engine, great, that narrows it down to about 9 different style_id's
