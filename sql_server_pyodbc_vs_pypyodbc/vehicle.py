#  encoding=utf-8
import db_cnx
# import unicodecsv as csv
import csv
file_name = 'files/ext_vehicle.csv'
database = None
drive_con = None
try:
    with db_cnx.drive_centric() as drive_con:
        with drive_con.cursor() as drive_cur:
            sql = """
                   select pkVehicleID,fkStoreID,Year,Make,Model,Price,Mileage,Image,DateCreated,DateModified,IsDeleted,VIN,
                 VehicleType,Notes,ConditionType,NewUsedType,Location,StockNumber,ModelNumber,Trim,StyleDescription,
                 BodyDescription,NumberOfDoors,ExteriorColorFull,ExteriorColorGeneric,ExteriorColorCode,ExteriorColorHex,
                 InteriorColorFull,InteriorColorGeneric,InteriorColorCode,InteriorColorHex,InteriorUpholstery,
                 EngineCylinders,EngineDisplacement,EngineBlockType,EngineAspirationType,EngineDescription,Transmission,
                 TransmissionSpeed,TransmissionDescription,DriveTrain,FuelType,EPACity,EPAHighway,WheelbaseCode,
                 IsCertified,IsHiddenRegularPrice,PriceMSRP,PriceBookValue,PriceInternet,PriceNADA,PriceInvoice,
                 DateInStock,URLVideo,URLImage,Description,Options,DealerOptions,SpecialsDisclaimer,MarketClass,
                 PassengerCapacity,Cost,IsInStock,DMS_Price,DMS_MSRP,DMS_Cost,DMS_Flooring,DMS_InventoryAmount,
                 InventoryPrice,InventoryCost,InventoryMSRP,InventoryInternetPrice,DMS_InventoryGlAmount,BlackBookRetail,
                 NADARetail,MMRRetail,MMRWholesale,DMS_XML,DMS_InventoryCompany,OverrideMileage,LastUpdatedBy,
                 DateModifiedDMS,DateModifiedInventory
                 from vehicle
                    """
            drive_cur.execute(sql)
            with open(file_name, 'w') as op:
                csv.writer(op).writerows(drive_cur)
    with db_cnx.pg() as database:
        with database.cursor() as pg_cursor:
            pg_cursor.execute("truncate dc.ext_vehicle")
            with open(file_name, 'r') as io:
                pg_cursor.copy_expert("""copy dc.ext_vehicle from stdin with csv encoding 'latin-1' """, io)
    print ('Passssssssssssssssssssssssssssssssssssssssssssssssssssss')
except Exception as error:
    print (error)
finally:
    if drive_con:
        drive_con.close()
    if database:
        database.close()
