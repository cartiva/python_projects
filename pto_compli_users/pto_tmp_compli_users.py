import ftplib
import datetime
import smtplib
import email.mime.text
import psycopg2
import csv
ftp = ftplib.FTP('10.130.196.138', 'rydell', '1rydell$')
ftp.cwd('/Downloads/compli_dls/')
filename = 'ExportUsers.csv'
server_date = ftp.sendcmd('MDTM ' + filename)
# converts what the ftp server returns, 213 20150731074509 to 2015-07-31 07:45:09
file_date = datetime.datetime.strptime(server_date[4:], "%Y%m%d%H%M%S").strftime("%x")
# print 'file date: ' + file_date
current_date = datetime.datetime.today().strftime("%x")
# print 'current date: ' + current_date
if file_date != current_date:
    email_address = 'jandrews@cartiva.com'
    message = email.mime.text.MIMEText('It appears that .138 is bombing on the compli scrape again')
    message['To'] = email_address
    message['From'] = email_address
    message['Subject'] = 'Compli scrape not current'
    smtplib.SMTP('mail.cartiva.com').sendmail(email_address, email_address, message.as_string())
localfile = open('files/' + filename, 'w')
ftp.retrbinary('RETR ' + filename, localfile.write, 1024)
ftp.quit()
localfile.close()
# truncate and populate jon_pto_tmp_compli_users with the file just downloaded
schema = 'jon'
table_name = 'pto_tmp_compli_users'
file_name = 'files/ExportUsers.csv'
new_file_name = 'files/new_ExportUsers.csv'
pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pgCursor = pgCon.cursor()
pgCursor.execute("truncate " + schema + "." + table_name)
pgCon.commit()
# a problem is that the compli download includes an extra line with nothing by crlf at the end of the file
with open(file_name, 'rb') as orig_file:
    with open(new_file_name, 'wb') as mod_file:
        writer = csv.writer(mod_file)
        for row in csv.reader(orig_file):
            if len(row) > 6:  # eliminate that annoying last line
                writer.writerow(row)
pgCursor.copy_expert("""copy """ + schema + """.""" + table_name + """ from stdin with csv header""",
                     open(new_file_name, 'rb'))
pgCon.commit()


pgCursor.close()
pgCon.close()
