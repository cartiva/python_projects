# encoding=utf-8
import pyodbc
import csv
import psycopg2

try:
    with psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'") as pgCon:
        pgCursor = pgCon.cursor()
        date_cursor = pgCon.cursor()
    with pyodbc.connect('DSN=iSeries System DSN; UID=rydejon; PWD=fuckyou5') as db2Con:
        db2Cursor = db2Con.cursor()
    file_name = 'files/ext_pdptdet.csv'
    sql = """
      delete
      from dds.ext_pdptdet
      where ptdate > 20160000
    """
    pgCursor.execute(sql)
    pgCon.commit()
    date_sql = """
        select the_date
        from (
          select *, the_year * 10000 + the_month * 100 + the_day as the_date
          from (
          select * from generate_series(2010, 2016, 1) as the_year) a
          cross join (
          select * from generate_series(1, 12, 1) as the_month) b
          cross join (
          select * from generate_series(1, 31, 1) as the_day) c) d
        where d.the_year = 2016
          and d.the_month <= extract(month from current_Date)
        order by the_year,the_month,the_day;
    """
    date_cursor.execute(date_sql)
    for dt_record in date_cursor:
        print str(dt_record[0])
        db2_sql = """
        select TRIM(PTCO#),TRIM(PTINV#),PTLINE,PTSEQ#,TRIM(PTTGRP),
          TRIM(PTCODE),TRIM(PTSOEP),PTDATE,PTCDATE,TRIM(PTCPID),
          TRIM(PTMANF),TRIM(PTPART),TRIM(PTSGRP),PTQTY,PTCOST,
          PTLIST,PTNET,PTEPCDIFF,TRIM(PTSPCD),TRIM(PTORSO),
          TRIM(PTPOVR),TRIM(PTGPRC),TRIM(PTXCLD),TRIM(PTFPRT),TRIM(PTRTRN),
          PTOHAT,TRIM(PTVATCODE),PTVATAMT,PTTIME,PTQOHCHG,
          PTLASTCHG,TRIM(PTUPDUSER),PTIDENTITY,PTKTXAMT,TRIM(PTLNTAXE),
          TRIM(PTDUPDATE),TRIM(PTRIM)
        FROM  rydedata.pdptdet a
        where ptdate =   """ + str(dt_record[0])
        db2Cursor.execute(db2_sql)
        with open(file_name, 'wb') as f:
            csv.writer(f).writerows(db2Cursor)
        f.close()
        with open(file_name, 'r') as io:
            pgCursor.copy_expert("""copy dds.ext_pdptdet from stdin with csv encoding 'latin-1'""", io)
        pgCon.commit()
except Exception, error:
    print str(error)
