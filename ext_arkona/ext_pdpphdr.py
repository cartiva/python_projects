# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pdpphdr'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pdpphdr.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),PENDING_KEY,TRIM(COUNTER_PERSON_ID),TRIM(RECORD_STATUS),
                    TRIM(DOCUMENT_TYPE),TRIM(TRANSACTION_TYPE),OPEN_TRAN_DATE,TRIM(DOCUMENT_NUMBER),
                    TRIM(CUSTOMER_NUMBER),CUSTOMER_KEY,TRIM(CUST_NAME),CUST_PHONE_NO,SHIP_TO_KEY,
                    TRIM(PAYMENT_METHOD),TRIM(SALE_TYPE),PRICE_LEVEL,TRIM(TAX_EXEMPT),PARTS_TOTAL,
                    SHIPPING_TOTAL,SPEC_ORD_DEPOSIT,SPEC_ORD_DEPOS_CR,SPEC_ORD_DEP_PCT,
                    TRIM(SP_ORD_DEP_ORIDE),TRIM(SPEC_ORD_ON_HOLD),PARTS_SALES_TAX_1,PARTS_SALES_TAX_2,PARTS_SALES_TAX_3,
                    PARTS_SALES_TAX_4,TRIM(CTRPERSON_NAME),TRIM(SORT_KEY),TRIM(AUTHOR_CTRP_ID),TRIM(CRED_CARD_AR_CUS_),
                    TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,TRIM(ORIG_DOC_NUMBER),TRIM(INTERNAL_ACCT_NO),TRIM(RO_TYPE),
                    TRIM(RO_STATUS),TRIM(WARRANTY_RO),TRIM(PARTS_APPROVED),TRIM(SERVICE_APPROVED),
                    TRIM(SERVICE_WRITER_ID),TRIM(RO_TECHNICIAN_ID),TOTAL_ESTIMATE,SERV_CONT_COMP,
                    SERV_CONT_COMP_2,SERV_CONT_DEDUCT,SERV_CONT_DEDUCT2,WARRANTY_DEDUCT,TRIM(VIN),
                    TRIM(STOCK_NUMBER),TRIM(TRUCK),TRIM(FRANCHISE_CODE),ODOMETER_IN,ODOMETER_OUT,
                    TIME_IN,PROMISED_DATE_TIME,REMAINING_TIME,TRIM(TAG_NUMBER),TRIM(CHECK_NUMBER),
                    TRIM(HEADER_COMMMENTS),TRIM(SHOP_SUPPLY_ORIDE),TRIM(HAZD_MATER_ORIDE),LABOR_TOTAL,
                    SUBLET_TOTAL,SC_DEDUCT_PAID,CUST_PAY_HZRD_MAT,CUST_PAY_TAX_1,CUST_PAY_TAX_2,CUST_PAY_TAX_3,
                    CUST_PAY_TAX_4,WARRANTY_TAX_1,WARRANTY_TAX_2,WARRANTY_TAX_3,WARRANTY_TAX_4,INTERNAL_TAX_1,
                    INTERNAL_TAX_2,INTERNAL_TAX_3,
                    INTERNAL_TAX_4,SVC_CONT_TAX_1,SVC_CONT_TAX_2,SVC_CONT_TAX_3,SVC_CONT_TAX_4,CUST_PAY_SHOP_SUP,
                    WARRANTY_SHOP_SUP,INTERNAL_SHOP_SUP,SVC_CONT_SHOP_SUP,COUPON_NUMBER,COUPON_DISCOUNT,
                    TOTAL_COUPON_DISC,PAID_BY_MANUF,PAID_BY_DEALER,CUST_PAY_TOT_DUE,TRIM(ORDER_STATUS),
                    DATE_TO_ARRIVE,A_R_CUST_KEY,WARRANTY_RO_,TRIM(WORK_STATION_ID),TRIM(INTERNAL_AUTH_BY),
                    APPOINTMENT_DATE_TIME,DATE_TIME_LAST_LINE_COMPLETE,DATE_TIME_PRE_INVOICED,TRIM(INTERNAL_PAY_TYPE),
                    TRIM(REPAIR_ORDER_PRIORITY),DAILY_RENTAL_AGREEMENT_NUMBE,TRIM(TOWED_IN_INDICATOR),
                    DOC_CREATE_TIMESTAMP,CT_HOLD_TIMESTAMP,TRIM(INVOICE_LIMIT_AUTH_),TRIM(LIMIT_OVERRIDE_USER),
                    DELIVERY_METHOD,MULTI_SHIP_SEQ_,
                    TRIM(DISPATCH_TEAM),TAX_GROUP_OVERRIDE,DISCOUNT_TAXABLE_AMT,TRIM(UPDATED_BY_OBJECT),
                    PARTS_DISCOUNT_ALLOCATION_AMOUNT,LABOR_DISCOUNT_ALLOCATION_AMOUNT
                FROM  rydedata.pdpphdr a
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_pdpphdr")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_pdpphdr from stdin with csv encoding 'latin-1'""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
