# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'arkona_meta_data_cdc'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/arkona_meta_data_cdc.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_production() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select trim(a.table_name), trim(a.table_text)
                from  qsys2.systables a
                where a.table_schema = 'RYDEDATA'
                  and trim(a.table_name) <> 'GLPAYWARE'
                  and a.table_name not like '$%'
                  and a.table_name not like 'X%'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_qsys2_syscst_tmp cascade")
            pg_cur.execute("truncate dds.ext_qsys2_syscstcol_tmp cascade")
            pg_cur.execute("truncate dds.ext_qsys2_syscolumns_tmp cascade")
            pg_cur.execute("truncate dds.ext_qsys2_systables_tmp cascade")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_qsys2_systables_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                update dds.ext_qsys2_systables_tmp
                set table_text = ''
                where table_text is null;
            """
            pg_cur.execute(sql)
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select trim(a.table_name), trim(a.system_column_name), trim(a.column_name),
                trim(a.ordinal_position),
                  a.data_type, a.length, a.numeric_scale, a.column_text
                from  qsys2.syscolumns a
                where a.table_schema = 'RYDEDATA'
                  and trim(a.table_name) <> 'GLPAYWARE'
                  and a.table_name not like '$%'
                  and a.table_name not like 'X%'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_qsys2_syscolumns_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                update dds.ext_qsys2_syscolumns_tmp
                set column_text = ''
                where column_text is null;
            """
            pg_cur.execute(sql)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
