# encoding=utf-8
import csv
import db_cnx
import ops
import string

# task = 'ext_glpdept'
pg_con = None
db2_con = None
# run_id = None
file_name = 'files/ext_boptrad.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  COMPANY_NUMBER,KEY,VIN,STOCK_,TRADE_ALLOWANCE,ACV,PAYOFF_AMOUNT,PAYOFF_TO,PAYOFF_ALIAS,PAYOFF_ADDR_1,
                  PAYOFF_ADDR_2,PAYOFF_CITY,PAYOFF_STATE,PAYOFF_ZIP,PAYOFF_PHONE_NO,PAYOFF_VENDOR,PAYOFF_LOAN_,
                  PAYOFF_EXP_DATE,FROM_LEASE,ODOMETER,INTL_POSTAL
                FROM  rydedata.boptrad
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_boptrad")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_boptrad from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
