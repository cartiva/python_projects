# encoding=utf-8
import db_cnx
import smtplib
from email.mime.text import MIMEText
import datetime
body = ''
frmt = "%A %x"
# todays date formatted Tuesday 07/14/15
the_date = (datetime.date.today()).strftime(frmt)
ads_con = db_cnx.ads_dpsvseries()
master_cursor = ads_con.cursor()
master_query = """
    SELECT b.stocknumber,
      trim(stocknumber) + ':  Inspected by ' + trim(c.fullname)
        + ' at '
        + iif(hour(a.VehicleInspectionTS) < 12, trim(CAST(hour(a.VehicleInspectionTS) AS sql_char)),
                trim(CAST(hour(a.VehicleInspectionTS) - 12 AS sql_char)))
            + ':'
            + iif(minute(a.VehicleInspectionTS) < 10, '0' + trim(CAST(minute(a.VehicleInspectionTS) AS sql_char)),
                trim(CAST(minute(a.VehicleInspectionTS) AS sql_char)))
            + iif(hour(a.VehicleInspectionTS) < 12, ' AM', ' PM'),
      trim(f.yearmodel) + ' ' + trim(f.make) + ' ' + trim(f.model) + ' '
          + coalesce(trim(f.TRIM), '') + ' ' + TRIM(coalesce(f.bodystyle))
        + '  Engine: ' + trim(coalesce(f.engine)),
        'Miles: ' +  cast(coalesce(h.value, 0) AS sql_char)
    FROM vehicleinspections a
    INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    INNER JOIN organizations bb on b.owninglocationid = bb.partyid
      AND bb.name = 'Rydells'
    INNER JOIN people c on a.technicianid = c.partyid
    INNER JOIN VehicleItems f on a.VehicleItemID = f.VehicleItemID
    LEFT JOIN VehicleWalks g on a.VehicleInventoryItemID = g.VehicleInventoryItemID
    LEFT JOIN VehicleItemMileages h on a.VehicleItemID = h.VehicleItemID
      AND VehicleItemMileageTS = (
        SELECT MAX(VehicleItemMileageTS)
        FROM VehicleItemMileages
        WHERE VehicleItemID = h.VehicleItemID)
    WHERE CAST(vehicleinspectionts AS sql_date) = curdate()
"""
master_cursor.execute(master_query)
master_result = master_cursor.fetchall()
if master_cursor.rowcount == 0:
    body += 'No inspections completed today'
for t in master_result:
    body += t[1] + '\n' + t[2] + '\n' + t[3] + '\n'
    detail_cursor = ads_con.cursor()
    stocknumber = "'" + t[0] + "'"
    detail_query = """
        SELECT b.stocknumber, d.description,
          d.totalpartsamount, d.laboramount, ee.description AS Area
        FROM vehicleinspections a
        INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
        INNER JOIN organizations bb on b.owninglocationid = bb.partyid
          AND bb.name = 'Rydells'
        INNER JOIN people c on a.technicianid = c.partyid
        INNER JOIN VehicleReconItems d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
          AND d.typ <> 'MechanicalReconItem_Inspection'
        INNER JOIN typcategories e on d.typ = e.typ
          AND e.category = 'MechanicalReconItem'
        INNER JOIN typdescriptions ee on e.typ = ee.typ
        WHERE b.stocknumber = {}
    """.format(stocknumber)
    detail_cursor.execute(detail_query)
    detail_result = detail_cursor.fetchall()
    if detail_cursor.rowcount == 0:
        body += 'No mechanical recon' + '\n'
    else:
        for k in detail_result:
            body += k[4] + ': ' + k[1] + '  Parts: ' + str(k[2]) + ' Labor ' + str(k[3]) + '\n'
    body += '\n'
    detail_cursor.close()
# print body
# Define email addresses to use
# this is just header info, as a string
# addr_to = 'jandrews@cartiva.com'
addr_to = ('jandrews@cartiva.com, bcahalan@rydellcars.com, aneumann@rydellcars.com, '
           'blongoria@rydellcars.com, jbear@rydellcars.com')
addr_from = 'jandrews@cartiva.com'
# this is the actual list of email recipients
receivers = ['jandrews@cartiva.com']
# receivers = (['jandrews@cartiva.com', 'bcahalan@rydellcars.com', 'aneumann@rydellcars.com',
#               'blongoria@rydellcars.com', 'jbear@rydellcars.com'])
# Declare SMTP email server details
smtp_server = 'mail.cartiva.com'
# Construct email
message = MIMEText(body)
message['To'] = addr_to
message['From'] = addr_from
if master_cursor.rowcount == 0:
    message['Subject'] = 'No vehcicles inspected on ' + the_date
else:
    message['Subject'] = 'Vehicles Inspected on ' + the_date
# Send email via SMTP server
e = smtplib.SMTP(smtp_server)
try:
    e.sendmail(addr_from, receivers, message.as_string())
    print "Successfully sent email"
except smtplib.SMTPException:
    print "Error: unable to send email"
e.quit()
master_cursor.close()
ads_con.close()
