# http://win32com.goermezer.de/content/view/82/187/
import os
import os.path
import sys
# the favorites folder of Windows
dir = 'C:\Users\jon\Favorites'
files = os.listdir(dir)
bookmarks = open('bookmarks.html', 'w')
head = "Bookmarks"
#print header
bookmarks.write(head)
for file in files:
        if not file[-4:] == '.url':
                continue
        title = file[:-4]
        fn = os.path.join(dir, file)
        body = open(fn).readlines()
        for line in body:
                token = 'BASEURL='
                if line[:len(token)] == token:
                        url = line[len(token):].strip()
                        url = url.replace("&", "&")
                        url = url.replace("<", "<")
                        url = url.replace(">", ">")
                        title = title.replace("&", "&")
                        title = title.replace("<", "<")
                        title = title.replace(">", ">")
                        main= (u'%s
' % (url, title)).encode('u8', 'ignore')
                        bookmarks.write(main)
bookmarks.close()